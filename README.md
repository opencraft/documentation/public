# OpenCraft Handbook

Public documentation repo for the OpenCraft handbook

See [handbook.opencraft.com](https://handbook.opencraft.com/) for the latest release.

## Building the handbook

This handbook is build using the static site generator [mkdocs](https://www.mkdocs.org/).

To build the docs to HTML, create a python 3.8 virtual environment.

```bash
virtualenv venvs/docs -p python3.8
source venvs/docs/bin/activate
```

Then, run from the root repository directory:

```bash
make install_prereqs
make compile
```

...or, to do a full installation, validation and build all in one go, do:

```bash
make
```

After the first build, to serve the docs locally, run from the root repository directory:

```bash
make run
```

Hooks for mkdocs are stored in the `hooks` directory. This includes python code that should be checked.
To check this code, run:

```bash
make quality
```

## Previewing the handbook changes in a merge request

When creating a merge request against the handbook, a preview is automatically built as part of the pipeline run on each merge request. The generated handbook preview can be accessed through the "View app" button in the pipeline box, displayed on the merge request.

Limitation: there is currently only one preview environment, so if another merge request is created or updated, the resulting artifacts will be overwritten. In such a case, to view the generated handbook for a specific merge request, you will need to regenerate the preview by re-triggering the pipeline. See https://gitlab.com/opencraft/documentation/public/-/issues/73

## Adding a new role to the handbook

When adding a new role to the handbook, you will also want to make sure to edit the file used to generate the [list of cell members and their roles](https://handbook.opencraft.com/en/latest/cells/list/), which is located at `hooks/data/opencraft.yml`.

In addition to assigning the role to the relevant cell members, you will also need to:

- Add the role to the schema in `hooks/schema.py` -> `KNOWN_ROLES`
- Make sure that there is a corresponding section named after the exact name of the role in `handbook/roles/list.md` - the generated cell members list will link to it, using an anchor corresponding to the role name (eg. `roles/list.html#firefighting-manager` for the role "Firefighting Manager").

# Read the Docs

* Configured with readthedocs.yml
* Account information [in Vault](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft Tools : Resources : Read the Docs)
* GitLab integration so the doc is rebuilt every time a commit is pushed in master is in the [settings of the public repository](https://gitlab.com/opencraft/documentation/public/settings/integrations)
* Make sure to set the DOCUMENTATION_SITE_URL [environment variable](https://docs.readthedocs.io/en/stable/environment-variables.html) with the domain name of the site.
    * Example: `https://handbook.opencraft.com` (do not add `/` at the end)

# Technical documentation

In February 2019 the technical documentation from this repository were
moved to a [dedicated
repository](https://gitlab.com/opencraft/documentation/tech).

