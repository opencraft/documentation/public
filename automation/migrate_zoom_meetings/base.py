"""
base file to migrate zoom recordings
"""
import logging
from tempfile import NamedTemporaryFile
from typing import Callable, Dict, List, Optional, Tuple, Union

from jinja2 import Environment, FileSystemLoader, select_autoescape
from pydantic import EmailStr  # pylint: disable=no-name-in-module

from automation.config import GoogleApiCredentials, ZoomAccountRecord, settings
from automation.libs.email import connect_to_smtp, send_mail
from automation.libs.google import (
    connect_to_google,
    create_folder,
    search_drive,
    upload_to_drive,
)
from automation.libs.zoom import (
    connect_to_zoom,
    delete_recording,
    download_recording,
    fetch_all_meetings,
    filter_recordings,
)

StatusListType = List[Dict[str, Union[dict, str]]]
GetNameStrategy = Callable[[dict, str], Optional[str]]

logger = logging.getLogger(__name__)


def log_error(msg: str, meeting_info: dict):
    """logs error with additional info"""
    logger.error("%s, Meeting details: %s", msg, meeting_info)


def _collect_failed_videos(msg: str, failed_videos: StatusListType, recording: dict):
    failed_videos.append(
        {
            "topic": recording["topic"],
            "reason": msg,
            "date": recording["recording_start"],
        }
    )
    log_error(msg, recording)


def migrate_meetings(
    zoom_account: ZoomAccountRecord,
    google_credentials: GoogleApiCredentials,
    drive_base_dir_id: str,
    get_file_name: GetNameStrategy,
) -> Tuple[StatusListType, StatusListType]:
    """
    Goes through given zoom account recordings and migrates it to google drive
    """
    uploaded_videos: StatusListType = []
    failed_videos: StatusListType = []
    with connect_to_zoom(zoom_account.client_id, zoom_account.client_secret, zoom_account.account_id) as conn:
        meetings = fetch_all_meetings(
            conn=conn,
            start=zoom_account.start,
            end=zoom_account.end,
            user_id=zoom_account.user_id,
        )
        if not meetings:
            return uploaded_videos, failed_videos
        recordings = filter_recordings(meetings, pattern=zoom_account.pattern)
        for recording in recordings:
            with NamedTemporaryFile(suffix=".mp4") as temp_file:
                result = download_recording(conn, recording, temp_file)
                if not result["success"]:
                    _collect_failed_videos(
                        f"failed to download recording: {recording['topic']}",
                        failed_videos,
                        recording,
                    )
                    continue
                name = get_file_name(recording, result["date"])
                if not name:
                    _collect_failed_videos(
                        "failed to generate file name",
                        failed_videos,
                        recording,
                    )
                    continue
                file_url, file_id = upload_meeting(
                    temp_file.name,
                    name,
                    google_credentials,
                    drive_base_dir_id,
                    zoom_account.drive_folder_name or recording["topic"],
                )
            if not file_url:
                _collect_failed_videos(
                    f"failed to upload recording: {recording['topic']}",
                    failed_videos,
                    recording,
                )
                continue
            # move recording to trash in zoom cloud if upload is successful
            if not settings.debug:
                delete_recording(
                    conn,
                    meeting_id=recording["meeting_id"],
                    recording_id=recording["id"],
                    meeting_name=recording["topic"],
                )
            uploaded_videos.append(
                {
                    "file_url": file_url,
                    "file_id": file_id,
                    "topic": recording["topic"],
                    "date": recording["recording_start"],
                    "matches": recording["matches"],
                }
            )
    return uploaded_videos, failed_videos


def upload_meeting(
    file_path: str,
    file_name: str,
    google_credentials: GoogleApiCredentials,
    drive_base_dir_id: str,
    drive_folder: str,
) -> Tuple[str, str]:
    """
    Uploads given file to drive in the given directory.
    """
    with connect_to_google("drive", google_credentials) as conn:
        meeting_folder = search_drive(
            conn,
            name=drive_folder,
            content_type="folder",
            parent_id=drive_base_dir_id,
        )
        # create folder if not found
        if not meeting_folder:
            logger.info("%s folder not found in drive, creating...", drive_folder)
            meeting_folder = create_folder(conn, drive_folder, drive_base_dir_id)
        # upload recording to the meeting_folder
        logger.info("uploading recording: %s", file_name)
        file_url, file_id = upload_to_drive(conn, file_path, file_name, meeting_folder, "video/mp4")
        return file_url, file_id


def send_email_notification(
    recipients: List[EmailStr],
    uploaded_videos: StatusListType,
    failed_videos: StatusListType,
):
    """
    send email with list of uploaded videos
    """
    email_settings = settings.email_settings
    subject = (
        f"{email_settings.subject_prefix} Automatic Zoom recordings migration status"
    )
    jinja_env = Environment(
        loader=FileSystemLoader(settings.templates_directory),
        autoescape=select_autoescape(["html", "xml"]),
    )
    data = {"videos": uploaded_videos, "failed_videos": failed_videos}
    html_template = jinja_env.get_template("email/migration_notification.html")
    content = html_template.render(data=data)

    plain_template = jinja_env.get_template("email/migration_notification.txt")
    plain_content = plain_template.render(data=data)
    logger.info("Sending email notification with body:\n %s", plain_content)

    with connect_to_smtp(email_settings) as conn:
        send_mail(
            conn=conn,
            subject=subject,
            from_email=email_settings.from_email,
            recipients=recipients,
            content=str(content),
            plain_content=str(plain_content),
        )
