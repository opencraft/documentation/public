"""
main file to migrate general zoom recordings
"""
import logging
from smtplib import SMTPException

from automation.config import settings
from automation.migrate_zoom_meetings.base import (
    migrate_meetings,
    send_email_notification,
)

logger = logging.getLogger(__name__)


def get_file_name(recording: dict, date: str) -> str:
    """returns file name from topic and date"""

    return f"{recording['topic']} - {date}"


def run():
    """
    wrapper function to run the whole process
    """
    drive_base_dir_id = settings.drive_folders.general_base_dir_id
    for zoom_account in settings.zoom_accounts:
        uploaded_videos, failed_videos = migrate_meetings(
            zoom_account,
            settings.google_credentials,
            drive_base_dir_id,
            get_file_name,
        )
        if not uploaded_videos and not failed_videos:
            return
        try:
            send_email_notification(
                recipients=zoom_account.recipients,
                uploaded_videos=uploaded_videos,
                failed_videos=failed_videos,
            )
        except SMTPException:
            logger.exception("Failed to send upload report via email")


if __name__ == "__main__":
    run()
