"""
main file to migrate sprint retrospective zoom recordings
"""
import logging
from typing import Dict, Optional

from pydiscourse import DiscourseClient  # type: ignore
import requests

from automation.config import settings
from automation.libs.discourse import connect_to_discourse, create_forum_post
from automation.migrate_zoom_meetings.base import (
    GetNameStrategy,
    StatusListType,
    log_error,
    migrate_meetings,
)

logger = logging.getLogger(__name__)
discourse_config = settings.discourse_settings
retro_config = settings.retrospective_config


def post_retrospective_recording(
    conn: DiscourseClient, file_id: str, sprint_name: str, topic_id: str
) -> Optional[str]:
    """
    Creates a post in cell topic for uploaded video

    Args:
        video_details: dict containing file_id, sprint_name and cell_name
    """
    # create forum post with video link

    # create post body
    body = settings.retrospective_config.message_body.format(
        file_id=file_id,
        sprint_name=sprint_name,
    )

    # send content to discourse api, which returns response body if successful else None
    if not settings.debug:
        create_forum_post(conn=conn, body=body, topic_id=topic_id)
    return body


def post_to_forum(uploaded_videos: StatusListType, sprint_names: dict):
    """
    Creates a post in cell topic for each uploaded video

    Args:
        uploaded_videos: list of dicts containing file_id, topic and matches
        dict containing cell_name
        sprint_names: dict of cell_name and their sprint names
    """
    with connect_to_discourse() as conn:
        for video_details in uploaded_videos:
            matches = video_details.get("matches", {})
            if not matches or not isinstance(matches, dict):
                log_error("Missing regex matches", video_details)
                continue
            cell_name = matches.get("cell_name")

            if not cell_name:
                log_error("Could not find cell_name", video_details)
                continue

            cell_name = cell_name.lower()
            sprint_name = sprint_names.get(cell_name)
            if not sprint_name:
                log_error("Could not find sprint_name", video_details)
                continue

            # get topic id from settings for given cell
            topic_id = discourse_config.topic_ids.get(cell_name)
            if not topic_id:
                log_error("Topic ID not defined in environment variable", video_details)
                continue

            file_id = video_details.get("file_id")
            if not file_id or not isinstance(file_id, str):
                log_error("Could not find google drive file_id", video_details)
                continue
            body = post_retrospective_recording(
                conn=conn,
                file_id=file_id,
                sprint_name=sprint_name,
                topic_id=topic_id,
            )
            logger.info(body)


def get_sprint_names() -> Dict[str, str]:
    """
    Fetch current sprint_names from sprintcraft
    """

    # return dummy_sprint_names if debug is True
    if settings.debug and retro_config.dummy_sprint_names:
        return retro_config.dummy_sprint_names
    response = requests.get(
        retro_config.sprint_name_url, headers=settings.retrospective_config.headers
    )
    response.raise_for_status()
    data = response.json()
    name_map = {
        row["name"].lower(): f"{row['key']}.{row['sprint_number'] - 1}" for row in data
    }
    return name_map


def get_file_name_strategy(sprint_names: dict) -> GetNameStrategy:
    """
    return function to create file name based on recording details
    and sprint_names
    """

    def _get_file_name(recording: dict, date: str) -> Optional[str]:
        cell_name = recording["matches"].get("cell_name")
        if cell_name:
            sprint_name = sprint_names.get(cell_name.lower())
            if not sprint_name:
                log_error("Could not find sprint name", recording)
                return None
            return f"Sprint-{sprint_name}.mp4"
        return f"{recording['topic']} - {date}"

    return _get_file_name


def run():
    """
    wrapper function to run the whole process
    """
    drive_base_dir_id = settings.drive_folders.retro_base_dir_id
    sprint_names = get_sprint_names()
    get_file_name = get_file_name_strategy(sprint_names)
    uploaded_videos, failed_videos = migrate_meetings(
        retro_config.zoom_account,
        settings.google_credentials,
        drive_base_dir_id,
        get_file_name,
    )
    if failed_videos:
        logger.error(failed_videos)
    if not uploaded_videos:
        return
    post_to_forum(uploaded_videos, sprint_names)


if __name__ == "__main__":
    run()
