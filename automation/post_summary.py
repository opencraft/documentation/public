"""
post handbook summary of changes to discourse
"""
import os
from datetime import datetime, time, timedelta, timezone

from automation.libs.discourse import connect_to_discourse, create_forum_post
from automation.libs.gitlab import connect_to_gitlab, get_handbook_changes


def post_handbook_summary() -> str:
    """Collect changes from handbook in last sprint and post a summary to forum"""

    # look for merge requests in last two weeks
    today = datetime.combine(datetime.utcnow().date(), time.min, tzinfo=timezone.utc)
    days = int(os.getenv("HANDBOOK_SUMMARY_NO_OF_DAYS", "14"))
    start = today - timedelta(days=days)

    with connect_to_gitlab() as g_conn:
        change_summary = get_handbook_changes(g_conn, start_date=start, end_date=today)

    topic_id = os.getenv("HANDBOOK_SUMMARY_POST_FORUM_ID")
    if not change_summary:
        return "No changes found!"
    if topic_id:
        # We really don't want to trigger this in the dev environment.
        with connect_to_discourse() as conn:
            create_forum_post(conn, body=change_summary, topic_id=topic_id)
    return change_summary


if __name__ == "__main__":
    print(post_handbook_summary())
