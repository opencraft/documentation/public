"""
functions to interact with google api
"""
from contextlib import contextmanager
from typing import Iterator, Optional, Tuple

from google.auth.exceptions import RefreshError  # type: ignore
from google.auth.transport.requests import Request  # type: ignore
from google.oauth2.credentials import Credentials  # type: ignore
from google_auth_oauthlib.flow import InstalledAppFlow  # type: ignore
from googleapiclient import discovery  # type: ignore
from googleapiclient.http import MediaFileUpload  # type: ignore

from automation.config import GoogleApiCredentials, settings


def _get_credentials(google_credentials: GoogleApiCredentials):
    """
    Create credentials from token saved in env variable.
    """
    err_msg = (
        "There are no (valid) credentials available, "
        "please run this script locally and copy contents "
        "of generated token.json file to env variable. "
        "For more details visit: https://handbook.opencraft.com"
        "/en/latest/automation/migrate_zoom_recordings/#running-via-gitlab"
    )
    try:
        creds = Credentials.from_authorized_user_info(
            google_credentials.creds_or_token, google_credentials.scopes
        )
    except ValueError as cred_err:
        raise ValueError(err_msg) from cred_err
    if creds.expired and creds.refresh_token:
        try:
            creds.refresh(Request())
        except RefreshError as refresh_err:
            raise ValueError(err_msg) from refresh_err
    return creds


@contextmanager
def connect_to_google(
    service: str,
    google_credentials: GoogleApiCredentials,
) -> Iterator[discovery.Resource]:
    """Connects to Google API with service account."""
    credentials = _get_credentials(google_credentials)
    api_version = {"drive": "v3"}
    try:
        service = discovery.build(
            service,
            api_version[service],
            credentials=credentials,
            cache_discovery=False,
        )
    except KeyError as err:
        raise AttributeError("Unknown service name.") from err
    yield service


def search_drive(
    conn: discovery.Resource,
    name: str,
    content_type: str = "folder",
    parent_id: Optional[str] = None,
) -> Optional[str]:
    """
    Search drive for file/folder of given name under given parent_id

    Args:
        name (str): name of the resource
        content_type (str): optional, default to 'folder'
        parent_id (str): optional, default to None, if provided it will search inside parent folder

    Returns:
        returns id of the first resource found else None
    """
    search_query = f"name = '{name}'"

    # build query as documented here: https://developers.google.com/drive/api/guides/search-files
    if content_type == "folder":
        search_query = (
            search_query + "and mimeType = 'application/vnd.google-apps.folder'"
        )
    if parent_id:
        search_query = search_query + f" and '{parent_id}' in parents"

    page_token = None
    while True:
        response = (
            conn.files()
            .list(
                q=search_query,
                spaces="drive",
                fields="nextPageToken, files(id, name)",
                pageToken=page_token,
            )
            .execute()
        )
        for file in response.get("files", []):
            # return id if file is found
            return file.get("id")
        page_token = response.get("nextPageToken", None)
        if page_token is None:
            # no more results
            break
    return None


def create_folder(conn: discovery.Resource, folder_name: str, parent: str) -> str:
    """
    Creates a folder in google drive under given parent

    Args:
        folder_name (str): folder name to create in drive
        parent (str): parent folder id

    Returns:
        id of the newly created folder in drive
    """
    file_metadata = {
        "name": folder_name,
        "parents": [parent],
        "mimeType": "application/vnd.google-apps.folder",
    }
    folder = conn.files().create(body=file_metadata, fields="id").execute()
    return folder.get("id")


def upload_to_drive(
    conn: discovery.Resource,
    file_path: str,
    filename: str,
    drive_folder: str,
    mimetype: str,
) -> Tuple[str, str]:
    """
    Upload local file to drive in drive_folder

    Args:
        conn: drive connection object
        file_path (str): local file path
        filename (str): file name in google drive
        drive_folder (str): folder id in google drive
        mimetype (str): file mimetype, eg. video/mp4

    Returns:
        Drive url and id of uploaded file
    """
    file_metadata = {"name": filename, "parents": [drive_folder]}
    media_body = MediaFileUpload(
        file_path,
        mimetype=mimetype,
        resumable=True,
    )
    drive_file = (
        conn.files()
        .create(
            body=file_metadata,
            media_body=media_body,
            fields="webViewLink,id",
            supportsAllDrives=True,
        )
        .execute()
    )
    return drive_file.get("webViewLink"), drive_file.get("id")


if __name__ == "__main__":
    # Run this script locally to generate token.json and add this as a secret
    # env variable to CI/CD.
    flow = InstalledAppFlow.from_client_config(
        settings.google_credentials.creds_or_token, settings.google_credentials.scopes
    )
    token_creds = flow.run_local_server()
    # Save contents of this file to env variable.
    with open("token.json", "w", encoding="utf8") as token:
        token.write(token_creds.to_json())
