"""
functions to interact with zoom api
"""
import http
import logging
import re
import shutil
from contextlib import contextmanager
from datetime import datetime
from typing import IO, Any, Dict, Iterator, List, Optional
from urllib.parse import quote_plus

import requests
from zoomus import ZoomClient  # type: ignore

logger = logging.getLogger(__name__)


@contextmanager
def connect_to_zoom(client_id: str, client_secret: str, account_id: str) -> Iterator[ZoomClient]:
    """
    Context manager for establishing connection with Zoom API.
    """
    conn = ZoomClient(client_id, client_secret, account_id)
    yield conn


def _get_recording_details(meeting_details: Dict[str, Any]) -> Optional[Dict]:
    """
    Given a dict of meeting details with recording_files return recording with extension MP4

    Args:
        meeting_details (Dict[str, Any]): dict of meeting details from zoom api

    Returns:
        dict containing recording details if extension is MP4
    """
    recordings = meeting_details.get("recording_files")

    if not recordings:
        return None

    for recording in recordings:
        if recording["file_extension"] == "MP4":
            recording["meeting_id"] = quote_plus(recording["meeting_id"])
            return recording
    return None


def fetch_all_meetings(
    conn: ZoomClient,
    start: Optional[datetime] = None,
    end: Optional[datetime] = None,
    user_id: str = "me",
    page_size: int = 300,
) -> Optional[List[Dict]]:
    """
    Fetch all meetings for the given range of dates and user_id (can be username or email)
    If date range is not provided, return all recordings available.

    Args:
        conn (ZoomClient): connection instance of ZoomClient
        start (datetime): start date
        end (datetime): end date
        user_id (str): zoom username or email
        page_size (int): number of records per page (<=300). To fetch more than
            300 records we would need to use pagination which is not the case now.

    Returns:
        list of dict containing meeting details
    """
    meetings = conn.recording.list(
        user_id=user_id, start=start, end=end, page_size=page_size
    )
    if meetings.status_code == http.HTTPStatus.OK:
        return meetings.json()["meetings"]
    logger.error(
        "failed to fetch recordings for user: %s, start: %s, end: %s date with status_code: %s",
        user_id,
        start,
        end,
        meetings.status_code,
    )
    return None


def filter_recordings(
    meetings: List[Dict], pattern: Optional[str] = None
) -> List[Dict]:
    """
    given a list of meeting details filter meetings with topic name matching given pattern and
    extract mp4 recordings.

    Args:
        meetings (List[Dict]): list of meeting_details from zoom
        pattern (str): regex pattern to match topic name

    Returns:
        list of dict containing recording_details and topic
    """
    recordings = []
    # catch all pattern in case pattern is undefined.
    # This helps to avoid none checks in loop below
    compiled_regex = re.compile(r".*")
    if pattern:
        compiled_regex = re.compile(pattern, re.IGNORECASE)
    for meeting in meetings:
        topic = meeting.get("topic", "")
        matches = compiled_regex.search(topic)
        if matches:
            recording = _get_recording_details(meeting)
            if recording:
                recording["matches"] = matches.groupdict()
                recording["topic"] = topic
                recordings.append(recording)
    return recordings


def download_recording(
    conn: ZoomClient, recording_details: Dict, file_obj: IO
) -> Dict[str, Any]:
    """
    Interface for downloading recordings from Zoom.

    Args:
        conn (ZoomClient): connection instance of ZoomClient
        recording_details (dict): dict containing recording details like download_url, recording_start.
        file_obj (IO): file like object, downloaded file will be saved here

    Returns:
        dict containing operation status, recording date and filepath.
    """

    result = {"success": False, "date": None}
    try:
        # Get URL and download the file.
        token = conn.config.get("token")
        logger.info("starting download for meeting: %s", recording_details["topic"])
        download_response = requests.get(
            recording_details["download_url"],
            stream=True,
            params={"access_token": token},
        )
        shutil.copyfileobj(
            download_response.raw, file_obj
        )  # Copy raw file data to local file.

        logger.info(
            "recording downloaded for meeting: %s to %s.",
            recording_details["topic"],
            file_obj.name,
        )
        return {"success": True, "date": recording_details["recording_start"]}
    except requests.ConnectionError as err:
        logger.exception(err)
        return result


def delete_recording(
    conn: ZoomClient,
    meeting_id: str,
    recording_id: str,
    meeting_name: str,
    action: str = "trash",
) -> None:
    """
    delete single recording for a given meeting_id and recording_id

    Args:
        conn (ZoomClient): connection instance of ZoomClient
        meeting_id (str): double encoded meeting id for example: "%2FajXp112QmuoKj4854875%3D%3D"
        recording_id (str): recording uuid for single recording
        meeting_name (str): meeting name or topic to identify in logs
        action (str): default to trash, which moves recording to trash in cloud
    """
    logger.info("moving zoom recording for meeting: %s to trash", meeting_name)
    response = conn.recording.delete_single_recording(
        meeting_id=meeting_id, recording_id=recording_id, action=action
    )
    if response.status_code != http.HTTPStatus.NO_CONTENT:
        logger.error(
            "failed to delete recordings for meeting: %s with status_code: %s",
            meeting_name,
            response.status_code,
        )
