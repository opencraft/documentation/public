"""
gitlab api client
"""
import logging
import os
from contextlib import contextmanager
from datetime import datetime
from typing import Iterator, List, Optional, Union

from dateutil import parser
from gitlab.base import RESTObject, RESTObjectList
from gitlab.client import Gitlab
from gitlab.exceptions import GitlabGetError

logger = logging.getLogger(__name__)


@contextmanager
def connect_to_gitlab() -> Iterator[Gitlab]:
    """
    Context manager for establishing connection with Gitlab API.
    """
    # Public repos are accessible without any credentials
    # Add credentials if required to connect to private repos
    conn = Gitlab()
    yield conn


def get_project_merge_requests(
    conn: Gitlab,
    project_namespace: str,
    start_date: datetime,
    end_date: datetime,
    state: Optional[str] = None,
) -> Union[List[RESTObject], RESTObjectList]:
    """
    get merge requests for given project

    Args:
        conn (Gitlab): gitlab instance
        project_namespace (str): project_namespace for example: "opencraft/documentation/public/"
        start_date (date): filter for merge requests after start date
        end_date (date): filter for merge requests before end_date
        state (str): state of merge_requests to filter by

    Returns:
        list of merge requests for given project
    """
    try:
        project = conn.projects.get(project_namespace)
        filters = {"updated_after": start_date.isoformat()}
        if state:
            filters["state"] = state
        merge_requests = project.mergerequests.list(**filters)
        mr_list = []
        for merge_request in merge_requests:
            if not merge_request.merged_at:
                continue
            merged_at = parser.parse(merge_request.merged_at)
            if start_date <= merged_at <= end_date:
                mr_list.append(merge_request)
        return mr_list
    except GitlabGetError as error:
        logger.exception(error)
        return []


def get_handbook_changes(
    conn: Gitlab, start_date: datetime, end_date: datetime
) -> Optional[str]:
    """
    get handbook repo changes summary in markdown

    Args:
        conn (Gitlab): gitlab instance
        start_date (date): filter for merge requests after start date
        end_date (date): filter for merge requests before end date

    Returns:
        a string summary of changes in handbook from start_date till now
    """
    project = os.getenv("CI_PROJECT_PATH", "opencraft/documentation/public")
    summary = f"@team\n\n## Summary of merge requests for `{project}` from `{start_date.strftime('%Y-%m-%d')}`\n\n"
    try:
        # Get merge requests after start_date
        merge_requests = get_project_merge_requests(
            conn, project, start_date=start_date, end_date=end_date
        )
        if not merge_requests:
            return None

        # generate summary for each mr
        for merge_request in merge_requests:
            diffs = "```diff"
            changes = merge_request.changes().get("changes", [])
            # check if no. of changes more than threshold
            threshold = int(os.getenv("HANDBOOK_SUMMARY_DIFF_THRESHOLD", "5"))
            # TODO: Fix or remove this functionality.  # pylint: disable=fixme
            enable_diffs = bool(int(os.getenv("HANDBOOK_ENABLE_DIFF", "0")))
            if not enable_diffs:
                diffs = ""
            elif len(changes) > threshold:
                diffs = "**Too many changes, please visit MR to review.**\n"
            else:
                # get diff text for each changed file
                for change in changes:
                    diffs = f"{diffs}\n# {change['new_path']}\n{change['diff']}\n"
                diffs = f"{diffs}\n```"
            summary = (
                f"{summary}\n**Merge request**: {merge_request.title}\n**Description**: {merge_request.description}\n"
                f"\n**Author**: {merge_request.author.get('name')}\n"
                f"\n{diffs}\n"
                f"For more details visit: [#{merge_request.iid}]({merge_request.web_url}).\n\n---\n"
            )
    except GitlabGetError as error:
        logger.exception(error)
    return summary
