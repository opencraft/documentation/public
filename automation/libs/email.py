"""
Functions for sending emails
"""
import smtplib
from contextlib import contextmanager
from email.message import EmailMessage
from typing import Iterator, List

from pydantic import EmailStr  # pylint: disable=no-name-in-module

from automation.config import EmailSettings


@contextmanager
def connect_to_smtp(config: EmailSettings) -> Iterator[smtplib.SMTP]:
    """
    Context manager for establishing connection with SMTP server
    """
    conn = smtplib.SMTP(host=config.host, port=config.port)
    if config.use_tls:
        conn.starttls()
    if config.host_user and config.host_password:
        conn.login(
            user=config.host_user, password=config.host_password.get_secret_value()
        )
    yield conn
    conn.close()


# pylint: disable=too-many-arguments
def send_mail(
    conn: smtplib.SMTP,
    subject: str,
    from_email: EmailStr,
    recipients: List[EmailStr],
    content: str,
    plain_content: str,
):
    """
    utility function to send a multipart email
    """
    # Create the base text message.
    msg = EmailMessage()
    msg["Subject"] = subject
    msg["From"] = from_email
    msg["To"] = recipients
    msg.set_content(plain_content)

    # Add the html version.  This converts the message into a multipart/alternative
    # container, with the original text message as the first part and the new html
    # message as the second part.
    msg.add_alternative(content, subtype="html")

    conn.send_message(msg)
