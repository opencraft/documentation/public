"""
test for gitlab client
"""
import os
from datetime import datetime
from unittest.mock import Mock, patch

from automation.libs.gitlab import get_handbook_changes

TOO_MANY_CHANGES = [
    Mock(
        title="mock merge request title 1",
        iid=2,
        author={"name": "opencraft1"},
        description="mock description 1",
        web_url="http://o1.com",
        changes=Mock(
            return_value={
                "changes": [
                    {"new_path": "some/path1", "diff": "mock diff 1"},
                    {"new_path": "some/another/path1", "diff": "mock diff 21"},
                    {"new_path": "some/another/path1", "diff": "mock diff 21"},
                ]
            }
        ),
    ),
]

DUMMY_GITLAB_MRS = [
    Mock(
        title="mock merge request title",
        iid=1,
        author={"name": "opencraft"},
        description="mock description ![screencast](/uploads/xyz/screencast.mp4)",
        web_url="http://o.com",
        changes=Mock(
            return_value={
                "changes": [
                    {"new_path": "some/path", "diff": "mock diff"},
                    {"new_path": "some/another/path", "diff": "mock diff 2"},
                ]
            }
        ),
    ),
    Mock(
        title="mock merge request title 1",
        iid=2,
        author={"name": "opencraft1"},
        description="mock description 1",
        web_url="http://o1.com",
        changes=Mock(
            return_value={
                "changes": [
                    {"new_path": "some/path1", "diff": "mock diff 1"},
                    {"new_path": "some/another/path1", "diff": "mock diff 21"},
                ]
            }
        ),
    ),
]

EXPECTED_CHANGE_SUMMARY = """
@team

## Summary of merge requests for `opencraft/documentation/public` from `2022-04-04`


**Merge request**: mock merge request title
**Description**: mock description ![screencast](/uploads/xyz/screencast.mp4)

**Author**: opencraft

```diff
# some/path
mock diff

# some/another/path
mock diff 2

```
For more details visit: [#1](http://o.com).

---

**Merge request**: mock merge request title 1
**Description**: mock description 1

**Author**: opencraft1

```diff
# some/path1
mock diff 1

# some/another/path1
mock diff 21

```
For more details visit: [#2](http://o1.com).

---
"""


@patch("automation.libs.gitlab.get_project_merge_requests")
@patch.dict(os.environ, {"HANDBOOK_ENABLE_DIFF": "1"}, clear=True)
def test_gitlab_handbook_changes(mock_mrs):
    """
    test change summary
    """

    mock_mrs.return_value = DUMMY_GITLAB_MRS
    changes = get_handbook_changes(Mock(), datetime(2022, 4, 4), datetime(2022, 4, 4))
    assert changes is not None
    assert changes.strip() == EXPECTED_CHANGE_SUMMARY.strip()


@patch("automation.libs.gitlab.get_project_merge_requests")
@patch.dict(os.environ, {"HANDBOOK_ENABLE_DIFF": "1"}, clear=True)
def test_empty_gitlab_handbook_changes(mock_mrs):
    """
    test empty change summary
    """

    mock_mrs.return_value = []
    changes = get_handbook_changes(Mock(), datetime(2022, 4, 4), datetime(2022, 4, 4))
    assert changes is None


@patch("automation.libs.gitlab.get_project_merge_requests")
@patch.dict(
    os.environ,
    {"HANDBOOK_SUMMARY_DIFF_THRESHOLD": "2", "HANDBOOK_ENABLE_DIFF": "1"},
    clear=True,
)
def test_too_many_gitlab_handbook_changes(mock_mrs):
    """
    test summary for diffs over threshold.
    """

    mock_mrs.return_value = TOO_MANY_CHANGES
    changes = get_handbook_changes(Mock(), datetime(2022, 4, 4), datetime(2022, 4, 4))
    assert changes is not None
    assert "**Too many changes, please visit MR to review.**\n" in changes.strip()


@patch("automation.libs.gitlab.get_project_merge_requests")
@patch.dict(os.environ, {"HANDBOOK_ENABLE_DIFF": "0"}, clear=True)
def test_static_asset_description(mock_mrs):
    """
    test static asset in description
    """

    mock_mrs.return_value = DUMMY_GITLAB_MRS
    changes = get_handbook_changes(Mock(), datetime(2022, 4, 4), datetime(2022, 4, 4))
    assert changes is not None
    # TODO: handle static asset url from gitlab  # pylint: disable=fixme
    assert "![screencast](/uploads/xyz/screencast.mp4)" in changes.strip()
