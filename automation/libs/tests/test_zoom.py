"""
tests for zoom client
"""
import pytest
from zoomus.client import API_BASE_URIS, API_VERSION_2  # type: ignore

from automation.libs.zoom import filter_recordings

TEST_MEETING_ID = "mock-meeting-id"
TEST_MEETING_NAME = "mock-meeting-name"
TEST_RECORDING_ID = "mock-recording-id"
ZOOM_API_URL = API_BASE_URIS[API_VERSION_2]
TEST_DOWNLOAD_URL = f"{ZOOM_API_URL}/recording/files/random-file"
TEST_RECORDING_DETAILS = [
    {
        "topic": "Bebop - synchronous sprint retrospective",
        "file_extension": "MP4",
        "download_url": TEST_DOWNLOAD_URL,
        "meeting_id": "uuid1",
        "recording_start": "2022-02-01T02:03:01Z",
        "matches": {"cell_name": "Bebop"},
    },
    {
        "topic": "some other meeting",
        "file_extension": "MP4",
        "download_url": TEST_DOWNLOAD_URL,
        "meeting_id": "uuid3",
        "recording_start": "2022-02-01T02:03:01Z",
    },
]
TEST_MEETING_DETAILS = [
    {
        "topic": "Bebop - synchronous sprint retrospective",
        "id": TEST_MEETING_ID,
        "recording_files": [
            {
                "file_extension": "MP4",
                "download_url": TEST_DOWNLOAD_URL,
                "meeting_id": "uuid1",
                "recording_start": "2022-02-01T02:03:01Z",
            },
            {
                "file_extension": "text",
                "download_url": "",
                "meeting_id": "uuid2",
                "recording_start": "2022-02-01T02:03:01Z",
            },
        ],
    },
    {
        "topic": "some other meeting",
        "id": TEST_MEETING_ID,
        "recording_files": [
            {
                "file_extension": "MP4",
                "download_url": TEST_DOWNLOAD_URL,
                "meeting_id": "uuid3",
                "recording_start": "2022-02-01T02:03:01Z",
            }
        ],
    },
]


@pytest.mark.parametrize(
    "meetings, expected, pattern",
    [
        (
            TEST_MEETING_DETAILS,
            [TEST_RECORDING_DETAILS[0]],
            r"(?P<cell_name>.*) - synchronous sprint retrospective$",
        ),
        (
            TEST_MEETING_DETAILS,
            TEST_RECORDING_DETAILS,
            None,
        ),
        ([], [], None),
        (
            [
                {
                    "topic": "Bebop - synchronous sprint retrospective",
                    "recording_files": [],
                }
            ],
            [],
            None,
        ),
        ([{"topic": "some other meeting", "recording_files": []}], [], None),
    ],
)
def test_zoom_filter_recordings(meetings, expected, pattern):
    """
    test filter recordings function with varied input
    """
    details = filter_recordings(meetings, pattern)
    if pattern is None:
        for row in expected:
            row["matches"] = {}
    assert details == expected
