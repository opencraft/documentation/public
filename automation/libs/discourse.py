"""
discourse api client
"""
import logging
from contextlib import contextmanager
from typing import Dict, Iterator, Optional

from pydiscourse import DiscourseClient  # type: ignore
from pydiscourse.exceptions import DiscourseError  # type: ignore

from automation.config import settings

logger = logging.getLogger(__name__)
discourse_config = settings.discourse_settings


@contextmanager
def connect_to_discourse() -> Iterator[DiscourseClient]:
    """
    Context manager for establishing connection with Discourse API.
    """
    conn = DiscourseClient(
        api_key=discourse_config.api_key,
        api_username=discourse_config.username,
        host=discourse_config.api_url,
    )
    yield conn


def create_forum_post(
    conn: DiscourseClient, body: str, topic_id: str
) -> Optional[Dict]:
    """
    wrapper function to create a post in forum

    Args:
        conn (DiscourseClient): conn object to discourse
        body (str): message body
        topic_id (str): topic id for the forum

    Returns:
        dictionary of response body data or None
    """
    try:
        # send content to discourse api, which returns response body if successful else None
        response = conn.create_post(content=body, topic_id=topic_id)
        return response
    except DiscourseError as error:
        logger.exception(error)
        return None
