"""
Tests for migrating zoom meetings
"""
import os
from unittest.mock import patch

from automation.config import settings
from automation.libs.tests.test_zoom import TEST_MEETING_DETAILS
from automation.migrate_zoom_meetings.base import migrate_meetings
from automation.migrate_zoom_meetings.generic import get_file_name

# test data


@patch("automation.migrate_zoom_meetings.base.filter_recordings")
@patch("automation.migrate_zoom_meetings.base.connect_to_zoom")
@patch("automation.migrate_zoom_meetings.base.fetch_all_meetings")
def test_empty_migrate_meetings(mock_fetch_all_meetings, _, mock_filter_recordings):
    """
    test migrate meetings with no meetings
    """
    mock_fetch_all_meetings.return_value = []
    uploaded, failed = migrate_meetings(
        settings.zoom_accounts[0],
        settings.google_credentials,
        settings.drive_folders.general_base_dir_id,
        get_file_name,
    )
    assert not mock_filter_recordings.called
    assert uploaded == failed == []


@patch("automation.migrate_zoom_meetings.base.delete_recording")
@patch("automation.migrate_zoom_meetings.base.upload_meeting")
@patch("automation.migrate_zoom_meetings.base.download_recording")
@patch("automation.migrate_zoom_meetings.base.connect_to_zoom")
@patch("automation.migrate_zoom_meetings.base.fetch_all_meetings")
@patch.dict(os.environ, {"DEBUG": "True"}, clear=True)
def test_migrate_meetings(
    mock_fetch_all_meetings,
    _,
    mock_download_recording,
    mock_upload_meeting,
    mock_delete_recording,
):
    """
    test migrate meetings with fake data
    """
    mock_fetch_all_meetings.return_value = TEST_MEETING_DETAILS
    mock_upload_meeting.return_value = "http://fake-google.com/some-video", "some-video"
    mock_download_recording.return_value = {
        "success": True,
        "date": "2022-02-01T02:03:01Z",
    }
    uploaded, failed = migrate_meetings(
        settings.zoom_accounts[0],
        settings.google_credentials,
        settings.drive_folders.general_base_dir_id,
        get_file_name,
    )
    assert not mock_delete_recording.called
    assert mock_download_recording.called
    assert uploaded == [
        {
            "file_url": "http://fake-google.com/some-video",
            "file_id": "some-video",
            "topic": "Bebop - synchronous sprint retrospective",
            "date": "2022-02-01T02:03:01Z",
            "matches": {},
        },
        {
            "file_url": "http://fake-google.com/some-video",
            "file_id": "some-video",
            "topic": "some other meeting",
            "date": "2022-02-01T02:03:01Z",
            "matches": {},
        },
    ]
    assert failed == []
