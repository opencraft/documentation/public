"""
Config settings from environment
"""

import logging
import os
import sys
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, Optional

from pydantic import (  # pylint: disable=no-name-in-module
    AnyHttpUrl,
    BaseModel,
    BaseSettings,
    EmailStr,
    Field,
    SecretStr,
    root_validator,
)


class DateRange(BaseModel):  # pylint: disable=too-few-public-methods
    """
    date range mixin to set start and end date OR last_n_days.
    Either start & end date or last_n_days can be set and not both.
    If last_n_days is set, start date is calculated based on its value.

    Example: if job runs on 2022-08-10 and last_n_days is set to 10, then start
    date is set to 2022-07-31
    """

    start: Optional[datetime] = None
    end: Optional[datetime] = None
    last_n_days: Optional[int] = None

    @staticmethod
    def _get_start_date(last_n_days: int):
        """get start date based on last_n_days"""

        today = datetime.utcnow()
        return today - timedelta(days=last_n_days)

    @root_validator
    @classmethod
    def validate_all(cls, values):
        """
        Raise error if both start/end dates and last_n_days is set
        Also, set start date based on last_n_days
        """

        start, end = values.get("start"), values.get("end")
        last_n_days = values.get("last_n_days")
        if (start or end) and last_n_days:
            raise ValueError("Either start/end date or last_n_days can be set")
        if last_n_days:
            values["start"] = cls._get_start_date(last_n_days)
        return values


class ZoomAPIKeyPair(BaseModel):  # pylint: disable=too-few-public-methods
    """
    Zoom api key and secret, user id
    """

    client_id: str
    client_secret: str
    account_id: str
    user_id: str = "me"


class ZoomAccountRecord(
    ZoomAPIKeyPair, DateRange
):  # pylint: disable=too-few-public-methods
    """
    Record containing zoom info and additional info required for migration
    """

    recipients: List[EmailStr]
    pattern: Optional[str] = None
    # folder name to be created or used for meetings in drive
    drive_folder_name: Optional[str] = None


class GoogleApiCredentials(BaseModel):  # pylint: disable=too-few-public-methods
    """
    Google credentials
    """

    # set as credentials_json locally to get token, once token is obtained
    # replace this attributes value with the token
    creds_or_token: dict
    scopes: List[str]


class EmailSettings(BaseModel):  # pylint: disable=too-few-public-methods
    """
    Email configuration
    """

    subject_prefix: str = "[Handbook Automation]"
    use_tls: bool = True
    port: int = 25
    host: str
    host_user: str
    host_password: SecretStr
    from_email: EmailStr


class DriveFolders(BaseModel):  # pylint: disable=too-few-public-methods
    """
    Google drive folder ids

    general_base_dir_id: directory used for general zoom meetings
    retro_base_dir_id: directory used for retrospective zoom meetings
    """

    general_base_dir_id: str
    retro_base_dir_id: str


class DiscourseConfig(BaseModel):  # pylint: disable=too-few-public-methods
    """
    discourse config
    """

    api_url: AnyHttpUrl = AnyHttpUrl(url="https://forum.opencraft.com/", scheme="https")
    api_key: str
    username: str
    # ids for a topic in forum, can be found in the url
    # Example: https://forum.example.com/t/topic-name/1012/126
    # here 1012 is the topic id
    topic_ids: Dict[str, str] = {}


class RetrospectiveConfig(BaseModel):  # pylint: disable=too-few-public-methods
    """
    config specifically for retrospective meeting migration
    """

    # Text to be used for generating forum post message
    message_body: str
    zoom_account: ZoomAccountRecord
    # api url to fetch sprint names from
    sprint_name_url: AnyHttpUrl
    # used for debug purpose only
    dummy_sprint_names: Optional[dict] = {}
    # auth header for sprint_name_url
    headers: dict


class ProdSettings(BaseSettings):  # pylint: disable=too-few-public-methods
    """
    Main class to hold all config from environment
    """

    google_credentials: GoogleApiCredentials = Field(..., env="GOOGLE_API_CREDENTIALS")
    zoom_accounts: List[ZoomAccountRecord] = Field(..., env="ZOOM_ACCOUNTS")
    drive_folders: DriveFolders = Field(..., env="DRIVE_FOLDER_IDS")
    email_settings: EmailSettings = Field(..., env="EMAIL_SETTINGS")
    debug: bool = Field(False, env="DEBUG")
    templates_directory: Path = Field(
        (Path(__file__).parent / "templates").absolute(), env="TEMPLATES_DIRECTORY"
    )
    discourse_settings: DiscourseConfig = Field(..., env="DISCOURSE_CONFIG")
    retrospective_config: RetrospectiveConfig = Field(..., env="RETROSPECTIVE_CONFIG")


class TestSettings(ProdSettings):  # pylint: disable=too-few-public-methods
    """
    Main class to hold all test config variables
    """

    google_credentials: GoogleApiCredentials = GoogleApiCredentials(
        scopes=["https://www.googleapis.com/auth/drive"],
        creds_or_token={},
    )

    zoom_accounts: List[ZoomAccountRecord] = [
        ZoomAccountRecord(
            account_id="1-fake-account_id",
            client_id="1-fake-key",
            client_secret="1-fake-secret",
            recipients=[EmailStr("1-fake@fake.com"), EmailStr("1-fake2@fake.com")],
            drive_folder_name="1-fake-folder",
        )
    ]

    drive_folders: DriveFolders = DriveFolders(
        general_base_dir_id="fake-drive-id",
        retro_base_dir_id="fake-retro-drive-id",
    )
    email_settings: EmailSettings = EmailSettings(
        use_tls=False,
        host="localhost",
        host_user="",
        host_password=SecretStr(""),
        from_email=EmailStr("test@test.com"),
    )
    debug: bool = Field(True, env="DEBUG")
    discourse_settings: DiscourseConfig = DiscourseConfig(
        api_key="fake-key", username="fake-username"
    )
    retrospective_config: RetrospectiveConfig = RetrospectiveConfig(
        message_body="",
        zoom_account=ZoomAccountRecord(
            account_id="1-fake-account_id",
            client_id="1-fake-key",
            client_secret="1-fake-secret",
            recipients=[],
            drive_folder_name="1-fake-folder",
            pattern="*.-fake",
        ),
        sprint_name_url=AnyHttpUrl(url="http://localhost:8000", scheme="http"),
        headers={},
    )

    class Config:  # pylint: disable=missing-class-docstring,too-few-public-methods
        env_prefix = "TEST_AUTOMATION_"


# logging config
LOG_FORMAT = "%(levelname)s %(name)s %(asctime)s - %(message)s"
logging.basicConfig(
    stream=sys.stdout, filemode="w", format=LOG_FORMAT, level=logging.INFO
)

# app config
config_map = {"prod": ProdSettings, "test": TestSettings}
settings = config_map.get(os.getenv("ENV", "prod"), ProdSettings)()
