# Team Compensation

## The issue with compensation decisions

Compensation is a difficult and often heavy topic. In many organizations, it is the source of a lot of politics. It can be difficult to decouple it from feelings of worth, and it often creates a perceived hierarchy between people, just because of salary differences. Seeing another worker who has a higher rate than ourselves, when we feel we do comparable work, can cause friction, conflict and resentment. 

And yet, we don't think there is a way to have a truly equal and fair approach to set everyone’s salary – all approaches inevitably make some people feel undervalued in some way.

A few remote companies, like Gitlab, try to solve the equality issue by having a [salary calculator](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator-formula) based on location, taking into account the fact that there are disparities in salary and cost of living across the world. But location is hardly a fair criteria – if two people produce the same thing, it shouldn’t matter which country they live in, and there are plenty of costs that are the same for everyone (computers, online services, travel, etc.). So having the same “standard of living” will actually depend as much on the person’s preferences as the location, and it just perpetuates world inequalities to say that someone in India can’t be paid more than someone in the US.

Another approach would be to set a unique salary for everyone. However, the issue then becomes: which salary should we choose? If we align on the highest end like Silicon Valley-type salaries, we instantly become non-competitive on the international market, and lose most of our clients, or our sustainability. If we align with the average world salary, we end up with half of the team feeling underpaid, or won’t have anyone in the team from parts of the world.

And it also still leaves us with differentiating depending on experience, which brings its own set of politics, as well as often ending up having to compare apples to oranges.

## OpenCraft's approach to compensation

The way we approach this isn’t perfect either, but [we have tried to strike a balance](https://forum.opencraft.com/t/openness-and-rates/855):

On one side, we acknowledge that we are all human, and that it would be really hard to avoid the usual negative feelings that come with individual merit-based raises, or in comparing our individual rates.

On the other side, we care a lot about fairness and openness. So we opted to be transparent about the process itself and its decision mechanisms, without publishing the individual compensation rates. Individual rates are considered private information, set by each team member for themselves when they join within the constraints of the process.

We have also designed the process around the principle that OpenCraft should stay out of taking the decisions setting individual compensation values for each member, as much as possible, and keep the process systematic and open. We believe this is more important than the numbers themselves, and critical to help avoid the usual politics and worth perception issues.

## How hourly rates are set

Remuneration is based on time spent working, as a hourly rate:

* Candidates decide on their hourly rate themselves when applying.

* OpenCraft has a range of hourly rates that we can accommodate - we filter out candidates that we can’t afford, but we don’t negotiate the hourly rate at all, we simply hire candidates with the rate they set. The range is kept confidential, to avoid having all candidates simply picking the upper bound: we want candidates to provide a middle ground, ie a rate which they think is fair and not below what they think their work is worth, but have an incentive to keep it reasonable, knowing that if it's too high the candidature will be filtered out.

* We never change or renegotiate the rate set by the candidate after they have been accepted, aside from two systematic mechanisms for raises. These apply to all core team members, from the time of acceptance in the core team (ie after completing the [onboarding period](./processes/onboarding_and_offboarding.md)): [minimum hourly rates](#minimum-hourly-rate) and [team-wide yearly raises](#team-wide-raises).

Note: hourly rates are applied to _all_ the time a member spends working (including overtime), but _only_ the time actually spent working (exclusing breaks, vacation, etc.). So the rate must be chosen in a way which factors in all costs, including taxes, vacation and benefits, including insurance for sickness or unemployment.

## Benefits

In addition to paying our developers hourly rates, OpenCraft offers a variety of somewhat intangible benefits, some of which are quite unique:

* Flexible working hours and location (no commute!)
* Take whatever unpaid time off you want, and take sick days or mental health days whenever you need
* We are discouraged from working evenings and weekends, and are encouraged to take regular holidays, even long ones
* Minimal meetings
* We can use whatever hardware and software we like (Framework or MacBook, Linux or MacOS, ...)
* A bullshit-free environment
* A drama-free environment
* Extremely competent coworkers
* Varied and interesting work (full stack++)
* The CEO is a developer himself and understands the work deeply
* The CEO and company are full committed to open source
* The CEO is the owner and there are no investors to satisfy by slashing costs every quarter
* Paid travel to the Open edX conference and OpenCraft coworking week, which is held in a different country every year
* Paid training (what would you like to learn?)
* We work on education software, something that’s unequivocally good for the world
* Our work is open source and builds a public portfolio
* We are paid to be involved in the Open edX community, which also builds our public reputation

## Minimum hourly rate

We want to make sure that everyone in the team has an hourly rate that allows living a comfortable life, and that nobody can lowball themselves to the point of not being able to live a happy life because of their compensation. 

So there is also a lower end of the range of hourly rates that we will accept, ie a minimum hourly rate. A candidate asking for a rate that is too low will still get accepted, but will get their hourly rate raised to the minimum hourly rate upon confirmation as a core team member

## Team-wide raises

The way we handle raises differs from the usual method of having individuals going to a manager and asking for a raise. Instead, they are applied to all team members systematically, without distinction or negotiation every year, whenever they have been part of the team for the full previous calendar year. The hourly rates are bumped by a percentage amount, based on the overall company results. People who joined during the year still get a raise, but in proportion of when they joined during that year (eg. someone joining on July 1st of a year with a 10% raise gets 0.5x10%=5%).

This is a way to ensure that everyone benefits from the company’s improvements without having to do individual negotiations for each person. Individual negotiations could be good for some of us who can negotiate well, but it could be bad for those who aren’t. It would also tend to make feelings of competitiveness rise up among us: individual performance (or rather, the appearance and publicity of one's performance) would become a factor in getting better raises than other team members. We like the team's collaborative atmosphere, and prefer to not create a reason to compete against our coworkers.

We also try to make these raises significant, whenever the company's results make it reasonable. [It is often x2-3 what would be applied in other companies](https://forum.opencraft.com/t/raises-for-2020/774), with annual raises often in the 5-10% range (though some years it can also be 0% when the results aren't good). There are several reasons behind giving larger raises, to everyone:

* Since everyone gets them, without having to negotiate, it doesn't put those who aren't good at negotiating or who shy away from asking raises at a disadvantage over the others, while still providing a raise matching or exceeding what those who know how to negotiate would be able to obtain.

* The significant raises ensure that even someone starting with the minimum hourly rate but who sticks around, will eventually come to exceed the average international hourly rate, after only a few years.

* We are building a team for the long haul, and want to reward those who choose to stay in the team. Too often, companies low-ball raises by taking advantage of the inconvenience and perceived difficulty of changing jobs. As a result, it rewards those who are willing to switch jobs more often, as it is then one of the only ways to get proper raises. This isn't good for anyone -- not for the companies who end up with a higher turn-over, not for team members who have to go through the job search grind to get proper raises, nor for those who choose to stick with a job or a team they like at the expense of the progression of their remuneration.

## How do we get paid?

Note: more information about our billing cycle can be found in our private docs — but here's some general information that will be useful for newcomers and visitors:

Team members at OpenCraft are paid once per month.

All time worked is logged daily, per-ticket, in a time management plugin integrated with Jira. Also see [time logging best practices](time_management.md#time-logging-best-practices) for advice on how to log your time.

The payment cycle works as follows:

+ If you're an employee, our accountant fills out your monthly payroll sheet in advance, and you'll receive your monthly pay as per our invoicing cycle.

+ If you're a contractor, the following happens:

    + At the beginning of our monthly billing cycle, our accounting bot Crafty warns team members that they should check their time logs.
    + Our automated [Accounting](https://gitlab.com/opencraft/billing/accounting) service generates a draft invoice based on the hours logged the month before — you read correctly, the invoice creation process is *automated, and handled by OpenCraft*. Easy!
    + Team members have a few days to update their time logs, regenerate a new draft if necessary, and approve their invoice draft before the approval deadline.
    + Once we're past the invoice approval deadline, bank transfers are automatically issued.

## Who has access to invoices and financial documents?

Although most of OpenCraft's files are shared and accessible to the entire team, access to invoices and other financial documents is restricted and granted only on a need-to-know basis. Therefore, Google Drive **view permissions** for all invoices (and any other accounting documents) should be **set to Restricted by default**. The only team members with default access to these documents are the CEO, the CTO, the Admin Specialist, and our lawyer.

## Authorizing expenses

To subscribe to new paid tools or services, or to submit receipts for work-related expenses, get approval from the Admin Specialist first. Send all communication about expenses and receipts to billing@opencraft.com, which is managed by the Admin Specialist.

The admin specialist is responsible for:

  + Approving expenses and invoices for amounts of 500 euros and below
  + Using the OpenCraft credit card for subscriptions and other work-related expenses
  + Reimbursing expenses
  + Preparing bank transfers

The CEO is responsible for:

  + Authorizing expenses and invoices above 500 euros
  + Authorizing bank transfers