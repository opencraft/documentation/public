# Cell-Specific Rule Definitions

This page lists rule definitions that are specific to individual cells.

## Serenity (DevOps cell)

### Sustainability Target

The DevOps cell focuses on internal work, so its members usually only log time on accounts that both Tempo and SprintCraft
consider non-billable.

We are planning to look into options for adjusting how sustainability ratios of individual cells are calculated:
At least some of the internal work that Serenity does to improve OpenCraft's hosting infrastructure is supposed to be covered
by monthly maintenance plans from clients who benefit from that infrastructure (i.e., whose instances are hosted on it);
and there might be a way to make SprintCraft's sustainability calculations reflect this.

However, at the moment there is no straightforward way to factor in hours provided by monthly maintenance plans
when calculating sustainability, and Serenity's sustainability ratio as displayed by SprintCraft is always
in the 98%-100% range.

So we currently don't define a specific sustainability ratio for Serenity to target.

Instead, we keep the number of non-billable hours that Serenity can generate each month from growing
in an uncontrolled manner by keeping the cell's size limited to 2 members,
and capping the total number of non-billable hours that the cell is allowed to spend per month
at 208h (cf. [Sustainability Management](#sustainability-management) for details).

### Epic Schedule

Being a small cell that focuses almost exclusively[^2] on internal infrastructure-related projects,
the DevOps cell should maintain a list of current and upcoming epics in a dedicated document
(such as [Serenity Epics Schedule (v4)](https://docs.google.com/spreadsheets/d/175zXrP9BVGxOtjyDPmODP5__ZeA9q2ciYTYJ1nvpNr8)),
so that it is easy for other team members to get a high-level overview of what the DevOps cell is currently working on,
as well as future plans.

### Firefighting

#### Rotations

Due to its small size Serenity does not explicitly designate two firefighters each sprint.
Instead, each member of the cell allocates a small amount of time for firefighting each sprint,
relative to their weekly commitments (in hours): The number of firefighting hours that each cell member
allocates should (roughly) match 7.5% of their committed hours. (This is in line with our
[general recommendation](../processes/firefighting/fire_management.md) that cells should allocate
about 7.5% of their total capacity to firefighting each week).

For example, if members of the cell are committed to working 40h and 12h per week, respectively,
their corresponding firefighting hours would be:

```
0.075 * 40h/week = 3h/week
0.075 * 12h/week ~= 1h/week
```

This would result in a total of 8h of firefighting time for each 2-week sprint.

#### Responsibilities

* Serenity is [solely responsible](../organization.md#the-devops-cell) for maintaining OpenCraft's internal infrastructure
  and must handle all fires affecting that infrastructure. (This helps minimize total time logged
  against non-billable DevOps accounts by other cells, which is important for company-wide sustainability.)
* Firefighting responsibilities for Serenity do not include addressing issues affecting client instances,
  unless there is an underlying infrastructure problem that is causing these issues and affecting multiple instances at once.

#### Delegation

In general, tasks that exceed the cell's capacity for firefighting may *not* be delegated to firefighters of generalist cells.

* In situations where Serenity’s firefighting hours would not allow the cell to handle all OCIM/DevOps-related fires
  coming up during a sprint, the cell must prioritize firefighting over other tasks,
  moving them out of the sprint to free up capacity or postponing them to a later part of the sprint as needed.
* In situations where firefighters from other cells become aware of *urgent* infrastructure-related issues
  while all members of the DevOps cell are out-of-office, they should start working on fixing them immediately.
    * This is the only exception to the general rule about FF task delegation mentioned above.
    * If a given issue has not been resolved by the time that members of Serenity are back to work,
      they should take over the remaining work.

### Roles

#### Cell Management

* Responsibilities belonging to the *Sprint Planning Manager* and *Sprint Manager* roles are reduced
  to a subset of items that is relevant for Serenity. The two roles are assigned to the same person.
* Responsibilities belonging to the *Epic Planning and Sustainability Manager* role are reduced
  to a subset of items that is relevant for Serenity.

##### Sprint Planning Manager

* **Task Estimation Session**: Serenity doesn't do task estimation sessions, so there is nothing
  to do for the cell's sprint planning manager in that regard.
* **Prioritization**:
    * On Thursday before the start of a new sprint (when all relevant tasks for the upcoming sprint are available),
      the Sprint Planning Manager arranges the tasks scheduled for the next sprint to match current priorities
      as laid out in the [epic schedule](#epic-schedule).
    * Additional tasks related to team-wide conversations and/or activities (such as discussing a process change on the forum or preparing for an upcoming conference) are slotted in as necessary.
* **Task Insertion**: [Default responsibilities](../roles/list.md#sprint-planning-manager) apply.
* **Sprint Preparation**: [Default responsibilities](../roles/list.md#sprint-planning-manager) apply.
* **Backlog refinement**: This happens in the context of epic management -- for each epic that a member of Serenity owns,
  they do a pass over the backlog and update/archive tickets as appropriate once per sprint. The sprint planning manager
  does not need to complete a separate pass over the backlog.

##### Sprint Manager

* **Sprint Preparation**: Every Monday before the start of the new sprint:
    * Double-check that each member of the cell has allocated a small amount of time for firefighting, relative to their weekly commitments (in hours).
* **Assigning Firefighter Rotations**: Not applicable (due to its small size Serenity does not keep a [rotation schedule](#rotations) for firefighting).
* **Mid-sprint updates**: [Default responsibilities](../roles/list.md#sprint-manager) apply.
* **Sprint Wrap-Up**: [Default responsibilities](../roles/list.md#sprint-manager) apply, *except* when it comes to
  sprint retrospectives. Serenity's members post a personal retrospective as part of their end-of-sprint updates.
* **Meetings**: Not applicable (Serenity does asynchronous planning).

##### Epic Planning and Sustainability Manager

###### Epic Planning

[Default responsibilities](../roles/list.md#epic-planning) apply, with the following additions and modifications:

* The Epic Planning Manager checks the [epic schedule](#epic-schedule) once per sprint and updates it
  to reflect the latest status and priorities.
    * Status mapping (epic schedule => JIRA):
        * On Hold => Offer / Waiting on client, Accepted
        * Not Started => Prospect
        * In Development => In Development, Recurring
        * Done => Delivered to client
* Serenity does not keep a rotation schedule for discovery duty, so updating DD assignments in the
  [Weekly Rotation Schedule][^1] is not necessary.
* Since [cell size will not change](#sustainability-target), it is not necessary to coordinate
  with the team's [recruitment manager](../roles/list.md#recruitment-manager)
  (nor the [business development specialist](../roles/list.md#business-development-specialist)) on a regular basis.
  The main scenario where help from the recruitment manager would be needed is
  if a member of the DevOps cell decides to leave the team: In that case,
  the epic planning and sustainability manager would work with the remaining cells
  to see if they could provide a replacement, and notify the team's recruitment manager
  of the capacity reduction so that they can take necessary steps to replenish the team's capacity
  (either by recruiting directly for the DevOps cell or by finding newcomers
  for the cell that donated one of their members to the DevOps cell).
* Since Serenity doesn't own any clients, adjusting client budgets in SprintCraft is not necessary.
* The epic planning and sustainability manager should use the modified checklist included [below](#bi-weekly-checklist)
  when compiling bi-weekly epic planning and sustainability updates.

###### Sustainability Management

As mentioned [above](#sustainability-target), Serenity (as the DevOps cell) is limited to 2 members
and its size will be kept constant for the foreseeable future. This sets an upper bound
for the number of non-billable hours that Serenity can generate per month:
i.e., the sum of the monthly hour commitments of its members.

As a result, responsibilities for sustainability management are reduced to making sure that the total number
of non-billable hours that Serenity accumulates each month does not exceed  **208h**, i.e., the total monthly
commitment of Serenity's current members, calculated as `(1 + 0.3) * 40h * 4 weeks`.

**Note that:**

* *This cap does not include an additional buffer for overtime:*
  We assume that reasonable amounts of overtime incurred over the course of some sprints
  will be offset by periods of less total time worked over the course of other sprints.
* *This cap applies to the total number of hours logged against non-billable SE tickets,
  irrespective of whether they were logged by members of Serenity or members of other cells.*
  For example, if Serenity decides to have a member of another cell provide a review
  for a given SE ticket, the time spent on the review will be logged on the SE ticket
  and thus count towards the total number of non-billable hours generated by Serenity
  over the course of the month where the review happened.

The sustainability manager should check the number of non-billable hours for the current month
once per sprint so that any issues can be caught early and necessary adjustments can be made
in the following sprint. They should record results of this check for future reference,
e.g. by putting them into a spreadsheet like [this one](https://docs.google.com/spreadsheets/d/1NgYKTuY1ysVEVwtwvmgJsilC6P2UAhRcf-f48B2eCA8/edit).

Aside from making sure that non-billable hours stay within the monthly limit mentioned above,
Serenity's sustainability manager should perform an [accounts review](../processes/budgeting_and_sustainability_management.md#every-month)
once per month (scheduled for the same day as the accounts review for other cells). The main
aspect to focus on in the context of the DevOps cell is making sure that tickets dealing with
incidents affecting multiple client instances are associated with the clients' *Instance Maintenance*
accounts instead of internal DevOps accounts.

###### Bi-Weekly Checklist

The bi-weekly checklist for compiling epic planning and sustainability updates for Serenity looks like this:

```text
h3. Epic planning and sustainability update (Sprint ???)

h4. Epic status

* ( ) Review and update epic statuses as needed.
** ( ) New epics ("Prospect", "Offer / Waiting on client", "Accepted"):
*** ( ) Add an entry to the [Time / Estimates|https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1] sheet of the epic planning spreadsheet.
*** ( ) Move to "In Development" if work has started.
*** ( ) Add to [epic schedule|https://docs.google.com/spreadsheets/d/175zXrP9BVGxOtjyDPmODP5__ZeA9q2ciYTYJ1nvpNr8/edit], setting appropriate status and priority.
** ( ) In development:
*** ( ) Make sure epic updates matching the template are posted by epic owner/reviewer and include relevant information about ongoing and future work.
*** ( ) Ensure delivery and bugfix deadlines of individual epics are on target (or are being actively discussed on the epics).
*** ( ) Move epics that have become (permanently) blocked on other work to "Offer / Waiting on client".
*** ( ) Update status on [epic schedule|https://docs.google.com/spreadsheets/d/175zXrP9BVGxOtjyDPmODP5__ZeA9q2ciYTYJ1nvpNr8/edit].
** ( ) Delivered epics:
*** ( ) Move to "Delivered to client / QA".
*** ( ) Update status on [epic schedule|https://docs.google.com/spreadsheets/d/175zXrP9BVGxOtjyDPmODP5__ZeA9q2ciYTYJ1nvpNr8/edit].
** ( ) Completed epics:
*** ( ) Move to "Done".
*** ( ) Update corresponding entries on the [Time / Estimates|https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1] sheet of the epic planning spreadsheet.
*** ( ) Remove from [epic schedule|https://docs.google.com/spreadsheets/d/175zXrP9BVGxOtjyDPmODP5__ZeA9q2ciYTYJ1nvpNr8/edit].
* ( ) Ensure the projects from your cell's clients are being properly delivered using the epic management process.

* ( ) Compare the Epics board with the epic planning spreadsheet and update the spreadsheet as necessary.

h4. Availability updates (departing members, weekly commitments, vacations)

* ( ) If someone decides to leave the team:
** ( ) Update their availability for the coming months.
** ( ) Help find new owners for their responsibilities (roles, clients, epics).
** ( ) Find someone to replace them by coordinating with the remaining cells and/or the recruitment manager.
** ( ) **External replacement** (newcomer):
*** ( ) Add availability and onboarding hours for the coming months to the epic planning spreadsheet.
** ( ) **Internal replacement** (existing member):
*** ( ) Move availability from the donating cell's planning sheet to the DevOps cell's planning sheet.
*** ( ) Help find new owners for their responsibilities (roles, clients, epics).

* ( ) Handle requests from cell members to change their weekly hour commitments.

* ( ) Check the calendar for vacations coming up in the next couple months. If someone will be away for a total of 1 week (or longer), [post a comment mentioning their availability|https://gitlab.com/opencraft/documentation/public/merge_requests/99#note_198626533] in the epic planning spreadsheet.

* ( ) Update [Role statistics|https://docs.google.com/spreadsheets/d/1i1BOjiikB_zFvd8PgB4pteNVYI5tmE7DNbrE8JQI7UQ/edit] for your cell.

h4. Capacity requirements

_For in-progress epics:_

* ( ) Evaluate the amount of time required to complete the accepted project scope over the next months and update capacity allocations in the epic planning spreadsheet.

h4. Budgeting

_Once per sprint:_

* ( ) Check number of non-billable hours logged for the current month to see if cell is on track to stay below monthly budget cap (208h), and follow up as necessary.

_Once per month:_

* ( ) *Before the 5th of the month:* Check that tasks from previous month use correct accounts, and post update on your cell's epic planning and sustainability epic.
* ( ) Add actuals for previous month to epic planning spreadsheet.

_As necessary:_

* ( ) Archive obsolete epics.
* ( ) Close obsolete accounts.

h4. Notes

...
```

### Vacations

Due to its small [size](#sustainability-target) Serenity's limit for the percentage of members that can be away
at the same time is increased from 20% to 50%.

## Bebop

### Sustainability target

In 2024, Bebop must target a sustainability ratio of **25%**.

This target was [set](https://tasks.opencraft.com/browse/STAR-3464?focusedCommentId=289657&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-289657)[^1] in the context of the latest company-wide sustainability review (completed in April 2024).

### Firefighting

Bebop designates three firefighters per sprint instead of two, but at an overall reduced volume of firefighting hours of 15h (not counting the time set aside by the rest of the cell members for firefighting).

#### Rotations

Bebop's rotations are designed to ensure there is at least one person from each of these timezones on firefighting duty at all times:

|**Sprint**|**Asia/Pacific**|**Europe/Africa**|**Americas**|
| --- | --- | --- | --- |
|A|Team member 1|Team member 1|Team member 1|
|B|Team member 2|Team member 2|Team member 2|
|C|Team member 3|Team member 3|Team member 1|
|D|Team member 4|Team member 1|Team member 2|
|E|Team member 5|Team member 2|Team member 1|
|F|Team member 6|Team member 3|Team member 2|

## Falcon

### Sustainability target

In 2024, Falcon must target a sustainability ratio of **26%**.

This target was [set](https://tasks.opencraft.com/browse/STAR-3464?focusedCommentId=289657&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-289657)[^1] in the context of the latest sustainability review (completed in April 2024).

### Firefighting

Falcon only handles alerts related to their own single project, and as such doesn't handle many alerts. As such the cell assigns pager rotations freely, as needed to ensure that alerts related to that project are correctly managed. It is also excluded from the requirement to keep 7.5% of their total capacity each sprint for firefighting, and its cell members are not required to be in the escalation queue.

In exchange for the lighter firefighting rotations, Falcon handles the firefighting managers roles, as well as providing backups for these roles during vacation.

<!-- Links -->

[Weekly Rotation Schedule]: https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit

<!-- Footnotes -->

[^1]: Private for OpenCraft Employees
[^2]: Sometimes, Serenity *will* be asked to work on client projects (like [Axim PR sandboxes](https://openedx.atlassian.net/wiki/spaces/COMM/pages/4309745665/Axim+PR+Sandboxes)) or on community-driven ones (like [K8s Harmony](https://github.com/openedx/openedx-k8s-harmony)).
