# Cell Life Cycle

This section discusses details of how cells of any type can start, develop, and end.

## Cell Birth

New cells can be created in one of two ways:

1. By splitting into two, where a portion of the original cell's members go off to start a new one, or
2. By uniting a subset of members from two parent cells into a third one.

It's important to note, however, that there are thresholds below which:

* The overhead of cell creation and deletion negates the new cell's usefulness.
* Some of the cell management work that will remain necessary to keep cells self-managed won't be practical.
* There would be too little capacity to appropriately serve the needs of all clients in either the new cell or the parent one(s).

The latter points need to be taken into account when deciding to create a new cell.

### Cell Splitting

A cell split is defined as the process for creating a new cell from a single parent, where the parent continues to exist.  It most commonly happens when a cell hits the limit of 12-16 core team members, but can also happen before that: for example, as a way to create [project cells](#creation-of-project-cells).  When the decision to do so is reached, the parent cell [gets together and decides](../processes/making_decisions.md):

* Which members and clients stay with the parent cell, and which ones go to the new one,
* How the [standard cell roles](../roles/list.md) will be assigned after the split,
* What the new cell is going to be named.

And finally,

* Who will be assigned the [cell split manager role](../roles/list.md#cell-creation-manager) to coordinate the split.

Once all of this is decided, the cell split manager can begin the [step-by-step process](creation_checklist.md) of creating the new cell itself.

### Cell Recombination

In some cases, it might be desireable to create a new cell out of two pre-existing ones, so that a third one is created while the two parent cells continue to exist.  For instance, it would be possible to:

* Take three members from Bebop and three from Serenity, creating a new cell with six members;
* Take a single member from Serenity, three from Falcon, and immediately recruit four newcomers, for a new cell with eight members.

In general terms, the new cell is allowed to recruit members from either parent cell that have indicated a desire to move to the new cell, for whatever reason, as long as the they're held responsible for handing off all existing roles and duties adequately.  Epic planning and sustainability managers from both parent cells should be consulted on the viability of each case.

The newly-created cell is also allowed to inherit at least one client (but possibly more) from its parents at the discretion of each cell - in particular, each cell's epic planning and sustainability manager.  Note that because changing client ownership is often disruptive to clients, we want to avoid it as much as possible.  In other words, unless a good reason for doing otherwise exists, when clients move to the new cell, so should their owners - and vice-versa.

Once team and clientele are defined, the process continues exactly like in a [cell split](#cell-splitting): assigning roles, choosing a cell name, and creating a new cell.

### Creation of Project Cells

Project cells can be created by either splitting or recombination, as described above.  First off, the process is instigated by a client owner, who after identifying a need for a dedicated cell, consults the cell that currently owns said client on whether they are willing to let them (including the owner) be transferred to a new, dedicated cell.

Depending on the current structure of individual cells and the team as a whole, a project cell might also be created by restructuring two existing generalist cells. In this scenario (which could be considered a special case of [cell recombination](#cell-recombination)), the total number of cells does not change. Instead, one of the cells becomes the project cell while the other remains a generalist cell. The generalist cell absorbs any clients and epics that the project cell will no longer be responsible for, as well as any team members that are (for whatever reason) not needed in the project cell.

## Cell Development

Cells can grow and shrink during their lives.  Growth will happen as a result of more budget or simply planning for the future, and is done by the [recruitment manager](../roles/list.md#recruitment-manager) as needed.

Reduction can also occur naturally when:

* A new cell is split off a parent one;
* A set of members leave to form a new cell via recombination;
* A member leaves the company;
* A member transitions to a support cell.

As part of the reduction process, the affected cell's epic planning and sustainability manager needs to decide whether recruiting is required to offset the lost members, and coordinate with the team's recruitment manager as necessary.

### Reduction of Project Cells

Project cells are faced with two specific downsizing situations that generalist cells don't have to deal with: when the cell's titular client or project budget is reduced, or when a member is no longer interested in working on the cell's project.  When either occur, the following should be observed.

For budget reduction:

1. The reduction is announced to all current project cell members.
1. In proportion to the reduction, a specific number of volunteers to leave the project, and thus the cell, are requested.
1. The volunteers then proceed to hand off current duties and roles, after which they'll be able to rejoin their original cell, or join another project cell that will accept them.

For leaving members:

* They can return to their original generalist cell if both cells agree, or join a different project cell that will have them.
* If necessary, recruiting is done as part of the offboarding process in order to offset the lost member(s).

## Cell Assimilation

For generalist cells, assimilation should be a rare, but possible, phenomenon.  For example, a cell may, for whatever reason, end up with too few members to handle self-management adequately.  The cell can always engage in aggressive recruitment to fix the problem, but it may not pan out.  In this case, the following outcomes are possible:

* The afflicted cell is assimilated back into the cell it was split from: members, clients, and all.

And/or:

* Current members that express a desire to do so can join existing project cells that will have them.

After the process of assimilation, the cell that was assimilated no longer exists.

### Assimilation of Project Cells

Project cells are different from generalist cells in that they can disappear at any time.  If any of the following are met, a project cell and its members are assimilated as described above:

* The client project is delivered, and there's no immediate replacement from the same client.
* The budget for the cell's project(s) runs out, or is reduced below the threshold where a separate, dedicated cell makes financial or practical sense.
* The project is halted by the client, for any reason.

If at this time the titular project still exists but with reduced budget, it should move back to the generalist cell it originally came from, if possible together with its owner and a sufficient number of onboarded developers.  Otherwise, members are free to join other project cells that will have them, or alternatively, rejoin the parent cell they left to join the project cell in the first place, including other project cells, as per [Project Cell Birth](#project-cell-birth).
