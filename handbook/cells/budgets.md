# Cell budgets

## Introduction

To give generalist and project cells autonomy to make decisions about the work they do, create and schedule some tasks independently, without approval, responsibility for some of the company's budgeting is delegated to them. The idea is to ensure that someone close to the work can take more of the decisions regarding internal budgets, someone who can have in mind all the details of the work. It keeps our efficiency high and allows for a better use of our internal budgets.

## Cell and company-wide sustainability

For this to work, a cell needs a simple way to check that it remains sustainable while it takes these decisions.

* In general, a cell is considered sustainable if and only if enough hours worked can be billed to clients, proportionally to the hours that aren't billable.
* The number of billable hours that a cell is realistically able to produce over the course of a month depends on multiple factors that change over time:
    * [Cell type](../organization.md#cell-types)
    * Cell size
    * Number of clients
    * Number, size, and development pace of billable projects
    * Budget status of billable projects (if a billable project is over budget, any additional time spent on it will be non-billable and negatively impact a cell's sustainability ratio)
* Cell sustainability affects company-wide sustainability: Even if individual cells are hitting their target sustainability ratios, the total number of non-billable hours logged by the team as a whole might still be too high for the company to be profitable.

As a result, we must define (and adjust 1-2 times per year):

* A target sustainability ratio for the company as a whole.
* Target sustainability ratios for individual cells that make it possible to hit the company-wide sustainability target while taking into account the cell-specific factors listed above.

Having a cell-specific sustainability ratio to target allows individual cells to schedule non-billable work without requiring management approval for each new task or epic budget -- as long as a cell doesn't exceed its target sustainability ratio, it is free to decide what projects to work on and when.

Current sustainability targets for individual cells are listed under [Cell-Specific Rules](./cell_specific_rules.md). The target sustainability ratio for the company as a whole is not disclosed in this handbook.

## Tooling

We use [SprintCraft] to track company-wide and cell-specific sustainability, as well as to define budgets
for individual Tempo accounts and monitor their usage.

⚠️ _Note that even if non-billable accounts have different categories in Tempo (this may or may not change in the future),
SprintCraft treats them all the the same when calculating sustainability._

The sustainability ratio of a cell, as well as the company as a whole, is calculated as:

```
non-billable hours / total hours
```

... where `total hours` is the sum of billable and non-billable hours logged over the period under review.

## Attribution of hours

Work is accounted per Jira project, no matter which team member logged it. For example:

- If time is logged on a ticket with an 'SE' prefix, it counts towards 'Serenity'.
- If time is logged on a ticket from a non-cell project (e.g. 'MNG'), it's counted only there.

Counting it this way allows a cell to ask other cells for help with non-billable work without affecting the sustainability of these cells. Additionally, members of a cell which is low on billable hours can find more work without negatively impacting their cell's sustainability ratio by picking up reviews for tasks from other cells that are currently sustainable.

However, counting it like this also means that the cell that owns a given task should [agree to external members logging the time there](../organization.md#cross-cell-planning-and-time-logging).

Things to keep in mind:

- **Task budgets are all-inclusive:** As stated in [Time logging best practices](../time_management.md#time-logging-best-practices), all work should be logged. If two people meet for 1h over a task, they log 1h *each*, for a total of 2h.
- Time logged on tasks impacts the parent epic's budget (and by implication the cell's too) even if that budget is not strictly binding. So, when creating tasks or requesting somebody's time, think about where that time will be logged and what budget it will affect. This, and warning before going over a task's original estimate, will help everyone have a more effective and predictable workflow.

## List of budgets managed by the cells

Hours from client epics (including [fixed-price projects](../processes/billable_work.md#fixed-price-accounts) as well as recurring maintenance and support) are billable. The rest isn't - like internal projects, forum discussions about internal matters and internal team meetings are non-billable.

Not all the internal/non-billable budgets are the responsibility of cells; some are handled by management (and are omitted below).

### All cells

The budgets and corresponding accounts for which cells of any type are responsible are:

#### Internal processes

* **Cell management**: Time spent fulfilling the cell management roles & discussing cell-related matters.
* **Meetings**: Time spent in things like sprint planning or all hands.
* **Recruitment**: Time spent on recruitment-related work, including the [Recruitment Manager](../roles/list.md#recruitment-manager) role, [developer reviews](../processes/onboarding_and_offboarding.md#evaluation-criteria), and onboarding epics.
* **Learning**: Time spent learning things, outside of the context of specific projects (almost every task requires learning something, that learning is part of that task's scope).

Except for **Learning**, these accounts are crucial to the functioning of each cell and regularly have time logged against them.

#### Conferences and contributions

* **Conferences**: Time spent preparing and attending the Open edX conference.
* **Contributions**: Making contributions to the platform for which we aren’t being paid by clients, including [Core Contributor](../roles/list.md#core-contributor) work and [miscellaneous contributions](../roles/list.md#miscellaneous-contributions).
* **Community Relations**: Time spent working on responsibilities belonging to the [Community Liaison](../roles/list.md#community-liaison) and [OSPR Liaison](../roles/list.md#ospr-liaison) roles. *Note that usage of this account is limited to current assignees of these roles.*

#### Support work

* **Accounting**: Time spent on accounting-related work (most often in collaboration with the [Administrative Specialist](../roles/list.md#administrative-specialist)).
* **Client Management and Briefing**: Time spent on [Strategic Client Management](https://docs.google.com/document/d/1bTgDk0NAj191hWNI-MSyM4Ihfa5DjlpDETLQddgyyro/), including creating and maintaining [Client Briefings](../processes/working_with_clients.md#client-briefings) and [Value Propositions](../processes/working_with_clients.md#value-propositions).
* **Legal**: Time spent following up on requests from OpenCraft's legal team (such as [obtaining a certificate of residency](https://tasks.opencraft.com/browse/MNG-3048)[^1]).
* **Marketing**: Time spent working on marketing-related tasks (such as writing [blog posts](../processes/blog.md)).
* **Sales Management**: Time spent on [sales](../processes/sales/overview.md) and business development-related work.

These accounts usually don't have a significant number of hours logged on them. However, just like the accounts listed above they represent non-billable work, so time spent on them needs to be taken into account in the context of [sustainability management](../processes/budgeting_and_sustainability_management.md#sustainability-management).

#### Non-billable development projects

Lastly, cells must track and adjust time spent on any accounts created for non-billable development projects (such as [SprintCraft] and [Listaflow]) that they own.

### Cells with clients

In addition to the budgets listed above, all cells that own at least one client are responsible for the following budgets:

* **&lt;Client&gt; instance maintenance**: Each cell is responsible for maintaining its client deployments, including handling Open edX stable release upgrades and applying security updates when necessary. Maintenance accounts for individual clients usually have a monthly recurring budget, and cells must make sure that they don't exceed the total number of hours that the budget provides for the duration of the maintenance contract. (I.e., going over budget for a couple months is fine as long as the excess is compensated for in the following months.)
* **Overhead**: Time spent in work for clients that exceeds a fixed budget we agreed to for the completion of an epic, and which will thus not be paid by the client.

### Generalist cells

The following budgets are the primary responsibility of generalist cells.  This does not mean that other types of cells can't help out with relevant tasks when the need arises - but they're not required to:

* **Instances Upgrades** (Open edX release upgrades): When a new stable version of Open edX is released, all Open edX instances that we host must be upgraded soon after.  While cells are responsible for their own clients' upgrades (as explained above), a general upgrade epic will be created for each release in order to drive the corresponding code and infrastructure changes common to most clients (Ocim, etc.).  Responsibility for owning this type of epic will alternate between generalist cells.  They will need to take this into account in their annual epics/budgets planning, to include the budget for supervising one upgrade (or more, depending on the number of generalist cells that currently exist).
    * Note that it is still the responsibility of each individual cell, regardless of whether it is a generalist cell or not, to ensure that the upgrade work for instances belonging to its client(s) is done as soon as possible after the release: preferably within 1-2 weeks of the release, and at most within 2 months.  This means that it'll be in the best interest of even a project cell to assist with the common upgrade work (assuming that it has an external client).
* **Prospects**: Generalist cells decide how much time to allocate to prospect work - the more prospect work, the more billed hours coming in later on, so this is a way to invest to increase future internal budgets and cell size.
    * In cases where a project warrants the creation of a project cell to house it, all prospect work specific to it - a discovery and its associated budget, for instance - is to be moved to and managed by the new cell.

### DevOps cell

Lastly, the following budgets are the sole responsibility of the DevOps cell (Serenity).

* **Instance Manager**: Work related to maintaining and improving Ocim, our hosting automation software.
* **Instances DevOps**: Work related to maintaining and improving our internal infrastructure, including OpenCraft-specific services such as Mattermost/Vault/etc. and shared hosting infrastructure (such as MySQL and MongoDB clusters).
* **Ocim Containers**: Work related to maintaining and improving containerized deployment methods for Open edX

## Cell budgets independence

The proportion of the time spent on individual budgets can vary per cell - that’s part of the decisions individual cells will be taking, with the decisions and monitoring of the budgets being coordinated by the person with the [sustainability management](../roles/list.md#sustainability-management) role for the cell.

Cell budgets for internal/non-billable projects are each proper to each cell, and they aren’t influenced by the other cells' budgets. No matter what happens, each cell must keep the percentage of non-billable hours that it generates within bounds that will allow the cell to hit its sustainability target (as listed under [Cell-Specific Rules](./cell_specific_rules.md)). To keep accounting and coordination work sane, budgets can't be exchanged between cells.

## Firefighting and cell budgets

Firefighting tasks can pose a unique challenge when it comes to cell budgets since in most cases these are not tasks that can be delayed.

Experience has shown that firefighting tasks are often small and don't take more than a few hours to complete.
So in many cases they will not greatly impact sustainability (assuming that the total number of hours coming from firefighting tasks
stays relatively small compared to the volume of hours coming from regularly scheduled work),
and the more important thing is to begin working on them immediately.

However, we still want to keep the sustainability impact of firefighting incidents in check,
and to that end we provide [guidelines](../processes/firefighting/triage_guidelines.md) for firefighters
to use when triaging incoming issues.

## Rolling budgets

To account for the fact that there are sometimes variations in needs, or in volume of work, budgets are accounted for on a rolling basis. If a cell uses less budget one month, it can spend it in the next months, and if the budget is exceeded one month, it is to be caught up on in the following months. It is recommended to build up a bit of a buffer for non-billable work, to be able to weather surprises without having to make cuts every time.

## Management

As always, Xavier or Braden can still make decisions on those topics, and these decisions would still take precedence over individual cells’ decisions. But like now for the rest of the decentralized responsibilities, ideally we’ll limit this to as few cases as possible - it’s simply a way to ensure we have a way to efficiently handle issues where self-management isn’t enough.

[SprintCraft]: https://sprintcraft.opencraft.com
[Listaflow]: https://gitlab.com/opencraft/dev/listaflow/

[^1]: Private for OpenCraft Employees
