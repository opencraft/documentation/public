# Cell Creation Checklist

This is an authoritative list of practical steps that need to happen when a new cell of any type is created.  It was distilled from the last cell split at the time of writing: for future ones, "Newcell", links, and references should be changed appropriately.

* Update the handbook to add the new cell
    * [List of Current Cells](list.md) page
    * [Cell-Specific Rules](cell_specific_rules.md) page
* Create a new team in the [CRM](https://opencraft.monday.com/users/team/all):
    * Move members of new cell to new team.
* Create the new forum group with the new members so that we can use the `@newcell` mention
    * [Newcell forum group](https://forum.opencraft.com/g/newcell)[^1]
    * This cannot be done by an admin, ask Xavier
* Create the sprint updates thread in the forum (such as [this one](https://forum.opencraft.com/t/sprint-updates-falcon/763/9)[^1]).
* Create the new [Jira team](https://tasks.opencraft.com/projects/NEW/)[^1]
    * Also known as "Project"
    * This should be done by Xavier
* Create the new Jira sprint and epics dashboards
    * Should be done by Xavier or with the [crafty](https://vault.opencraft.com:8200/ui/vault/secrets/secret/show/core/OpenCraft%20Tools%20:%20Resources%20:%20Jira%20-%20crafty%20bot%20account) user
    * [Newcell Epics Dashboard](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=50)[^1]
    * [Newcell Issues Dashboard](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=45)[^1]
* Assign the new clients and services accounts to the new cell on Jira
    * Should be done by Xavier or the Administrative Specialist
* Update Jira Tempo accounts
    * Should be done by Xavier or the Administrative Specialist
* Create current sprint and next one
    * Even if the current sprint will be empty this is needed for [SprintCraft][^1]
* Create firefighting epic and first set of firefighting tickets for the new cell:
    * Such as, for instance, Falcon's: [FAL-1](https://tasks.opencraft.com/browse/FAL-1)[^1]
    * Add new cell to [Weekly Rotation Schedule][^1],
      create a template for the default order of rotations, and schedule the first set of rotations
      (starting with the upcoming sprint).
    * Update firefighting and discovery duty rotations for existing cell(s) as necessary:
      Schedules for upcoming sprints (as well as templates for the default order of rotations)
      should no longer include team members that are moving to the new cell.
    * If upcoming sprint(s) for existing cell(s) already contain SF and/or DD tickets, update or archive them as necessary.
    * Create two firefighting tickets for next sprint of new cell and assign them
      based on rotation schedule created earlier.
* Update client and epic ownership:
    * Clients and epics move to the new cell with their owner, except in the case of project cells.
      In the latter case, anyone who owns an epic that isn't related to the project needs to find a new owner
      for the project (preferably from the cell that the member of the new project cell is coming from),
      and transition the client accordingly.
    * For any clients that changed owners, update their entry in the [CRM](https://opencraft.monday.com/boards/1077823105).
    * Example request to move Serenity epics owned by Falcon user to Falcon project: [Jira query](https://tasks.opencraft.com/browse/SE-250?jql=project%20%3D%20SE%20AND%20issuetype%20%3D%20Epic%20AND%20status%20in%20(%22In%20progress%22%2C%20Backlog%2C%20%22Need%20Review%22%2C%20Recurring%2C%20%22External%20Review%20%2F%20Blocker%22%2C%20Merged%2C%20%22Deployed%20%26%20Delivered%22%2C%20Accepted%2C%20%22In%20development%22%2C%20Offer)%20AND%20assignee%20in%20(membersOf(Newcell)))[^1]
    * For all epics that are still in progress, make sure that the owner and the reviewer belong to the same cell
      (this does not apply to cross-cell projects) - find new reviewers when necessary.
* Move all past, present, and current tickets that belong to accounts owned by the new cell to the new cell
    * This is required in order to be able to correctly keep track of new cell's budgets and sustainability
    * Note that moving a ticket that is assigned to a person that's not a member of the new cell to the new cell may automatically unassign the owner since Jira enforces that the ticket owner is a member of the project (cell) that the ticket belongs to - that is an unfortunate side-effect, but the benefit of keeping the accounts clean is more important and it should always be possible to infer the previous owner from ticket comments and/or worklogs anyway
    * Search for all billable accounts that were moved to the new cell: [Jira query](https://tasks.opencraft.com/secure/TempoAccounts!default.jspa#query/category=2&status=OPEN&project=11800)[^1] - this query also returns accounts owned by multiple cells, you should ignore those
    * Run a query to find all tickets for each billable account that was moved to the new cell: [Jira query](https://tasks.opencraft.com/issues/?filter=-4&jql=project%20%3D%20FAL%20AND%20Account%20%3D%2026%20order%20by%20created%20DESC)[^1]
    * Use the "Bulk Change" tool under "Tools" in the top right part of the screen to move tickets in bulk
    * If a task that you want to move is a subtask of a task that belongs to an account from a different cell, the bulk tool will refuse to move it. A solution in that case is to convert the subtask to a proper ticket before moving
* Ensure that the Budget dashboard in [SprintCraft][^1] reflects the changes that you made:
    * The budget dashboard in [SprintCraft][^1] is heavily cached, so make sure to clear the cache before doing any checks: [How to clear the cache](https://tasks.opencraft.com/browse/FAL-743?focusedCommentId=189959&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-189959)[^1]
    * Go to the new cell's board in [SprintCraft][^1] and check the budgets and actuals for the past two years. Ensure that only the accounts owned by the new cell show up under the "Budgets" section and that the numbers look correct.
    * Do the same check on the old cell's board. Ensure that none of the accounts that were moved to the new cell show up under the "Budgets" section - if they do, double check that you've moved all the tickets and cleared [SprintCraft][^1]'s cache.
* Move incomplete onboarding epics of newcomers to the new cell and reassign mentors as necessary.
* Update [epic planning spreadsheet]:
    * Create a new sheet for the cell being created, following the format of existing sheets, and populate it with relevant information.
    * Update sheets for any existing cells that are being altered as a result of the creation of the new cell.
* Start the current sprint
    * This won't be possible if the current sprint is already started. In that case, [SprintCraft][^1] dashboard won't be available until the next sprint
* Create the retrospective preparation ticket (Clone [FAL-8](https://tasks.opencraft.com/browse/FAL-8)[^1], for instance.)
* Move current sprint's tickets to the new cell's sprint
    * Example request to move Serenity tickets from current sprint to the Newcell current sprint: [Jira query](https://tasks.opencraft.com/issues/?jql=project%20%3D%20SE%20AND%20Sprint%20%3D%20368%20AND%20assignee%20in%20(membersOf(Newcell)))[^1]
    * Repeat the process for any future sprints in case any tickets were already scheduled
    * Repeat the process for `Stretch goals`
* Confirm that sprint boards for existing cell(s) and new cell show appropriate subsets of tickets.
* Create the new Google Calendar and add both service accounts
    * `calendar@sprints-dev.iam.gserviceaccount.com`
    * `sprints@sprints-242609.iam.gserviceaccount.com`
* Create the new Mattermost channel ([Newcell channel](https://chat.opencraft.com/opencraft/channels/newcell))
* Update the [Spillover spreadsheet](https://docs.google.com/spreadsheets/d/1aJD-e2NkDsyBq_yBHykGiMYE29FvSOuYSCZjOJ5sjkM/edit)
    * Create two new sheets: `Newcell Spillovers` and `Newcell Commitments`
* Update Jira scripts and rollout a new version
    * Previous Newcell [merge request](https://gitlab.com/opencraft/dev/jira-scripts/-/merge_requests/20)[^1]
    * Get it merged and then ask Braden to deploy the changes on the Jira server
* Update Mattermost to handle ticket links
    * Newcell [pull request](https://github.com/open-craft/ansible-secrets/pull/228)[^1]
* Update Jira quick filters of the original cell to remove former members
* Add the new cell's name and its abbreviation (used in Jira) to [Crafty](https://crafty.opencraft.com/)
    * Look up the admin access details in Vault
* Connect the cell's (automatically created) Sprint evaluation board with the Cell's (automatically created) Sprint retrospective board on [opencraft.monday.com](https://opencraft.monday.com) by setting an "automation" on Sprint evaluation board as "When an item is created in this board, create an item in Sprint retrospective and connect them in the selected board". Ensure the "Sprint retrospective" part of the sentence points to the corresponding board as the default is the template which is used to clone the board.
    * Change the "Column Setting" for the linked column to point to the same board specified in the Automation you set
    * Do a test evaluation form submission and validate it arrives on the retrospective board (almost immediately)

[epic planning spreadsheet]: https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit
[SprintCraft]: https://sprintcraft.opencraft.com
[Weekly Rotation Schedule]: https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit

[^1]: Private for OpenCraft Employees
