# Epics

Epics are large projects that involve many tasks and span multiple sprints, sometimes indefinitely. OpenCraft has both internal and external (client) epics. Epic owners at OpenCraft straddle a position of technical leadership and project management. Each member of OpenCraft is expected to lead one or more of them.

Internal epics (among other things) are funded by the margin between your rate and the rate we bill the clients. This means that some epics' funding is heavily reliant on our ability to maintain a steady stream of [billable hours](processes/billable_work.md#what-is-billable-work-what-is-non-billable-work). Internal epics are also secondary to client work. However, just because internal epics are not client work does not mean you don't have a client. It's just that the client is OpenCraft.

## Epic ownership

Just like a regular ticket, an [epic] has both an assignee (which we refer to as the [epic owner]) and a reviewer (which we refer to as the epic reviewer).

Concrete expectations that we have for epic owners are laid out in the [epic owner] role of this handbook. To get a refresher on these expectations, go ahead and have another look at that section now.

The main role of the epic reviewer is to take over the responsibilities of the epic owner while they are away, as well as any additional tasks and responsibilities that are specific to a given epic. It is the responsibility of the epic owner to coordinate their time off with their epic reviewer.

Also keep in mind that if you do the [discovery] for a future epic, you will be expected to take on the epic owner role for that epic later on (if/when the client gives us the go-ahead for starting the work).

### Understanding the scope of Epic Ownership

While we do have concrete expectations for Epic Owners, we also must construct a way of conceptualizing the role of an epic owner. The precise role of an epic owner can vary depending on the project, as different clients have different needs, however, it's useful to understand the epic this way:

> A task assignee is reponsible for the implementation, execution, and deployment of a specific story or feature. In the same way, the epic owner is responsible for the planning, execution, and delivery of an entire project.

Necessarily, this means having a firm grip on budget, communications with the client, and prioritization.

#### Budget

The Epic owner is responsible for guarding the Epic project budget. That means keeping track of the hours spent on tickets and making sure the project does not go over budget. This means holding assignees to their estimates, making priority adjustments with the clients when tasks take longer than expected (or leveraging extra budget if they're proven easier.) Be on the lookout for, and be proactive about, [stuck tickets](#handling-stuck-tickets).

It also means walking the client through things which **will** impact the budget. When the client asks for changes, new features not in the original scope, or other changes, you **must** inform the client of how this will impact the budget. Many do not think about how adding a last-minute feature will impact the overall project budget, since they don't have a concept of how many hours it will take to develop, test, and deploy: that's what they come to us for.

#### Communications

Epic owners are in frequent communication with the client. They are expected to come up with a communications plan, as outlined in the [epic description](#epic-description-template). [Client owners](roles/list.md#client-owner) are often epic owners for their clients, but this is not always the case. Much like client owners are [a general purpose contact](processes/working_with_clients.md#client-owners-are-the-point-person) for all of a client's needs, epic owners are the general purpose contact for all needs related to an epic. If the client contacts the epic owner, the epic owner should either answer the client directly or make sure their request is properly routed and handled by the most appropriate team member.

[Specific best practices for communicating with the client](#client-communication) are found later in this document.

#### Prioritization

As the epic owner, you are expected to prioritize tasks to complete the project (or guide its continued development). This is usually done in tandem with the client as part of your communications plans, as the client's priorities may shift between sprints. Client owners also are responsible for guarding the sprint, and helping the client understand the agile process (especially in regard to the structure of sprints, and [how the process affects estimates and budgets](https://opencraft.com/how-agile-methodology-impacts-planning-and-budgets/)). This means preventing task injection and walking the client through the impact of any change in focus.

Epic owners create the tickets ahead of the ticket cutoff each sprint and give them initial assignment each sprint. They may create several tickets in the backlog to make this easier for them, or they may create them as they go. It is strongly recommended to create tickets ahead of time whenever possible. This allows team members to work ahead if tasks are completed early, and allows for easier selection of [trial projects](processes/recruitment.md#trial-project).

One other element of prioritization is important to note: Not only does the epic owner need to prioritize tasks, they need to communicate to the [epic planning and sustainability manager](roles/list.md#epic-planning-and-sustainability-manager) about how many hours a project is expected to spend per month to make sure that the epic is also prioritized internally.

### Project management for non-project managers

A lot of the work involved in epic ownership falls under the category of project management. We strongly recommend every epic owner read through Atlassian's excellent guide ["**Project management for non-project managers**: The steps, skills, and troubleshooting techniques you'll need to deliver the goods without losing your mind"](https://www.atlassian.com/project-management).

As described in that guide, there are some things that every epic should have in place. We've summarized them in [the checklist in the epic description template below](#epic-description-template).

If your project is a large one (>1000 hours), you should also read [Estimating Large Projects](how_to_do_estimates.md#estimating-large-projects). If your project has grown large over time, it's a good idea to schedule a discovery task to determine if some changes to communication, approach, or scope can help keep the project manageable.

### Tips and tricks

Here are some tips/tricks/strategies concerning different aspects of epic ownership that should be helpful to you going forward.

1. Be mindful about the number of epics you take on as an epic owner. If you're new to epic ownership, it's best to focus on a single epic; and even if you have some experience managing epics, it's better to keep the total number of epics that you own fairly small. Otherwise you'll need to do a lot of context-switching during the day/week/sprint, which tends to affect productivity and can even become frustrating at times. Also, if you own too many epics it can be hard to devote enough time to managing each one and being proactive where you should be. As a rule of thumb, **aim to own one or two epics at most**.

2. Keep your epic to a manageable size. If necessary, break a big client project up into smaller phases and pieces, with each phase/piece becoming a separate epic. See [Estimating Large Projects: Create manageable deliverables](how_to_do_estimates.md#create-manageable-deliverables) for more details.

3. When estimating for an epic, add estimates to onboard developers into the project, and after kicking off the epic, take some time to write what’s needed to get set up and working. Also consider the technologies you’re dealing with (React, Terraform, etc), and add some more onboarding time to cover that too. See [Estimating Large Projects: Consider your team](how_to_do_estimates.md#consider-your-team) for more details.

4. If the discovery just provided ballpark estimates, take a good chunk of time when the epic is kicking off to sort things out. If estimating in ballpark estimates, go for overestimation, and consider that the work in the epic will have to cover a full estimation of every requirement. See [capturing the technical requirements](#capturing-the-technical-requirements) for more details.

5. Prepare to delegate work: before delegating work to others, make sure the tickets in your epics have proper setup instructions and that the requirements are crystal clear, so that team members and newcomers can take on tasks. Otherwise you risk running into spillovers and going over budget, which can impact the original epic plan.

6. Plan to get newcomers involved *after* some core team members are onboarded, so that they can help the newcomers get up to speed quicker and use their time more efficiently. Also, if [newcomers end up with stuck tickets](#a-note-on-newcomers-with-stuck-tickets), then core team members are better equipped to recognize the issue and step in to help.

7. Don’t assume client expectations, set them clear early on: make sure the client knows exactly what they're getting, and if possible, make a clear section explaining what's not included in the work. Sometimes clients come from past assumptions that might lead to disappointment (e.g. some providers include ecommerce features in the "default" package, while we don't). See [Estimating large projects: Developing a shared understanding of requirements](how_to_do_estimates.md#developing-a-shared-understanding-of-requirements) for strategies.

8. If you don't have time to manage the project while doing dev/ops for it, stay involved in the day-to-day work by taking on task reviews, and at least some small tasks. If the project is large, split the operations and development work into separate epics, with one person owning the ops and reviewing the dev epic, and the other owning the dev and reviewing the ops epic. That way, you can stay informed about the whole project.

9. Choose a communication channel to communicate overall project strategy, process, or scope changes to the rest of the development team. For example, starting a forum thread for the project is a good way to do this, and also allows the team to communicate development or deployment process changes to each other.

### Epic Description Template

When creating an epic on Jira, here is a template you should use, in the same way as we have the [task description standard](./task_workflows.md#task-description-standard) for regular tasks.

```text
h2. Context

Explain the problem that the client is facing, and how this epic will help to solve it. Link to any related epics/discoveries/discussions.

h2. Scope

Explain what is in and out of scope for the epic. This may be fuzzy if the discovery hasn't been done yet.

h2. Roles

List the key people involved:

* OpenCraft team members who are involved in the project, and what their roles are. This way it will be clear when assigning tasks if more people can be onboarded or not, and if they need to be, the expected impact on the budget.
* All stakeholders on the client/project side, and their respectives roles within the project. Include anybody who has potential influence or impact on the project, such as higher management, external partners, funders, etc. Clearly indicate who are "primary stakeholders" or "decision-makers", i.e. people who have authority to approve changes to budget, scope, timeline, etc. Primary stakeholders should have their contact info added to the CRM if not done already (please check).

h2. Communication plan

Explain:

* Which communication channels are used for project and sprint planning/retro, support requests, and other client communication (emails/Jira/Zoom/other)?
* The frequency and schedule of planning/retro sessions (if any).
* Who is involved in task planning on the client side?

Make sure the client is aware of this communication plan, and update it as needed.

h2. Budget & Capacity Allocation

* Break down the budget for the project:
** How many hours do we have available?
** Is it a fixed-price project, or will we invoice on an hourly basis (time & materials)?
** Is the project split into phases/deliverables/milestones, each with their own budget?

* Explain how much time will be allocated to the project over what period of time, e.g. "we are planning to allocate x hours/points per sprint/month".

Make sure the client is aware of how much capacity we are planning to allocate, and update the plan as needed.

h2. Timeline

List key dates and deadlines. This should be formatted as a checklist that can be checked as each target is met. Example:

* 2020-09-01 Project kickoff (/)
* 2020-11-15 MVP ready for testing

Set the "Due Date" on this epic in Jira to the first incomplete key date / deadline.

h2. Epic Checklist

* If you are new to epic/client ownership, create a task in the client's epic to block out time to onboard to this process and plan your approach.
* Review the [Epic Management|https://handbook.opencraft.com/en/latest/epic_management/] section of our handbook for any useful updates relevant to your project.
* If you are newly taking on this client as a client owner, review the [Strategic Client Management Guide|https://docs.google.com/document/d/1bTgDk0NAj191hWNI-MSyM4Ihfa5DjlpDETLQddgyyro/].
* Created epic with this template (/)
* Define the problem you're trying to solve, and the value in solving it. Put a summary in "Context" above, and make sure everyone who will be working on the epic understands this.
* Agree with client on goals and a definition of success, in writing. (Make sure that what the client says they want is what they actually want, and what the decision makers in their org will approve!)
* Update the client's [CRM entry|https://crm.opencraft.com/] with a link to this epic and brief summary of the project.
* Roles and responsibilities – define each person's role on the project, and what's needed of them so the team can be successful. Put in to "Roles" above.
* If this project involves change to the Open edX platform that isn't trivial, make sure that it is added to the [public roadmap|https://github.com/orgs/openedx/projects/4] if it has not been already, and add a link to the entry on this epic for periodic update.
* Nail down the project's scope - note that this isn’t about creating detailed tickets/stories but drawing a fence around the project, to keep it focused.
* Understand and manage dependencies - what could jeopardize the schedule?
* Anticipate and mitigate risks - do a "pre-mortem" to brainstorm things that might go wrong.
* Prepare a budget
* Get the client to approve the key items in writing (definition of success, roles/responsibilities, dependencies, budget, timeline/deadlines).
* Build a backlog of tasks.
* Make a communications plan - including regular updates to the client and demos.
* Plan time to incorporate feedback from demos and other tests.
* Plan for a retrospective at the end of the epic.
* For the client epic, add {% raw %}{{client-epic}}{% endraw %} label to it. For such epics, client strategy tickets will be automatically scheduled on the regular basis. More on this [here|https://handbook.opencraft.com/en/latest/roles/list/#client-owner] and [here|https://forum.opencraft.com/t/client-strategy-tickets-scheduling-automation/1403].

```

### Epic Update Template

We expect [epic owners](roles/list.md#epic-owner) to post bi-weekly updates that describe the current status of their epics.

They should use the following template when compiling these updates:

```text
h1. Epic Update (Sprint NNN)

h2. Budgeting

h3. Current status

h4. What's the status of the hour budget for this epic?

We've used __ hours of the monthly/yearly/total __ hour budget.

h3. Past work {color:#c1c7d0}(billable projects only){color}

h4. How many non-billable hours did we log last month?

{color:#c1c7d0}
Specify the number of hours by which we exceeded the existing budget(s) for this epic.
If this epic combines tickets from multiple accounts, list them separately and specify the number of non-billable hours logged against each one of them.
For any account whose budget we did not exceed, simply put 0h.
{color}

(specify)

h4. How many of them might _become_ billable (i.e., how many of them might the client agree to pay for retroactively)?

(specify)

h3. Remaining work

h4. Will the remaining budget cover the remaining work?

* Yes
* No

h4. How much additional budget would be required to complete the remaining work?

{color:#c1c7d0}
Provide an estimate for the number of hours we would need to add to the existing budget(s) for this epic to cover the remaining work.
If you expect that the remaining budget(s) will cover the remaining work, simply put 0h.
{color}

(specify)

h4. What adjustments are you planning to make to align the remaining budget with the remaining work?

* *Scope reduction*: Adjust the volume of work for the coming month(s) to make up for increased budget usage over the past month(s).
* *One-time budget extension*: Ask the client if they would be willing to provide *NNN hours* of additional budget for the remaining work.
* *Permanent budget extension*: The volume of work requested by the client each month is generally increasing, so I'm planning to ask for a permanent increase of the monthly budget.
* *N/A*: I expect that the remaining budget will cover the remaining work.

h4. Has the client approved these adjustments?

{color:#c1c7d0}For internal projects, Xavier is the client.{color}

* Yes
* No
* I have yet to start a conversation with them.
* The conversation with them is still in progress.
* N/A (no adjustments needed)

h2. Capacity planning

h3. Remaining work

h4. What needs to happen to make sure we can complete the remaining work before the deadline?

* We'll need to allocate more capacity and get more work done each sprint.
* We'll need to talk to the client about setting a new deadline.
* We'll need to talk to the client about adjusting the current scope of the project.
* Some combination of these options. (Specify which ones.)
* N/A: We're on track to complete the remaining work before the deadline.

h4. Has the client approved these adjustments to the scope and/or timeline of this project?

{color:#c1c7d0}For internal projects, Xavier is the client.{color}

* Yes
* No
* I have yet to start a conversation with them.
* The conversation with them is still in progress.
* N/A (no adjustments needed)

h4. How many hours per month should we allocate to this epic starting next month?

(specify)

h2. Epic management

h3. Are the deadlines in the "Timeline" section of the epic description correct?

(/) or (x)

h3. Are relevant details mentioned in the remaining sections of the epic description up-to-date?

(/) or (x)

h3. What's going on this sprint?

Take some time to:

h4. Summarize the work that is currently in progress.

...

h4. List important points and/or decisions from meetings and asynchronous conversations that happened since the previous update. Anything affecting scope, budget, timeline, and general direction of this epic is worth mentioning.

* ...
* ...
* ...

h3. What's going to be worked on next?

(describe)

h2. Sprint management

h3. Is the status of each story in the epic correct?

(/) or (x)

h3. Are all upcoming stories that we should work on in the next week or two created and in the correct upcoming sprint?

(/) or (x)

h3. If someone is looking for additional work, they're welcome to pick up the following tickets:

{color:#c1c7d0}
If you think it would make sense for this epic, consider going to https://tasks.opencraft.com/issues/?jql= to create an appropriate filter listing relevant tickets, save it, make sure it's public, and link to it here.

Example query: "Epic Link" = <XY-1234> AND assignee in (EMPTY) AND status not in (Archived, "Deployed & Delivered", Done) AND Sprint in futureSprints()
{color}

(list tickets or insert link to appropriate filter)

h2. Other Notes (optional)

{color:#c1c7d0}
Use this section to list any additional concerns / questions / information that you think is worth mentioning because they could affect planning of the next sprint or two (e.g. upcoming vacations).
{color}

...
```

### Discovery phase

Below is a list of points to be aware of when performing a [discovery] for a new project/client.

* Before writing the [discovery], get clarity on the level of detail that is required. If the ticket description doesn't say, ask the person that created the ticket whether you should do a high-level discovery with [ballpark estimates], or a more detailed discovery with stories and detailed estimates. Make sure to read the entire documentation on [how to do estimates] at least once, and reference relevant parts of it while doing the discovery.
* Most discoveries are [timeboxed], so plan your time carefully against the requirements of the discovery. If you don't have enough time to go into detail on a particular part of the estimation, either designate it as something that will require further investigation, or give a quick ballpark estimation range.
* Think about whether or not it would make sense to set up a weekly stand-up meeting with the client for the duration of the project. This is especially important if multiple teams are involved in the project.
   * The purpose of that meeting would be to provide status updates to the client, get status updates from other teams that are involved in the project, address any open questions, and make sure all parties are unblocked and clear on the next steps.
   * It should be enough to allocate 30-45m per meeting.
   * It's important to determine whether this is something that the client would want prior to the implementation phase because we will need to account for it in the [discovery] document and quote that we send to the client. A budget of 1-2h per week for meetings should work well in most cases. (Note that it can't just be 30m because you'll also need a little bit of time to prepare for/follow up on each meeting.)
   * Don't schedule the weekly meeting (or any other meeting that you might need to set up over the course of the project) for right before your EOD. To make sure you'll have time to list out (or directly follow-up on) any action items and add your notes from the meeting to existing tickets as necessary, the meeting should end no later than 30m to 1h before the time you normally stop working.
* Discuss the hours requirement for the project with the cell's [epic planning manager] and Braden and/or Xavier, so they can determine whether we have capacity within the team for the project, or need to hire to accommodate.
* Note that the number of hours that OpenCraft can allocate to the project per month might differ month-to-month. If that's the case you'll need to keep it in mind when planning out different phases of the project, and when scheduling stories for specific sprints.
* If the project has a deadline built-in (such as the start of a course that is supposed to be using a set of features to develop as part of the project), the number of hours that OpenCraft can allocate to the project will affect the scope of the project. (More specifically, it will be an upper bound for the amount of work that we can schedule and complete prior to the deadline.)
* If the project does not have a deadline built-in, the number of hours that OpenCraft can allocate to the project will affect the duration of the project.
* Leave time for a QA phase after delivering the features laid out in the [discovery] document. This is especially important for projects that are about developing features that a client wants to use in an upcoming course. Ideally, you should allow 2-3 sprints for QA for projects with specific deadlines, but 1 sprint is the absolute minimum. If you only have 1 sprint for QA, make sure that the client delivers their feedback within the first half of the first week of that sprint. Otherwise it will be very tricky to address their feedback, deploy changes, and give the client enough time for another round of testing before the final deadline (which might coincide with the start of a course that depends on the features developed over the course of the project).
* If your epic has any dependencies on edX (product approval of the plan, PR reviews, etc.) then leave lots of time for those to happen and have a contingency plan for what to do if edX delays things too long. Also, try to get specific people from edX to be aware of the project and its timeline, and to commit to being available for product and technical reviews.
* Keep in mind that OpenCraft has a [policy to upstream as much work as possible]. That aspect can have a major impact on the budget and timeline of a project and should therefore be taken into account right from the start. In some cases it might be necessary to reduce the scope of the changes to include in an epic, to make sure that the client's budget will also cover the costs for upstreaming those changes.

#### Capturing the technical requirements

When capturing techinical requirements we want to establish a shared understanding between the client and the development team. There must be written acceptance criteria, and the client's primary contact / product owner should review them before work begins on the feature, either on the discovery document or on the task description itself.

To achieve a shared understanding between parties we follow the [Behavior-driven development principles](https://en.wikipedia.org/wiki/Behavior-driven_development). Behavioral specifications describe the various actions users can perform in the software and the expected response of the software. Relying solely on UX designs alone is not sufficient as specifications - because too often these do not capture the error paths, or can hide an assumed complexity of features.

When writing these specs out they should be expected to be used for 3 purposes:

1. Help dev and product agree on how the system should behave from the point of view of the user.
1. The end to end tests should map directly to these specs. They don’t need to cover all the possible situations but should cover the common ones.
1. The specs should also be used during manual testing and verification by code reviewers and QA.

What should they include?

* Each spec should be written in the form: "When the user \<action\>, \<response\>”
* The specs should include the common happy paths.
* They should also include paths which result in errors.

##### Examples of behavioral specs

**[LX-1764](https://tasks.opencraft.com/browse/LX-1764)[^1] UI update: show only those tabs on the content picker modal that contain content visible to the user**

When a user views the content picker:

  * when
    * editing a pathway or composite asset
    * adding assets to a classroom
    * assigning assets to a mentee
  * if the user
    * is an org member, the "Org. library" tab is visible.
    * is not an org member, the "Org. library" tab is not visible.
    * has marked assets as favorites, the "Favorites" tab is visible.
    * has not marked assets as favorites, the "Favorites" tab is not visible.
    * has created assets, the "My content" tab is visible.
    * has not created assets, the "My content" tab is not visible.

**[LX-1176](https://tasks.opencraft.com/browse/LX-1776)[^1] VideoJS: Add Vimeo support**

When a user wants to add a Vimeo video as an asset:

  * they click "Add new content" on the dashboard and then select Video a modal shows up.
  * the modal has the Youtube logo, "To add a YouTube video.." text, the url field and "Import Video" button
  * the user pastes the url in the field of a Vimeo video and clicks the "Import Video" button. If:
    * the URL is of a public embeddable Vimeo video:
      * they are redirected to the video asset create page.
      * the video preview is visible on the asset create page.
      * The user can add a transcript to the video.
    * the URL has the domain vimeo.com but is not embeddable:
      * an error is shown below the text field: "Please enter a valid video URL".

When a user views a Vimeo video added:

  * the Vimeo video is embedded by the videosjs player.
  * the transcript is shown using the standard transcript view.
  * if the Vimeo video is not embeddable (e.g. because it has been deleted or embedding has been disabled) the player will show an error: "Video cannot be embedded."

### Implementation phase

Once the discovery and estimates are accepted by the client, the project enters the implementation phase.

* To help everyone (including yourself) get a sense of where a project is at, add a timeline to the epic description.
    * In the beginning stages of a project, the timeline can be fairly high-level. For example, it could just list the main phases of the project:
        * Jan 2019: Discovery phase
        * Feb 2019 -- April 2019: Implementation phase
        * May 2019: Deployment phase
        * June 2019: QA
        * etc.

* Note that for future reference it's helpful to mention all phases of the project, even ones that are already in the past at the time you create the timeline.
* At any given stage of the project, the timeline for the next two sprints should be laid out in detail: Add the name of each sprint to the project phase that it belongs to, mentioning when it ends, and list the stories that are scheduled for it. Once all of the stories belonging to a given sprint are done, put a checkmark next to the name of the sprint. A good point in time to make these updates is towards the end of a sprint, when working on the epic update.

    This format provides a concise overview of the project's status to anyone that might be looking at your epic.

* If the scope/plan for the project changes significantly, try to keep the epic description field in Jira updated, so that it always has a useful and accurate explanation of the project, plan, timeline, etc.
* Look at epic updates as a service to yourself, not just to the person who is supposed to eventually read it, whoever that may be. It allows you a dedicated time to consolidate what is going on with the epic, to do any higher-level planning work, and to ask epic-level questions to your peers in the context of the update. It's one of the duties that at first may look to be a chore you just have to do as epic owner, but you and your epic can benefit tremendously from it just through a change in perspective. Spend the next epic update trying to really drill a little deeper, and you'd be surprised how much more of a handle you feel you have on it.
* If you are planning to go on vacation at some point over the course of the project, make sure to coordinate with your epic reviewer a few weeks ahead of time. If the project includes weekly stand-ups, your epic reviewer will need to be able to fill in for you at these meetings. This means that they will need to be familiar with the structure of the meetings, and that while you're away the meetings will need to happen at a time that is not in the middle of the night for them. So if necessary, make sure to move the meeting to a different time before you go on vacation. Let the client know when you will be away, and who will be filling in for you. Also, have the epic reviewer attend the last one or two meetings before you leave so they can meet the client and see first-hand what the structure of the meetings is like.

### Day to day

At the start of your day, get an overview of all updates related to your epic. (You can log time spent on this on the epic itself.)

* Create tasks for any urgent fires that might have come up and coordinate with the rest of the team to make sure they can be addressed right away.

   * In most cases your first point of contact should be the epic reviewer and/or other team members that already have context on the project. If they don't have time to address the fire, the next step will be to involve the sprint firefighters to see if they can help free up some time for the epic reviewer and/or other team members that already have context on the project and should ideally be working on the fire.
    * Don't forget to notify the client that we are looking into whatever issue they reported.
* Determine priority for the remaining updates, relate them to existing tickets, and decide when to work on them.

At the end of this process you should be fully up-to-speed on the project's status, and it should be clear to you what aspects of the project (tasks, reviews, client communication) you'll need to work on that day. It should also help minimize interruptions throughout the rest of the day, allowing you to focus on completing the work that's on your agenda for the day.

### Communicating Changes to the Group

As an epic owner, changes under your tasks may result in new features becoming available to the platform, or in new
projects that may be of interest to the team at large. On previous occasions, discoveries have been in full swing only
to find out another cell recently implemented the target feature.

To avoid such missteps, we have a two-step process for disseminating knowledge of new features. The first is that we
use the [ADR process](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions), adding ADRs to
repositories as decisions are made and explaining their reasoning.

When a new project is started or a new feature lands, go to the
[New Feature Announcement Thread](https://forum.opencraft.com/t/new-features-and-projects/852) and post a summary of
the new feature or project, with a link to the relevant ADR, in a post. You're encouraged to include a video
demonstrating the new feature if possible!

### Client communication

Guidelines to help communication with clients:

* When a client reaches out to you (via e-mail, Trello, JIRA, etc.), don't feel compelled to reply right away. Instead, take a moment to determine
    * how important their request is relative to other work from the project that is currently in flight,
    * whether the current sprint includes a task that the request is related to, and
    * what the best way of addressing the request would be.
* We have a [policy to respond to client requests within 24h], so unless you're dealing with something that needs to be addressed right away (such as a client's instance being down), it's perfectly fine to take some time to figure out the best way forward.
* Make sure to thank the client for any feedback that you get from them, even if it's negative. If necessary, apologize and admit mistakes, but mainly focus on finding a solution, and involve the client in the process of coming up with a solution as much as possible.
* Tailor the level of detail to your client and their interests. For example, if you're speaking to an operations specialist, it's ok to go into technical details about a change or question. But if you're speaking to a site manager, they may only want to know the benefits and risks of a change, not the technical details.
* If you need to receive sensitive information from a client (e.g. a SSO client ID and secret), ask them to use [onetimesecret.com](https://onetimesecret.com), and to send each part of the secure information using separate OTS links. Reference our [security policy] for more details.

### Handling Changes in Requirements

A common problem that Epic Owners may face is significant changes in requirements. This can happen due to pressures from inside our client's organization, or it can happen after testing the project in progress and realizing that the direction needs to change. When this happens, remember the mantra:

> No changes without budget implications

Projects begin with a discovery where we build estimates for the client. While every project deviates somewhat from requirements, most still follow the general shape and size of the estimates built in discovery. If the client has approved a budget for an epic, but the requirements change mid-implementation, it is important to formally re-evaluate the estimates. Schedule a discovery for the new requirements and check if they still fit within the client's approved budget. If they do not, the client will need to approve additional budget, or else shave down the requirements until they fit.

**Note:** This discovery must necessarily also consume budget, so inform them of this fact and confirm that they want to proceed with re-estimation instead of continuing their original plan.

Smaller changes to requirements may seem minor, but they can add up to large deviations from the budget over time. When a client asks for a change to a task that will put that task over budget, the time to raise the issue is during that task, not later when other tasks may be affected by the loss. Make sure the client knows the impact of every decision they make on costs.

There is another way in which requirements may 'change.' Sometimes, you may discover that requirements were misunderstood. While it is important to avoid this by being thorough and asking questions during the discovery phase, it is still nevertheless possible that we discover that our understanding of the requirements and our client's understanding have diverged, or that a discussion with the client reveals a hidden requirement. The moment this is detected, it is important to determine its impact to the budget. If you cannot do so at that moment, schedule a task to evaluate the impact. Some of the largest budget overruns happen when an unstated requirement surfaces mid-project, so this must be addressed immediately.

### Handling 'Obvious' Issues

At times you may run into issues within a project that seem obvious-- for example, you might notice that there isn't a cohesive vision for part of the work, or that your PRs are not getting reviewed as quickly as you need them to in order for the project to meet deadlines. It is important to remember that while these issues can be obvious to you, they may not be obvious to the client.

For example, the client may be focused elsewhere and the issue has escaped their notice. Or, the client may know about the issues, but is just as paralyzed as you are on what to do with them, hoping you'll intervene. In rare instances, you may have a problem client which is setting us up to fail as part of a larger political move within their organization (yes, this has happened). For this reason, make sure that you bring up any significant issues you see explicitly, even if they seem obvious to you. As the epic owner, you are responsible for the delivery of the project, and thus are responsible for identifying any obstacles that are in the way.

Bring potential solutions with you as you identify the issue. PRs not getting reviewed fast enough? You might request:

* A dedicated resource to focus on reviews for a week or two
* An adjustment in deadlines if the reviewer can't be freed
* Advice on how to better prepare the PRs for more expedient review

Discovered that a project's requirements have turned out far more nebulous than they need to be? You might suggest:

* Scheduling a meeting to clear up the functionality and expectations of a component
* Recruiting one of the Product team members to draft a spec or some wireframes

Always be sure to not only bring up these issues (and potential solutions) in meetings, but also write them into emails. This helps ensure that the problems do not get lost, and in the rare occasion that we need it, provides documentation down the line of our attempts to remedy any issues. A problem well stated is half solved.

### Handling Slipping Deadlines

As the Epic Owner, it is important to guard deadlines and ensure projects stay within them. We do everything in our power to meet our promises on timelines. Be on the lookout for blockers you can clear for developers working on your projects, look for stumbling blocks down the line and clear them, and make sure to proactively request resources from the client ahead of when you need them (such as making sure reviewers are available when they will be needed.)

Even so, you may run into an issue which sets the project back later than expected. Like budgets, Epic Owners are responsible for guarding deadlines. If there is an issue outside our control that is affecting deadlines (such as information not being given in a timely fashion, or changes to upstream libraries) it is important to bring this up with the client as soon as it is noticed. Make sure the clients understand the impact to deadlines as soon as you see them. [The impact may be obvious to you, but it may not be obvious to them](#handling-obvious-issues). As with other problems you bring up, offer solutions: Request resources be available, suggest reordering priorities to make sure that something is released even if it's not everything originally planned, or forecast a revised deadline that the client can communicate to their team.


### Handling stuck tickets

Occasionally when supervising an epic, there will be a ticket that risks jeopardizing the epic. For whatever reason, the
task is taking much longer than it should. This might be due to unforseen complexity, an assignee's lack of experience
with the tools, Heisenbugs, or lack of aptitude for the task. Whatever the reason, as the epic owner, you must guard your
budget and find a way to keep the task from sinking the epic.

The best approach is to take preventative measures. Keep close watch on tasks from your current epic in the sprint.
Encourage your assignees to keep you up to date and to share code early. Be especially vigilant if the assignee is a
newcomer. They sometimes get stuck and go quiet for fear that they may be disqualified for asking for help, or from
being overwhelmed by the task. Proactive measures are always preferred over other options.

If it's clear that the current assignee isn't going to complete the ticket in a reasonable amount of time, consider if
there's someone else who might be able to handle the task better. Check with the ticket reviewer and firefighters to see
if there may be another approach. You may reassign a ticket if it will guard the budget. However when doing so, be sure
to investigate what went wrong and talk with the assignee about it. Assignees are responsible for the work they take on.
You may wish to talk with Xavier about the incident as it may require some additional training to prevent from happening
again.

If the ticket appears to be a timesink regardless of who would take it, consider postponing the ticket until more
information can be gathered. You may also talk with the client to verify/change the priority of the ticket. They might
decide that the task being worked on isn't quite as important, if it is risking the whole project. Note that postponing
should NOT be done in the case that upstreaming a change is taking longer than we'd like, and it should not be done if
there is another way we can find to complete the task. Failure to complete a task can cause strain on our relationships
with a client. If you take this route, be sure to detail why and what happened in your next epic update.

#### A note on newcomers with stuck tickets

Sometimes, the stuck ticket may be assigned to a newcomer and require reassignment. This requires careful consideration.
Is the reason the ticket is stuck because of unexpected complexity, or other factors beyond the newcomer's control?
If it is, you will want to communicate this to the newcomer and work with their mentor to find a new task for them to
work on. After they are reassigned, perform a [root cause analysis](processes/root_cause_analysis.md) on how the ticket got assigned to a newcomer when it
should not have been, include this in your next epic update for review.

However, if the work done by the newcomer is not up to standards, and a new assignee will have to redo it:

1. Recreate the ticket and reassign it.
2. Set the account of the old ticket to the newcomer's onboarding epic and the account to the recruitment account.
3. Contact Xavier as their contract will need to be terminated.
4. Send an email to dev@ detailing what happened, in response to the newcomer's review thread, and notify
   the newcomer's mentor.

**Note:** If the newcomer's work is still useful and the ticket is reassigned for other reasons than aptitude issues,
you will need to keep the hours spent as part of the task, and work to complete it in the remaining budget.
For this reason (and others), it is important to keep on top of communication for tickets assigned to newcomers.

### Handoffs

Sometimes, an epic needs to change hands, or your point of contact within the client's organization may change. As the epic owner, you are tasked with ensuring continuity. If you're taking over the epic from someone else, this means scheduling any knowledge transfer work required. If you're dealing with a changing contact point, schedule a meeting to brief them on the project, the current status of items, key players, and anything else your contact may need to make effective decisions.

If you are handing off an epic, work with your successor to make sure all information is handed off as needed. Even if you do not anticipate handing off your epic any time soon, the epic description and epic updates exist partially to assist anyone (especially your [epic reviewer](roles/list.md#epic-reviewer)) in picking up where you left off in the case of unexpected absense. It is thus especially important to keep good updates and maintain your epic descriptions.

### Retrospective

The end of a project or client relationship is an opportunity to reflect on what went well, and what can be improved.

As part of completing your epic, be sure to schedule a task to write up your experiences and observations. Use the [epic retrospective template] to create a document for your epic, and link to it from your client's CRM entry.

[^1]: Private for OpenCraft Employees

[epic]: glossary.md#epic
[discovery]: glossary.md#discovery
[epic owner]: roles/list.md#epic-owner
[epic planning manager]: roles/list.md#epic-planning-and-sustainability-manager
[ballpark estimates]: how_to_do_estimates.md#how-to-do-a-ballpark-estimate
[how to do estimates]: how_to_do_estimates.md
[timeboxed]: glossary.md#timebox
[security policy]: security_policy.md#sending-sensitive-information-to-clients
[policy to respond to client requests within 24h]: roles/list.md#communication
[policy to upstream as much work as possible]: sustainability_policy.md#objectives
[epic retrospective template]: https://docs.google.com/document/d/1vXepOMHvpr6G5032dp1HuHZT7pviMX-4hR9GONiVlns/edit?usp=sharing
