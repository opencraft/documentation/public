# Roles and Expectations

One other important way that we do differently than a lot of companies is the way we assign
responsibility. Instead of titles, which are linked to the concept of hierarchies and ranking,
we use [roles](#roles), and all people in the organization share a
[set of expectations](#what-is-expected-from-everyone).

## What is expected from everyone

The general expectations for anyone working at OpenCraft:

* I can work from wherever I want, provided I have access to a reliable high-bandwidth internet
  connection good enough for video calls.
* I can set my own work schedule, and work when I want during the week, as long as **I remain
  available to answer to others** (please see: the **Communication** section below)
* (In general) **I am *not* expected to work weekends** (unless I'm behind on my commitments),
  which should remain an exception, not the rule! If you find yourself forced to work on week ends
  more than once per month, that likely reflects an issue with your time management that needs
  fixing. If I work week ends as a personal choice, I will not expect other team members to also
  work week ends.
* I will make sure my cell's responsibilities are continuously being properly
  taken care of, by reviewing their status at least once per week.
* I will work at least the amount of hours per week that is specified in my contract (e.g. 40
  hours/week), averaged over each month, excluding scheduled holiday time.
* I will ensure that clients and people on the cell that I'll be working with (e.g. code reviews)
  know my availability. (e.g. If I'm taking Friday off, I'll make sure people who need me
  to do code review know that).
* When taking time off I will follow the procedure described on the [vacation section](../processes/vacation.md).
* If I'm unexpectedly sick or unavailable due to an emergency, I'll make every effort to notify
  my cell and ask the [sprint firefighters](#firefighting) to take over my duties.
* I will provide my own hardware (laptop).
* I will keep my computer(s) secure and up to date as described in the
  [security policy](../security_policy.md).
* I will record all hours that I spend on a task using the Tempo timesheets tool in JIRA
  (including things like setting up devstacks and time spent learning, which we all need to do
  and is an important part of the task, especially at the beginning, or when joining a project).
* If I use another tool for tracking my time,
  I will update the JIRA Tempo timesheets within 24 hours (excluding weekend days).
* I will attend the Open edX Conference every year, unless exceptional and important personal
  circumstances prevent me from being present (Note that team members who joined OpenCraft before
  July 31st 2018 are [only encouraged to attend the conference, but not strictly required to](https://forum.opencraft.com/t/open-edx-conference-attendance/134/20)[^1]).
* As a conference attendee, I will submit a talk proposal and present it. If my talk isn't
  accepted, I will co-present a talk with someone else from OpenCraft.

### Communication

* **If any client email is addressed to me, I will respond to it within 24 hours (excluding weekend
  days), or ensure that someone else on my cell does.**
    * I will CC or forward to help@opencraft.com all client emails and replies, so that others can take over the thread
      with that client if needed, or refer to it for context.
    * If I can't answer immediately, I will at least send a quick reply to let them know we're
      working on a response, and when they should expect it.
* **I will reply to pings/emails from other OpenCraft team members within 24 hours**
  (again excluding weekend days and scheduled time off).
    * But this should be a "worst case" scenario - completing the work on time
      is still the primary goal, so when someone is blocked and pings me for help,
      I will try to do as much as I can to unblock them quickly,
      rather than starting a 24h ping-pong cycle that takes up all the time
      in the sprint without accomplishing any work.
* **I will respond to pings on GitHub or GitLab within 24 hours**
  (with the usual exclusion for weekend days and scheduled time off).
    * If a ping has no corresponding ticket, or the ticket is not scheduled for
      the current sprint, I will respond to the ping with an estimate for when
      they can expect a full response.
* I will read and respond to forum threads:
    * On the [announcements forum](https://forum.opencraft.com/c/announcements/7)[^1]: within 24h, excluding week-ends & time off;
    * On the rest of the forums, within 2 weeks, except for the [off topic forum](https://forum.opencraft.com/c/off-topic/8)[^1], which doesn't need to be read or replied to at all.
* I will be professional and polite when communicating with clients.
* I will prefer asynchronous over synchronous processes (keep meetings to a minimum). A chat
  conversation is a form of a meeting.
    * Generally, discussions should happen first asynchronously on the JIRA tickets;
      if there is really something that can't be efficiently sorted out asynchronously,
      have a chat or schedule a meeting. JIRA might have long response cycles (around
      1 day turnaround). If this time isn't enough to unblock someone and
      finish their sprint commitments, use Mattermost, even though
      it's more disruptive to people's workflow.
    * If you do have a synchronous conversation with someone about a particular task,
      post a summary of the result/decision from that conversation on the JIRA ticket
      for easier future reference.
    * When scheduling meetings, give them a precise agenda. For people using Calendly,
      like Braden and Xavier, book meetings there, as it allows us to avoid
      the scheduling overhead.
    * Try, as much as possible, to use a similar approach with clients - don't
      let them invade your days with meetings. Calendly is good for this too,
      as it allows to define time slots where you'll have the meetings,
      to minimize the disruption they cause to your day and productivity.
      If you want a calendly account, let Xavier know and he will set
      you up with the OpenCraft account.
* I will post on public channels on Mattermost rather than private 1-to-1 channels
  whenever possible, even if the message is just for one person.
  This allows us to know what others are working on, and helps to replace
  the overheard discussions in physical offices - it can also be an occasion
  for someone else with knowledge about your issue to get the context,
  and to intervene if it is useful to the conversation.
* I will make sure I communicate with my reviewer(s) on tasks about availability
  and timezone overlap if I didn't have knowledge about it before. I will use the
  [contact sheet](https://docs.google.com/spreadsheets/d/107dR9H1vWjLpJlXPuBaFJIFaEPEmaSh50xLow_aEGVw/edit)
  where necessary.

### Sprint tasks

* I will follow the process and expectations outlined in the [pull request process](../processes/pull_request.md)
  section.
* I will never add my code (even DevOps code!) to a production branch directly - I will always
  create a Pull Request.
* I will always ensure my Pull Requests are reviewed by another OpenCraft team member **before** merging,
  except if I am a *core team member* and I'm merging *trivial changes*.
  In this exceptional case, I may merge the *trivial changes* before receiving a review,
  but I will then ensure all of those *trivial changes* are reviewed and acknowledged post-merge or
  post-deployment by another OpenCraft team member. Trivial changes include:
    * Small Open edX environment tweaks ([example](https://user-images.githubusercontent.com/514483/34581710-c167d10c-f191-11e7-9a66-7fa8c59def82.png))
    * Minor/bugfix package version bumps ([example](https://github.com/open-craft/ansible-rabbitmq/pull/4/files))
* I will only commit to work each sprint that I believe I can comfortably complete within the
  allotted sprint time (two weeks). Here, "complete" means "get to external review or get merged, deployed, and delivered." (See [Task statuses](../task_workflows.md#task-statuses)).
* I will get all of my tasks ready for internal review (by someone else on the OpenCraft team) by the
  **end of Wednesday in the second week of the sprint** at the latest. This will ensure that
  there is time for the review to take place, and for me to address the review comments
  and get the internal reviewer's approval in time.  If a ticket potentially requires multiple
  review cycles, get it into review as early as possible.  Schedule reviews with your reviewer
  to make sure they have time when you get your work ready for review.
* I will spend time to plan each task at the beginning of each sprint, including scheduling the
  review with the reviewer. Make sure you have an answer for:
    1. When do each step of the task need to be completed?
    1. Which days will you work on which task?
    1. When do individual parts need to be completed to be on time?
* It's also important to keep some margin on the sprint, in case something doesn't go as expected.
* I will get my tasks to at least **External Review** (or "Deployed & Delivered"
  if no external review is required) by **one hour before the next sprint**. As
  this is also dependent on the internal reviewer, I'll make sure she/he will be
  available around the time I finish.
* If it is looking like I will have trouble meeting one of these deadlines,
  I will inform the [sprint firefighters](#firefighting) (and [epic owner](#epic-owner)) as early as possible,
  so they can get me additional help, start planning for contingencies,
  and/or warn the client.
* If necessary, I will work overtime to complete my tasks before the end of the sprint.
* I will prioritise stories that spilled over from the previous sprint.
* I will "work from the right", prioritizing responding to external reviews
  as the highest priority, then doing reviews / responding to questions
  from others on the team, and finally working on implementing my own "In Progress" tasks.
* I will be helpful to team members, responding to questions,
  helping with debugging, providing advice, or even taking over tasks
  if I have time and they are overloaded. This has precedence over starting
  to work on new tasks, when finishing a sprint early.
* Once a sprint, I will review all of my tasks that are in "Long External Review/Blocked"
  and if needed, I will ping the external reviewer/blocker or pull the task into
  an upcoming sprint.
* I understand that if a task has a timebox/specific budget, it is my responsibility
  to stop working on it *before* the budget runs out, and to let the owner of the parent epic know
  that a budget extension will be needed to complete the remaining work.
  So before spending significant time working on a task I will check it for information
  about potential budget limits and reach out to the epic owner for clarification if necessary.

## Roles

Roles can be taken on by willing people, and people will usually take on multiple
roles, changing over time, based on interest and availability. This allows for a much smoother
progression of responsibilities than the ladder climbing game.

Note that responsibility is also uncorrelated with compensation and raises, which are given the
whole core team at once, based on time spent working at the company and overall financial results.
Compensation isn't a good source of motivation beyond a certain level, and this approach removes
a lot of politics.

If there is any role you would like to take up, the best way is to say it publicly - for example
in the forum. Then when opportunities arise others will remember it, and point you to tickets
you can own. Keep in mind that role assignments are intended to be permanent, so keep that in
mind before picking a role. Ideally you should work in a role for at least one year before you
consider swapping/dropping it.

### Changing Roles

Once someone takes up a role in a call there generally no need to reassign it unless someone leaves
OpenCraft. While an appointment to a role is generally permanent, no one should feel stuck in a
role if they are no longer comfortable in it. As such it is desirable to openly discuss with other
members of the cell on the forum.

It is the responsibility of the current person with a role to find a replacement in their cell and
help their replacement during a transition period while they are still getting comfortable with
their new responsibilities.

Note that for roles like Client Owner or Technical Security Manager where the role involves a fair bit of
specialised knowledge or context, great care should be taken to find a suitable replacement with
a similar level of knowledge and context.

### Types of OpenCraft members

There are three types of members at OpenCraft:

1. *Core team member* - the recruits who have passed their trial project.
2. *[Newcomers](#newcomer)* - the new recruits who are in their onboarding period (8 Weeks).
   Once the newcomers complete their onboarding period, they start to
   handle additional responsibilities by being on a [Weekly Rotation
  Schedule] where they often have to take on some additional roles, including [Sprint
  Firefighter](#firefighting) and [Discovery Duty](#discovery-duty-assignee).
3. *Short-term members* (temporary contractors), who have been hired for a specific task, scope
      or period of time. They are the most external members of the team.

However, in all other regards, all types of developers are put to the same expectations -- no
politics or special treatment between short-term developers, newcomers, and core team members.

The only acceptable exception is when providing extra mentoring on tasks for newcomers (or anyone
known to not have much context in the task), which is expected and useful.

### Trial Project Candidate

Trial Project candidates are considered temporary short-term members of the team with their scope limited to their trial projects.

As a trial project candidate:

* I will review the ticket assigned to me and get necessary clarifications by commenting on the ticket itself.
* I will proactively reach out to my mentor throughout the trial project.
* I will follow the [communication guidelines](#communication) through the period of the trial project.
* I will follow the [time logging best practices](../time_management.md#time-logging-best-practices) when logging time on the ticket.
* When I start working on the ticket, I will comment on the GitHub/GitLab issue to indicate it is being worked on, to avoid any duplication of work.
* When I start working on the solution, I will create a PR marked as draft, rather than waiting to create a PR until I have a working solution.
* I will link the PR to the JIRA ticket as soon as it is created and ping the reviewer to get early feedback on my work.
* I will inform the Reviewer and the Reporter of the ticket, if I feel the effort and the time allotted for the ticket are grossly under or overestimated.
* I will use the upstream tools like GitHub issues, [Open edX Forum discussions](https://discuss.openedx.org/) to discuss the issue with other community members where necessary.
* I will follow the [conventional commits](https://open-edx-proposals.readthedocs.io/en/latest/best-practices/oep-0051-bp-conventional-commits.html#specification) format when writing my commit messages.

### Recruitment manager

The recruitment manager role is responsible for organizing the selection of candidate hires
to match the needs of the [epic planning spreadsheet](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit) for the cell, as determined by the
[epic planning manager](#epic-planning-and-sustainability-manager). This role is intended
as a support role that would provide recruitment work for all the other cells. This includes:

* Selection of newcomers:
    * [Pre-selection of candidates](../processes/recruitment.md#pre-selection-of-candidates-for-interviews) for interviews among the received candidatures;
    * Interviews (scripted, recorded for asynchronous review);
    * Selection of newcomers among the interviewed candidates (with review & confirmation by CEO);
    * Correspondance with candidates (positive & negative answers), except contract negotiation handled by the CEO;
* Onboarding and offboarding of newcomers, including:
    * Starting the onboarding process;
    * Creation of their accounts on the OpenCraft tools & granting rights;
    * Creation of their onboarding epic and tasks;
    * Ensure the newcomers have good conditions to work: ahead of a newcomer start date and before assigning them to a cell, ensure the cell has a mentor assigned and can guarantee a good line up of tasks.
    * And after, ensure the trial goes well, implement changes like the trial projects, etc.
    * The role would be responsible, overall, for increasing the number of newcomers who become core team members
* Organization of the review and confirmation of core developers:
    * Ensure that all core members of the cell participate in the review, as well as one core member from each other cell;
    * Compile the feedback from individual reviews in a consolidated list, without including the names of individual reviewers and pass it on to the mentor.
* Regularly review recruitment trial results for interview selection accuracy.
* Every month, consult with the team's [Epic Planning and Sustainability Managers](#epic-planning-and-sustainability-manager) and decide the pace of hiring.
    * This can be done async, e.g. by commenting on the epic planning and sustainability epic of each cell.
    * Alternatively, if a cell holds synchronous meetings to discuss epic planning, the recruitment manager can decide to join these meetings once per month.

We will have a single backup for the recruitment manager (in cases of unavailability).

### Cell management

The coordination on each of the cell management roles listed below is to be assigned
to an individual member of the cell as a recurring epic. However, the way that each role
is handled beyond what is described in this handbook is the responsibility of the cell as a whole,
with all of its members being considered as weekly reviewers:

If desired, each role can be handled via multiple tasks assigned to different owners,
as long as the overall responsibility for coordinating the work involved in a specific role
is assigned to a single person.

Unless otherwise stated, all of these roles should exist on any type of cell:

#### Sprint Planning Manager

This role is different from the [Sprint Manager](#sprint-manager), though both roles can be taken by the same person if desired.

The planning steps and their schedule are described in more detail in [Sprint Planning Process](../processes/sprints.md#sprint-planning-process), but here is an overview:

* **Task Estimation Session**: Each sprint an estimation session for tasks that are scheduled for the following sprint
  is created automatically in JIRA. Towards the end of the sprint the estimation session is closed and story points
  are assigned to tickets based on estimates that team members provided. This step also happens automatically.
  The only manual step that the Sprint Planning Manager needs to complete is to *add epic owners as Scrum Masters
  to the estimation session* so that they can update the session with any tickets that did not exist prior to the initial launch
  of the session, and that need to be included in the upcoming sprint.
    * At the sprint estimation cut-off point, make sure all tasks have been properly estimated, and work with tasks owners and creator to address any missing estimation.
    * Then check in [SprintCraft] that the work scheduled for the sprint matches the cell's capacity.
* **Prioritization**: Each cell is closest to the needs of its clients and knows best the work that needs to be performed
  in the coming weeks or months, so it's in the best position to prioritize its own backlog accordingly.
  As part of this step, tasks should be arranged in the following order (decreasing priority):
    1. Client work to ensure that the clients' needs are met, and that they are happy with
       our work.
    1. Internal projects flagged by the management as priorities.
    1. Additional work the cell finds useful to its or OpenCraft's function (or the Open
       edX community's), as long as the cell remains sustainable in proportion of
       billable hours. The cell defines and prioritizes the additional internal work,
       without a specific monthly budget cap. (Epic budgets remain a good practice to
       follow, but the amount of internal work scheduled for a given sprint doesn't require approval from management.)
* **Task Insertion**: See [Sprint Planning Process](../processes/sprints.md#sprint-planning-process) > [Task insertion](../processes/sprints.md#task-insertion) for details.
* **Sprint Preparation**
    * Make sure that your cell and its members aren't overcommitted, and work with epic owners to push tasks out to *Stretch Goals* if necessary.
    * Make sure that your cell and its members aren't undercommitted either, and work with epic owners and the epic planning manager to get tasks created or scheduled if more work is needed
    * Make sure that tickets for next sprint are green (i.e., that they have an estimate, an assignee, and a reviewer). Follow up with team members on tickets that aren't green, and push unassigned tickets out to *Stretch Goals*.
* **Backlog refinement**: Go over the backlog and future sprint tasks to ensure its tasks are up to date or archived as needed. As sprint planning manager, you are the backlog's editor, and need to proactively chase people to keep it tidy. Ensure tickets ready to be pulled into a sprint are properly marked and ranked higher in the backlog.

Note: It's important to keep in mind that to remain capable of leading initiatives at the company
level, not just at the cell level, hierarchy can retain an important role in prioritization.
Anyone in a cell can propose or prioritize a company-wide initiative, provided they get approval
and buy-in from people it would affect. It would escalate to management if there's a conflict or
disagreement about that, for example about how priorities relate to each other.

#### Sprint Manager

Each cell has its own sprint board and tickets, and is responsible for managing the sprint,
including necessary preparations and wrapping up completed sprints.

* **Sprint Preparation**: Every Monday before the start of the new sprint:
    * Double-check if any of the people assigned to a rotation in the upcoming
      sprint are off, and if so that a backup will be available (e.g. that at least the required number of [firefighters](#firefighting) for the cell will be present).
* **Assigning Firefighter Rotations**. The Sprint Manager should keep the firefighter assignments in the [Weekly Rotation Schedule][^1] up-to-date.
    * This can happen at any time during the sprint, as long as the spreadsheet is checked at least once per sprint, and updated as necessary.
* **Mid-sprint updates**:
    * Confirm that all members of your cell have posted a mid-sprint update (text only), and follow up with people as necessary via your cell's *Sprint Updates* thread until everyone's posts are available.
* **Sprint Wrap-Up**:
    * Confirm that the [spillovers spreadsheet](https://docs.google.com/spreadsheets/d/1aJD-e2NkDsyBq_yBHykGiMYE29FvSOuYSCZjOJ5sjkM/edit#gid=0&fvid=787610946) for your cell
      contains explanations for all spillovers listed for the previous sprint. It is updated by [SprintCraft] at the end of each sprint.
    * Create the Sprint Retrospective forum post/poll and create new entries in the [retrospective spreadsheet](https://docs.google.com/spreadsheets/d/16zyBstwMrcNJXELUB4DmCeG9IVACrZEC3M3SHoEtvq0/edit) for the new sprint.
    * Confirm that all members of your cell have posted an end-of-sprint video update, and follow up with people as necessary via your cell's *Sprint Updates* thread until everyone's posts are available.
    * Review sprint deliveries: Move tasks from *Deployed & Delivered* to *Done*, after double-checking that all [the criteria for calling it "Done"](../task_workflows.md#done) have been met.
* **Meetings**: For cells, such as the business cell, which don't have an asynchronous planning session, the Sprint Manager leads the sprint planning meeting.

#### Epic Planning and Sustainability Manager

See [Processes > Epic Planning](../processes/epic_planning.md) and [Processes > Budgeting and Sustainability Management](../processes/budgeting_and_sustainability_management.md)
for additional information about the responsibilities listed below.

##### Epic Planning

*Every sprint*:

* Review and evaluate status changes of individual epics from the past sprint.
    * Make sure that:
        * Each epic that is *In Development* has an epic update
          that matches the [epic update template](../epic_management.md#epic-update-template)
          and includes relevant information about ongoing and future work.
        * The Epics board correctly reflects the status of each epic.
        * The ["Time / Estimates"](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1)
          section of the epic planning spreadsheet is up-to-date with info on new and completed epics.
    * In case of any doubt about the correct status for an epic leave a comment on the epic.
* Compare the Epics board for your cell with the [epic planning spreadsheet] and update the spreadsheet as necessary.
* Add/Update team member availability in the [epic planning spreadsheet].
* Handle requests from cell members to change their weekly hour commitments.
* Record information about upcoming vacations in the [epic planning spreadsheet].
* Update [Role statistics](https://docs.google.com/spreadsheets/d/1i1BOjiikB_zFvd8PgB4pteNVYI5tmE7DNbrE8JQI7UQ/edit) for your cell.
* Bring the [Discovery Duty](#discovery-duty-assignee) rotations in the [Weekly Rotation Schedule][^1] up-to-date.
* Adjust capacity allocations for coming months as necessary.
* Ensure delivery and bugfix deadlines for individual epics are on target.
* Coordinate with the team's recruitment manager to slow down/speed up recruitment as necessary.
    * As mentioned [above](#recruitment-manager), this can be done synchronously or asynchronously, depending on your cell's preferences and established workflows.
* Inform the CEO about any decisions made in regards to recruitment, so they can keep them in mind (e.g., in the context of financial forecasting). Make sure to communicate the following points:
    * Whether to start or stop recruitment, or keep the current status unchanged
    * How many newcomers to target
    * A recap of the reasoning behind these decisions
* Post an update on the "Epic planning and sustainability management - &lt;your cell&gt;" epic
  with the following checklist:
```text
h3. Epic planning and sustainability update (Sprint ???)

h4. Epic status

* ( ) Review and update epic statuses as needed.
** ( ) New epics ("Prospect", "Offer / Waiting on client", "Accepted"):
*** ( ) Add an entry to the [Time / Estimates|https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1] sheet of the epic planning spreadsheet.
*** ( ) Move to "In Development" if work has started.
** ( ) In development:
*** ( ) Make sure epic updates matching the template are posted by epic owner/reviewer and include relevant information about ongoing and future work.
*** ( ) Ensure delivery and bugfix deadlines of individual epics are on target (or are being actively discussed on the epics).
*** ( ) Move epics that have become (permanently) blocked on the client to "Offer / Waiting on client".
** ( ) Delivered epics:
*** ( ) Move to "Delivered to client / QA".
** ( ) Completed epics:
*** ( ) Move to "Done".
*** ( ) Update corresponding entries on the [Time / Estimates|https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1] sheet of the epic planning spreadsheet.
* ( ) Ensure the projects from your cell's clients are being properly delivered using the epic management process.

* ( ) Compare the Epics board with the epic planning spreadsheet and update the spreadsheet as necessary.

h4. Availability updates (recruitment, weekly commitments, vacations)

* ( ) Follow up on pings from the team's recruitment manager:
** ( ) For people joining the team, add availability and onboarding hours for the coming months to the epic planning spreadsheet.
** ( ) For people leaving the team:
*** ( ) Update their availability for the coming months.
*** ( ) Help find new owners for their responsibilities (roles, clients, epics).

* ( ) Handle requests from cell members to change their weekly hour commitments.

* ( ) Check the calendar for vacations coming up in the next couple months. If someone will be away for a total of 1 week (or longer), [post a comment mentioning their availability|https://gitlab.com/opencraft/documentation/public/merge_requests/99#note_198626533] in the epic planning spreadsheet.

* ( ) Update [Role statistics|https://docs.google.com/spreadsheets/d/1i1BOjiikB_zFvd8PgB4pteNVYI5tmE7DNbrE8JQI7UQ/edit] for your cell.

* ( ) Update the Discovery Duty assignments in the [Weekly Rotation Schedule|https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit]. If this is the first sprint where no team members are available for discovery duty, inform Xavier so he can verify no internal projects should be paused.

h4. Capacity requirements

_For in-progress epics:_

* ( ) Evaluate the amount of time required to complete the accepted project scope over the next months and update capacity allocations in the epic planning spreadsheet.
* ( ) Coordinate with the team's recruitment manager to slow down/speed up recruitment as necessary, and communicate decisions to the CEO.
* ( ) Update the team's business development specialist about raw capacity and role load.

CC <CEO>

h4. Client updates

* ( ) Check the descriptions of the Hosted Sites Maintenance and Small Projects & Customizations epics (STAR-3058, STAR-3059) and update the list of clients for your cell, following the existing format.
** ( ) Add info about new clients that we recently on-boarded. (Make sure to skip clients that haven't moved past the prospect stage.)
** ( ) Remove info about clients that we no longer work with. (Make sure they have been off-boarded completely before doing this.)
** ( ) Other updates

h4. Budgeting

_Once per sprint:_

* ( ) *Monday of W3:* Review amount of billable and non-billable work scheduled for next sprint, and post update on your cell's epic planning and sustainability epic.

_Once per month:_

* ( ) *Before the 5th of the month:* Check that tasks from previous month use correct accounts, and post update on your cell's epic planning and sustainability epic.
* ( ) Add actuals for previous month to epic planning spreadsheet.
* ( ) Adjust client budgets for sustainability dashboard as necessary.

_Once per quarter:_

* ( ) Post update about cell's sustainability on forum.
* ( ) Review and update budgets for non-billable accounts.

_As necessary:_

* ( ) Archive obsolete epics.
* ( ) Close obsolete accounts.

h4. Notes

...
```

*Every month:*

* Add actuals for the previous month to the epic planning spreadsheet.
* Adjust client budgets in [SprintCraft] as necessary to keep the sustainability dashboard
  for your cell up-to-date.
* Check the planned workload on epics, and that it matches the cell capacity for the upcoming month. If it doesn't match:
    * The workload is above cell capacity -- do we need to hire? Inform the recruitment manager.
    * The workload is below cell capacity -- do we need to preload more work?
        * Create tasks for epic owners to create more tasks (splitting up existing tasks, or working ahead), as well as to clean-up the tasks from their epics in the backlog, moving them to upcoming sprints as much as possible
        * Communicate with the [business development specialist](#business-development-specialist), to get more work lined up
        * Bring up the topic on the forum


##### Sustainability Management

The sustainability manager of a cell is responsible for ensuring that the cell keeps the amount of non-billable hours logged in check.
In particular, the sustainability manager must monitor the cell's [sustainability ratio](../cells/budgets.md#cell-and-company-wide-sustainability)
and make sure that the cell hits its current sustainability target (as listed under [Cell-Specific Rules](../cells/cell_specific_rules.md)).

To help with this, the sustainability manager must perform the following activities:

*Every sprint:*

* *Monday of W3:* Compile and post sustainability preview for the upcoming sprint,
  using the following template:
   ```text
   h4. Sustainability preview for Sprint <next> (<cell name>):

   * Period selected: Tue <date> (beginning of Sprint <current>) -- Mon <date> (end of Sprint <next>)
   * Total number of billable hours estimated: h (left this sprint) + h (next sprint) = h
   * Total number of non-billable hours estimated: h (left this sprint) + h (next sprint) = h
   * Resulting sustainability ratio (non-billable / (billable + non-billable)): **%, {lower,higher} than last sprint.
   * Notes:
   ** ...
   * Questions:
   ** ...
   ```
* Follow up with epic owners about budget issues as necessary.

*Every month:*

* *Before the 5th of the month:* Check that tasks worked on over the course of the previous month
  use correct accounts, and post an update on your cell's epic planning and sustainability epic.
  You can use the following template:
   ```text
   h3. Accounts review for <month> <year> (<cell name>):

   h4. Client accounts

   h5. Updates applied

   * <ticket>: Changed account from <previous> to <new>.

   h5. Open questions

   * <ticket>: ...

   h4. Internal accounts (non-cell)

   h5. Updates applied

   * <ticket>: Changed account from <previous> to <new>.

   h5. Open questions

   * <ticket>: ...

   h4. Internal accounts (cell)

   h5. Updates applied

   * <ticket>: Changed account from <previous> to <new>.

   h5. Open questions

   * <ticket>: ...

   ---

   CC <Administrative Specialist>
   ```

*Every quarter:*

* *In January/April/July/October:* Compile and post sustainability update on the forum,
  choosing from the templates listed below.
    * If your cell is currently sustainable, the following format works well for the quarterly sustainability update:
      <details>
        <summary>(click to expand)</summary>

        ```markdown
        # <cell name> - {January,April,July,October} <year> ({Q1,Q2,Q3,Q4} <year> update)

        You can log time spent reading and responding to sustainability updates on the sustainability epic of the corresponding cell:

        * [BB-283](https://tasks.opencraft.com/browse/BB-283) for Bebop updates
        * [FAL-49](https://tasks.opencraft.com/browse/FAL-49) for Falcon updates
        * [SE-248](https://tasks.opencraft.com/browse/SE-248) for Serenity updates

        ## Goal

        <cell name> has been targeting a sustainability ratio of <target>% since <month> <year>.

        ## Sustainability Ratio

        ### Entire reference period (<month> <year> -- <month> <year>)

        The sustainability ratio for the period <yyyy-mm-dd> -- <yyyy-mm-dd> is **<percent>%**.

        ### {Q1,Q2,Q3,Q4} only (<month> <year> -- <month> <year>)

        The sustainability ratio for the period <yyyy-mm-dd> -- <yyyy-mm-dd> is **<percent>%**.

        ## Billable Hours ({Q1,Q2,Q3,Q4})

        The largest source of our billable hours is <client>.

        Top contributors of billable hours by account:

        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * ...

        ## Non-Billable Hours ({Q1,Q2,Q3,Q4})

        Top contributors of non-billable hours by account:

        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * ...

        ## Projections for (the rest of) <year>

        ...
        ```
      </details>

    * If your cell is currently above its sustainability target, you'll probably want to
      go into a bit more detail with your review and use the following format for the quarterly sustainability update:
      <details>
        <summary>(click to expand)</summary>

        ```markdown
        # <cell name> - {January,April,July,October} <year> ({Q1,Q2,Q3,Q4} <year> update)

        You can log time spent reading and responding to sustainability updates on the sustainability epic of the corresponding cell:

        * [BB-283](https://tasks.opencraft.com/browse/BB-283) for Bebop updates
        * [FAL-49](https://tasks.opencraft.com/browse/FAL-49) for Falcon updates
        * [SE-248](https://tasks.opencraft.com/browse/SE-248) for Serenity updates

        ## Previous updates

        To see where <cell name>'s budgets stood at the end of {Q1,Q2,Q3,Q4}, see the latest sustainability report from [<month> <year>](<forum link>).

        At that time <cell name> had a sustainability ratio of **<percent>%** for the current reference period (<first day of entire reference period> to <last day of quarter under review>).

        ## Goal

        <cell name> has been targeting a sustainability ratio of <target>% since <month> <year>.

        ## {Q1,Q2,Q3,Q4} <year> Sustainability Ratio

        At the end of **{Q1,Q2,Q3,Q4} <year>**, <cell name>'s sustainability ratio for the entire reference period (<first day of entire reference period> to <last day of quarter under review>) is **<percent>%** (an {increase,decrease} of <percent>% since last update).

        Detailed breakdown:

        ### Sustainability numbers from *<first day of entire reference period> to <last day of quarter under review>* (entire reference period)

        | Total hours | Billable hours | Non-billable hours | % non-billable | Remaining non-billable hours |
        |---|---|---|---|---|
        | <hours> | <hours> | <hours> | **<percent>%** (max <current target>%) | **-<hours>** |

        ### Sustainability numbers from *Jan 1st <year> to <last day of quarter under review>* (Q1-{Q1,Q2,Q3,Q4} of <current year>)

        | Total hours | Billable hours | Non-billable hours | % non-billable | Remaining non-billable hours |
        |---|---|---|---|---|
        | <hours> | <hours> | <hours> | **<percent>%** (max <current target>%) | **-<hours>** |

        ### Sustainability numbers from *<first day of quarter under review> to <last day of quarter under review>* ({Q1,Q2,Q3,Q4} only)

        | Total hours | Billable hours | Non-billable hours | % non-billable | Remaining non-billable hours |
        |---|---|---|---|---|
        | <hours> | <hours> | <hours> | **<percent>%** (max <current target>%) | **-<hours>** |

        ## Billable Hours ({Q1,Q2,Q3,Q4})

        The largest source of our billable hours is <client>.

        Top contributors of billable hours by account:

        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * ...

        ## Non-Billable Hours ({Q1,Q2,Q3,Q4})

        Top contributors of non-billable hours by account:

        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * **<account name>**: <period spent>h
        * ...

        ### Changes since previous update

        The sustainability ratio for the previous quarter ({Q1,Q2,Q3,Q4}) was **<percent>%**, so {Q1,Q2,Q3,Q4} brought with it a noticable {increase,decrease} in the ratio of non-billable hours.

        <List reasons for changes.>

        ## Changes affecting sustainability over the entire reference period

        Aside from the {Q1,Q2,Q3,Q4}-specific changes described above, there's a few additional factors that affected <cell name>'s sustainability ratio for the entire reference period:

        <List changes (if any) or delete this section.>

        ## Other notes

        <List notes (if any) or delete this section.>

        ## Projections for (the rest of) <year>

        ...

        ## Plans for improving sustainability

        ...
        ```
      </details>

* Review and update budgets for non-billable accounts in [SprintCraft],
  coordinating with epic owners as necessary.

*As needed:*

* Adjust your cell's sustainability target in SprintCraft.

#### Cell creation manager

A temporary role to manage the creation of a new cell, where the assignee:

* Creates and self-assigns a dedicated epic.
* Starts a forum thread to coordinate communication on the subject.
* Handles the [step-by-step process of cell creation](../cells/creation_checklist.md) itself.

General information on the life cycle of cells, including how and why they're born, can be found in the [cell life cycle page](../cells/lifecycle.md#cell-birth).

### Code reviewer

As a **code reviewer**:

* I will give prompt, thoughtful, thorough, constructive code reviews.
* (When reviewing OpenCraft work): I will expect that the PR author has done everything outlined in the
  *PR expectations* part of the [pull request process](../processes/pull_request.md)
  section - if not, I will ask them to fix that before I start the review.
* (When reviewing non-OpenCraft work): If I get pinged to review a PR, I will
  respond to it within 24 hours. If it is for a ticket in the current sprint, I
  will ensure that I review it within 24 hours, or at least indicate to the
  author when they can expect a review.
* I will aim to minimize review cycles (especially when reviewing non-OpenCraft
  PRs) by leaving as complete a review as possible. Ideally it should point
  authors to the exact changes needed for their PR to be accepted.
* When reviewing work (or discussing it with the assignee before coding even
  begins), I will make sure that the assignee is not introducing code drift,
  i.e. that everything which could be upstreamed is (planned to be) upstreamed
  as part of the work.
* I will always read through the code diff, considering things like:
    * Is the code easily understandable by humans?
    * Is the code of high quality?
    * Are all relevant coding standards followed?
    * Are all UX / a11y / i18n considerations addressed?
    * Is this introducing tech debt?
    * Is the new code well covered by tests?
* I will always test the code manually, either locally or on a sandbox, unless there is no
  reasonable way to do so.
* I will always check if any updates to the software's documentation are required,
  and either ask the author to update the docs, or ensure the relevant documentation team is pinged.
* If there is any part of the code that I am not confident in reviewing,
  I will indicate that in my comments.
* If there is any part of the code or PR that I particularly like, I will say so - we want to reinforce
  good practice as well as flagging issues.
* I will
  [set up the following template as a "Saved Reply" in GitHub](https://github.com/settings/replies)
  and use it for the "stamp of approval" on all PRs I review:

```markdown
:+1:

- [ ] I tested this: (describe what you tested)
- [ ] I read through the code
- [ ] I checked for accessibility issues
- [ ] Includes documentation
- [ ] Added to the [Code Drift](https://github.com/orgs/open-craft/projects/1) project board (for backports)
```

  Here is a screenshot showing how to conveniently access this template
  when completing a code review using the dropdown button in the top right
  of the review panel (on the "Conversation" tab only):

  ![Review template](../images/review-template.png)

* If I'm the assigned reviewer for someone who is new to OpenCraft,
  I will reserve extra time to provide additional mentoring and support,
  and I will be especially responsive to questions.
  I will also provide feedback to the newcomer which will assist them during their onboarding period, and raise any major
  issues with their mentor.
* If the assignee is clearly behind the schedule and doesn't respond at all to pings on the ticket or Mattermost within
  48 hours, the code reviewer should determine whether this ticket is urgent. If it is, ask the
  [firefighter](#firefighting) for help to reduce the risk of spillover.

### Newcomer

If I'm **new to the team**, I will:

* Not commit to more than [3 story points](../task_workflows.md#general-tasks) of tasks during my first week, or 5 points
  my second week (so no more than a total of 8 points for the first sprint).  It's really easy to over-commit at first,
  so keep your commitments manageable. You can always take on more work later in the sprint if you finish early.
* Discuss general issues like time management and sprint planning with my mentor.
* I'll prepare for every 121 with my mentor based on the [newcomer 121 checklist](#newcomer-121-checklist)
* Tag my task reviewer(s) with task-specific questions on the ticket, or if I'm completely blocked, try using
  Mattermost to contact my [reviewer](#code-reviewer), [mentor](#mentor), [sprint firefighter](#firefighting), or other people working on the same project.
  Reviewers have time allocated to help during Onboarding, so it's ok to reach out!
* Ask my Reviewer questions **early in the sprint** if there is missing information or if the requirements are unclear.
  The tasks I am going to work should have a minimum set of information that should be included in the
  task description, but if this isn't complete, ask the task creator and/or your reviewer to do it.
* Provide lots of updates to my assigned reviewer/mentor for each task, so they can provide timely help.
* Assign myself as [reviewer](#code-reviewer) for core team member tasks to learn about the various projects and systems
  that we support.
* Log time spent "onboarding" (reading documentation, learning new systems, setting up devstacks) to my onboarding task.
  Time spent completing the work described in a task can be logged to the task itself, but please take care not to
  exceed the estimated time without **approval from the owner of the parent epic**.  Some leeway will likely exist
  within the epic budget, but it's best to check to be sure.

Also, it is not compulsory to log X hours per week as stated in the contract -- our contracts give an estimated amount
of time expected per week, but the actual work required for each sprint can vary due to many factors.

See the [Process: Onboarding & Offboarding](../processes/onboarding_and_offboarding.md) page for more advice on the onboarding, the evaluation
process and criteria.

#### Newcomer 121 checklist

Before every 121 with my mentor, I will create a new checklist from the [Newcomer 121 checklist template](https://app.listaflow.com/templates) and prepare by going through each item.

#### Onboarding epic ownership

As a newcomers, I am the owner of my onboarding project, and will:

* Keep time spent by me and my mentor on the onboarding epic to less than 80 hours for the initial 2 month onboarding period.

* The remaining hours of my onboarding budget must be preserved for the developer review at the end of onboarding period and setting up final set of tools, for a maximum total of 95h.

    !!! info

        Expect around 15 hours to be spent by the core team on the developer reviews. For example, if the developer review requires 1 hour for
        each core team member and there are 14 of them, then 14 hours is required for those activities.

* Treat my onboarding epic as a proper epic and manage it like an [epic owner](#epic-owner).<br/>
  The epic and the budget also includes my onboarding ticket, my mentor's mentoring ticket and any other tickets created
  specifically for my onboarding like the ones for my developer review.

    !!! tip

        In the onboarding epic, there is a `Summary Panel` section on the right side, which shows various details about
        the budget, logged and available time etc. See the below screenshot for an example.

        ![Summary Panel](../images/epic_summary_panel.png)

        Hover over the Time numbers below the progress bar to see an explanation.

* I will create tasks under this epic for distinct pieces of work, set estimates and schedule them with the help of my
  mentor.<br/>
  This will help us to track if a particular onboarding/learning task is taking too much time. Since the onboarding
  budget is limited, all the learning time I log on the onboarding epic will be related to the tasks that I work on.

* Log my time with detailed descriptions about the work performed.

* Log my time with detailed descriptions about the work performed.  For example:<br/>

    *"Reviewed task and asked questions on ticket, requested repo access, started setting up devstack."*

    Or as an example where logging "onboarding" time spent on another task on my onboarding ticket:

    *"Onboarding for SE-123: read XBlock Tutorial and configured XBlocks in my devstack"*

### Mentor

The role of the mentor starts when the candidate starts on their trial project.

* I will familiarize myself with the current [onboarding & evaluation process](../processes/onboarding_and_offboarding.md), so I can
  answer questions and help the newcomer through the process.

#### During the Trial Project

* I will inform the team that I'll mentor a newcomer before their first day.
* I will be the reviewer for the trial project's JIRA ticket.
* I will schedule an initial 121 meeting with the newcomer on their first day.
* I will check with the candidate to make sure they understand the requirements and are able to get started on the project.
* I will provide timely support by directly answering their questions on the trial project's JIRA ticket or directing them to appropriate Mattermost channel.
* In case trial project's complexity is under or over-estimated, I will immediately inform the Recruitment Manager and find a way make the trial project viable for evaluation.
* I will communicate the [trial project candidate expectations](#trial-project-candidate) where and when necessary.
* I understand a trial project's reviewer responsibilities require more attention than a regular ticket and will allot extra attention during the sprint.
* If the new member doesn't meet the expectations during the trial project review,
  I will discuss the completion status of the trial project ticket with the
  Recruitment Manager and reassign the it to a team member for the following sprint
  or pass it on to the next candidate depending the progress and urgency of the ticket.

#### After Team Member Onboarding

* I will allocate sufficient time in my sprint, especially at the beginning, to assist and mentor the newcomer.
* I will help the new member in selection of tasks during sprint planning and ensure their assigned tasks meet the goals [specified below](#how-to-select-tasks-for-new-members).
* I will schedule a recurring 121 meeting for every week until the end of the onboarding period. These can be spaced out later on if the meetings become less useful. Refer to
  [the mentor checklists](#mentor-checklists) for the topics to go through.

    !!! note

        While we are very much an asynchronous team, scheduling sychronous time at the beginning of the onboarding period is very important to make the process easier for both the mentor and the mentee. It will help surface blockers or issues more easily than through text, and it is more friendly this way too.

* For the first 121 with the newcomer, I will share the anonymized feedback from the trial project review.
* During every 121 with the newcomer, I'll go through the [mentoring 121 checklist](#mentor-121-checklist).
* I will participate in developer review tasks, taking into account these [evaluation criteria](../processes/onboarding_and_offboarding.md#evaluation-criteria).
* I will schedule a 121 after the developer review and share the consolidated anonymized feedback with the mentee.
* I will evaluate the newcomer's usage of the onboarding budget and raise any issues with Xavier. I'll also help the newcomer with
  creating, estimating and scheduling learning tasks in the onboarding epic, tracking them and with
  precise time tracking of their tasks if the work logs show too many approximate (i.e. rounded) values.
* I will also explain to the newcomer, the guidelines to manage the onboarding epic and its budget like
  any other epic that we work on and point them to the relevant items in the [newcomer](#newcomer) section.

#### Mentor 121 checklist

* Review the work done by the newcomer since the past mentoring session in prior to the meeting
* Review the onboarding budget status
* Provide feedback about strengths
* Provide feedback about areas of improvement, and suggest methods for improvement
* Review tickets assigned to the newcomer for the upcoming sprint
* Help find tickets for the upcoming sprint if needed
* Discuss current issues/blockers if there are any
* Ask for feedback about the processes and the ease of onboarding; if an issue comes up, open a [GitLab issue](https://gitlab.com/opencraft/documentation/public/-/issues)

#### How to select tasks for new members

Selecting the right tasks for the onboarding period is very important. The goals are:

* In the first 2 weeks when the new member is trying to understand the different stacks they will need easier tasks to start with.
* From the second sprint they should be picking up at least a couple of moderately complex (5 points) tasks.
* By the end of the onboarding period there should be sufficient number of (5+) complex tasks to showcase the new member's skills and learning abilities.
* There should be variety in the tasks to allow evaluation for the different types of skills. It is particularly
  important to have 3-4 reasonably complex dev tasks (historically this has been a weak point) and 2+ ops tasks (preferably from client maintenance epics).
* While lines of code is not a measure of complexity having 2-3 thousand lines of code makes it much easier for reviewers to do their evaluation.

* Before the new member's first day they should be assigned a 2 or 3 point task from one of the main projects of the cell. This will allow them to get familiar with the relevant project and be able to pick up more complex tasks in the next sprints.
* The evaluation of a new member's skills is incredibly important so feel free to take tasks assigned to any other team member and re-assign them to the new member (unless it looks urgent or has a tight budget).
* It is okay to assign very small discoveries, which do not result in a standalone epic. (See [Newcomer-friendly tasks discussion](https://forum.opencraft.com/t/newcomer-friendly-tasks/318)[^1])
* Tasks which require access to production systems are not recommended for new members. If a task requires deployment, one option is that the reviewer does the deployment, but lets the newcomer know what they’re doing at each step, to help them learn for the future. Any additional access granted needs to be documented in the Onboarding checklist and accesses granted document on their onboarding epic so they may be rotated in case we need to offboard them.
* During each sprint planning, remember the goals of assigning varied tasks, of increasing complexity.
* If none of the above work, escalate the issue to the [recruitment manager](#recruitment-manager).

#### Mentor checklists

##### Initial 121

This meeting objective is to get the mentor to know better about the newcomer capabilities and help sort out initial onboarding issues. Here are some example questions to help with that:

* Are you comfortable working in a POSIX environment?
* Do you have experience coding in Python? How many years?
* What is your skill level in frontend development? Do you have experience with any frameworks?
* How do you feel working 100% remotely? Is this your first experience?
* Do you have a technical task assigned to work during the first sprint?

##### Weekly 121s

These meetings will be used mostly for answer questions, both technical or related to OpenCraft processes.

* Evaluate the newcomer's current sprint and tasks currently at risk of spilling over.
At-risk tasks should be flagged with the sprint firefighter, to avoid missing delivery deadlines.
* Help the newcomer prioritize work and check if there are any difficulties working remotely.
* Provide feedback, both on technical and other skills such as communication, time management etc. This feedback may come from other team members, or from my observations on the newcomer's assigned tasks.
* Help the newcomer use accurate task statuses during their sprints to ensure the [Sprint Commitments spreadsheet](https://docs.google.com/spreadsheets/d/1aJD-e2NkDsyBq_yBHykGiMYE29FvSOuYSCZjOJ5sjkM/edit) for
their cell is accurate when it comes time to do the developer review. If there are any issues with what is recorded
there, let the [Sprint manager for your cell](../cells/list.md) know.

### Firefighting

Firefighting is the handling of pager alerts, and the work on unscheduled urgent issues that arise during a sprint, and couldn't have been properly planned.

See [Processes > Firefighting](../processes/firefighting/fire_management.md) for general rules about firefighting, as well as the specific requirements and responsibilies of each role. See also the [guidelines for triaging issues](../processes/firefighting/triage_guidelines.md).

There are several types of roles involved in firefighting:

#### Firefighter

The role meant to handle the bulk of the actual work involved in firefighting, such as fixing the issues that trigger alerts.

#### Cell Member

When firefighters are not available to deal with an urgent issue, all cell members are called to help.

#### Firefighting Manager

The coordination role of firefighting, ensuring that roles and rotations are properly assigned, and alerts handled; in last resort, alerts get escalated to them

#### Managers

If an alert also escapes the firefighting managers, the pager escalates it to the CEO and CTO.

### Community Relations

One of OpenCraft's most important core values is community involvement.  To reflect this, we have three roles that are focused on community involvement:

#### Core Contributor

edX has a [Core Contributor Program](https://openedx.atlassian.net/wiki/spaces/COMM/pages/3143205354/Open+edX+Core+Contributor+Program) which allows people outside of edX to review and merge OSPRs, contribute to platform development in other ways (e.g. by reviewing translations), mentor new community members, and participate in the strategy and direction of the platform.

Our goal is for every OpenCraft member to eventually become a Core Contributor and for every Core Contributor to spend a minimum of 20 hours per month on contributions -- assuming that their total hourly commitment per month reasonably supports this.  For team members with commitments below 120h/month (30h/week), joining the Core Contributor program would require making adjustments to the expected volume of monthly contribution hours. This is not possible based on current rules of the program. However, organizational commitments for the Core Contributor program are under discussion and requirements may change going forward. When in doubt, team members looking to join the Core Contributor program in the near future can reach out to the [owner of the Core Contributor epic](#internal-ownership-of-the-core-contributor-program) to get the latest information on the number of contribution hours they would be expected to put in per month.

##### Priorities for Core Contributor Work

We set priorities that we want our team's Core Contributors to focus on - see the section on "Priorities for Core Contributor Work" in the description of the [Core Contributor Program](https://tasks.opencraft.com/browse/MNG-2649)[^1] epic for details.

##### Other Responsibilities

Aside from current priorities, it's important for Core Contributors to spend time on community engagement and technical upkeep. Responsibilities include:

###### Open edX Forum Moderation

* Help moderate the [official Open edX forum](https://discuss.openedx.org/):
    * Welcome new posters.
    * Help community members use the forums well, for example by giving them advice (where applicable) about:
        * Choosing the right categories for their posts, and moving/splitting/concatenating threads when needed.
        * Providing enough information about their situation. (If they make their requests more precise they may get better answers, and more quickly.)
        * Using existing resources or documentation.
    * Review posts flagged automatically by Discourse (for example, someone finishing a post too quickly after creating
      their account).
    * Make sure "Open edX" is spelled (and capitalized) correctly in post titles. Don't bother with the body of posts,
      but the topic lists should show only show the correct usage.
    * Set the right tone for discussions and, in general, be a model Open edX citizen.
* Monitor OEPs, ADRs, and community discussions on the forum, and ensure that OpenCraft participates in relevant ones.
* Answer questions from the community.
    * Note that you should generally favor helping one person more fully, over having many smaller contributions to more threads.

###### Communication via Other Channels

* Monitor the Open edX [Slack](http://openedx.slack.com/), in particular the [#opencraft channel](https://openedx.slack.com/archives/C0F63UDL0). Follow up on community questions that pop up there, and mentor other community members as needed.

###### Technical Upkeep

This involves tasks such as updating documentation, [fixing bugs](../processes/firefighting/triage_guidelines.md#upstream-issues), updating packages, and improving developer onboarding.

#### Community Liaison

This role involves the largest commitment of the Community Relations roles, at **8 hours a week**, and is meant to be
taken by a single person across all cells.

Work belonging to this role is to be logged against the *Community Relations* account by default (which is an internal non-billable account).
However, the following applies:

* Whenever possible the Community Liaison should link individual tasks to other accounts, using the *Community Relations* account only as a fallback.
* The *Community Relations* account can only be used on tasks that are either assigned to the Community Liaison themselves,
  or to the [OSPR Liaison](#ospr-liaison). In other words, it should **not** be used for Core Contributor tasks, or, for example, for other team members to attend community meetings.

Responsibilities of the Community Liaison role include:

##### Engaging OpenCraft's Open Source Community

* Monitor OpenCraft's public repos; specifically, the following areas on [GitHub](https://github.com/open-craft?q=&type=public&language=) and [GitLab](https://gitlab.com/opencraft):
    * [List of open issues](https://github.com/search?q=org:open-craft+is:public+is:issue+state:open) (GitHub)
    * [List of open PRs](https://github.com/search?q=org:open-craft+is:public+is:pr+state:open) (GitHub)
    * [List of open issues](https://gitlab.com/groups/opencraft/-/issues) (GitLab)
    * [List of open PRs](https://gitlab.com/groups/opencraft/-/merge_requests) (GitLab)
* Watch these channels for technical questions (e.g. about our XBlocks) and answer them.
* Watch for bug reports and pull requests submitted by the community and make sure that they get a prompt answer and/or review:
    * Acknowledge the contribution within **24 hours** (i.e., within the same time frame that we generally set for external and internal [communication](#communication)).
        * To help with this, set up appropriate e-mail filters so that you are immediately aware of incoming communication from the community.
    * If circumstances allow (i.e., if you have time and a reasonable amount of relevant context),
      you may fix the reported bug or review and merge the submitted PR yourself.
    * If circumstances do not allow you to fix the bug or review and merge the PR yourself
      (i.e., if you're lacking time and/or relevant context), you will:
        * Create an internal ticket for proper follow-up.
        * Set a deadline of **2 weeks** (from creation of the ticket) for completing the work.
        * Set a timebox for the work based on guidelines from [Running a Free Software project](../free_software.md#running-a-free-software-project).
        * Assign the ticket to a firefighter so that work on it can start immediately. (Yes, this
          should be treated as a fire: contributions are rare enough that it's worth acting quickly to encourage them.)
            * If the task is complex or requires domain knowledge, the firefighter will do a first superficial pass immediately
              to show the contributor that we care, and then mention that another pass by someone else will need to happen, and
              when. Even in these cases, though, the Community Liaison should set an early deadline for the first review,
              and the assignee of the ticket must remain reactive to subsequent updates and comments from the contributor.
        * Let the contributor know that next steps have been scheduled.
* Monitor [OpenCraft's forums](https://forum.opencraft.com/) for posts from people external to the team, and follow up on them as necessary.

##### Open edX Community Engagement

Complementing the work that Core Contributors do, the Community Liaison will both engage in and
help coordinate company efforts pertaining to the Open edX community as needed, across all contact points.
Specific duties include:

* Being available as a community reference ("Talk to Ned about this, Felipe about that"), not only
  internally at OpenCraft, but also for the community itself.

###### Attending Working Groups and Community Meetups

The Community Liaison is expected to attend the following community meetups:

* [Contributor Coordination Working Group](https://openedx.atlassian.net/wiki/x/AQCa4) (monthly)

#### OSPR Liaison

This role involves a commitment of 1 hour per sprint, per cell. (For example, if there are 3 cells
the total commitment per sprint for this role will be 3 hours.)

Like the [Community Liaison](#community-liaison) role, it is a company-wide role
and uses the *Community Relations* account. Specifically, time spent working on responsibilities
listed below should be logged on [SE-4942](https://tasks.opencraft.com/browse/SE-4942)[^1].

The OSPR Liaison is responsible for coordinating OSPR reviews. This involves:

* Maintaining a prioritized list of PRs in a document shared with edX's community management team:
  [OSPR priorities from OpenCraft](https://docs.google.com/spreadsheets/d/1DPfQg8O09PqW4BnQTJdE3FwvWb_EzbGiE74cG3LkP7w/edit)
* Sending the top 3 PRs on the list to the community management team (usually [Natalia](https://github.com/natabene)) once a week.

#### Miscellaneous Contributions

*All team members*, irrespective of whether they hold Core Contributor status or not, are encouraged to be involved in the Open edX community on the forum and help with platform maintenance as described [above](#other-responsibilities): If you spend some time on community involvement and it doesn't fall under the scope of one of your current tickets or roles, just create a ticket for it in your sprint (e.g. "Answer XBlock question on forum") and put it on the *Contributions* account.

Note that time logged against the *Contributions* account is non-billable. So for larger contributions,
team members without Core Contributor status should reach out to their cell's sustainability manager
before starting to work on them and confirm that the cell can spare the necessary number of non-billable hours.

### Ops reviewer

1. Receive ops@opencraft.com and the pager alerts - check alerts/emails to ensure the alerts
   have been properly handled by the other recipients and nothing has slipped through.
1. Be a backup on the pager (alerts sent first to the other(s) recipient(s), but escalated if not
   acknowledged within 30 minutes).
1. If none of the other recipients are around and an issue is left unsolved, and it either is sent
   to urgent@, or was sent more than 12h ago to ops@opencraft.com and needs to handled
   quickly, warn the [sprint firefighter](#firefighting) about it: create a ticket about the issue and assign it.

Note that keeping an eye on [build-test-release CI notifications](https://groups.google.com/g/open-edx-btr-notifications)
triggered by periodic builds and following up as necessary is part of the [Community Liaison](#community-liaison) role,
and out of scope here.

### Discovery duty assignee

This role is specific to a generalist cell: because of the focus on a single client, project cells need not have a developer on standby for new prospects.

Discovery Duty is only handed to team members that have room to take clients or epics. Typically, this will be newer team members, or team members whose clients have recently closed their contracts.

If no team member has room for additional epics or clients, the cell will be closed to taking on new clients until such room opens up via additional recruitment or client departure. In this case, the role will not be assigned.

As the **week's discovery duty assignee**:

* If I have doubts on my ability to estimate a discovery accurately, I will find a reviewer who is more familiar with the technologies involved and make them aware of my concerns, asking for an especially close review.
* I will keep at least 5 hours of my time to do discovery work on any small leads/client
  requests that come up during the week.
* If I do some discovery tasks, I will ping the [sprint firefighters](#firefighting) to review the result (unless the above note about estimate accuracy applies-- then I shall ping the appropriate reviewer.)
* As always, if I do the discovery, I understand that I would be expected to complete the epic
  work later on if the client accepts it.
  (To avoid people being forced to commit to being the owner of big epics, any
  huge discovery stories should always be scheduled into future sprints rather
  than directly assigned to the discovery duty assignee. In the case that no team member can take the work, the person assigning the Discovery task must either coordinate a solution to free up a team member, or else we will decline the work.)
* I will ensure an additional task is left either in the "stretch goals" or the following sprint
  during sprint planning, in case there isn't enough discovery duty work to fill my hours.
    * I will only pull this task into the current sprint if I am confident that I will have time to finish it in addition to the discovery duty. For the task to be ready to pull in, it should be assigned to me and have a reviewer.
    * Before pulling the task into the sprint, I will first check if I could instead use my discovery hours to help others finish their sprint commitments.

### Specialist roles

Specialists are people in the cell with more context and understanding about a certain topic, such as security or analytics/Insights.
The goal of having this role is to allow cells to self-sustain and run their own projects without being blocked on context from people from other cells.

While specialists have priority when it comes to taking tasks related to their specialty, there's no restriction on other team members and taking "specialty" tasks is encouraged to spread knowledge around the cell.
However, specialists should always take at least the role of 2nd reviewer so they are in a position to track progress and provide help.

Specialists are not only responsible for handling tasks within the cell, but also for coordinating with other specialists to discuss and schedule improvements when necessary.

Specialist roles and corresponding responsibilities include:

#### Technical Security Manager

* Responsible for the **technical security** of the work we do, our platform and OpenCraft as
  a whole.
* Responsible for **[OpenCraft's Security policy](http://opencraft.com/doc/handbook/security_policy/)**.
* **Offboarding** of team members who have left.

### Epic owner

As an **epic owner**:

* I will make sure the epic has tasks with time estimates and a global budget. A discovery will likely be required for this - read the [estimates](../how_to_do_estimates.md) section and follow the steps listed there.
* I will track and proactively manage budgets for my epics.
    * If the client approved a specific budget for a task or set of tasks
      belonging to my epic, I will make sure that assignees are aware of
      the budget limits for the tasks that they take on before they
      start working on them.
    * I will also remind assignees of their responsibility to stop
      working on tasks with budget limits *before* the budget runs out,
      and to let me know that a budget extension is needed
      so that we can request it before spending more time on the tasks.
      *This is necessary because if we ask for retroactive approval, there is a chance
      that the client won't agree to paying for any/all hours exceeding the original budget,
      and that will [make these hours non-billable](../processes/billable_work.md##how-do-we-know-what-work-is-billable-and-what-work-is-non-billable).*
    * If a budget extension is needed, I will ask the client for it myself
      or delegate that step to task assignees (if appropriate in the context
      of my epic).
    * I understand that it is ultimately my responsibility as an epic owner
      to make sure that we don't exceed the budgets that are available for my epics.
      So I will keep an eye on time logged against tasks belonging to my epics
      (as well as the epics themselves) to the extent that is necessary for keeping
      budget usage on track.
      *For many projects, tracking budget usage at the epic level
      is sufficient. But there are situations where tracking budget usage at task level
      will be (or becomes) necessary. It is your job as an epic owner to understand
      the level of granularity that is needed for each of your epics.*
* I will ensure the "Timeline" section of the description and the epic due date
  are kept up to date. The "Timeline" section should include a list of
  milestones and deadlines for each (these milestones could be as simple as
  "check in with client"). The due date should be set to the closest
  deadline, and must be revised once each deadline is past/updated.
* I will keep clients updated on the status of tickets
  (for clients with long term monthly contracts, this means updating
  their JIRA or Trello boards as work progresses).
* I will attend meetings with the client about the project as required,
  e.g. weekly scrum meetings.
  (We try to limit these sort of meetings to once a week per client at the most.)
* I will create tickets on OpenCraft's JIRA board for upcoming work,
  and place them into the appropriate upcoming sprints
  so that we can get the work done well ahead of applicable deadlines.
    * If a ticket without point estimates is scheduled for the upcoming sprint,
      and an estimation session is open, I will make sure to add it to that session,
      so everyone on the team can help estimate it.
* For tickets that already exist, I will review and adjust their scheduling
  for upcoming sprints as necessary.
* For longer-term planning which includes sprints that do not yet exist in JIRA,
  I may add a timeline of future sprints to the description of my epic and list the tickets
  that we should complete in each of these sprints. 💡 *This will help me determine
  if the epic is on track progress-wise, and allow me to easily add existing tickets
  to new sprints as they are created.*
* I will aim to split out any suitable trial project work into separate tasks, label them as
  [trial-project](../task_workflows.md#trial-project-tasks) in the Backlog, and ensure they contain the required information.
  To guard the budgets of my epics, "onboarding" time for trial projects can be logged against the newcomer's epic.
* If applicable, I will create corresponding tickets on clients' JIRA or Trello boards
  and link to those tickets from the description of internal tickets as necessary.
  (In many cases, a single internal ticket from OpenCraft's JIRA
  will correspond to a single ticket from a client's JIRA or Trello board.
  But there are also cases where a single internal ticket might map to
  multiple external tickets or vice versa.)
* I will set the "Original Estimate" of each ticket to the number of hours
  listed in the discovery document. If a ticket covers a larger story
  from the discovery document only partially, I will set its "Original Estimate"
  to an appropriate fraction of the number of hours listed in the discovery document.
  For any ticket that does not directly correspond to a story from the discovery document,
  I will set the "Original Estimate" based on the amount of time that I think
  will be required to complete the work, taking into account the number of hours
  that remain in the epic's budget at that point in time. 💡 *This will help me detect
  potential savings or -- more importantly -- potential cases of budget excess
  if the assignee of a ticket specifies a different estimate via the Remaining Estimate
  field later on.*
* On the [day of the epic update deadline](../processes/sprints.md#epic-update-deadline),
  I will post an epic update that uses the [epic update template](../epic_management.md#epic-update-template)
  and provides information about the current status of my epics.
* On the [day of the ticket creation deadline](../processes/sprints.md#new-tasks-deadline), I will assign
  upcoming stories from my epics that need to be done next sprint
  to myself - or I will ask others from my cell to be either the assignee or the reviewer.
* If I'm going on leave, I will follow the [vacation procedure](../processes/vacation.md), and hand off my responsibilities for my epic to
  Epic Reviewer.

### Epic Reviewer

Epic reviewers are assigned to the 'Reviewer' field on an Epic ticket. As an **epic reviewer**:

* I will keep up to date on the progress of the epic.
* I will review discoveries, planning, budget, and documentation written at the top level of the epic.
* I will read each epic update and give feedback as needed.
* I will be ready to perform the tasks and duties of an epic owner in case they go on vacation or are otherwise
  unavailable for an extended period.

### Client owner

* Client owners manage their relationship with their clients. All the epics and tasks from a
  given client are done within a single cell, the one the client owner belongs to, but see
  [cross-cell collaboration](../organization.md#cross-cell-collaboration) for some nuances around
  task reviews.
* Initial contact with prospects, estimations work. Note that a portion of Business development's
  time is assigned to each cell to do most of the initial contact and quote work with prospects.
* Client owners keep the [Client Briefings](../processes/working_with_clients.md#client-briefings) and [Value Propositions](../processes/working_with_clients.md#value-propositions) up to date.
* Client owners complete a [Client Strategy Ticket](../processes/working_with_clients.md#client-strategy-tickets) once per quarter.

For each client, we have a single person who is designated as the owner for that client, called
the "client owner." The client owner is responsible for handling most communications with that
client. The client owner is designated in [OpenCraft's CRM](https://opencraft.monday.com/boards/1077823105/),
along with the contact details and background for the client.

In general, regardless of which email address the client sent their question to, the client owner
should be the one who replies, though they may hand off any conversation to anyone on the team as
needed (such as their cell's [firefighter](#firefighting)). If someone else received the email, please "Reply All" to pass the
message on to the client owner for them to reply (add the client owner to the "To" field, and add
"OpenCraft <help@opencraft.com>" to the CC field, and say something like "Passing this on
to (client owner name)").

There are a few exceptions:

* If it's an urgent problem, whoever sees it first should reply and CC the [firefighters](#firefighting),
  help@opencraft.com, and the client owner. One of the firefighters should respond.
* If it's a general issue/question:
    * If the email is related to a specific project/epic with a different epic owner, that epic
      owner can reply directly (make sure the client owner and help@opencraft.com are CC'd).
    * If the client owner is away, their designated backup person (usually the reviewer on the
      "Support" epic/ticket for that client) should take over this role. (Or the firefighters
      if no backup person was planned.)

We have a few clients with monthly development budgets. For those clients, the assigned client owner
is also responsible for reviewing the monthly budget before and during each sprint,
to ensure that we are not going too far above or below the budget. They should refer to
[SprintCraft] which can monitor and project the status of each budget as
of the end of the upcoming sprint.

When we get a new client, or when the client owner changes (or we start working with a new
person coordinating things on the client's side), we send them
[a welcome email](https://docs.google.com/document/d/1TFBgmq5QA318IfVLBbg9owuqyVl7e4UX5ZWsFQl-u6o/edit)
explaining who the client owner is and how to contact us.

Client owners are in a prime position to anticipate the needs of our clients, which can help us start new projects with them. Read the (private) [Sales Opportunity: developer stories] forum thread for examples of how we've created solid client
relationships and handled challenging situations. Client owners are also advised to read the [Strategic Client Management Guide]. Take a bit of time each sprint thinking about new ways we might create value for our clients.

### Cell Supporter

**Role type**: This is a full time role, dedicated to helping the members of other cells.

**Description**: The main purpose of the Cell Supporter role is to help lighten the load of management work
that cell members have to take care of, so that they can focus more on coding/DevOps, and on handling their clients/epics.
The purpose of this role is not to take away cell members’ autonomy in making decisions about their projects,
nor to introduce another layer of management between them and their clients, nor to centralize cell-level decision making
in a single role. As mentioned previously, we believe that the people who are closest to a given client or project
are in the best position to make decisions about how to handle and progress that project.

*Note that cell supporters are not project managers, and they also do not manage client relationships.*

These duties are part of [epic ownership](#epic-owner) and [client ownership](#client-owner),
which are responsibilities to be taken on by core team members of individual cells.

#### Responsibilities

Responsibilities of a **cell supporter** include:

##### Cell management support

* Helping cells by taking on [cell management roles](#cell-member) that they are currently unable to fill themselves
(e.g. due to lack of interest or capacity).
    * Cell members who express interest in taking these roles get precedence over cell supporters
      when it comes to assigning cell management roles.
    * Cell supporters should not take all cell management roles for a specific cell,
      even if it would be possible to do so without impacting a cell supporter's capacity
      for fulfilling the remaining responsibilities listed below.
    * As a cell’s capacity and/or interests change, cell supporters may hand cell management roles back to the cell.
      To make sure that cell management roles do not remain assigned to cell supporters indefinitely,
      they perform a quarterly check-in to see if cells wish to take back any of their delegated roles,
      and if there are other roles that the cells need to delegate instead.
* Coordinating training initiatives for cell management roles, and making sure documentation on cell management roles
  is kept up-to-date.
    * For example, prior to a pending cell split, cell members that will be taking on a cell management role
      for the first time should get a chance to familiarize themselves with the new role
      before having to jump in head-first and being expected to handle the role according to our standards.
* Helping cell managers address specific challenges requiring cross-cell coordination.
    * For example, if more than one cell is facing capacity issues, it may be necessary to shuffle
      projects around between cells. The effort involved in leading this type of cross-cell coordination
      goes beyond the amount of work that is usually involved in epic planning (and when capacity is low
      it may be especially difficult to find time for additional planning work), so cells may decide
      to delegate this work to a cell supporter.

*Notes on capacity and budgeting:*

* As indicated above, the amount of cell management support that a cell supporter commits to
  should leave room for fulfilling the remaining responsibilities listed below.
* Cell management epics and tickets will stay with their respective cells, even if they are
  temporarily delegated to a cell supporter. This means that in terms of sustainability impact
  there is no difference between having cell members handle cell management roles and
  delegating them to cell supporters.

##### Process improvements

* Conducting process reviews, ideally at regular intervals, with the goal of keeping "process debt" in check.
* Coordinating work on implementing process improvements based on results of process reviews.
* Addressing individual shortcomings of internal processes that surface over time (such as sustainability
  blocking development, how to deal with sustainability in the context of cell splits, and
  improving coordination between cells and business development/marketing).

##### Cross-cell planning

* Taking care of scheduling upgrade epics:
    * Keeping track of the rotation of responsibility for upgrades between cells.
    * Informing epic planning managers of cells ahead of time that their cell’s turn is coming up,
      and that they’ll need to work on freeing up capacity as necessary.

##### Other responsibilities

* Helping the Open edX community in one of the [non-technical core contributor roles](https://openedx.atlassian.net/l/c/QEEtuNeo).
* Acting as a backup for other support roles (based on interest/availability).

### Business Development Specialist

As a Business Development Specialist, the main responsibility is to conduct market research, analyze and pursue new business opportunities, while ensuring clear, prompt, and professional communication with prospects, leads, and clients. This role is designed to bridge the gap between initial client contact and project handoff, providing support throughout the sales process and beyond.

#### Responsibilities

1. Communication Management
    * Ensure clear and prompt communication with prospects, leads and clients.
    * Respond to emails sent to contact@opencraft.com, ensuring that inquiries are addressed swiftly and appropriately.
      * Set the auto-responder for contact@opencraft.com when applicable during the [holiday vacation process](../processes/vacation.md#end-of-year-holidays).
    * Manage and respond to leads received through our CRM web-to-lead form.
2. Client and Project Coordination
    * Coordinate with the Epic Planning and Sustainability Manager of each cell to [assess availability](../processes/epic_planning.md#determining-capacity-for-upcoming-billable-work) and plan for new clients and projects.
    * Serve in an advisory role to client owners looking to propose new projects to existing clients.
3. Business Opportunity Identification
    * Regularly identify and evaluate new business opportunities that align with OpenCraft’s strategic goals.
    * Discuss strategies and time spent pursuing new opportunities with the sponsoring cell and, when necessary, consult with Xavier for key prospects.
4. Lead and Prospect Management
    * [When a lead becomes a prospect](../processes/sales/customer_lifecycle.md), ensure timely follow-up by providing information, [drafting quotes](../processes/sales/overview.md#writing-proposals), [creating discoveries](../processes/sales/overview.md#an-overview-of-a-sale-at-opencraft), and if needed, creating epics to track especially large discoveries (such as government [tenders or critical RFPs](../processes/sales/overview.md#request-for-proposal-rfp-and-tender-projects).)
   * Keep the CRM up to date and post notes about progress so team members can follow along.
5. Client Onboarding
    * Manage the onboarding of confirmed clients, ensuring they understand how to work with OpenCraft. This includes instructions on when/how to use the urgent@opencraft.com email address, when/how to use help@opencraft.com, who their main contact(s) should be and how billing works.
    * Draft the initial [Client Briefing](../processes/working_with_clients.md#client-briefings) and [Value Propositions](../processes/working_with_clients.md#value-propositions) in our CRM to ensure all team members and the client are aligned from the start.
6. Initial Project Coordination
    * Handle initial communication and coordination between new clients and the project cell until the official kickoff and handover to the client owner.
    * Ensure a smooth transition by clearly outlining project goals, timelines, and expectations to both the client and internal teams.
7. Communication Protocols
    * When communicating with clients, always ensure the relevant OpenCraft email addresses are CC'd to maintain transparency and proper documentation:
        * **contact@opencraft.com:** For matters related to leads, prospects, and quotes.
        * **billing@opencraft.com:** For invoices and billing-related communications.
        * **help@opencraft.com:** For all other non-financial communications.
8. Pricing and Quotation Management
    * Regularly review completed discoveries for any estimates that can be reused in future projects.
    * Add reusable estimates to the [Price list](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=1171479307) sheet in the epic planning spreadsheet, and update price items in Proposify as necessary.

#### Resources:

Consult the [Sales](../processes/sales/overview.md) section of the handbook for specific resources and processes.

### UX Designer

Responsibilities of a UX Designer include:

* UX Discovery: Informing engineers of the expected hours required to design a component, page, or interface.
* Wireframe Design: Designing accessible, consistent interfaces as intuitively as possible.
* UX Testing: Validating UX designs through user testing.

### Product Manager

Responsibilities of a Product Manager include:

* Product Design
    * Working in partnership with (or as) the [UX Designer](#ux-designer) to create the workflows for a product.
    * Setting vision for the features and user stories of a project.
* User Validation
    * Validating product design choices through user feedback and testing.

**The two product managers at the time of writing are Ali and Cassie.**

### Developer Advocate

Responsibilities of a Developer Advocate include:

* Monitoring the well-being of team members.
* Posting the [sprint check-in report](../processes/developer_advocacy.md#sprint-check-in).
* Checking for indicators of burnout.
* Proposing and designing solutions to improve personal satisfaction and work-life balance for team members.

### Sales Mentor

Responsibilities of a Sales Mentor include:

* Reviewing sales metrics.
* Designing sales and related training curriculum for team members.
* Developing proposals to increase sales performance.

### Chief Technology Officer (CTO)

* **Technical Arbiter**
    * Final arbiter for technical decisions
* **Technical Excellence**
    * Ensuring technical & product quality of everything we do
    * Ensuring software we produce is as open as possible and everything that can be contributed upstream is
    * Championing and improving processes (like our code review process) to improve the quality and efficiency of our software development
    * Reviewing code (in addition to the regular code review process), mentoring developers
    * Being aware of the "big picture" to help developers avoid duplicating each other's work
    * Being available on call 24/7 to respond to emergencies, if regular [firefighters](#firefighting) are not available
* **Epic/Client Owner Coach**
    * Supporting epic and client owners in client meetings, discussions, and in turning requirements into actionable plans/epics
    * Reviewing all discovery stories and estimates
* **Security Manager**
    * Ultimately responsible for security of our software, both proactively improving it and responding to incidents effectively

The CTO isn't part of a cell, but can review and participate in any cell's work as needed.

### Chief Executive Officer (CEO)

* **Business development**:
    * Additional relationship with key clients and prospects
    * Support role for the epic owner, in difficult situations or contract negotiations
    * Reviewer of quotes
    * Initiator for opportunities that are strategically important for OpenCraft, either
      because of size or side effects
* **Financials**:
    * Accounting
    * Invoicing (clients & team)
    * Forecasting
* **Admin**:
    * Holidays reviews
* **Legal**:
    * Relationship with lawyers
    * Contracts drafting and signature (team and clients)
    * Ownership of legal projects
* **Management**:
    * Management of non-technical matters for all cells
    * Management of CTO
    * 121 meetings: continuous rotation of all employees, at 2-3 meetings/week
    * Handle reports of performance issues & process to address it, including firing if needed
* **Hiring**:
    * Review of candidate selections by the team's recruitment manager
    * Contract negotiation with accepted candidates
* **Sprint management**:
    * Occasional second review of sprint deliverables, sprint planning & epics management on any
      cell
* **Strategy**: evolution of the company
* **Ocim**: product management and some code
* **Spreading the word** (mailing lists, conferences, opencraft.com)

### Marketing Specialist

As a **marketing specialist**:

* I will discuss and plan marketing strategies with the business development specialist, the CEO, and external marketing contractors
* I will schedule and coordinate tasks related to the agreed-upon marketing strategies
* I am responsible for coordinating maintenance, improvement, and all tasks related to the [OpenCraft website](https://opencraft.com)
* I am responsible for coordinating the publication of articles on OpenCraft's [blog](https://opencraft.com/blog)

### Administrative Specialist

As an **administrative specialist**:

* I will coordinate and execute all administrative tasks that are related to team member billing, client billing, invoice payments, bookkeeping, and other accounting-related matters at OpenCraft
* I will participate in the recruitment and onboarding [process](../processes/recruitment.md#administrative-specialist-recruitment-tasks-details) of new team members, and collect all necessary information to set up their invoicing process
* I coordinate the [end of year vacation planning](../processes/vacation.md#end-of-year-holidays).

### Lawyer

Our lawyer provides legal advice and helps us with contractual matters.

[epic planning spreadsheet]: https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit
[Sales Opportunity: developer stories]: https://forum.opencraft.com/t/sales-opportunity-developer-stories/1055
[SprintCraft]: https://sprintcraft.opencraft.com
[Strategic Client Management Guide]: https://docs.google.com/document/d/1bTgDk0NAj191hWNI-MSyM4Ihfa5DjlpDETLQddgyyro/
[Weekly Rotation Schedule]: https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit

[^1]: Private for OpenCraft Employees
