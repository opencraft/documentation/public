# Recruitment Email Template

Hi GREAT CANDIDATE,

My name is FIRST NAME. I am a Software Engineer at OpenCraft. I just reviewed your application and I was glad to see your contributions! I got interested in what you have shown me and in the work you have done. I would like to meet you.

The meeting will be in Zoom and should take about 30 minutes. It's the first contact. I'm using a tool to make it easier for you to pick up a time that works for both of us. Could you please book a time there?

https://CALENDLY.URL/

By the way, in case you are not using Zoom regularly, make sure you have its plugin installed: https://support.zoom.us/hc/en-us/articles/207373866-Zoom-Installers. Please, test it out, have a headset ready and a functioning microphone. It seems silly to say, but that's a frequent problem, :-)

Also, could you please have a development editor ready for some live coding? We will work on a short Python exercise. For that, please make sure you can share your whole screen using Zoom and double-check if your font is big enough for reading through the screen sharing. It won't be a tricky algorithmic problem, so please don't stress too much about it. 

Finally, I would like to record the meeting to share it with my team afterward - let me know if you have any issues with this, though!

I am looking forward to talking to you!

YOUR-NAME
@OpenCraft
