# Client: *FauxIT*

## Points of Contact:
* **Primary**: Person McPersonface someone@example.com
* **Billing**: Penny Wise billing@example.com
* **Client Owner**: Devy Developerson

## URLs:

* **CRM Entry:** *Link to CRM entry*
* **LMS:** https://lms.example.com/
* **Studio:** https://studio.lms.example.com/
* **Website:** https://example.com/

## Projects/Current Epics:

* Hosted OCIM instance (see LMS link)
* Walkthrough LTI Service https://github.com/example/Walkthrough
* *Link to Support epic*

## Contract Information:

* Contract period (i.e. rolling, yearly, etc.)
* Contract renewal date
* Retainer info (have they committed to x amount of hours per month with a notice period in exchange for a lower rate? If so, indicate what the notice period is. If not just write "standard rate, no notice period")

## Background

### General description of client

FauxIT is a web-based customer support  tool that provides  wiki, issue-tracking and continuous integration and deployment features, using an open-source license, developed by FauxIT Inc.

Recently, FauxIT became a public company. They believe that the problems solved by a ticketing system can be used to track any kind of work.

### What are the client's most important goals?

FauxIT wants to be an all-in-one customer support solution. They are building an open core service to host these features. They want to greatly enhance the usability of their solution by providing user resources and training to their customers.

Further, they want to expand the market available to include disciplines not typically covered by ticketing systems. As an adjunct to this, they’d like to build plugins and custom wrappers that help with different disciplines using their system.

### What challenges do they face in achieving these?

FauxIT faces challenges from proprietary solutions which have established market share and offer many of the same solutions FauxIT does. However, their competitors’ codebases are not open source, and so can’t be customized to the end organization’s specific needs.

Further, since they are trying to get other fields to use unfamiliar software, they need some way to convince clients that the software is useful and applies to their needs.

### How is OpenCraft helping them achieve their objectives or overcome their challenges?

OpenCraft is helping the team deploy their training via standard LMS courses to start.

OpenCraft is also integrating the ticketing system into the LMS to allow for tickets that track the learner’s training, so that concepts within the course line up with their activities.

### Is there more we could do to help?

OpenCraft could also create a custom integration, possibly via LTI, that instructs the learner how to use the software step-by-step, and actually follows along as they perform actions like ticket creation and scheduling, reporting progress back. This would allow the learners to ‘learn-while-doing’ and have confidence in their ability to navigate the software and see what it offers.

Since we specialize in Open edX integrations, and LMS capabilities more generally, we could develop plugins for FauxIT that assist with administrative tasks for institutions that fall outside the scope of the platform. Examples might be things like tracking course development, stakeholder onboarding, and cohort prep/assembly.

We could confer with existing clients that might be interested in funding or using these features, especially as they pertain to work we do with them (tracking tickets, etc). This would expand adoption of FauxIT’s platform while solving problems for us and our other clients. We're exploring use of their platform to track our work at this time.

Furthermore, we might even be able to do contracting work to expand the FauxIT platform itself, if we have team members familiar with the language it’s written in. After all, it’s Open Source, and we ourselves might find the software useful.
