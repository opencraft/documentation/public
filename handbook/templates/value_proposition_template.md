# Based upon FauxIT's desire to...

## ...accomplish this objective:

Become the leading all-in-one solution for customer support by building an open core service to host the features to accomplish this.

Greatly enhance the useability of their solution by providing user resources and training to their customers.

Expand their market to include customers not typically covered by ticketing systems by building plugins and custom wrappers that help these new customers to use their system.

## ...overcome this challenge:

Compete effectively against proprietary, closed source competitors.

## OpenCraft can help them do the following:

Expand their reach via online events by offering training and integration with learning systems.

Add integration for Open edX courses via LTI.

Build plugins to integrate FauxIT’s ticketing system with Open edX in order to better manage administrative tasks.

### ...which will help them to:

Get users up to speed faster, enabling them to more quickly and easily leverage their solution.

Begin attracting new customers to their solution who don’t normally use ticketing systems.

## OpenCraft is best equipped to help them, because:

Unlike other e-learning partners, OpenCraft’s core focus on Open Source solutions and methodology directly aligns with FauxIT’s mission and values.

OpenCraft’s deep knowledge of the Open edX platform and community engagement within the ecosystem gives us a great advantage in building key integrations and gaining community support for changes to the upstream codebase this may require.
