# Trial Project Email Template

Hi [CANDIDATE NAME],

Welcome to OpenCraft. Thank you for taking the time to participate in the Trial Project as a part of our hiring process. Here are some details to get you started:

1. **Trial Project Ticket** - The ticket you will be working for the next 2 weeks is [JIRA-ID](https://tasks.opencraft.com/browse/JIRA-ID). You have already been added to our JIRA system and should have received an email regarding it. Kindly create a password when you login and let me know if you run into issues while accessing the ticket. The ticket should have the necessary information about the project.
2. **Communication** - There are 2 ways to communicate with the team members, and you should have received email invites for both of them.
    * **JIRA** - The ticket you have been assigned is a good place to discuss anything regarding the Trial Project and the reviewer on the ticket will be your mentor and primary contact during the tria project and as a newcomer if you are inducted into the team.
    * **Mattermost** - This is our Slack like chat system that you can use to talk to the entire team. This will be very useful to ask questions that are general in nature like the team process, a coding bug, or anything you need help with.
3. **Mentorship** - In order to support the candidates during the course of the trial project, they are assigned with a Mentor. In your case, it will be [TRIAL PROJECT REVIEWER]. Kindly reach out to them proactively if you are unsure about anything.
4. **Time Tracking** - As the Trial Project is a timeboxed project of 20 hours, kindly log all the time you spend working on the ticket in near-realtime. You can use the tracker available on JIRA under "Tempo" top menu -> "Tracker". This will open a time tracker on the bottom right corner that you can start and stop while you work. OR you can use any time tracking tool of your choice and enter the value at the end of the day using the "Log Work" button below the description of your ticket. For more details on time management, kindly refer to our [handbook page on Time Management](https://handbook.opencraft.com/en/latest/time_management/).

All the best. Looking forward to seeing you ace the trial and join the team.

Regards,
[RECRUITMENT MANAGER]
