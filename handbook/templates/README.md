# Templates Directory

The templates in this directory are not included in the navigation for the handbook. Further, they are not rendered by MkDocs, and so you can't link to them even if you get the relative pathway right. However, you can link to their presence in the repository instead, if you need to link them within the handbook.

If you really need to render a template in the navigation, you will either need to make it a real page, or make some significant alterations to the MkDocs configuration/plugins.

One advantage of NOT rendering the templates in this directory with MkDocs is that the GitLab renderer is much simpler in its output. This makes it useful to copy the formatted text with more fidelity than MkDocs would. As an example, every header in a MkDocs render (with the template we're using) creates a link anchor point with a hidden symbol, ¶, next to it, which will appear where you paste the copied text.

You can build the URL to a template with this snippet:

```md
{{ config.repo_url }}-/blob/{{ git.commit }}/handbook/templates/template_filename_here.md
```

This will render a URL based off the Repo URL set in the mkdocs configuration parameters and the current git commit. It assumes GitLab is the host, since that's how GitLab constructs the relevant URL for rendering a previewable file in a repository.
