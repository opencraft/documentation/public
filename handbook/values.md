# Our values

**OpenCraft** is a service company specializing in the edX platform.  The core values of OpenCraft are *openness*, *quality*, *commitment* and *empathy*.
  
* *Openness*: We believe strongly in open source, transparency and honesty. We aim to contribute most of our work to the community as open source software. We want to ensure our work benefits the community, and even humanity as a whole, as much as it benefits our clients. We are radically honest when reviewing or providing feedback to each other.
* *Quality*: We create high-quality code and provide professional support for our work. All team members are senior professionals. All developers have extensive experience in Python, Javascript and most of the technologies used by the Open edX project.
* *Commitment*: We do what we say, and do it on time.
* *Empathy*: We actively pay attention to how clients or other team members feel about the way we act, and aim to be kind. Even if sometimes we have to hurt feelings or take painful decisions, it's not unnecessarily so.

## Our vision

OpenCraft focuses on performing very high quality work in a sustainable way, specifically open-source work which contributes to the whole community. We want our work to contribute to great human endeavors - such as education. And unlike most companies and all startups, we aren't focused on hypergrowth and profits, but try to grow in a way that provides a nice lifestyle (remote work, flexible hours, expertise and growth) for everyone on the team.

We want to grow, and don't put a limit on our potential size, but only as long the size doesn't jeopardize the lifestyle or atmosphere. When it does, we stop growing to make sure we are actually able to handle our current size before considering growing bigger.

We also try to maximize how we benefit from automation, to keep us humans busy with the creative/non-automatable work, while delegating the things that are automatable to algorithms. If you know The Culture series by Iain Banks, that should give you a pretty good idea of a (fairly remote) end goal for that aspect of OpenCraft. ;p
