javascript:(async function() {
    const ISSUE_KEY = JIRA.API.IssueSearch.getActiveIssueKey();
    /* The id of the 'Technical task' issue which can be a sub-task issue-type. */
    const SUB_TASK_ISSUE_TYPE_ID = '10002';
    const JIRA_SITE_URL = 'https://tasks.opencraft.com';
    const JIRA_API_URL_PREFIX = `${JIRA_SITE_URL}/rest/api/2`;
    const ISSUE_DETAILS_URL = `${JIRA_API_URL_PREFIX}/issue/${ISSUE_KEY}`;
    const JIRA_CUSTOM_GET_GROUP_MEMBERS_URL = (
        `${JIRA_SITE_URL}/rest/scriptrunner/latest/custom/getGroupMembers`);

    const issueDetailResponse = await fetch(ISSUE_DETAILS_URL);
    if (issueDetailResponse.status !== 200) {
        let jsonResponse = await issueDetailResponse.json();
        throw Error(
            `[Unable to fetch issue details]: ${jsonResponse.errorMessages}`);
    }
    let issueDetails = await issueDetailResponse.json();
    const issueFields = issueDetails.fields;
    const projectKey = issueFields.project.key;
    const projectName = issueFields.project.name;

    const groupMembersResponse = await (
        await fetch(JIRA_CUSTOM_GET_GROUP_MEMBERS_URL)).json();
    if (!groupMembersResponse.hasOwnProperty(projectName)) {
        throw Error(`Invalid team name: ${projectName}`);
    }
    let teamMembers = [];
    for (let member of groupMembersResponse[projectName]) {
        teamMembers.push(member.username);
    }

    const teamMembersString = teamMembers.join(',');
    const userPromptMessage = (
        `Creating sub-tasks for the following members:
        (You can add/remove members if needed)`);
    let updatedNamesString = prompt(userPromptMessage, teamMembersString);
    if (updatedNamesString === null || updatedNamesString === '') {
        alert('Creating sub-task proccess stopped!');
        return;
    }
    if (updatedNamesString !== teamMembersString) {
        teamMembers = [];
        for (let memberName of updatedNamesString.split(',')) {
            teamMembers.push(memberName.trim());
        }
    }

    let issueUpdates = [];
    for (let member of teamMembers) {
        let updateFields = {
            project: { key: projectKey },
            parent: { key: ISSUE_KEY },
            issuetype: { id: SUB_TASK_ISSUE_TYPE_ID },
            summary: `[@${member}] Subtask: ${issueFields.summary}`,
            /* Add link to the original issue in the sub task description. */
            description: (
                `Please check the original task: ${JIRA_SITE_URL}/browse/${ISSUE_KEY}`),
            assignee: { name: member },
            /* Set the assignee as the reviewer. */
            customfield_10200: { name: member },
            /* Set the sub-task's account same as the original task account value. */
            customfield_10011: issueFields.customfield_10011.id,
        };
        issueUpdates.push({ fields: updateFields });
    }

    const response = await fetch(`${JIRA_API_URL_PREFIX}/issue/bulk`, {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({ issueUpdates }),
    });

    if (response.status !== 201) {
        let jsonResponse = await response.json();
        throw(`[Unable to create sub-tasks] ERROR: ${jsonResponse.errorMessages}`);
    } else {
        alert(
            'Successfully created all the sub-tasks. ' +
            'Click "Okay" to reload the page to see the changes.');
        location.reload();
    }
})().catch(error => { alert(`ERROR: ${error}`); throw error; });
