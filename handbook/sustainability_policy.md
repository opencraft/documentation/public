# OpenCraft's Sustainability Policy

## Vision, values and principles

Our sustainability policy aligns with our [vision](http://opencraft.com/doc/handbook/values/#our-vision) and [core values](http://opencraft.com/doc/handbook/values/#our-values), which are **openness**, **quality**, **commitment**, and **empathy**.

The way we approach our work is also guided by commitments to the following principles:

**Maintainability**: We provide the real long-term cost of the solutions we provide from the beginning, and avoid trapping clients into cycles of ever-increasing maintenance fees.

**Contributions**: Open edX is a community-driven project and the feature contributions it receives are essential to its success. We thus contribute most of our work to the community as open source software. We want to ensure our work benefits the community, and even humanity as a whole, as much as it benefits our clients.

## Issues and key environmental and social impacts of our business

Producing and using software are activities that have an **environmental impact**.

In this respect, we have prioritized the maintainability and long-term reusability of software and the sustainability of our business practices.

Our activities also have a **social impact**. Our core business is development of the Open edX platform, which was created and released as open-source with the goal of building a worldwide community of educators and technologists who share innovative solutions to benefit students everywhere. An important portion of Open edX’s users have very little budget or resources, making accessibility an important aspect of the project.

In this respect, the issues we have prioritized are to ensure that our work benefits the greatest amount of people, especially within the communities in which we evolve and interact with, and responsible treatment of our employees and co-workers.

## Our commitment and scope

This policy applies to all work done by our organization. Staff and contractors are expected to uphold objectives under this policy to the fullest extent possible within prevailing budgets. And to inform management of financial and other barriers to achieving goals.

We acknowledge we have limited influence over third parties we work for or services we use, whose events we may attend such as conferences, trade shows or business meetings. While we cannot control the decisions of these parties, we commit to educate them of our policy and encourage them to align their practices with our policy objectives.

Our attention to environmental, social and economic responsibility includes working within the law and matching or exceeding legal requirements.

## Objectives

We commit to:

### Maintaining sustainable coding practices

- Produce high-quality code using popular open-source technologies and frameworks
- Facilitate long-term maintenance by:
  - Writing code that complies with best practices oriented toward long-term sustainability
  - Contributing changes and features we develop by modifying open source projects (as per the [GPL License](https://www.gnu.org/copyleft/gpl.html)) to those same projects. Sometimes clients will opt to not open source and contribute some of the software they develop independently - we will respect that choice, but encourage them to release all their code as open source software.
  - Contribute code from code drift of existing setups that we take over

### Giving back to communities

 - For most of our work, release it under open source licenses, and contribute it to the upstream projects we are modifying, to benefit the whole community of users, and facilitate access to online education around the world
 - Provide free course hosting for select community projects
 - Remain involved in the Open edX community:
   - By providing support to users, stakeholders and collaborative development projects
   - By contributing talks and workshops at the annual Open edX Conference
  - Provide monetary or in-kind support to open source digital initiatives
  - Provide monetary or in-kind support to charities or Non-Governmental Organizations

### Minimizing environmental impacts of our business practices

 - Allow team members to work from any location they want, as long as they have access to a broadband internet connection, which reduces commuting
 - Ask team members to attend meetings remotely instead of travelling when an in-person meeting wouldn't provide a significant advantage over the remote meeting.
 - Provide public transit and bike sharing passes when attending conferences or doing team retreats, when the location makes it practical
 - Review the efficiency of data center cooling technologies when selecting our server providers. The Power Usage Effectiveness of our current providers is 1.2 or lower.
 - Offer vegetarian or vegan meals as the default option for staff gatherings and business meals

### Providing staff with opportunities for professional growth and development

  - Offer newcomers an onboarding course
  - Offer newcomers a mentoring program
  - Offer remuneration to team members for improving their professional skills through activities such as reading books and taking courses

### Overseeing application of this policy

The [Administrative Specialist](roles/list.md#administrative-specialist) is responsible for communication and application of this policy.

## Reporting

We commit to report on our progress against a set of chosen indicators and share that report upon request.
