# Glossary

There's plenty of jargon that goes around which may overwhelm a newcomer. See [edX's glossary](http://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/glossary.html) for any terms that may not be listed here.

If you feel there's a piece of lingo that is neither covered by [edX's glossary](http://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/glossary.html) nor by this document, [let us know](https://gitlab.com/opencraft/documentation/public/-/issues), or open a merge request to add it in.

A note on entries: We try not to create new acronyms. We do use standard industry acronyms. [Acronyms seriously suck](https://gist.github.com/klaaspieter/12cd68f54bb71a3940eae5cdd4ea1764). We have enough terms to memorize-- so try to avoid introducing new acronyms and encourage others to do the same.   

## Apros

A custom LMS front-end, used by one of our clients (Yonkers). It replaces the standard Open edX LMS front-end.

## Calendly

Calendly is a handy web service that allows you to share a link so that people can easily choose a time for a meeting based on your availability. See [Calendly's homepage](https://calendly.com/). The [Administration Specialist](roles/list.md#administrative-specialist) can create a Calendly account for any team member upon request.

## Code drift

This refers to changes in forks that aren't present in upstream. Code drift must be maintained (ported, tested, sometimes rewritten) across new release versions, and so adds to our maintenance costs. OpenCraft aims to minimize code drift by upstreaming as much as possible and consolidating the rest into common branches that are shared among several client sites.

## Consul

Software that stores current infrastructure information and configuration. It allows us to configure various services across dynamic and distributed infrastructure.
 See [Consul's homepage](https://www.consul.io/).

## Customer Lifecycle

The customer lifecycle is the path a prospective customer takes from a lead to a paying customer. See the [sales documentation](processes/sales/customer_lifecycle.md). 

## Crafty

In-house Jira bot used to simplify some actions like filling spillover reason to a Google Spreadsheet directly from Jira. See [Crafty' repository](https://gitlab.com/opencraft/dev/crafty).

## CRM

Customer Relationship Manager. A CRM is where we track sales leads and current clients. Check the list of current clients and read about them [here](https://crm.opencraft.com/index.php?action=ajaxui#ajaxUILoc=index.php%3Fmodule%3DAccounts%26action%3Dindex%26parentTab%3DSales), and our current sales leads [here](https://crm.opencraft.com/index.php?action=ajaxui#ajaxUILoc=index.php%3Fmodule%3DLeads%26action%3Dindex%26parentTab%3DSales).

## Discovery

The process of "discovering" the set of tasks from client requirements. This is usually the first step when an [epic](#epic) is created. A completed discovery involves scoping the tasks, making [time estimations](how_to_do_estimates.md), and assessing the level of effort needed for the work.

## ELK

A software stack composed of Elasticsearch, Logstash and Kibana. This stack is moslty used to store logs, metrics and to create dashboard. We are actually using the ELK stack for [logs.opencraft.com](https://logs.opencraft.com/). See [ELK's homepage](https://www.elastic.co/what-is/elk-stack).

## Epic

A big chunk of work that has one common objective. It could be a feature, customer request or business requirement. These would be difficult to estimate or to complete in a single iteration. Epics contain smaller tasks meant for iterative completion over one or more sprints.

## Firefighter

A firefighter is a sprint's facilitator. The [firefighter's responsibilities](roles/list.md#firefighter) include handling emergencies, attempting to unblock people, watching over potential spillovers, and more.

## Gandi

Gandi is a French company providing domain name registration, web hosting and related services. We are using it as our DNS provider. See [Gandi's homepage](https://www.gandi.net/en).

## GDPR

Short for "General Data Protection Regulation". This is a regulation in European Union law on data protection and privacy in the EU. See [GDPR's Wikipedia page](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation).

## Honcho

Honcho is a handy Python tool used to run [Procfile](https://devcenter.heroku.com/articles/procfile) files. Honcho is actually [used by Ocim](https://ocim.opencraft.com/en/latest/installation/#process-description). See [Honcho's homepage](https://honcho.readthedocs.io/en/latest/#).

## IDA

Short for "Independently Deployable Application". These are separate applications which may integrate with Open edX via APIs or by sharing authentication.

## Institutional

Institutional customers, as opposed to [Pro & Teacher](#pro-teacher) customers, are customers that have signed up for higher tier Institutional Plan with a stronger SLA, more flexible feature options, and a [Client Owner](roles/list.md#client-owner). Typically, these are larger organizations looking to use the full power of the platform. Institutional customers frequently work with us to create new and custom features.

## Lead

A sales lead is a potential customer. See the [customer lifecycle](processes/sales/customer_lifecycle.md).

## Lead Prioritization Rubric

The Lead Prioritization Rubric is the tool used for evaluating the priority of different leads we might pursue. See [Lead Prioritization](processes/sales/lead_prioritization.md).

## LMS

[Learning Management System](https://en.wikipedia.org/wiki/Learning_management_system)

## Mattermost

Open-source instant messaging tool that OpenCraft uses for internal communication, hosted at [chat.opencraft.com](https://chat.opencraft.com/). See [Mattermost's homepage](https://mattermost.com/).

## New Relic

Web service used to store software metrics and errors. See [New Relic's homepage](https://newrelic.com/).

## Ocim

Short for "OpenCraft Instance Manager". It's our in-house, open source deployment service for Open edX instances,
which:

* Manages configuration and automates deployments for client production sites.
* Provides continuous integration by watching our PRs against the `edx/edx-platform` repository and automatically spinning up a sandbox for the PR's version of the platform. See [how to spin up sandboxes](https://gitlab.com/opencraft/documentation/private/blob/master/docs/howtos/sandboxes.md)[^1] and the [Ocim documentation](https://ocim.opencraft.com/en/latest/).

## Opsgenie

Atlassian tool used to alert and page team members in case of incident. See [Opsgenie's homepage](https://www.atlassian.com/software/opsgenie).

## OSPR

Short for ["Open Source Pull Request"](processes/pull_request.md#ospr). This is the edX process for reviewing pull requests from the open source community.

## OVH

The [OpenStack](https://www.openstack.org/)-based cloud computing service we use to host OCIM VMs and more. See [OVH's homepage](https://www.ovh.co.uk/).

## Pro & Teacher

Pro & Teacher was the name of our SaaS offering of the Open edX platform. Pro & Teacher clients were clients that had registered on our website and had an instance automatically created for them. We included basic features, maintenance and backups. Please DO NOT abbreviate this to "P&T." If you must use shorthand, use "Pro Plan." See this notice on [acronyms](https://gist.github.com/klaaspieter/12cd68f54bb71a3940eae5cdd4ea1764).

In the latter half of 2022, the Pro & Teacher plan was discontinued to allow our team to focus on our niche of providing premium Open edX development and support services.

## Points

Also known as "story points", these represent the approximate "level of effort" and time a task would require. See [Task
Workflows](task_workflows.md#general-tasks) for a description of how we use points. The cells collectively vote on story
points for the tasks in the upcoming sprint, see [Process for sprints](processes/sprints.md) for details.

## Prometheus

Database used to store metrics which can also be used for alerting, hosted at [prometheus.net.opencraft.hosting](https://prometheus.net.opencraft.hosting/). See [Prometheus's homepage](https://prometheus.io/).

## Proposal

A formal document (or set of documents) delivered to a client detailing the work to be done and the cost of performing that work.

## RFP

Request for Proposal. Especially large or organized companies that want a project done will publish a document specifying their needs for a particular project. Companies (such as ours) are encouraged to create a proposal in response to this request for their consideration. If they accept our propsosal, they become a paying customer. See also: [tender](#tender)

## Qualification

In sales, qualification is the process of determining whether a lead is a good fit for our team. Factors which impact whether a lead qualifies include budget, willingness to engage in open source methodologies, and more. See [Lead Prioritization](processes/sales/lead_prioritization.md).

## Quote

See [Proposal](#proposal).

## Sandbox

Represents an environment that should be isolated from the production one. It allows us to test things without potentially breaking the production.
This term could refer to [Ocim instances](https://gitlab.com/opencraft/documentation/private/blob/master/docs/howtos/sandboxes.md)[^1] created to test pull requests.

## Scope of Work

A Scope of Work document (commonly abbreviated SoW or SOW in most contracting industries) is a document detailing the work that a contracted team will be performing for a project. It includes milestones, deliverables, and other information on how the project is to be managed and delivered.

In most cases, we use our [Discovery documents](how_to_do_estimates.md) and [proposals](processes/sales/overview.md#writing-proposals) in place of a Scope of Work, but sometimes a client requests a Scope of Work in a specific format, usually as part of responding to an [RFP](#rfp) or [Tender](#tender).

## Story

Also know as "user story", is a specific Jira ticket that represents a user need or a specific scenario. This is the most common Jira ticket type we are using.

## Tender

A tender is a highly formalized [request for propsal](#rfp) published by a government entity. Most tenders require the use of an intermediary company incorporated within their country to act as a middle-man for these proposals. Tenders are best bid on when we have assisted in their creation, or have been contacted ahead of time by the entity planning to publish one.

## Task estimation / Agile poker

Also know as "planning poker" or "Scrum poker" is a gamified technique for estimating tasks. We are exclusively using the [agile-poker Jira plugin](https://marketplace.atlassian.com/apps/700473/agile-poker-for-jira-planning-estimation) for our [estimation session](processes/sprints.md#estimation-session).

## Tempo

Atlassian's tool to track time which is integrated with Jira. We use it to log time against Jira tickets and track epic budgets.
. See [Tempo's homepage](https://marketplace.atlassian.com/apps/6572/tempo-timesheets-time-tracking-report?hosting=cloud&tab=overview).

## Timebox

Maximum amount of time allocated to a task -- going over the timebox is not permitted. If it looks like there is a risk of going over the timebox, ping Braden and/or the epic owner to discuss.

We sometimes also use timeboxes to manage our sprint commitments. For instance, if a task needs to be started during a
given sprint, but the assignee does not have time to complete it, we may timebox the task to a maximum amount of hours
for the current sprint, and complete the remaining work during the next sprint. Because this affects sprint commitments,
this type of task timeboxing must be decided before the sprint begins, and agreed to by the epic owner.

[^1]: Private for OpenCraft Employees