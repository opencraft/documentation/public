# Developer Advocacy

At OpenCraft, our team members are our greatest resource. We structure ourselves to make decisions as peers, avoiding top-down [decision-making](./making_decisions.md) where possible. In a traditional setting, it is the job of managers to try and resolve issues that face the team at large or which are causing a particular team member trouble.

Of course any person who has worked in a traditional environment knows that a hierarchical manager has incentives which aren't always in line with the team at large-- they may push the team forward without examining why they are stuck, or they may choose punitive measures when other options are available. Having a flat structure means we get to solve our own problems without the incentives of a middle manager working against us. Our processes give each member the power to make changes. However, it does make certain classes of problems harder to solve. For example:

* Issues which are large enough to cause team members issues, but too small for individual developers to feel an immediate need to fix
* Issues which everyone is experiencing but is unaware others are experiencing
* Issues for which it is quite unclear who is best positioned to tackle them
* Issues which primarily affect one person where they need a bit of help to get 'unstuck'

For these issues, the Developer Advocate can assist. The Developer Advocate's job is not to solve these problems, but to facilitate their solution. They are not a manager, but someone to help push things along. They bring team members' attention to issues which affect the group, and may also propose solutions for the team to review. They meet with team members which have been struggling to try to help them out-- usually by suggesting tweaks in their processes, or process tweaks for the group at large if the situation warrants.

Note that a developer advocate always seeks to have team members solve their own issues first. When the individual team member cannot solve an issue on their own, the developer advocate encourages them to start a discussion with the team to resolve larger issues. When other options are exhausted, the developer advocate may start an initiative directly, but this is a last resort.

## Sprint Check-in

Each sprint, everyone on the team fills in two questions on their sprint checklist which are not directly related to sprint planning. The first is a rating of how their sprint felt overall, and the second is asking what caused that feeling. These questions are used by the [Developer Advocate](../roles/list.md#developer-advocate) to analyze the team's current sentiment toward work, and identify if any team members need extra support.

### Analysis

After a sprint has kicked off, all team members should have completed their sprint checklist in [Listaflow](https://www.listaflow.com/). Listaflow will then allow the advocate to review a report showing the answers from the team members-- both their number values, and their sentiment. It will also show the completion rates of sprint checklist tasks, which may be helpful for diagnosing process issues.

The Developer Advocate must not only look at the last sprint's values, but must consider the values over time, looking for any patterns in the data-- common complaints-- anything the team seems confused or unhappy about, are the impetus for investigation. They then write up a summary of their findings on the [forum post](https://forum.opencraft.com/t/stress-levels-statistics-and-analysis/470/)[^1], logging time for this analysis on [this ticket](https://tasks.opencraft.com/browse/STAR-2219)[^1].

### 121 Meetings

They also check to see if any particular person has been having rough sprints, and the reasons for them. If someone has had an especially bad sprint, or a series of low-rated sprints, the Developer Advocate schedules a 121 with that team member to check in with them learn more about the issues affecting them. Much like with group problems, the Developer Advocate does not aim to solve the problem for the team member directly-- but rather assist them in solving it themselves, or if it's part of a larger issue, coordinate bringing it before the team to resolve.

## Facilitating Solutions

As mentioned earlier, a Developer Advocate's job is not to solve all team-wide problems, but to facilitate solutions to problems that arise despite our processes. The Developer Advocate acts as an advisor, and does not prescribe top-down solutions. Some examples of things a Developer Advocate might do:

* Encourage a team member to start a forum thread to offload roles if they are over-committing 
* Offer advice on tackling a persistent issue with a particular client
* Suggest scoped time off or time off if they think the team member would benefit most from a break
* Suggest a process change for the team member specific to them
* Identify a larger cause for an issue the team member is experiencing acutely, and encourage the team member to start a forum discussion on the matter.
* Suggest an automation ticket that might obviate the team member's problem

Things a Developer Advocate should avoid doing:

* Complete a team member's ticket for them (it's preferable to find a way to prevent the overcommitment or adjust expectations to spreading the over commitment to two people)
* Take over a user's role for them (same issue-- the Developer Advocate MIGHT be a good person for that role, and if they have the bandwidth and are interested, it might be OK, but they should not take the role out of an obligation to relieve the overcommitment)
* Suggest time off as the first and/or only course of action. Everyone needs some time off, but if the team member is likely to dread coming back to work because larger issues are not resolved, this only kicks the can down the road.
* Suggest team-wide process changes when an issue does not seem to affect the entire team, and it's within the team member's power to resolve the problem.
* Suggest a team member depart the team for another opportunity. It's conceivable this might be the best option for the team member and the team, but in most cases there are less disruptive solutions where everyone wins.

Some things a Developer Advocate straight up can't do:

1. Fire a team member or take disciplinary actions. That's not a power the Developer Advocate has.
2. Make unilateral decisions for the team to solve an issue. They must follow the [decision-making](./making_decisions.md) process like everyone else.

## Contacting the Developer Advocate

The Developer Advocate is available to assist even outside the sprint check-in. You may contact the Developer Advocate any time, on behalf of yourself or others! Just check the [Cell Listings](../cells/list.md) to see who has the Developer Advocate role and email them.

[^1]: Private for OpenCraft Employees