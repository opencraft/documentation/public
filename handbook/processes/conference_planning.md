# Conference Planning Guide

## Introduction

This guide is for Conference Epic Owners, providing steps to coordinate our team’s participation in the annual Open edX Conference and OpenCraft’s Coworking Week. As per the commitments outlined in the contract and in the [handbook](../roles/list.md#what-is-expected-from-everyone), conference attendance is mandatory for anyone who joined OpenCraft after July 2018. Additionally, all team members must submit a talk proposal, regardless of attendance, and present it if able to attend. If a proposal is not accepted, attendees will co-present with another OpenCraft member.

The conference spans four days (typically Tuesday to Friday), followed by the coworking week, which begins the day after the conference and runs from Saturday to Saturday.

## Pre-Conference Planning

### Announce the conference

* If an announcement hasn't yet been posted to the forum, create a forum thread ([like this one](https://forum.opencraft.com/t/open-edx-conference-2025-coworking-week)) with details such as location, dates, and the name of the epic owner (that’s you!) Include a poll where team members can indicate whether they will be able to attend
* Ask team members to check if they need a visa (or visas, if the conference and coworking week are in different locations)
* This thread will serve as the central hub for all conference-related discussions (a separate thread will be created for the coworking week; see below)
* All conference/coworking-related forum threads should be in the “Discussions - Public” category and tagged with “conference”

### Select a Location for the Coworking Week

* The coworking week may be held in the same location as the conference, but not always. This decision often depends on the costs of accommodation, dining, and transportation at the conference location. It’s sometimes more cost-effective to host the coworking week in a different city or even another country. The Epic Owner usually selects a location after conducting research, though sometimes the team votes on a few location options
* The location is typically chosen based on the availability of coworking spaces, quality dining options, walkability, and easy access to public transport (with Uber as a plus since the team can use OpenCraft’s account)

### Create a Conference Epic

* Create a new epic for the conference, adding yourself as the assignee, and Xavier Antoviaque as the reviewer
* [Here is an example](https://tasks.opencraft.com/browse/MNG-4429) from a previous year

### Create a Budget Spreadsheet

* Begin drafting a budget spreadsheet for the conference and coworking weeks. Include anticipated costs such as conference tickets, [sponsorship tier](https://openedx.org/wp-content/uploads/2023/10/Open-edX_2024-Sponsorship-Tiers-1.pdf), estimated flight costs, visa costs, accommodation, client meals, team T-shirts, team dinner, coworking space, and professional photographer (optional). The budget may look something [like this](https://docs.google.com/spreadsheets/d/1QGXRNsyTfsdFJEXw7K2-0A1uvHqPpWJRDTPvwy_TAdA)
* Factor in the following expenses for each attendee, keeping in mind that these amounts may vary annually: 

    * Conference planning: Visa, flights, mobile data - **1 hour**
    * Time selecting conference and coworking accommodation - **0.5 hours**
    * Brainstorm talk topic and submit first draft proposal - **2.5 hours**
    * Refine and resubmit talk proposal - **1 hour**
    * Prepare conference talk - **20 hours**
    * Conference attendance - **12 hours**
    * Transport during conference week - **50 Euros**
    * Transport during coworking week - **50 Euros**
    * Mobile data - **20 Euros**

* Include allowances for the following Epic Ownership tasks:

    * Preparing the budget and Discovery
    * Sourcing and booking accommodation
    * Finding and booking a coworking space
    * Conference admin 
    * Discussions (visas, conference, coworking etc)
    * Preparing cover letters for visa applications
    * Buying conference tickets 
    * Scheduling tickets for the team
    * Writing an epic retrospective

* Add a link to the Budget in the Epic ticket

### Arrange Accommodation

* We tend to use Airbnb for team accommodations during the conference and coworking weeks, encouraging team members to share apartments to minimize costs. We set a budget per person per night based on the average accommodation cost for the location
* Apartments should have wifi and kitchenettes, and ideally be within walking distance of the conference and coworking space
* Create a list of accommodation options and add them to the budget spreadsheet ([here’s an example](https://docs.google.com/spreadsheets/d/1QGXRNsyTfsdFJEXw7K2-0A1uvHqPpWJRDTPvwy_TAdA/edit?gid=1444861959))

### Create a Discovery Document

* Add a folder to the [Shared OpenCraft Drive](https://drive.google.com/drive/u/1/folders/0BwOvzxi2VL0AMFFjN3pBdnVQakU) titled with the year of the conference. Store all conference and coworking-related documents here
* Create a Discovery Document similar to [this one](https://docs.google.com/document/d/1fOSJdp_cgV6UMYtGOtK_lGvQoQkEAwVJOuVwWp5CsJA). While the content is generally consistent each year, be sure to include any additional information relevant to the specific location.
* Add links to the Epic and Budget at the beginning of the document, and seek approval for both from Xavier Antoviaque
* Add a link to the Discovery in the Epic ticket

### Budget Revisions

If unexpected costs arise, add a new tab ([like this one](https://docs.google.com/spreadsheets/d/1QGXRNsyTfsdFJEXw7K2-0A1uvHqPpWJRDTPvwy_TAdA/edit?gid=112746960)) to the budget spreadsheet, get approval from Xavier Antoviaque, and update the budget total. Be sure to mention any approved budget revisions in the Discovery document.

### Track Actual Costs

* If the estimated costs in the Budget don’t match the actual expenses, don’t adjust the original numbers. Instead, create a separate spreadsheet to track the actual costs. This will help us compare estimates to real expenses and improve future planning
* Create a spreadsheet similar to [this one](https://docs.google.com/spreadsheets/d/1PGd29hAhGwmpgdUOwojWmXc9PyT10iPrFJNdjP87xgw) and record the actual costs there
* Add a link to the spreadsheet in the Epic ticket

### Create Jira Tickets

During the epic, you will create Jira tickets for each attendee and for yourself as the Epic Owner. Assign these tickets to the most appropriate sprints based on the timing.

#### Tasks for each attendee:

* [Conference planning: Visa, flights, mobile data](https://tasks.opencraft.com/browse/BB-8530) - **1 hour**
* [Time selecting conference and coworking accommodation](https://tasks.opencraft.com/browse/BB-8562) - **0.5 hours**
* [Brainstorm talk topic and submit first draft proposal](https://tasks.opencraft.com/browse/FAL-3555) - **2.5 hours**
* [Refine and resubmit talk proposal](https://tasks.opencraft.com/browse/STAR-3447) - **1 hour**
* [Prepare conference talk](https://tasks.opencraft.com/browse/FAL-3670) - **20 hours**
* [Conference attendance](https://tasks.opencraft.com/browse/STAR-3636) - **12 hours**

#### Tasks for you as the Epic Owner:

* [Preparing the budget](https://tasks.opencraft.com/browse/STAR-3355) (includes sourcing accommodation and coworking spaces)
* [Creating the Discovery](https://tasks.opencraft.com/browse/STAR-3377)
* [Conference admin](https://tasks.opencraft.com/browse/STAR-3023) 
* [Conference discussions](https://tasks.opencraft.com/browse/STAR-3358)
* [Visa discussions](https://tasks.opencraft.com/browse/STAR-3359)
* [Preparing cover letters for visa applications](https://tasks.opencraft.com/browse/STAR-3555)
* [Arranging coworking space](https://tasks.opencraft.com/browse/STAR-3361)
* [Booking accommodation](https://tasks.opencraft.com/browse/STAR-3504)
* [Buying conference tickets](https://tasks.opencraft.com/browse/STAR-3357)
* [Epic retrospective](https://tasks.opencraft.com/browse/STAR-3694)
* Scheduling tasks for the team:

    * [Brainstorm talk topic and submit first draft proposal](https://tasks.opencraft.com/browse/STAR-3352)
    * [Arranging flights, visas, and mobile data](https://tasks.opencraft.com/browse/STAR-3360)
    * [Final talk proposal submissions](https://tasks.opencraft.com/browse/STAR-3362)
    * [Preparing conference talks](https://tasks.opencraft.com/browse/STAR-3391)
    * [Book accommodation for conference and coworking weeks](https://tasks.opencraft.com/browse/STAR-3363)

### Create Forum Threads

* All conference/coworking-related forum threads should be in the “Discussions - Public” category and tagged with “conference”
* Continue using the initial [conference announcement thread](https://forum.opencraft.com/t/open-edx-conference-2025-coworking-week) for any further conference-related discussions
* All conference attendees are required to submit a talk proposal. Create a thread [like this one](https://forum.opencraft.com/t/talks-brainstorm-open-edx-conference-2025) for brainstorming talk ideas where attendees can post and discuss their proposals with the team
* Create a thread for discussing the coworking week ([here is an example](https://forum.opencraft.com/t/2024-co-working-week-in-hermanus)). Use this thread to plan [any activities](https://forum.opencraft.com/t/2024-co-working-week-in-hermanus/1647/7) the team may want to participate in during the week. These activities are typically organized by individual team members and are not billable
* Add a link to the forum threads in the Epic ticket

### Arrange a Coworking Space

Find and book a coworking space for the team during the coworking week, ensuring it’s within walking distance of the accommodation and within the approved budget.

### Organize Team T-shirts

Each team member should have an Opencraft T-shirt, and all Core Contributors on the team should have a Core Contributor T-shirt. Check with the team to see who needs a shirt, and ensure it is ready for them before the conference.

### Hire a Professional Photographer

* The conference and coworking weeks are a good opportunity to get some photos of our team. These photos help to give our marketing materials (like the OpenCraft website) a more authentic feel
* Start by confirming with the Marketing Specialist and Xavier Antoviaque whether a photographer is needed this year. If so, get quotes from several professional photographers to compare, ensuring the costs stay within budget
* Consider hiring a local photographer (or photographers, if the conference and coworking weeks are in different locations) to avoid additional expenses related to travel and accommodation

### Arrange Team Dinner

Opencraft treats all its members to a team dinner during the coworking week. It’s up to you to find a restaurant, discuss the menu, obtain a quote, and ensure it fits within the budget. Remember to ask team members about any [dietary restrictions](https://forum.opencraft.com/t/2024-co-working-week-in-hermanus/1647/59).

### Create a Mattermost Channel

Set up a Mattermost channel for team communication during the conference and coworking weeks. Notify the team about the channel in advance.

### Create a Splitwise Group

Create a group on [Splitwise](https://www.splitwise.com/) and add all of our team members. This will allow us to keep track of shared expenses during the conference and coworking weeks. These expenses are for team members’ own accounts.

### Send Reminders

* Remind the team to check that they have access to OpenCraft’s Uber account before leaving their home country. If not, they should contact Gabriel D’Amours to ask to be added
* Remind everyone to make sure they will have access to mobile data or wifi when they arrive at the destination airport, so that they can book an Uber, contact their Airbnb host, and liaise with other team members on Mattermost

### Buy Conference Tickets

* Wait for Axim to announce the availability of conference tickets. Try to purchase tickets during the early-bird period to take advantage of lower prices
* Remember to use any discount coupons we may have from previous years. If you aren’t sure whether we have coupons, contact Eden Huthmacher from Axim to find out

### Arrange Sponsorship

Confirm the [sponsorship tier](https://openedx.org/wp-content/uploads/2023/10/Open-edX_2024-Sponsorship-Tiers-1.pdf) OpenCraft will be offering for the conference, and communicate this with Axim. If offering “in-kind” sponsorship (where we volunteer our time rather than money), make sure all team members sign up for the required volunteer hours for the tasks [specified by Axim](https://docs.google.com/spreadsheets/d/1o6mspTcBwSKaoIyWmj_9J8RxmWhEXy9ynICXMRyEycs).

### Nice-to-Haves

If the budget allows, consider creating a [detailed schedule](https://docs.google.com/document/d/1uPkXw-qGRzO0rgdEEBdab_Ybkh1f2cK1zyK5GWE6Tr0) for the conference and coworking weeks, and/or a [travel guide](https://docs.google.com/document/d/1PZ2gt3s6XcYiG9_-BHxvmYVh-42Y4GVqOd1TEfxvb2I) for the conference location. These can help to reduce the number of questions from the team.

## During the Conference

### Monitor Mattermost

Keep an eye on the Mattermost channel for any questions or issues that arise during the conference.

### Keep the Team Informed

Although this is not solely your responsibility, it can be helpful to ensure the team is aware of any social events, dinners, and networking sessions during the conference week.

## During the Coworking Week

### Share Coworking Space Details

Although some team members may choose to work from the Airbnbs during the coworking week, most will probably work from the coworking space. Provide the team with all necessary information about the coworking space, including the location, opening hours, and any other important details.

## Post-Conference Tasks

### Budget Reconciliation

Remind the team to send receipts to Gabriel D’Amours for reimbursement, and ensure all expenses are recorded in the [Actual Costs spreadsheet](https://docs.google.com/spreadsheets/d/1PGd29hAhGwmpgdUOwojWmXc9PyT10iPrFJNdjP87xgw).

### Create Google Drive Folder for Photos

Create a folder [like this one](https://drive.google.com/drive/folders/1ILv3r_GohLLdbALLrAEIRJZVKu9pAnuK) for everyone to upload their photos from the conference and coworking week. This will be helpful for the post-conference blog post that we will add to our website.

### Improve this Guide

Now that you’ve gone through this guide and experienced the Epic Owner role firsthand, you’ll have invaluable insight on how this guide could be improved. Be sure to make any changes or improvements you can think of. Next year’s Epic Owner will thank you!

### Retrospective

* Once you have finished all the tasks associated with the conference epic, it’s time to write a retrospective reflecting on what went well and what could be improved. Use the [epic retrospective template](https://docs.google.com/document/d/1vXepOMHvpr6G5032dp1HuHZT7pviMX-4hR9GONiVlns) and add a link to the conference epic. Here’s an [example retrospective](https://docs.google.com/document/d/1J-LS3SlWMuWx5vXrEa-MHzQaG67TJuPoHZIYpziEVRc) from a previous year
* Create a ticket for Xavier Antoviaque and everyone involved in organizing the conference to review the retrospective and provide their feedback.

### Pat Yourself on the Back

Being the Epic Owner for the conference is no easy task. Take a moment to recognize the work you’ve done in bringing the team together and allowing us to foster connections - something that’s especially important for remote teams. Thank you for all the hard work!