# Billable Work vs. Non-Billable Work

## "Should this work be charged to the client as support"?

As an OpenCraft developer, it is common to question whether a task should be logged as billable hourly support, or not. This handbook section aims to provide a deeper understanding of the principles of billable and non-billable work, and to assist in determining when work should be logged as billable hours to a client's support account.

## What is billable work? What is non-billable work?

> Billable means **OpenCraft will charge the client for it**.

> Non-Billable means **OpenCraft will pay for it**.

While this concept may seem straightforward, it is a daily consideration for developers and has a significant impact on the company's financial performance. As OpenCraft team members are responsible for balancing billable and non-billable work to ensure the [sustainability](budgeting_and_sustainability_management.md) of their projects, this concept becomes even more critical.

## How do we know what work is billable, and what work is non-billable?

In most cases, all work performed for an OpenCraft client is billable. However, work performed for an OpenCraft client can become non-billable under certain circumstances:

+ If it belongs to a fixed-price client project whose budget has been exceeded.
+ If it is performed without explicit approval from the client, and the client ends up refusing to pay for it.

We refer to non-billable time from client projects as **Overhead** (and it shows up under that name on SprintCraft's budget dashboard).

"Internal" work such as maintaining our internal infrastructure, automating internal processes, updating documentation, and internal meetings, etc. is also generally non-billable. Tickets representing internal work are linked to [non-billable accounts](https://tasks.opencraft.com/secure/TempoAccounts!default.jspa#query/category=3&category=4&status=OPEN&orderBy=NAME%20ASC)[^1].

## Billable account types

There are two types of billable accounts you may encounter when performing client work for OpenCraft. Billable accounts are either *fixed-price*, or work logged against them is billed hourly.

To determine the type of a specific account, locate the account in SprintCraft and check the "Is fixed price?" column. If it is checked, the account is fixed-priced. If not, the account is billable hourly.

### Fixed-price accounts

If an account is classified as fixed-price, this means that any hours logged in excess of the corresponding budget will not be billable to the client
(and count as [Overhead](../cells/budgets.md#cells-with-clients) in the context of sustainability calculations).

Fixed-price accounts may belong to:

- *Fixed-price projects* such as development of a specific set of features or the initial setup of a client instance.
    - Billing for these types of projects usually happens when they are complete. At that point we invoice the client for the full budget
      (i.e., the total number of hours quoted at the beginning of the project).
- Epics with a fixed *monthly* budget such as &lt;Client&gt; instance maintenance.
    - Billing for these types of epics happens on an ongoing basis:
      We usually invoice the client for the recurring budget once per month.

### Hourly billing

If an account is *not* classified as fixed-price, we usually bill the client once per month,
for the number of hours logged against the account over the course of the previous month.

💡 Note that even if work is billed hourly, there might still be a (soft) monthly, yearly, or scope-based budget cap that we need to take into account
for the purpose of both epic-level and cell-level planning (as well as sustainability management).

## Support vs. Maintenance

One of the most common dilemmas that developers face is determining whether a client-related task should be classified as [fixed-price maintenance](../cells/budgets.md#cells-with-clients) or hourly support work. The decision on whether a task is classified as maintenance or support determines which Tempo account time is logged against. This is crucial, sustainability-wise, as maintenance budgets are fixed-price, meaning that if they are exceeded we can't charge the client for the excess. (And we don't want to increase maintenance budgets too much over time, to avoid overcharging clients.)

**Maintenance** work encompasses all tasks related to upgrading the instance, applying patches and security fixes, and addressing any issues classified as "Critical Incident" and "Major Incident" in our [Service Level Agreement](../resources/sla.md). However, maintenance **does not** include:

+ Fixing issues related to custom code
+ Upgrading and maintaining themes (except for upgrading the basic *simple_theme* package that comes standard with an instance).
+ Fixing issues caused by unwarranted/unplanned client action (i.e. "you break it, you buy it")
+ Additional work resulting from clients purposefully delaying or refusing version upgrades
+ Fixing bugs in the upstream platform
+ Installing additional platform features such as ecommerce, course-discovery, and programs.
+ Installing XBlocks or other types of add-ons (IDAs, plugins).
+ Consulting work
+ Implementing new features

**Support** work is anything that does not qualify as maintenance, and it is charged hourly at our blended rate. As the list above indicates, it includes consulting and development, but also project and epic planning, meetings, discovery, and so on.

A few important notes to keep in mind:

+ If you're unsure about whether the work you're about to perform qualifies as support or maintenance (it *will* happen), **ask the team**! You can reach out to your cell's epic planning and sustainability manager, or the administrative specialist. This will provide clarity, and avoid billing and administrative issues further down the road.
+ When a client request requires work that is classified as billable support, it is essential to **communicate this to the client and obtain their explicit approval** before proceeding. It is also beneficial to provide an estimate and a resolution timeline for the issue. This not only allows the client to approve the work, but also to raise any concerns or questions related to the work or budget.

[^1]: Private for OpenCraft Employees
