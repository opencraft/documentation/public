# Client Onboarding And Offboarding

This section describes the processes we use when we welcome new clients and say goodbye to existing ones.

## Creating a work epic

When working with a new lead or client, a Jira epic can be created to:

* Capture information that is essential or useful to developers
* Group tasks together and facilitate coordination when multiple discussions/discoveries are involved
* Find a client owner early in the process

The moment at which we create a work epic for a new lead or client changes from project to project.

If no discovery is required, the epic can be created after contract signature.

If an initial discovery (blueprint) process is required, epic creation can be handled in one of two ways:

* If doing a single and overall straightforward discovery, there is probably no need to create an epic until the contract is signed.
* If the discovery process involves lots of discussions, multiple discovery tasks, and multiple meetings, then we should create and assign an epic during the blueprint/discovery phase.

## Once a client has accepted our estimates

If the proposal resulting from the discovery is accepted, we move its **epic** to the "Accepted" column in Jira, and set a time budget and a timeline based on estimates from the discovery. Discoveries and epics are related through their estimates: tasks in an epic use the estimates and budget of the features the client selected from the quote, which was based on the discovery document. You can validate this with the [Business Development Specialist](../roles/list.md#business-development-specialist).

## New client onboarding

Here's the workflow we use when a new client accepts our quote:

[![Client Onboarding workflow](https://docs.google.com/drawings/d/e/2PACX-1vQg2vl0awtHZro8gDdkaxtGKZipmlcET7RcitXUsfPsLfi2fCxMr1zhzMtGYJh7sihPPnY1RYFdOVHp/pub?w=1349&h=614)](https://docs.google.com/drawings/d/15RceotA5oqKNr0gQCR924B9OnbY5xy6E7b2xezaIkyc/edit?usp=sharing)

## Offboarding process for departing clients

We use the following steps to complete the offboarding of an existing client:

[![Client Offboarding workflow](https://docs.google.com/drawings/d/e/2PACX-1vSPf0UTwORLh-9uSIvVmfE6K9qs65zLUl3JleVYmQ4cX3REzs_bnmtoX_2gYdxfSMTQOE4gw1v7-3Kl/pub?w=1222&h=518)](https://docs.google.com/drawings/d/1n2MGglI0eK9pN-9k6UPyI5cj110M5k0ojQpZmhSbWqQ/edit?usp=sharing)

During the offboarding process, the following [technical document](https://gitlab.com/opencraft/documentation/private/blob/master/ops/client_data.md)[^1] provides guidelines on how to handle client data.

[^1]: Private for OpenCraft Employees