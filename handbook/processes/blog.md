# Blog

## Writing in the [OpenCraft Blog](https://opencraft.com/blog)

Members of the OpenCraft team publish blog posts on our website. Such blog posts provide news about OpenCraft, updates about the Open edX platform, technical tutorials, and other news. If you have an idea and would like to prepare and publish a blog post, you are welcome to!

### Who at OpenCraft is responsible for maintaining the blog?

The [Marketing Specialist](../roles/list.md#marketing-specialist) is responsible for maintaining the blog. Contact them if you have any questions!

### Blog Publishing Process

The following is the procedure to publish an article on the [OpenCraft Blog](https://opencraft.com/blog).

#### Share your idea

Have an idea for a blog post? Create a public thread on the [OpenCraft Discussion Forum](https://forum.opencraft.com/), draft a small brief for your blog post, and ping the Marketing Specialist. There is no formal approval process, but:

* The Marketing Specialist will let you know if there's budget available to prepare the blog post. If not, they can let you know once we have budget available.
* Team members might request that you refine your idea.

#### Collaborative blog posts

We will sometime publish blog posts in partnership with people outside of OpenCraft. For example, we [co-publish](https://opencraft.com/blog/open-edx-core-committers-program/) articles about projects we've done in collaboration with Open edX, and the blog post will appear both on our blog and the Open edX blog. Collaborative blog posts require additional research, preparation work, and budget.

#### Create a ticket

Once the Marketing Specialist gives you the green light to write and publish a blog post, follow these steps:

* Create a ticket to prepare the blog post. The account should be [*Marketing*](https://tasks.opencraft.com/secure/TempoAccount.jspa?key=OPENCRAFT-MARKETING)[^1].
* Find a reviewer.
* Assign the Marketing Specialist as 2nd reviewer.

For a standard blog post, the budget for the ticket should be **10h**. The budget is roughly distributed as follows:

* 6h for preparing the blog post (assignee)
* 3h for the review process and proofreading (reviewers, assignee)
* 1h for publishing (Marketing Specialist)

For a collaborative blog post, the budget for the ticket should be **15h**. The budget is roughly distributed as follows:

* 4h for coordinating with the other author(s)
* 6h for preparing the blog post (assignee)
* 4h for the review process and proofreading (reviewers, assignee)
* 1h for publishing (Marketing Specialist)

#### Write a draft

Write a draft in a Google Doc. Make sure the doc is in the following [folder](https://drive.google.com/drive/folders/1XZuCMpp6_3zowZJCE2uKDB_kmdtB1ycM?usp=sharing), and set permissions so that any member of OpenCraft can edit the doc.

Once ready for review, ping your reviewers.

#### Proofreading Process

Once the draft has been reviewed and the comments addressed, it's time to have your copy proofead. Follow the steps from [proofreading procedure](proofreading.md) in the Handbook to have your copy proofread.

#### Choose a Featured Image

You must choose a Featured Image — it will be used as the cover image for your article. Choose an image that reflects the main topic or emotion of your post. Readers should get an idea of the post's theme just from the image. Also, make sure you have permission to use the image (([Unsplash](https://unsplash.com/)) is great for this).

Please follow these guidelines:

 * The ideal image size is 965px by 715px.
 * Avoid overly detailed images. A simple, clean image makes it easier for readers to focus and can also look better on mobile. 
 * Aim for a total image size of under 1 MB per page load. This is a general guideline to ensure fast loading times, as images can significantly impact site performance. For example, if you have five images on a page, aim for each to be around 150 KB, totaling approximately 750 KB.
 * Use compressed images, ideally under 200 KB each, where possible. This allows multiple images to be loaded efficiently without exceeding the total recommended size.
 * Use appropriate formats: JPEG for photographs (which can be compressed significantly), PNG for graphics with transparency, and GIFs sparingly for animations.

#### Finalize your post

It's now time to create the blog post in Wordpress admin. You'll ping the Marketing Specialist at the end of the process so they can review and publish your draft. 

* Log into https://opencraft.com/login. If you don't have an account, ask the Admin Specialist or Marketing Specialist to create one for you.
* Go to Posts -> Add New
* Add a title.
* Paste your copy. Headings will be used to generate a table of contents -> [example](https://opencraft.com/blog/a-new-editable-gradebook-for-open-edx/).
* Add images. We recommend that you add a few images to your blog post. It engages readers, makes the post easier to read by adding visual breaks, helps clarify detailed points, and improves SEO. If you add images to your post, please follow these guidelines:

    * The maximum width should be 1200 px (by any height).
    * Aim for a total image size of under 1 MB per page load. This is a general guideline to ensure fast loading times, as images can significantly impact site performance. For example, if you have five images on a page, aim for each to be around 150 KB, totaling approximately 750 KB.
    * Use compressed images, ideally under 200 KB each, where possible. This allows multiple images to be loaded efficiently without exceeding the total recommended size.
    * Use appropriate formats: JPEG for photographs (which can be compressed significantly), PNG for graphics with transparency, and GIFs sparingly for animations.

* In the right-hand vertical menu:
    * Pick a Category.
    * Add relevant Tags.
    * Set a [Featured Image](#choose-a-featured-image).
    * Attribute credits for the image at the end of your post.
* Review your draft thoroughly, and save it. 
* Ask the Marketing Manager to review and publish your draft. Bravo!

#### Share your blog post with the Community

Share the blog post with the Community by creating a thread in the [Community - News](https://discuss.openedx.org/c/community/community-news/35) category on the official Open edX forum. Write a very brief description of your article, and share a link to the blog post. 

#### SEO process (optional but recommended)

Our blogging platform in Wordpress uses a Search Engine Optimization (SEO) tool called [Yoast](https://yoast.com/) to review your copy and suggest changes that will increase the searchability and placement of your article on search engines.

Once your copy is in Wordpress, the Yoast tool will automatically review the copy and suggest SEO improvements. The suggestions can be found in the post creation tool, in the right-hand vertical menu, under "SEO Analysis".

We encourage you to take the suggestions into accounts, and do the necessary changes to your text to improve SEO readability. You can ping your reviewers for one last quick review after this, if necessary.

[^1]: Private for OpenCraft Employees