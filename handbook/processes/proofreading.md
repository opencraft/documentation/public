# Proofreading

We at OpenCraft lean heavily on asynchronous processes, and write tons of documents as a result. Any team member at OpenCraft has access to a proofreading service for work-related documents. We encourage you to use this service as needed, to improve the quality of your documents (discoveries, documentation, blog posts, etc.)

We work with a proofready company which employs humans to proofread your text and make suggestions. Here is how to submit your copy for proofreading:

* Upload a Google Doc containing your copy to the "To Proofread" [folder](https://drive.google.com/drive/folders/12baJQFQcne5nBOTvtR7mC2xJytrf74Tr?usp=sharing)
* Send an email to "rushang@gramlee.com" with a link to your document, asking them to proofread the document. Do not use "opencraft@gramlee.com" when communicating, as the emails end up in spam.
* A proofreading professional will review your copy and submit a commented version in the "Proofreading done" [folder](https://drive.google.com/drive/folders/10AaVn64a0oDiLgC1VB8qoR22iQlaL3qD?usp=sharing). They will ping you once done.

Voilà!
