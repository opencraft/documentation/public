# Root Cause Analysis

At OpenCraft, we do Root Cause Analyses (RCAs) for incidents, problems, or setbacks that require a deeper understanding of their underlying causes. The purpose of an RCA is to determine what went wrong and come up with ways to prevent similar issues from happening in the future. 

## When to Write an RCA

RCAs are written for scenarios such as:

* [Security breaches](../security_policy.md#response-to-a-security-breach)
* Tasks taken from the [Last Resort Backlog](./sprints.md#last-resort-backlog)
* Stuck tickets [incorrectly assigned](../epic_management.md#a-note-on-newcomers-with-stuck-tickets) to newcomers

## Steps to Create an RCA

1. Visit the [RCA folder](https://drive.google.com/drive/folders/0B5PSJLly8y3ARTdrWkNxdXBxMjA) in Drive 
1. Check if an RCA already exists for this issue
1. If not, duplicate the [RCA template](https://docs.google.com/document/d/1ra7dK6_PvHAL4b6mx3E8icl73N_ZZ3FV-6RsOsRVa3A/edit)
1. Give the new document a descriptive name
1. Follow the sections in the template to complete the document
1. Request a review from the CEO, as well as a team member with insight into the issue at hand
1. If more reviewers are needed, use the Core Committer field in the related Jira task
1. Address reviewers’ feedback and adjust the RCA as needed
1. Attend to any action items identified during the RCA process

You’re done! Thanks for taking the time to complete this RCA and help prevent similar hiccups in the future.