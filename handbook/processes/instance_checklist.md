# Instance Test Checklist

Purpose: Things to check after making a deep change on a running instance,
e.g. changing or upgrading the MySQL database.

Create a checklist in Listaflow by using `Openedx Instance Test Checklist`
template in [Templates](https://app.listaflow.com/templates) and use it for
testing a running instance.
