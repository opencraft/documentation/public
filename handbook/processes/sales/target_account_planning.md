# Target Account Planning

Historically, OpenCraft’s clientele has come through word of mouth. As we continue to grow, we’ve started identifying and targeting specific companies we’re interested in working with, companies that score highly on our [Lead prioritization rubric](lead_prioritization.md) but have not yet approached us.

Although we currently lack fully defined processes for managing target accounts, we do utilize a few key tools. First, [our CRM](https://crm.opencraft.com/) tracks the target accounts we are actively pursuing, along with key contacts within those organizations. This system helps us stay organized and manage our outreach efforts effectively.


Second, we have a Target Account Planning template. This can be found by creating a new Google doc from a template, which you can find in the [Template Gallery](https://docs.google.com/document/u/0/?tgif=d&ftv=1) in Google Docs. Once you've created this document, link it to the entry on the board using the 'Planning Link' field.

These documents should be stored in the OpenCraft shared drive, under [BizDev->Target Accounts](https://drive.google.com/drive/u/0/folders/1d_68yVh8ZV8WiLmPNJ7Pajum8qEC8LN4).

The planning template will guide your research on target accounts, helping you identify how our services can benefit them and how best to approach each opportunity.