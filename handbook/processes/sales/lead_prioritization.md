# Lead Prioritization

If you have been working at OpenCraft for a while, you may have noticed the time spent on automation, the care given to individual members, and the work done to avoid 'crunch time' in favor of more sustainable paces of development. This allows us to deliver the level of code quality we want alongside solid reliability.

Much of our pricing builds in buffers to handle issues before they come down 'to the wire', but this also means that our level of service is outside the budget of most leads. Our style of work is best done with clients who share our values, and we love to work with those that have great ambitions for their projects.

These factors, and a few more, compose our 'Lead Prioritization Rubric'. This rubric entails the factors we use to determine if a lead is a good fit for us, and how much effort will be spent trying to work with them. **Note:** These criteria are primarily for determining how much proactive investment will be performed to gain an institutional customer. For prospects seeking affordable hosting or development services, we have established referral agreements with other providers, whom we can refer prospects to as needed.

There is, of course, some risk of publishing this rubric-- if potential clients know what our criteria are, it might be seen as gauche. However our belief in openness drives our decision here: If a client reads this rubric, they may know for themselves whether we are a good fit for their organization. Most will not, but if you're the sort of potential client curious enough to read our manuals-- Hi! You sound like you might score well on our 'people profile' factor :)

## Rubric

The lead prioritization rubric is built from seven factors. Each factor can have a scoring of 0, 1, or 2. Each factor also has a weight multiplier. Some factors are worth twice as much. Others are worth three times. After all factors are considered, we reach a composite score, which can then be used to determine how much time and energy we will put into a potential project.


|**Factor**|2 points| 1 point                                                                                                                                   | 0 points                                                                                                                                                                                                    |Weighting|
|----------|--------|-------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|
|People profile|Client is competent and knowledgeable enough not to make unrealistic demands regarding deliverables and timeframes. They share our values, especially in regards to open source and contributing code upstream, quality, transparency, and empathy.| Awareness of the value of open source and its community. May be involved in altruistic work that helps the local or global community.     | No indication that they're oriented to open source, try to maximize what can be kept closed. Difficult to work with.                                                                                        |2|
|Need|Compelling business need, a project critical to their business/survival. Possibly a long term recurring need for the client that supports our long term growth.| Project would address a medium-level need that is important but not urgent.                                                               | Importance unknown or potentially a side project that doesn't matter for their bottom line. If a project ensues, most likely would be a one-off, with long term work to be assumed by their internal team.  |2|
|Budget|They are comfortable with our institutional pricing, including standard monthly maintenance fees and hourly rates for support and development. There is a confirmed budget and they will be reasonable with what they expect from that budget, or ready to listen and downsize their ambitions to match what they can afford.| Have indicated they can afford standard quoted prices.                                                                                    | Expressed reluctance or surprise at our pricing structure.                                                                                                                                                  |2|
|Contract Type|Institutional with Blended hourly rate. Willing to work iteratively, with a set monthly hours budget, adapting their scope progressively over time, and matching our preferred way of working.| Short Term Contract with specific end date.                                                                                               | Pro/Teacher plan with fixed pricing, or institutional plan with no custom development                                                                                                                       |1|
|Prestige|A leading organization in their field that is well known within their industry (or even beyond) and which would be a valuable reference.| Moderately known in their domain with some reference value, or a referral from an important client like edX, Harvard, MIT, etc.           | Relatively unknown except within their geography or field.                                                                                                                                                  |3|
|Market Niche|The customer requires a solution that is:<br /><br />1. Ambitious -- either hosting many, many users or providing unique experiences and functionality.<br />2. Worry-free -- happy to invest as needed to make sure it will never bother them in the middle of the night or make them wonder if the task will get done right.<br />3. Requires a lot of coordination, planning, and solid communication within the community. We know the community inside out and the community knows us. We have consistently worked to hash out requirements for the group at large.| We can help but so can others. There will be work to do to demonstrate how OpenCraft's unique capabilities can better serve the customer. | Customer can get what they think they need from others more cheaply. No unique capability alignment.                                                                                                        |2|
|Internal positive bias|Validated internal champion/supporter for OpenCraft; demonstrated propensity to adopt Open edX| Possible pro-Open edX bias. Potential emerging champion for OpenCraft.                                                                    | No indication of a positive bias toward Open edX or there's a steep hill to climb to garner decision maker support. Possibly inclined to build their own homebrewed solution that doesn't involve Open edX. |2|

The composite scores fall within three ranges:

|Category|Score Range| Stance                                                                                                          |
|--------|-----------|-----------------------------------------------------------------------------------------------------------------|
|High Priority|20-28| Proactively pursue this highly viable and desirable lead! Invest in technical discoveries at no upfront charge. |
|Medium Priority|10-19| Pursue. Actively follow up with lead and offer to perform paid technical discoveries.                           |
|Low Priority|Below 10| Pursue if time allows or when factors improve. If they're not a good fit, refer them to another provider.       |

For any row of the rubric which we did not manage to get sufficient information to determine, award zero points. We should not drive our data based on what we think is true, but on what we have evidence for. These determinations don't need to be 'fully proven', but they must have some backing other than a hunch or an "I don't know, so I'm giving points by default."

### Example scoring

Here are a few imagined scenarios with fictional clients and how the rubric would work with them.

#### Hedera University

Hedera University is a well known institution whose students are selected through a rigorous admissions process and which has a reputation for innovating in education. The university has decided to start offering microdegrees that carry its name and use their content but which do not require the same level of financial investment as its in-person courses. Normal annual tuition for Hedera University is near the cost of a house.

Jenny from their curriculum department has heard of us from one of their competing universities and knows we're the best in the field. She has reached out to us to build their microdegree project, but isn't a software person. She's heard of Open Source, knows Open edX is open source, and has an understanding that this means that anything can be changed. People from her team have asked about hours spent contributing code upstream and don't understand the reduction in maintenance costs this brings.

This is a long-term project-- they'd like to build an ecosystem around their microdegree program with special showcasing and sharing to business partners for hiring. It also needs to integrate with their existing services/authentication systems.

|Factor| Comment                                                                                                                                                                                                           |Score|Weighted Score|
|------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----|--------------|
|People Profile| They have some understanding of open source but they're not so experienced in being contributors. Their values aren't too far from ours but it's not a perfect fit, either.                                       |1|1 * 2 = 2|
|Need| Hedera University will probably lose out to some other universities also creating microdegree programs, but they have a very solid offline school program that people are willing to take a second mortgage over. |1|1 * 2 = 2|
|Budget| They clearly have the means and have only ever commented on hours spent in terms of not understanding 'why' some are spent where, not on their ability to pay.                                                    |2|2 * 2 = 4|
|Contract Type| This involves custom work and hosting their own copy of the platform. It will take months of work to make their ecosystem add-ons.                                                                                |2|2 * 1 = 2|
|Prestige| Everyone knows who Hedera University is! (Or so we shall pretend). They're huge, and a degree from them gets you into top paying jobs.                                                                            |2|2 * 3 = 6|
|Market Niche| With as big of a project as this is, and as much integration as it will take, we're the best fit for this project.                                                                                                |2|2 * 2 = 4|
|Internal Positive Bias| Jimmy likes us and is convinced we're the right group for this job.                                                                                                                                               |2|2 * 2 = 4|
| |                                                                                                                                                                                                                   |Total:|24|

This lead lands in our 'High Priority' category. We should do everything we can to close this sale!

#### Affogato International

Affogato international is an organization dedicated to spreading the wonders of coffee espresso desserts to areas that have previously been unable to afford such decadence. They were featured in a magazine once.

As a non-profit organization, they don't have the budget that some bigger entities have, but their commitment to deliciousness has elicited enough donations to allow them to afford a solid e-learning platform to teach remote learners how to make drinks which expand the mind and the waistline. They need only a handful of customizations to their instance-- a custom XBlock or two. Their courses are very basic but they do have a lot of interested learners-- around 1500. They don't seem surprised when we tell them what their budget will cover and what they can expect from it.

They're looking at a few different LMSes, but they are learning toward us because Open edX is open source. They like that there's a community benefit. Juxtaposed with their judicious use of java, that jives.

|Factor| Comment                                                                                                                                                                                           |Score|Weighted Score|
|------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----|--------------|
|People Profile| They are enthusiastic about open source and like the community benefit it brings. They may not be software-centric but they have quite realistic expectations about their budget.                 |2|2 * 2 = 4|
|Need| They have operated so far without an LMS, but having one would greatly improve their reach in coffee education.                                                                                   |1|1 * 2 = 2|
|Budget| They do have the money needed to afford our institutional plan but won't be contracting for much additional development.                                                                          |1|1 * 2 = 2|
|Contract Type| As mentioned, they're looking for an institutional plan, but the custom development work is short term.                                                                                           |1|1 * 2 = 2|
|Prestige| They're somewhat known but aren't especially well known outside of their sphere of influence.                                                                                                     |1|1 * 3 = 3|
|Market Niche| There are other providers that could handle their LMS needs, and while they have many users, it's not too high of an amount and they're not using anywhere near the capabilities of the platform. |0|0 * 2 = 0|
|Internal Positive Bias| There's no point person that is championing us in particular, but they seem to like us conceptually and we may be able to find one.                                                            |1|1 * 2 = 2|
| |                                                                                                                                                                                                   |Total:|18|

This lead fits in our 'Medium Priority' category. There's a decent chance we can close on this sale, so it's worth pursuing, just not with the fervor of the higher priority leads.

#### Mr. Brown's 5th Grade Class

Mr. Brown is trying to work computer literacy in with his other educational aims. He doesn't have more than one classroom at a time, but does take his students to the computer lab at least once a week for typing and other computer skills. He would like to use some of that time to perform assessments that can be instantly graded via the platform. It's not clear whether he's paying for it out of his school budget or his own pocket, but he cannot afford the cost of our institutional hosting.

When he was younger, he took a course on [edX.org](https://edx.org/) and heard that it was possible to get a copy of that software to use for your own courses. So he looked up on their website, found us, and reached out to ask if we offer any discounts for public school teachers.

|Factor| Comment                                                                                                                                                                                                           |Score|Weighted Score|
|------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----|--------------|
|People Profile| Mr. Brown hasn't shown any particular propensity toward Open Source. We don't have much information to go on here, overall, so we award no points.                                                                |0|0 * 2 = 0|
|Budget| He is not in a position to do custom development, and has a small budget.                                                                                           |0|0 * 1 = 0|
|Contract Type| Mr. Brown is not looking for an institutional-level plan, but is a good fit for affordable hosting solutions provided by partner providers                                                                                                                               |0|0 * 2 = 0|
|Prestige| Mr. Brown is a public school teacher, and is not an organization with any particular visibility.                                                                                                                  |0|0 * 2 = 0|
|Market Niche| Mr. Brown's needs are very straightforward-- just performing mini-lessons and assessments with his students. There are other providers who can do this for him readily, at an affordable budget. |0|0 * 2 = 0|
|Internal Positive Bias| Mr. Brown likes Open edX and is very familiar with it. He is hesitant to choose another LMS when this one works.                                                                                                  |2|2 * 1 = 2|
| |                                                                                                                                                                                                                   |Total:|2|

This lead fits into our 'Low Priority' category, as we're focusing on institutional customers for the bulk of our sales team's time, and don't have a budget offering. Directing him to another provider in the space which provides low-cost, low-touch hosting helps engender goodwill between us, the lead, and the other provider.

### When to Score

In most cases we are unable to score a lead immediately. Much of what is important to the rubric doesn't come through the initial contact email. This means that there isn't an obvious point when scoring is best done.

Scoring should be completed after meeting with a client for the first time. If a lead never gets to the point of having a call, the information required would be too low to perform a useful scoring. The most essential information for scoring should come out during the meeting.
