# OpenCraft's Customer Lifecycle


## What is it?

The OpenCraft customer lifecycle is a process that defines how we work with customers.
It describes the steps of our client’s journey, beginning as a sales lead and progressing through a sequence of discrete stages until they become a paying customer.
It defines our objectives as well as recommended activities at each stage as well as the tools and resources to help us achieve these objectives.
There are gates between each stage that describe what customer behaviors should occur before an opportunity can advance from one stage to the next.
The customer lifecycle should clarify how we can work together to advance sales opportunities and make our interactions with customers more consistent and effective.
These guidelines are a living document that is periodically enhanced and updated with the teams' ideas and suggestions.

The following figure provides a visual summary of the stages of our customer lifecycle:

|Generate Leads|Prospect Qualification|Assess Needs|Blueprint|Propose|Close|Develop|Bill|
|--------------|----------------------|------------|---------|-------|-----|-------|----|
|**Objective of Each Stage**|
|Generate demand for our services through marketing and community involvement|Identify and Qualify prospects who represent new business opportunities|Learn enough to create a proposal that addresses client's business challenges|Prepare a discovery report that maps to the prospect's needs, budget, and timeline|Prepare and present proposal demonstrating solution, scope, budget, and timeline|Client agrees to contract, including pricing and terms|Team develops and implements the solution|Invoice the client for the work.|
|**Gate to advance to next stage**|
|Client expresses interest in our services|Prospect scores as high or medium priority|Info on client's challenges and needs, as well as value proposition, all documented in CRM|Project blueprint completed and reviewed by client|Client has reviewed proposal|Prospect accepts our proposal, delivers deposit and becomes a client|Project completed, if using fixed price, or have completed a month of work (hourly project)|Client makes payment|

## Customer lifecycle: Stages

### 1. Generating Leads

#### What is it?

At this stage, we perform 'Lead Generation.' Lead generation consists in conducting activities that will generate demand for our services. This includes activities like marketing and community involvement.

#### What activities are involved (and who does them)?

* Preparing blog posts (everyone)
* Preparing the monthly newsletter (marketing specialist)
* Helping community members on the [official Open edX forum](https://discuss.openedx.org/) and [Slack team](https://openedx-slack-invite.herokuapp.com/) (everyone)
* Hosting talks at the annual Open edX Conference (everyone)
* Social media: sharing blog posts, community news, etc. (marketing specialist)
* Community involvement: attending working group meetings, community meetups, workshops, doing code reviews and platform contributions, etc. (everyone)
* Sending cold emails (bizdev):
    * To attendees of the annual Open edX Conference
    * To the attendees of the annual edX Partners Forum
* Subscribing to RFP alerts using keywords (bizdev)

#### What resources and tools are used?

* (TBD) A "developer's playbook" for prospecting within existing accounts
* List of attendees for Open edX community events (conference, meetups, workshops, etc.)

#### How does the opportunity advance to the next stage (stage gate)?

Someone inquires about our services (and becomes a Lead)!

### 2. Prospect Qualification

#### What is it?

At this stage, we determine if a lead is a good fit for us, and how much effort will be spent trying to work with them. This process, called "qualification", is all about gathering insights necessary to make a good judgment.

#### What activities are involved (and who does them)?

* Keep a lookout for new potential projects with contacts and within current client organizations (bizdev, developers)
* Greet our leads, and send them our standard hosting/support quote (bizdev)
* Assess public [Requests For Proposals](../../glossary.md#rfp) (RFP) and determine if they're a match
* Apply the [Lead Prioritization Rubric](lead_prioritization.md) to qualify leads and determine next course of action
* Capture information in the CRM (bizdev)
* Do periodical follow-ups (bizdev)

#### What resources and tools are used?
* [Lead prioritization rubric](#rubric)
* [Customer Relationship Management (CRM) tool](https://crm.opencraft.com/)
* [RFP match criteria document](https://docs.google.com/document/d/1evZOp13EHbaUpgMw70cYhAQEXz7Beo8deQ1kMIGD7Jw/edit#) and [RFP Policy](overview.md#request-for-proposal-rfp-and-tender-projects)

#### How does the opportunity advance to the next stage (stage gate)?

* Opportunity is scored as high or medium priority in the Lead Prioritization Rubric
* Lead is responsive, and becomes a serious prospect

### 3. Assess Needs

#### What is it?

In this stage, we learn enough about the customer to enable OpenCraft to create a compelling proposal. We also ensure that the opportunity passes a higher stage of qualification.

We need to get a clear understanding of:

* The customer’s challenges and business issues
* A high level outline of the solution the customer needs to address their current challenges
* Functional and technical requirements for the solution
* The client's internal decision-making process
* Potential alternatives (other product, internal solution, etc.) and competitors, strengths and weaknesses relative to OpenCraft

#### What activities are involved (and who does them)?

* Identify and meet with key stakeholders to gather above information (bizdev, dev)
* Use additional information gathered to revisit the Lead Prioritization Rubric to assess how the lead qualifies, based on:
    * Ability of prospect to pay for the discovery work and is comfortable with our hourly rates
    * Customer’s need is compelling and something we can address in a unique way
    * Prospect is competent, reasonable and is aligned with our open source / upstreaming approach (bizdev)
* Capture information in the CRM (bizdev)

#### What resources and tools are used?

* [Lead prioritization rubric](#rubric)
* [Customer Relationship Management (CRM) tool](https://crm.opencraft.com/)

#### How does the opportunity advance to the next stage (stage gate)?

* Information about the Prospect's challenges and needs + value proposition is documented in the CRM
* The opportunity’s score on the Lead Prioritization Rubric is improving

### 4. Blueprint

#### What is it?

In this stage, we use all the information that we captured to prepare a project blueprint that maps to the prospect's needs, budget, and timeline.

The project blueprint contains:

* A description of the solution designed by OpenCraft
* Acceptance criteria
* Tasks and estimates
* A project timeline

#### What activities are involved (and who does them)?
* Schedule necessary discovery tasks (bizdev)
* Prepare project blueprint (often also called a "discovery report") (dev)
* Communicate with prospect as needed during the discovery (bizdev, dev)
* Capture information in the CRM (bizdev)

#### What resources and tools are used?

* Handbook: ["How to do estimates"](../../how_to_do_estimates.md)
* Discovery [templates](../../how_to_do_estimates.md#estimation-worksheet-templates)
* [Customer Relationship Management (CRM) tool](https://crm.opencraft.com/)

#### How does the opportunity advance to the next stage (stage gate)?

A project blueprint has been completed.

### 5. Proposal

#### What is it?

In this stage, we prepare and present a comprehensive proposal demonstrating the proposed solution, scope, budget, and timeline. The proposal comprises the project blueprint and a financial quote.

Proposals consist of a [discovery document](../../how_to_do_estimates.md) and proposal for signature. For an example proposal, [check this link](assets/institutional_plan.pdf).

#### What activities are involved (and who does them)?

* Review the project blueprint (bizdev)
* Prepare a quote (bizdev)
* Offer to book a proposal presentation with prospect (bizdev)
* Prepare proposal presentation (bizdev)
* Attend proposal presentation meeting (bizdev + dev (if applicable))
* Send quote + blueprint to  prospect after the presentation (bizdev)
* Adjust blueprint and quote as needed (bizdev + dev)
* Capture information in the CRM (bizdev)

#### What resources and tools are used?

* Proposal creation software ([Proposify](https://opencraft.proposify.com/dashboard))
* OpenCraft [slidedeck](https://docs.google.com/presentation/d/1QnTmhlAH8CI6i1KrbKY1ZESLE0T5uEmh931Hg3hDSso/edit?usp=sharing)
* [Customer Relationship Management (CRM) tool](https://crm.opencraft.com/)

#### How does the opportunity advance to the next stage (stage gate)?

We deliver a final proposal to the prospect.

### 6. Close

#### What is it?

In sales terms, closing is generally defined as the moment when a prospect or customer decides to make the purchase. Some prospects will self-close, and some will require us to instigate the close. When needed, we close a sale by summarizing the benefits and value of our proposal, and prompting the prospect to communicate their decision.

In this stage, the client formally commits to OpenCraft by:

* Agreeing on deal contract, including pricing and terms
* Sending a [Scope of Work](../../glossary.md#scope-of-work) (if applicable)
* Delivering a deposit payment (if applicable)

#### What activities are involved (and who does them)?

* Summarizing the benefits and value of our proposal, when required (bizdev)
* Completion of [onboarding steps](../client_onboarding_and_offboarding.md)
* Resolve any outstanding legal and business issues (bizdev, admin specialist)
* Conduct kickoff meeting (kickoff) (bizdev, dev)
* Capture information in CRM

#### What resources and tools are used?

* Handbook: [Onboarding a client](../client_onboarding_and_offboarding.md)
* [Customer Relationship Management (CRM) tool](https://crm.opencraft.com/)
* Project kickoff [guide](https://plan.io/blog/kickoff-meeting/)

#### How does the opportunity advance to the next stage (stage gate)?

* The prospect accepts our proposal and becomes a client.
* All onboarding steps have been completed
* Kickoff call is done
* Communication, budget tracking, and project updates are set
* Epics and tasks are scheduled
* Deposit payment has been received (if applicable)

### 7. Development

#### What is it?

The development stage is where the team develops and implements the solution.

#### What activities are involved (and who does them)?

* Executing the technical tasks (dev)
* Project management, including: stand-up meetings, task-budget-timeline tracking, resolving issues (dev)
* Prospecting for new opportunities (go back to stage 1) (dev)

#### What resources and tools are used?

* Project tracking software (Jira)

#### How does the opportunity advance to the next stage (stage gate)?

We have completed the project (if using a fixed price) or have completed a month of work (hourly project).

### 8. Billing

#### What is it?

In this stage, we bill the client for the work.

#### What selling activities are involved (and who does them)?

* Upload Jira work logs to Freshbooks ([automated](https://gitlab.com/opencraft/ops/infrastructure/-/tree/main/functions/freshbooks_sync)[^1])
* Review and send invoices (admin specialist)
* Budget tracking (dev)
* Discuss and resolve budget issues (dev, admin specialist)

#### What resources and tools are used?

* Jira
* Freshbooks

[^1]: Private for OpenCraft Employees