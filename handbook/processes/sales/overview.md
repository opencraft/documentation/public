# Sales

Sales and Business Development at OpenCraft thrive through two main channels: deepening strategic partnerships with existing clients, which leads to expanded work, and building a strong reputation that generates valuable referrals. While advertising and marketing plays a huge  role in our broader strategy, our core approach aligns with OpenCraft’s [values](../../values.md) of openness, quality, and commitment.

Empathy is central to our sales process. We prioritize understanding our clients' goals and determining whether we are the right fit to help them achieve those objectives. If not, we are transparent about our limitations. We do not make promises we cannot keep. We set realistic expectations with prospects about what we will deliver, at what cost, and when. We are leaders in our field and do not skimp on quality.

## Parameters for Sales

At OpenCraft, most projects stem from contracts with existing clients who engage us for new ventures. We prioritize expanding relationships with current clients over recruiting new ones. This preference allows each [Client Owner](../../roles/list.md#client-owner) to focus on strategic planning with their assigned clients, ensuring a deeper, more thoughtful engagement. 

Since each Client Owner is also a developer and we recognize that most developers want to focus on development, we strive not to overload team members with too many client responsibilities. This approach limits the number of clients we can take on but ensures a higher quality of service and a better work-life balance for our team.

Whether we are open to new contracts depends primarily on role load, the number of roles (including but not limited to client ownership) our team members are handling, and whether adding more would risk burnout. Capacity, defined as the number of hours we could dedicate to client contracts, is also a factor. However it is rarely a factor before role load is considered:

* A cell's role load is lowered as we hire more team members and close out contracts.
* A cell's role load is heightened as team members depart and contracts are added.
* The [Epic Planning and Sustainability Manager](../../roles/list.md#epic-planning-and-sustainability-manager) keeps track of their cell's role load, and advises the [Business Development Specialist](../../roles/list.md#business-development-specialist) when capacity is sufficient to take on more clients.

## An Overview of a Sale at OpenCraft

Sales at OpenCraft can come through multiple vectors. The most common, as mentioned earlier, is starting a new project with an existing client. When a [Client Owner](../../roles/list.md#client-owner) or the Business Development Specialist identifies a new way for us to help the client, they can pitch the idea for [Discovery](../../glossary.md#discovery). If the client agrees to spend the hours on Discovery, the Client Owner can either have someone on [Discovery Duty](../../roles/list.md#discovery-duty-assignee) take the discovery, or else they can schedule a task in the next sprint.

The other ways we get sales are through various leads-- some come through our website, some via referral, and still others through encounters in our community participation. For example, we might encounter a lead at a conference, or in a [community working group](https://openedx.org/open-edx-community-working-groups/). In this case, the client reaches out to us asking for more information, and we respond with an email giving a basic overview of OpenCraft and what we do. After initial exchanges, the [Business Development Specialist](../../roles/list.md#business-development-specialist) schedules a [meeting](#prospect-intro-meetings) with the client and gets an overview of their project. [Based on what the Business Development Specialist learns](./lead_prioritization.md), they may assign a task to the person on Discovery Duty or schedule a task in the next sprint to perform a discovery.

After completion of the discovery, the Business Development Specialist integrates the figures determined by the developer into a full proposal document for signature, which is sent alongside the developer's discovery document. [Here is an example proposal](assets/institutional_plan.pdf) for an institution that wants hosting via our Institutional Plan, and also wants a custom 'Wormhole XBlock' to be developed.

The customer then reviews these documents, and, if satisfied, signs. If the client is a new client, the [Administrative Specialist](../../roles/list.md#administrative-specialist) [onboards the client](../client_onboarding_and_offboarding.md). The developer who performed the discovery takes charge of the client's epic, and begins scheduling tasks, and the development cycle is underway. The developer who performed the discovery also becomes the client owner, if the client is new.

[A full view of the customer lifecycle is available here.](customer_lifecycle.md)

## Prospect Intro Meetings

As part of qualifying leads and better understanding how we may meet the needs of clients, initial introductory meetings are scheduled.

### Meeting Preparation

Before the meeting, take time to research the client online. Most organizations have a website where you can learn about who they are and what they do. Checking the contact’s LinkedIn profile can also provide valuable insights.

When gathering information, keep these key points in mind:

1. Situation: What is the organization's mission? What do they do? Are there any industry trends or changes that may be affecting them?
2. Problem: Based on what they've communicated or from your experience with similar clients, what challenges might they be facing?
3. Implications: How could these challenges impact their ability to achieve their goals?
4. Need Payoff: How can OpenCraft uniquely address these challenges and help the client be more successful?

Take time to reflect on this information and bring it into the meeting to help build rapport by addressing their specific needs. Make sure to document your findings with an update in the CRM.

## Kickoff Meetings

Once the contract has been signed and the deal is closed, the [Business Development Specialist](../../roles/list.md#business-development-specialist) is expected to schedule and attend the initial kickoff meeting for the project. This meeting is set up between the [Client Owner](../../roles/list.md#client-owner), the key stakeholders on the client's end to start the work. Since it is the first meeting, there are likely to be questions surrounding things like:

* Processes and procedures
* Onboarding to tools-- either the client to our tracking tools, or us to theirs
* Questions about invoicing and billing
* Other miscellaneous queries

The Business Development Specialist attends the first call to answer these or otherwise help them get resolved afterwards (such as passing items on to the [Administrative Specialist](../../roles/list.md#administrative-specialist)). By the end of this onboarding, handling the client's sprint-to-sprint care and meetings should be firmly in the hands of the Client Owner.

### Meeting agenda

While these meetings are fluid and should not have a strict structure, there are some helpful anchor points to keep in mind.

1. If you're not sure how familiar the client is with OpenCraft, ask if they'd like a basic rundown of who we are and what we do. If they say yes, give them a basic introduction, going over these points:
    * Our experience and knowledge of the Open edX platform
    * Our involvement in the Core Contributors program
    * Our commitment to Open Source
    * A summary of clients we work with
    * The volume of contributions we've made (and continue to make) to the platform
2. Preferably, you'll have already read up a little on their organization. Ask them about it, especially any specific questions you may have come up with when browsing their website.
3. Ask about the problem they're trying to solve.
4. At some point in the meeting (if it doesn't come up in 1), be sure to mention our policy of upstreaming contributions. The earlier we can gauge the client's appetite for open source, the sooner we can determine their [people profile](./lead_prioritization.md#rubric), and whether they'll be asking us to work under terms we'll accept.
5. If they have not tried Open edX yet, you can [bring up the demo instance](https://demo.opencraft.hosting/) and show them around.
6. Determine next steps and lay them out for post-meeting follow-up. This might be getting them to send a detailed list of requirements, or starting the discovery process. If starting a discovery, make sure to get them to agree to pay for the time spent on discovery. If they're a high priority lead, this cost can be deferred until they agree to start the project-- though you may have to guess at this point if you haven't already run the rubric.
7.

When the meeting is finished, write an email to the client summarizing what you went over in the meeting, and what the next steps are. Trigger the [CI job here](https://gitlab.com/opencraft/documentation/public/-/pipeline_schedules) to [migrate the meeting](../../automation/migrate_zoom_recordings.md) recording to google drive. Add an update with the link to the video (received via email after migration) and a summary of the discussion, and update the prioritization for the lead [in the CRM](https://crm.opencraft.com/). Also, set the due date on the lead to a week in the future (or other time as appropriate) to get a reminder to follow-up.

If you haven't already, be sure to add a [mail archive link](https://private.doc.opencraft.com/mailing_lists/#opencraft-client-mailing-lists) to the email thread with the client into an update as well.


## Small Projects

Occasionally, a lead will come in asking for a single feature change to the platform.  The feature is apparently small and the person or team is not looking for an extended engagement.

In cases like these, the client can be offered two choices:

1. Run a discovery of up to five hours of billable time to create a blueprint of how long the feature will take and a final estimate.
2. Have them agree to up to X hours of development (no fewer than 10) to see how far we can get on the feature. Deliver what we can if we don't complete it in that time.

Option 1 is unlikely to be chosen for these cases. Smaller features might take just as long to do a standard discovery as it would be to build them. Option 2 should be a ballpark estimate. Increase this estimate if you foresee any issues.

In cases like these, we do not assign a new client owner, but instead make this a one-off task assignment.

## Request for Proposal (RFP) and Tender Projects

While most of our clients approach us to ask for quotes, a subset of companies we sell to publish a 'Request for Proposal' (private companies) or a 'Tender' (public ones) which are a general call for bids from vendors.

Tenders and RFPs are unique in that they require upfront investments from our team in order to submit our bids. In these cases we are unable to bill out the discovery time if we are not chosen. Sometimes, we cannot bill out even if we **are** chosen. To that end, we have some rules for engaging requests for proposals.

1. We do not respond to RFPs and tenders for any group which does not score into the High Priority category of our lead prioritization rubric. All other organizations must go through the normal discovery process, or we will decline to work with them.
2. If a lead does score into the high priority category, we will require payment for the discovery work upon acceptance of the terms.
3. ...With one narrow exception. If the legal dictates of a government client's tender process prevent us from billing for discovery work, we will not bill for it. This will need close assessment from the Business Development Specialist to verify that the risk of sinking time into the tender is worth the cost. Private and nonprofit entities, as categories, will not be allowed to forgo discovery charges after acceptance.


## Writing Proposals

All proposals should capture the full breadth of the discovery process costs. Meetings, discovery work, management overhead-- all of these need to be factored into the final price. The [discovery process](../../how_to_do_estimates.md) gives us a good view of what these costs will be.

Proposals are generated using [Proposify](https://proposify.com/), sometimes with accompanying documents. [Here is an example proposal.](assets/institutional_plan.pdf)

If a project is fixed-price (that is, not to be long-term or iteratively developed), the proposal should contain the full estimated hours of the work, including time spent on discovery if it has not already been billed. Including the estimates sheet from the discovery process is recommended. You should always send the main discovery document.

If it is to be iteratively developed, the proposal should include the expected monthly development hours in one of the time banks at the bottom of the proposal template, rather than a lump sum. The proposal should be accompanied by the estimates spreadsheet, and the main discovery document, as always. Depending on the size and length of the engagement, a supplementary sheet showing year-by-year budget needs may also be necessary-- in most cases a client will ask for this if required.

Additionally, always send a link to [this article](https://opencraft.com/blog/how-agile-methodology-impacts-planning-and-budgets/) to clients who will be engaging in iterative development with us, and make it clear that as we learn more about the project, the actual costs, features, and other work, are likely to change. Emphasize that estimates are based on what we know at this time only.

In some cases, you may wish to send an executive summary of OpenCraft and our work. These can be created in Proposify. [Here is an example executive summary](assets/exec-summary.pdf).

### Transparency in Pricing

Including the estimates spreadsheet is important to make pricing transparent. While it is possible to 'roll' discovery and management hours into individual development tasks to pad them for required overhead, this makes a full understanding of how resources are to be spent difficult. It also requires a lot of manual work from the proposal writer to 'make everything fit.' It's better to dispense with this and lay out the costs out as transparently as possible.

## Maintenance Contracts

While some of our clients only require our engineering expertise for custom development, most host their Open edX instances with us. Open edX is a complex system, and one result of that is that the number of hours in any given month required to maintain it is highly variable. On months where a new release is cut, many hours must be spent on upgrades. On other months, we may have only the odd security patch.

This leaves the client in an unenviable position of being unable to predict their costs in any given month. In order to alleviate this problem, we offer maintenance contracts. These amounts of these contracts are based on our experience and calculations on the average number of hours required per month over the entire year. Keeping the maintenance the same amount every month is preferred by most clients.

However there are some risks involved-- a single maintenance contract size does not work for all clients. Smaller instances require very little maintenance, but larger or more complex instances require much more. One nice thing about our focus on automation means that while the larger instances may require more maintenance, the amount of time required as an instance grows is not linear, and the rate of growth decays quickly. Note that [what counts as maintenance](../billable_work.md#support-vs-maintenance) is important to keep in mind here.

There is still a gap, however, and so our maintenance contracts come in two primary tiers. Picking the proper maintenance client is a multi-factor choice, and will require some discernment. The following factors should be considered:

1. **Number of users, especially monthly active users**. While we do not do per-user billing, the more active users are on a platform, the more data is being generated, and the more time is required to handle tasks such as migrations or recovery during emergencies. It also affects the number of VMs and containers employed, creating an overall greater amount of 'drag' on maintenance tasks.
2. **Type of Deployment**. There are two main types of deployment we handle-- those on our shared infrastructure, and those running fully independent clusters. Those on shared infrastructure benefit from us upgrading shared services in one go, whereas those on independent clusters must be upgraded individually. Traditionally, our shared infrastructure has been on a selected non-AWS provider, such as OVH, which provides OpenStack hosting, or DigitalOcean, one of many Kubernetes providers. However, even on these providers, the customer may be deployed as an independent cluster when desired. If a customer is deploying on AWS, they are always on a dedicated cluster.
3. **Total Number of Instances** A customer can have multiple instances on their cluster (or, if desired, multiple clusters). The more instances, the more time is required to maintain them. Common cases of multiple instances are stage vs production environments, or clients who are reselling the Open edX platform as part of their offering. In these cases, the clients may need data segregation warranting independent instances. Maintenance contracts are ***per instance***, and so if a client has multiple instances, they will need to purchase multiple maintenance contracts. Coordinating multiple instances is also more complex, and so the maintenance contracts tends toward the larger tier.

Generally, a client may hit 'tier two' if any of these factors is true, but might not if the instance is otherwise simplified or small. Generally, a rule of thumb to give to clients without further information is that instances with more than 1000 users will require the higher tier contract. However, the full decision can only be taken when all the factors are known, and may change later on if a client's infrastructure changes significantly.

The two tiers are as follows:

| Tier   | Monthly Cost |
|--------|--------------|
| Tier 1 | 7 hours      |
| Tier 2 | 11 hours     |

Note that these are denominated in hours rather than Euros. The final prices are determined based on our on-demand hourly rate (regardless of time bank discounts for support contracts.) Our prices change depending on inflation and other market forces, but maintenance generally stays the same in terms of hours spent. Each year, then, the amount charged may increase if our prices do, but will always be proportional to the work involved.

These tiers provide a great deal of convenience, and we encourage their use in nearly all situations. However, we do get the occasional client whose instance size or maintenance needs are so great and complex that using a maintenance contract is not possible. In these cases, clients are usually large enough to better handle changes in month-to-month costs, and we switch to hourly work similar to billing support.

## Referrals from Axim

Because we are a trusted Open edX Partner, we receive referrals from the Open edX marketing site, which is run by Axim Collaborative. Every sprint or two, the Business Development Specialist needs to [update the entries in SalesForce](https://tasks.opencraft.com/browse/STAR-3528)[^1] giving a status update on these leads. This helps Axim judge the effectiveness of their efforts to bolster the larger ecosystem.

[^1]: Private for OpenCraft Employees
