# Budgeting and Sustainability Management

## Budgeting

OpenCraft team members monitor and regulate internal budgets through the seamless integration of two primary tools: **Tempo**, a Jira-connected time tracking tool, and **[SprintCraft]**, an open source tool for tracking budgets, sprint commitments, vacations and spillovers. The tools also play a pivotal role in generating accurate and transparent OpenCraft invoices sent to clients.

Here are the most common uses for Tempo and SprintCraft:

* When team members plan and estimate their work, the information is captured in SprintCraft for budget forecasting.
* When team members execute work, they log their time in Tempo.
* The [Sustainability Manager](https://handbook.opencraft.com/en/latest/roles/list/#sustainability-management) uses Sprintcraft to monitor budget usage and calculate our sustainability.
* The [Admin Specialist](https://handbook.opencraft.com/en/latest/roles/list/#administrative-specialist) runs automations that transmit Tempo time logs to Freshbooks (to generate client invoices).

### Tempo Accounts

Tempo uses an `Account` to associate time logs with a client and a project.

Every Jira task or epic has an `Account` field. Tickets must be linked to an account in order to be created — the account field cannot be left blank.

#### What are accounts used for?

Every single time log is associated with an account. This setup enables detailed tracking of time spent for both internal budgeting and invoicing purposes.

* SprintCraft tracks time spent on every account, for internal budgeting purposes. It does this by fetching relevant data from Tempo.
* When a client receives their OpenCraft invoice, each line item on the invoice corresponds to a Jira account. 

#### Can I create Tempo accounts?

There's only one simple rule: **do not create Tempo accounts yourself**.

Ask the Admin Specialist, or the Cell Supporter if the admin specialist is unavailable. The CEO is an acceptable fallback option. 

**Why?**

Because simply creating an account and assigning tickets to it right away will break automations and require manual fixing. 

Creating Tempo accounts requires a few additional, manual steps to ensure that our budgeting and billing automations work. These steps are described further [below](#creating-sprintcraft-accounts).

#### I'm creating a new Jira ticket. Which account should I use?

Clients often have multiple, separate accounts for different purposes such as support, maintenance, and other projects. Choosing the right account for a new ticket is important, and it may not always be obvious to choose which account to use. If unsure, **ping the epic or client owner**.

For additional information about whether a new ticket qualifies as support or maintenance, billed or non-billed, you can look at:

* The section of the handbook about [billable vs. non-billable work](billable_work.md#billable-work-vs-non-billable-work)
* The section about [support vs. maintenance work](billable_work.md#support-vs-maintenance)
* The section about [account association](#checking-account-associations) right below for how to manage edge cases.



### Reviewing account usage

#### Checking account associations

Getting accurate information about budget usage for individual projects requires that tickets be associated
with correct accounts. Since tickets can't be created without setting an account, any questions that might
come up around the correct account to use for a specific ticket are usually addressed at the time of ticket
creation; and the choices being made as part of this process are usually correct. However, there are some
edge cases where the chosen account for a ticket ultimately ends up being incorrect. For example:

* A discovery ticket that starts out on the *OpenCraft - Prospects* account (a non-billable account)
  may need to be moved to the client account that corresponds to the project resulting from the discovery
  later on (after the prospect accepts the quote from the discovery). In practice, we sometimes end up
  forgetting about this step.
* A firefighting issue may initially be associated with an internal account (e.g. *OpenCraft - Instance Maintenance*)
  when it should have been associated with a specific client's maintenance account.

Once per month we therefore need to check whether tickets worked on over the course of the previous month
are using the right accounts, and make adjustments as necessary (cf. [Sustainability Management](#sustainability-management)).

This also helps us make sure that clients are billed correctly each month, and that the sustainability impact
(or lack thereof) of individual tickets is correctly reflected in SprintCraft.

The process of checking ticket associations can be combined with the process of [checking and recording actuals](#checking-and-recording-actuals)
for the purpose of efficiency, and is described in the following section.

#### Checking and recording actuals

The main purpose of reviewing and recording actual time spent on individual accounts is to enable
a high-level comparison between where we thought our time would go, and where it actually went
over the course of a given month. This knowledge can help detect potential budget issues and allow us to follow up
on them with epic owners. It can also inform decisions about capacity allocations for future months.
(For example, you can set/adjust capacity allocations for ongoing internal work for coming months
by calculating the average of actuals recorded for the last three months. You can also use
actuals from past months to detect trends, which might be relevant for sustainability reviews.)

The combined process of [checking account associations](#checking-account-associations) and
checking and recording actuals looks like this:

1. Go to the [list of billable accounts](https://tasks.opencraft.com/secure/TempoAccountBoard!timesheet.jspa?v=1&categoryId=2&periodType=BILLING&periodView=PERIOD&period=0122)[^1]
   and select the month for which to review account usage.
    * This list includes *all* billable accounts that the team logged time against over the course of the selected month.
      For the purposes of doing a cell-specific review, you can limit the steps listed below to accounts that either:
        * belong to a client that your cell owns, or:
        * are shared between multiple generalist cells (such as *OpenCraft - Instances Upgrades*).
1. For each relevant account:
    1. Click on the name of the account to see the list of tickets worked on and hours logged
       over the course of the selected month.
    1. *[Checking account associations]* Review the list of tickets that your cell worked on,
       looking for tickets that, from a high level, seem like they should be associated
       with a different account.
        * Follow up on any clear-cut cases right away. I.e., if it's clear that a specific ticket
          does not belong to the current account, and you don't have any doubts about which
          account the ticket should be associated with instead, go ahead and change the account
          on the ticket right away.
        * Make a note of any tickets that will require raising some follow-up questions
          to determine whether the current account is appropriate for them or not,
          and which accounts they should be moved to.
    1. *[Checking and recording actuals]* Add the total number of hours logged against the account
       by your cell to the *Actual* column for the selected month in the epic planning spreadsheet.
        * If the epic planning spreadsheet only has a single entry for a given client
          and the team logged time against multiple accounts belonging to that client
          over the course of the month you are reviewing, the actual to enter into the epic planning spreadsheet
          should be the sum of all hours logged against each individual account.
          For example, the epic planning spreadsheet may include a single entry listing capacity
          allocations for AwesomeClient&trade;, while the work that we perform for that client
          is divided into maintenance and support-related tasks (meaning that for the month under review,
          you'll find hours logged against both AWESOMECLIENT-MAINTENANCE and AWESOMECLIENT-SUPPORT).
          To obtain the total number of hours to enter into the *Actual* column for AwesomeClient&trade;,
          you can either sum up the actuals from AWESOMECLIENT-MAINTENANCE and AWESOMECLIENT-SUPPORT,
          **or** use the drop-down menu at the top of the page to search for and bring up
          the client's top-level Tempo entry which will list all tickets associated with any
          of the client's accounts, as well as the total number of hours logged across all accounts.
          (In the example above, the top-level Tempo entry would likely be called *AwesomeClient*.)
        * Actuals for small clients that don't have their own entry in the epic planning spreadsheet
          may be summed up under the actual for "Client Support" and/or "Client Maintenance".
        * If you found one or more tickets that might need to be moved to a different account,
          it may make sense to hold off on recording the number of hours logged against the current account
          at this stage, and complete this step once any remaining questions about account associations
          have been resolved (and tickets moved to different accounts as necessary).
          You should consider this approach if the number of hours logged against tickets
          to potentially move to a different account is substantial.
          If it's small (less than 10h), you can record the actual for the current account
          right away; there will be no need to update it again in the epic planning spreadsheet later
          (because doing so wouldn't impact any calculations that you might make based on actuals
          in a meaningful way).
1. Go to the [list of non-billable accounts](https://tasks.opencraft.com/secure/TempoAccountBoard!timesheet.jspa?v=1&categoryId=3&periodType=BILLING&periodView=PERIOD&period=0122)[^1]
   and select the month for which to review account usage.
    * This list includes all non-billable *non-cell* accounts that the team logged time against
      over the course of the selected month.
      For the purposes of doing a cell-specific review, you only need to consider accounts that either:
        * belong to an accelerated epic that your cell has been working on, or:
        * belong to a company-wide role that is assigned to someone from your cell (such as [Community Liaison](../roles/list.md#community-liaison)), or:
        * are used to log other types of cell-level work that are tracked in the epic planning spreadsheet.
          (Aside from *OpenCraft - Recruitment*, which tracks all activities related to recruitment including
          onboarding time, accounts to check include, e.g., *OpenCraft - Accounting*, *OpenCraft - Legal*,
          and *OpenCraft - Marketing*. Hours for these accounts may be added to the actual
          recorded for the "Other OpenCraft" entry; cf. below).
1. For each relevant account, click on the name of the account and repeat the steps for checking account associations
   and checking and recording actuals listed above.
1. Go to the [list of non-billable cell accounts](https://tasks.opencraft.com/secure/TempoAccountBoard!timesheet.jspa?v=1&categoryId=4&periodType=BILLING&periodView=PERIOD&period=0122)[^1]
   and select the month for which to review account usage.
    * This list includes all non-billable *cell responsibility* accounts that the team logged time against
      over the course of the selected month.
1. For each relevant account, click on the name of the account and repeat the steps for checking account associations
   and checking and recording actuals listed above.
    * The names of some accounts match corresponding entries in the epic planning spreadsheet more or less exactly.
      For example, to get the actual for the "Meetings" entry in the epic planning spreadsheet,
      you'll need to check the number of hours that your cell logged against the *OpenCraft - Meetings* account.
      However, there are are also some cases where there isn't a 1:1 match between account names
      and spreadsheet entries; we list them here for easy reference:
        * "Estimates/Prospects": *OpenCraft - Prospects*
        * "Management": *OpenCraft - Cell management* (and **not** *OpenCraft - Management* or *OpenCraft - Stress management*)
        * "Onboarding": *OpenCraft - Recruitment*
        * "Other OpenCraft":
            * *OpenCraft - Accounting*
            * *OpenCraft - Client Management and Briefing*
            * *OpenCraft - Legal*
            * *OpenCraft - Marketing*
            * *OpenCraft - Sales Management*
            * *OpenCraft - Conferences*
            * *OpenCraft - Contributions*
            * *OpenCraft - Learning*
        * "OpenCraft IM": *OpenCraft - Instance Manager*
        * "DevOps": *OpenCraft - Instances DevOps*
        * "&lt;Open edX release&gt; upgrades": *OpenCraft - Instances upgrades*
    * When it comes to checking account associations, pay special attention to the following cases:
        * *OpenCraft - Instance Manager* / *OpenCraft - Instances DevOps*: Look for tickets that could be moved
          to billable maintenance accounts.
        * *OpenCraft - Instances Upgrades*: Look for tickets that could be moved to billable maintenance
          or support accounts.
        * *OpenCraft - Learning*: Look for tickets from newcomer onboarding epics that could be moved
          to *OpenCraft - Recruitment*. (Tickets from these epics should be using the *OpenCraft - Recruitment*
          account by default, even if they are about learning some relevant technology or exploring
          part of our infrastructure.)
1. *[Checking account associations]* Post the results of checking account associations as a comment on your cell's
   "Epic planning and sustainability management - &lt;your cell&gt;" epic. Include a summary of the changes that you made
   and list any open questions about further adjustments that you think should be made, pinging other people (epic owners,
   CEO/CTO, Administrative Specialist) as necessary.
1. *[Checking account associations]* Adjust account associations as necessary based on the answers that you get.
1. *[Checking and recording actuals]* As a last step, update the epic planning spreadsheet with any actuals
   that you held off on entering earlier.

### Working with SprintCraft

#### Creating SprintCraft accounts

- In Tempo, accounts are identified by a *name* (e.g. "OpenCraft - Instances upgrades")
  and a *key* (e.g. "OPENCRAFT-INSTANCE-REBASES").
- Account names may have a prefix such as "(Inc) - " which specifies the legal entity
  through which the corresponding work logs will be invoiced.
    - 💡 Note that SprintCraft does not display prefixes of account names on the Budget dashboard.
- To tell SprintCraft about the existence of a specific Tempo account, create a corresponding SprintCraft account
  via *Django Admin > Sustainability > Accounts*:
    - The **Name** field of the SprintCraft account needs to match the name of the Tempo account *exactly* (including any prefixes).
        - 💡 This means that if an account is renamed in Tempo, the **Name** of the corresponding SprintCraft account will need to be updated accordingly.
    - If the account belongs to a fixed-price project, make sure to check **Is fixed price**.
        - 💡 As mentioned [here](./billable_work.md#fixed-price-accounts), accounts for recurring maintenance (i.e., accounts with a fixed *monthly* budget) are considered fixed-price as well.

##### Division of responsibilities

The [Administrative Specialist](../roles/list.md#administrative-specialist) is responsible for creating Tempo accounts and corresponding SprintCraft accounts,
as well as keeping account names in sync between Tempo and SprintCraft.

#### Creating budget entries

- To tell SprintCraft about budgets for individual accounts, set them via *Django Admin > Sustainability > Budgets*:
    - The **Date** field of a budget entry should be set to the month to which the budget is supposed to apply.
    - The **Hours** field of a budget entry should be set to match the available budget.
    - The **Account** field of a budget entry must be set to the SprintCraft account to which the budget is supposed to apply.
        - As mentioned [above](#creating-accounts), the **Name** field of the SprintCraft account
          needs to match the name of the corresponding Tempo account *exactly*.
          Otherwise SprintCraft will not be able to correctly associate budget entries with Tempo accounts.
- If the budget for a specific account is the same for multiple months,
  there is no need to create a budget entry for each month:
  Just create a single entry for the first month to which the budget applies;
  SprintCraft will then automatically apply it to the following months.
    - 💡 This means you must explicitly reset the budget for a specific account to 0h
      when the corresponding contract ends (or when we've run out of budget for the account).
      Otherwise the budget will be applied to future months indefinitely.
- If you create multiple budget entries for the same month (e.g. Jun 2021),
  SprintCraft will use the newest budget entry for display on the Budget dashboard
  and for sustainability calculations. (In other words, it is possible to overwrite
  earlier budget entries for a given month and account by creating new budget entries for the
  same month and account later on).
- Budgets are specified in (full) hours per month. Decimals are not accepted.

##### Automation

For *regular* accounts (i.e., billable accounts that aren't bound to a fixed budget), budget entries are [generated automatically](https://doc.sprintcraft.opencraft.com/overview/sustainability/#automation-budgets-for-billable-clients)
once per month, based on the number of hours logged against these accounts in Tempo.

##### Division of responsibilities

- The [Administrative Specialist](../roles/list.md#administrative-specialist) is responsible for setting initial budgets for fixed-price accounts (including maintenance).
- Sustainability managers are responsible for retroactively adjusting monthly budgets for fixed-price accounts (excluding maintenance) to match actuals as necessary.
- The [Administrative Specialist](../roles/list.md#administrative-specialist) is responsible for updating budgets for fixed-price accounts with recurring monthly budgets.
  This is necessary if the number of hours invoiced per month changes.
- The [Administrative Specialist](../roles/list.md#administrative-specialist) is responsible for retroactively adjusting automatically-generated budgets for regular accounts.
  This is necessary if the number of hours invoiced for a specific month ends up differing from the number of hours logged over the course of that month.

#### Reviewing budget information

- To review budget information for one or more accounts in SprintCraft:
    - Go to the Sustainability dashboard for your cell (e.g. [Falcon](<https://sprintcraft.opencraft.com/board/45>)).
    - Select the desired period to review (e.g. Jun 1st 2021&#x2013;Dec 31st 2021).
    - Hover over the name of a specific account with your mouse to get a list of
      budgets entries for:
        - individual months of the current year, starting with the first budget entry
          that was explicitly set for that year in SprintCraft.
        - the first month following the selected period.
    - For high-level information about account budgets check the *Period Goal* / *YTD Goal*
      and *Period Spent* / *YTD Spent* columns.
      For each account:
        - The *Period Goal* column shows the total number of hours budgeted for the selected period
          (i.e., the sum of all budget entries for the selected period).
        - The *YTD Goal* column shows the total number of hours budgeted
          from the beginning of the first year within the selected period
          (i.e., Jan 1st in the example mentioned above) to the end of the next sprint.
        - The *Period Spent* column shows time logged during the selected period.
        - The *YTD Spent* column shows time logged from the beginning of the first year within the selected period.
    - At the bottom of the Budget dashboard you'll see an item called "Overhead".
      *This item does not correspond to an account.* Instead, it lists computed values
      for YTD Spent and Period Spent that represent the sum of non-billable hours logged
      against client accounts for the corresponding time spans (i.e., YTD and selected period).
- If you find any inconsistencies or outdated information, adjust budgets in the Django admin as necessary
  (as described above).
- Reload SprintCraft to see your changes reflected in popups and *YTD Goal* / *Period Goal* columns.

### Choosing appropriate budgets

- Getting accurate numbers for quarterly sustainability reviews requires creating
  appropriate budget entries for all client accounts belonging to your cell.
    - A client account belongs to your cell if the [client owner](../roles/list.md#client-owner)
      is a member of your cell.
    - On the Budget dashboard, client accounts have a category of "Billable" and
      are listed at the top (above non-billable accounts).
- *Budget entries should reflect reality:* For each month since the beginning
    of the (first) contract belonging to a given account, budget entries should match
    the number of billable hours that we had available for that month.
    - ⚡ If the sum of all budget entries for a given period *exceeds* the number of hours
      that we were able to bill for that period, this will artificially *lower* your cell's
      sustainability ratio and *hide* budget issues.
    - ⚡ If the sum of all budget entries for a given period *is lower than* the number of hours
      that we were able to bill for that period, this will artificially *increase* your cell's
      sustainability ratio and *inflate* budget issues (i.e., it will create the illusion of budget excess
      even if there wasn't any / even if it was lower that the sustainability numbers suggest).
    - When in doubt, check with the [Administrative Specialist](../roles/list.md#administrative-specialist)
      and see if budget entries need to be updated based on the number of hours that were billed to a client
      for a specific period.

#### Unused budgets from fixed-price projects

Note that the current implementation of SprintCraft does *not* take unused hours
from fixed-price projects (which we sometimes refer to as "anti-overhead")
into account for sustainability calculations. More specifically, when calculating
[Overhead](../cells/budgets.md#cells-with-clients), SprintCraft does not subtract
the sum of unused billable hours from fixed-price projects (including maintenance)
from the sum of non-billable hours that other client epics may have incurred.

#### Budgets 💰 vs. capacity allocations ⏱️

*Keep in mind that budgets are not the same as capacity allocations:* When a project
first starts, we usually base capacity allocations for coming months on budgets.
However, there are several situations where budgets and capacity allocations
may no longer be tied together as closely as they were in the beginning of a project.
For example:

- We may have more budget left than hours needed to complete the scope of an epic
  in the coming months.
- An epic may have a lot of budget left but the work may become completely blocked
  on the client, upstream etc. (which means we can lower capacity allocations
  for the coming months to 0h).
- An epic may run out of budget before the agreed-upon scope has been completed.

### Budgeting workflows for different types of epics

💡 Unless indicated otherwise, steps listed below are the responsibility of the epic planning and sustainability manager.

#### Fixed scope / Fixed budget

*These types of epics typically represent fixed-price projects and are linked to fixed-price accounts.*

- Initially:
    - The [Administrative Specialist](../roles/list.md#administrative-specialist) sets the budget for the first month to the total number of hours that we have available for the project,
      and to 0h for the following month.
- While project is running:
    - Adjust budget entries for past months as necessary:
      If actuals for past months differ significantly from budgets that were set initially,
      budget entries will need to be adjusted to match actuals more closely.
      (This makes it possible to get accurate numbers for quarterly sustainability reviews
      while the project is still running.)
    - Set budget for current month to remaining budget.
    - Reset budget to 0h from following month on.
    - If the project runs out of budget before its scope is complete:
        - Adjust budgets for past months to match actuals.
        - For the month during which the budget ran out, set the budget to the remaining hours
          for that month (i.e., the number of hours that was left in the budget at the end
          of the previous month).
        - Set the budget to 0h from the following month on.
- When project is over:
    - If no budget is left, or the project went over budget *(same as above)*:
        - Adjust budgets for past months to match actuals.
        - For the month during which the budget ran out, set the budget to the remaining hours
          for that month (i.e., the number of hours that was left in the budget at the end
          of the previous month).
        - Set the budget to 0h from the following month on.
    - If below budget: No changes needed (unless some budget updates were missed while the project was running,
      i.e., unless budgets and actuals still differ significantly for one or more past months).
    - Either way: Double-check that budget entries for individual months sum up to the total number of hours
      that the client paid for (i.e., the total number of hours from the initial quote/contract for the project,
      plus any budget extensions that the client approved in the meantime).

#### Dynamic scope / Monthly budget

*These types of epics typically represent recurring maintenance work and are linked to fixed-price accounts.*

- Initially:
    - The [Administrative Specialist](../roles/list.md#administrative-specialist) sets the monthly budget to the number of billable hours that we have available per month.
    - If the end date of the current contract is known, the [Administrative Specialist](../roles/list.md#administrative-specialist)
      sets the budget for the month that follows the end date to 0h.
- While project is running:
    - If the default number of hours invoiced per month changes, the [Administrative Specialist](../roles/list.md#administrative-specialist) will create a new budget entry for the month from which the change applies.
        - For example, if the monthly volume of work drops or increases significantly,
          the epic owner may work with the client to come up with a new monthly budget
          that better matches the recurring workload.
    - If an epic from this category happens to be billed hourly: At the beginning of each month, a new budget entry that matches time logged over the course of the previous month will be [generated automatically](#automation).
- When project is over:
    - Double-check that budgets for previous months match billed hours. (This should already be the case
      if budgets were updated while project was running.)

#### Dynamic scope / No specific budget

*These types of epics typically represent recurring support work and are linked to regular (i.e., non fixed-price) accounts.*

- Initially:
    - If there is an approximate volume of hours that we are expected to work per month,
      you may optionally set the monthly budget to that volume.
      (The initial budget that you set will be overwritten by the next [automated budget update](#automation).
      So this is mainly helpful if you don't want SprintCraft to treat time logged over the course of the first month as overhead.)
- While project is running:
    - At the beginning of each month, a new budget entry that matches time logged over the course of the previous month will be [generated automatically](#automation).
    - If the number of hours invoiced for a specific month ends up differing from actuals, the [Administrative Specialist](../roles/list.md#administrative-specialist) will retroactively adjust budget entries as necessary.
- When project is over:
    - Double-check that budgets for previous months match billed hours. (This should already be the case
      if budgets were updated while the project was running.)

#### Exceptions

Note that you may encounter some situations and/or edge cases that the instructions above
don't explicitly cover. In these cases the main aspect to guide your decisions should be
that *budgets set in SprintCraft should never exceed the number of billable hours
that we had available in the past, present, or future* (as this would hide sustainability
issues).

## Sustainability Management

Each generalist and project cell is meant to be a sustainable entity by itself (with one exception -- the [DevOps cell](../cells/list.md#serenity)).
Its members are the closest to most of the work that impacts its sustainability - the successful estimation and delivery of each client project.

Responsibility for tracking the volume of billable and non-billable hours (i.e., hours logged on internal/non-billable accounts)
is decentralized to individual cells. See [Cell Budgets](../cells/budgets.md) for more details.

The sustainability manager of a cell is responsible for ensuring that the cell keeps the amount of non-billable hours logged in check.
In particular, the sustainability manager must monitor the cell's [sustainability ratio](../cells/budgets.md#cell-and-company-wide-sustainability)
and make sure that the cell hits its current sustainability target (as listed under [Cell-Specific Rules](../cells/cell_specific_rules.md)).

To help with this, the sustainability manager must perform the following activities:

### Every sprint

* **Sustainability preview for upcoming sprint**.
    * When [planning the next sprint](sprints.md#sprint-planning-process), the sustainability manager should:
        * Review the amount of billable and non-billable work scheduled by the cell for the next sprint.
        * Warn epic owners about any budget issues and get them to address those proactively.
      If the situation cannot be redeemed immediately, the sustainability manager should get a timeline
      and plan from the concerned epic owner(s) to resolve the issue.
    * The **Budget** section of the [SprintCraft] dashboard includes **Left this sprint**, **Next sprint** and
      **Remaining for next sprint** columns that can be used to estimate next sprint's sustainability.
    * The sustainability preview for a specific cell should be posted on the cell's epic planning
      and sustainability epic (or integrated into the cell's main [epic planning](../roles/list.md#epic-planning)
      update for the current sprint.)
    * See [this section](../roles/list.md#sustainability-management) of the roles page
      for the template to use for the sustainability preview.

### Every month

* **Accounts review for previous month**.
    * At the beginning of each month, after the deadline for recording/adjusting
      time logs for the previous month, the sustainability manager should check that the tasks
      worked on over the course of the previous month are [using the right accounts](#checking-account-associations).
    * For the purpose of efficiency, this review can be combined with [adding actuals](#checking-and-recording-actuals)
      for the previous month to the [epic planning spreadsheet].
    * In particular, incidents that affected one or more client instances should use the clients'
      *Instance Maintenance* accounts instead of an internal DevOps account.
    * This review needs to happen before invoices are sent to clients, and results
      should be posted as a comment on the "Epic planning and sustainability management - &lt;your cell&gt;" epic.
    * See [this section](../roles/list.md#sustainability-management) of the roles page
      for the template to use for the accounts review.

### Every quarter

* **Sustainability review**.
    * Every three months the sustainability manager should post an update on the forum
      about the current status of the cell's sustainability ratio and budgets for internal work,
      as well as plans for the coming months that ensure sustainable ratios.
    * This is done in January (Q1 update), April (Q2 update), July (Q3 update), and October (Q4 update).
    * *It must wait until after client account budgets for the previous quarter are
      fully up-to-date in SprintCraft to make sure that the reported numbers are correct.*
    * See [this section](../roles/list.md#sustainability-management) of the roles page
      for a selection of templates you can use for the sustainability review.
* **Budget updates for non-billable accounts**.
    * After each sustainability review, the sustainability manager should review
      and update budgets for non-billable accounts in [SprintCraft],
      in coordination with owners of internal epics that are associated with these accounts.

### As needed

When a cell's sustainability target [changes](../cells/budgets.md#cell-and-company-wide-sustainability),
the sustainability manager should [adjust the current target in SprintCraft](https://doc.sprintcraft.opencraft.com/overview/sustainability/#setting-up-sustainability-targets)
so that the value shown on the [sustainability dashboard](https://doc.sprintcraft.opencraft.com/overview/sustainability/#sustainability-dashboard) matches the new target.

[epic planning spreadsheet]: https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit
[SprintCraft]: https://sprintcraft.opencraft.com

[^1]: Private for OpenCraft Employees