# Working with Clients

## Client Briefings

We have many clients, and not all of them are covered in this handbook. If you're encountering a new client for the first time, you're encouraged to visit [our CRM](https://crm.opencraft.com/)[^1] and read up on their entry.

Briefings with current clients are combined with [Value Propositions](#value-propositions) into one document for each client, stored in [this directory](https://drive.google.com/drive/folders/1WMVBAJZoldCzrrqCrZkpNObEAy5fGQOf?usp=share_link)[^1] on the shared Google Drive.

For older clients, client briefing information may be stored under the 'History' accordion expander in their CRM entry. One of the listed notes is likely to be the client briefing, another the value proposition. Often, both are in the entry labeled 'Client Briefing' for simplicity's sake, so check there first if there's not an explicit 'Value Proposition' entry.

Maintaining these client briefings is a joint effort between the [Business Development Specialist](../roles/list.md#business-development-specialist) and the [Client Owner](../roles/list.md#client-owner) for that client.

### Initial Briefing

When a new client signs a contract with us, the [Business Development
Specialist](../roles/list.md#business-development-specialist) will post an initial 'Client Briefing' with [this template]({{ config.repo_url }}-/blob/{{ git.commit }}/handbook/templates/client_briefing_template.md), which has been
made into a separate page so its formatting can be copy-pasted. Time for this work should be logged to whatever epic
we start for this client.

### Scope of the Briefings

The briefings should contain information that is useful to all (or nearly all) of their epics, as well as other background and contact information that may be helpful to know. Details relevant only to one epic should be kept in the relevant epic ticket, and that epic ticket should be linked in the briefing. If an epic is completed and not likely to be especially important for understanding the current needs of the client, it can be removed from the listing.

### Briefing Maintenance

Information about clients changes over time-- which projects we have, important links, points of contact-- all of these can change, as well as background knowledge. The purpose of a briefing is to allow a developer to get up to speed quickly on a client's most important information.

As part of the [periodic client strategy tickets](#client-strategy-tickets), client owners are expected to update their client briefings and [value propositions](#value-propositions).

## Value Propositions

Value Propositions are an adjunct to our client briefings which help summarize the overall benefit (our strategic value) to the client. The process for these is the same as the client briefings-- they are initially created by the Business Development Specialist and are afterward updated and maintained by client owners. If you want a better sense of the strategic vision between us and the client, read up on these.

[The template for value propositions is available here]({{ config.repo_url }}-/blob/{{ git.commit }}/handbook/templates/value_proposition_template.md). Like the client briefing template, it is placed on its own page for easy copy/pasting into the CRM.

Each account entry in the CRM has a 'History' accordion expander which contains comments on the client. One of these is likely to be the client briefing, another the value proposition. Often, both are in the entry labeled 'Client Briefing' for simplicity's sake, so check there first if there's not an explicit 'Value Proposition' entry.

## Client Strategy Tickets

For client epics (epics labeled `client-epic`), approximately once a quarter, a task will be scheduled automatically, timeboxed to three hours, where client owners review the briefing, current epics, and value proposition, looking for patterns. If they see an opportunity for a new project with the client, they should propose spending a few hours of discovery to the client on the project idea. If the client accepts, they shall schedule a discovery with one of the team members on [Discovery Duty](../roles/list.md#discovery-duty-assignee).

This ticket is also used as an opportunity to update the [Client Briefing](#client-briefings) and [Value Proposition](#value-propositions) if they have not been updated in a while.

## Communicating with Clients

We prefer direct communication between our team members and clients. There are a few things to keep in mind, based on medium and situation.

### Client owners are the point person

[Client Owners](https://handbook.opencraft.com/en/latest/roles/#client-owner) manage the relationship with the client and help them with ongoing and new initiatives. They keep an eye on epics for that client and keep the briefings updated, as mentioned above.

They're good to include by pinging on conversations with clients for anything which may have larger context/impact than the task you are currently on, so that they can have visibility and the opportunity to offer input.

### Email

When clients write to you via email, always CC `help+client@` in your replies, where `client` is the name of the client's mailing list label, such as `esme` or `campus`. If you're unsure of the client's mailing list label, ask the client owner. This allows any developer to have record to refer back to as needed. If you are not the client owner, CC them (and the epic owner as necessary) as well to make it clear you're specifically looping them in.

### Issue trackers

Many of our clients have their own issue trackers. You can (and should) talk directly with your contact on that ticket about any questions you have about the requirements, or if you need the client to review your work. If you get stuck from a technical perspective, consult with fellow team members instead.

## Clients
### edX

We work with edX all the time, in various ways - but this document is specifically about how to work with edX _as a client_, when they're paying us for the work. edX calls this "**Blended Development**", because the team is a blend of OpenCraft and edX people.

An overview of when, why, and how edX does "blended development" is on their wiki at [Blended Development Runbook and Best Practices](https://openedx.atlassian.net/wiki/spaces/COMM/pages/1863189038/Blended+Development+Runbook+and+Best+Practices).

#### Links

* [edX Jira](https://openedx.atlassian.net/jira)
* [edX wiki](https://openedx.atlassian.net/wiki/home)
* [Blended PR Workflow](https://openedx.atlassian.net/wiki/spaces/COMM/pages/1947926666/Blended+PR+Workflow)

#### General tips:

* Before opening a [pull request](pull_request.md), review the [Blended PR Workflow](https://openedx.atlassian.net/wiki/spaces/COMM/pages/1947926666/Blended+PR+Workflow) which explains the procedure for opening a PR on any blended project. (In addition to the usual [PR process](pull_request.md) that we follow on all projects.)
* Remember to default to public communications. This is an open source project and generally most of the aspects of blended development can be public - so default to discussing and documenting things on public Confluence pages, Jira tickets, ADRs, Discourse threads and public Slack channels.
    * If that doesn't get enough attention or timely response, then ping on synchronous and/or private tools, linking to the public question.
* Slack is one of edX's major communication channels and we should not be hesitant to ping people on it, in the same way we try to reduce synchronous pings on Mattermost within OpenCraft.
    * Note: understand that edX has two Slacks: their internal Slack (which we cannot access directly), and the Open edX Slack (which we do have access to). Most edX employees are on both, and some channels are shared between both Slacks, such as [the #edx-shared-opencraft channel](https://openedx.slack.com/archives/C01ATGRBBDM).
    * Some projects we do with edX have their own public Slack channel on the Open edX Slack channel, which should be documented on the project's confluence page.
    * If you don't have access to [the #edx-shared-opencraft channel](https://openedx.slack.com/archives/C01ATGRBBDM), ask someone to invite you. It is OK to share some non-public information on this channel, as it is private to OpenCraft and edX.
    * Slack should be treated as ephemeral, so if it's used for making any decisions impacting the project, those decisions should be documented in Confluence right away.
* Iterative development and regularly shipping PRs to production is the expectation; if your PR is languishing without a review for more than a week, please considering that a big problem that you need to solve immediately. Ping people and escalate as needed.
* If newcomers are working on tickets for edX, they should first submit their PR to an internal fork (OpenCraft fork) and get it reviewed, and only submit it upstream once the reviewer has approved it.

#### How to escalate an issue / how edX will escalate an issue

If you are blocked or have an urgent problem (or edX is in that situation), the procedures for escalating issues with each other are documented in Confluence at [Blended Provider Status Pages: OpenCraft: Escalation Path](https://openedx.atlassian.net/wiki/spaces/COMM/pages/1529676506/OpenCraft) ; also see the project page for each blended project, to see who the "edX lead" is as well as any project-specific escalation instructions.

#### How to mention people on the edX Jira

Due to [an annoying bug](https://jira.atlassian.com/browse/JSDCLOUD-1476), you may find that you are not able to ping people on the [edX Jira](https://openedx.atlassian.net/jira). There used to be a workaround within Jira but it is no longer available. Until this bug is fixed, please do the following after commenting on the edX Jira:

1. [Join the edX Slack instance](https://open.edx.org/blog/open-edx-slack/).
2. Find the person you want to ping and let them know you've updated the ticket, with a link to your comment.
3. If they ask why you are pinging, point them to [this section of the handbook](#how-to-mention-people-on-the-edx-jira), which is publicly available.

---
<!-- Yonkers  -->
### Yonkers

See [our private documentation](https://gitlab.com/search?&repository_ref=master&search=yonkers&group_id=3203078&project_id=7478477).

[^1]: Private for OpenCraft Employees
