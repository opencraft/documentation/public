# Sprints

## Overview

We work in two-week sprints.  While each cell has its own board, backlog and
sprint, they are all held in sync on their start and end.

Here's an overview of the process highlighting typical milestones and
activities.  For a full detailed description of the steps see the [Sprint
Planning Process](#sprint-planning-process).

| Day of Sprint     |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|-------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Day 0 (Monday)    | Last day of the previous sprint's [planning process](#sprint-planning-process). We use [SprintCraft] to review cell member's time allocation and ensure everyone has enough work and is not overcommitting.  We also collectively make sure we're staying on track with each client's monthly budget and each epic/project budget.                                                                                                                                                                                                                                                                                                                                               |
| Day 0 or 1        | Each cell member should review every ticket they have in the sprint, to confirm the due date, plan when/how to approach the work, coordinate with the reviewer for time-sensitive reviews, etc.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| Days 0-4          | Development - each team member works on their tickets and code reviews. Tasks move from left to right until completion on our JIRA tracker.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Day 2 (Wednesday) | <a id="epic-update-deadline"></a> Epic owners review the status of their epics and post an [epic update](../epic_management.md#epic-update-template) for each one of them. This includes creating additional tickets and scheduling them for future sprints.                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| Day 7 (Monday)    | Each cell member shares a [mid-sprint update](#sprint-updates). It should communicate if everything is going smoothly or if there are tasks at risk of not being finished — cell members coordinate with each other to help.                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| Days 7-11         | Development continues.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Day 9 (Wednesday) | <a id="new-tasks-deadline"></a> **End of day Wednesday is the deadline for creating and pre-assigning stories in the upcoming sprint**. Epic owners double-check that the upcoming sprint contains all tickets that their cell should work on.                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| Day 10 (Thursday) | <a id="estimation-session-deadline"></a> Cells collaboratively and asynchronously refine and estimate task complexity (i.e. story points) via asynchronous estimation sessions.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| Day 11 (Friday)   | When Friday starts, the estimation session is closed. At this point cell members must go through their tickets that are going to be in the next sprint, and update the estimated hours. For the new tickets, they have to set both original and remaining estimated hours. For other tickets, only the remaining estimated hours have to updated. [Sprint Planning Manager](../roles/list.md#sprint-planning-manager) pings team members on Mattermost about any anomalies in assignments (such as tickets without assigness/reviewers, or overcommitment). Cells update task assignments as necessary and pick up reviews as their availability for the upcoming sprint allows. |
| Day 14 (Monday)   | On Monday Crafty will ping team members on Mattermost about any remaining anomalies in assignments (such as tickets without assigness/reviewers, or overcommitment). By that day, most members should have solid plan for the next sprint, and the day can be spent making final adjustments. Depending on their timezone, some cell members may have some time on Monday to finish up development or work ahead before the new sprint starts. They also share a [end-of-sprint update](#sprint-updates).                                                                                                                                                                        |

## Additional Information & Tips

### Planning

* All tasks are split up into epics, and each epic has an [epic owner](../roles/list.md#epic-owner). Before
  Day 1 (Tuesday), everyone asynchronously assign each story in the upcoming
  sprint to a developer and a code reviewer.
    * The epic owner is responsible for making sure this happens for each story
      in their epics that must happen in the upcoming sprint. For stories that
      don't have an epic owner, either the tentative assignee (if there is one
      already) or the person who reported the ticket is responsible for doing
      this; this is also documented
      [in our sprint bot, Crafty](https://gitlab.com/opencraft/dev/jira-scripts/blob/bf4ea0ae3447fc2ae2098444c2b13a7d2f9e06c3/ReadyForSprintReminder.groovy#L112-124)[^1].
    * Epic owners should also check their cell's
      [SprintCraft] board
      for the upcoming sprint, ensuring that the tasks the team plans to take on
      for the upcoming sprint are in line with each client's budget and each
      epic's budget.

### During the sprint

* Take a look at the current sprint board of your cell. There are filters at the top such as
  *Me*, *My reviews*, *My WIP*, and your name, which can be toggled to show only issues relevant to you:
    - [Serenity](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=24)[^1]
    - [Bebop](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=26)[^1]
    - [Falcon](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=45&view=detail)[^1]
* Drag a task from column to column on the sprint board to update its status as you work through it.
  The various statuses/columns are described below. Tasks move from left to right until completion.
* In general, "work from the right" - it's better to finish a task that's already in progress
  than it is to start a whole new task. This reduces context switching, which will help get
  things done more quickly. With that said, if you are blocked on a task, move on to another
  task, until unblocked.
* Daily, use the Tempo time tracking on the Jira board to record the time spent on each task
  and update each ticket being worked on once finished on it for the day. Even just a quick
  summary or sentence of where you're at with the task is useful to your reviewer.

## Sprint Planning Process

Since we're a remote team spread globally around the world, having synchronous meetings might not be fair for different members because it's difficult to find a decent time for everyone. Therefore, our sprint planning process is asynchronous.

Planning for a given sprint is done asynchronously, with the steps happening during the previous sprint. So, in any given sprint, one of the tasks everyone has is to plan for the following sprint.

In order to guide you through planning your sprint, a [sprint planning checklist](#sprint-planning-checklist) is already created for you at the start of each sprint.

In the following sections, you can find additional details for some of the steps in the sprint planning process.

### Sprint planning checklist

As mentioned earlier, everyone is responsible for planning their own sprint. In order to facilitate that, we have a sprint planning checklist for each member to follow to ensure they haven't missed any of their responsibilities.

All sprint planning checklists are created based on a single [template](https://api.app.listaflow.com/admin/workflow/checklistdefinition/H1vu2XiXT56A/change/), configured in [Listaflow]. For the development cells, [SprintCraft] triggers a webhook in Listaflow to create the checklists for each user. For the support cells, [Listaflow's recurrence feature](https://gitlab.com/opencraft/dev/listaflow/-/tree/1d284f50bae20752da82345fe8ebcb48095e3c0b#how-to-use-listaflow) is used to create the checklists.

Your current Sprint Checklist is always available by logging into [Listaflow]. The tasks are organized according to their deadline. Your checklist will be automatically customized per your roles, such as firefighter, sprint planning manager, discovery duty, epic owner, etc. For development cells, SprintCraft provides role information to Listaflow based on a combination of the relevant rotations (such as the [firefighter rotation](https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit)[^1]) and the [opencraft.yml file](https://gitlab.com/opencraft/documentation/public/-/blob/master/hooks/data/opencraft.yml). For non-dev cell members, the roles are updated manually as needed in [Listaflow's team admin settings](https://api.app.listaflow.com/admin/profiles/team/USnUAnBqQAaA/change/).

A good habit is starting off your day by finalizing your sprint planning responsibilities for that day in the sprint. This would ensure that you're always on top of your sprint planning responsibilities, and a well planned sprint is a good one!

### Reserved time

Sprint planning is an essential part of our job. It is a task that extends through the whole sprint. To make sure there's enough time for sprint planning, [SprintCraft] reserves 8 hours of time for it each sprint. (Specifically, for each team member it subtracts 8h from their total availability as configured in [JIRA](https://tasks.opencraft.com/secure/TempoSchemeWorkload!default.jspa)[^1] and sets the remaining hours as the team member's commitment `Goal` for the upcoming sprint.)

Although we could create tickets for every member to block enough time for sprint planning, that wouldn't be manageable, and that would pollute the sprint board too.

The goal is to let everyone enough time to plan their sprint properly to reduce burnout, stress, and other negative factors. While pursuing every opportunity to give the best quality and more features to our clients, it can be easy to treat this buffer time as a "safety buffer" where other client tasks can fit, but please do **not** do that. This time is reserved for you and those tasks that must be in the next sprint.

Of course, if the next sprint is planned and nobody needs help with its tickets, and there are no fires, you are welcome to work ahead, but please keep the reason for this buffer in mind.

### Tentative assignees

To ensure that tasks lined up for the sprint are all actively being taken care of, from the beginning of sprint planning, some tasks are tentatively assigned by their creator. To show the tentative assignment, these tasks are flagged in Jira (they appear in yellow in the backlog).

The tentative assignee isn’t meant to be necessarily the person who will be actually working on the task. Like for PR reviews, the goal is to ensure proper focus and avoid dilution of responsibility, so we need one specific person assigned to each ticket to do that review early on, even if the actual assignee changes later.

The assignment is done without considering size/availability, since the tickets aren’t yet scoped or estimated. The tentative assignee is merely the person who will be responsible for finding the final assignee, and getting the task through the planning steps in the meantime, such as reviewing the task description.

The tentative assignee is responsible for reassigning and/or rescoping the tickets before the sprint starts. Note that if no action is taken, by default the tentative assignee will become the actual task assignee when the sprint starts.

### Estimation session

We each estimate the complexity of the tasks for the upcoming sprint in terms of "story points". For this purpose an estimation session is created automatically in Jira. An email with a link is sent out when the session opens.

We use story points as a shorthand for agreeing about the complexity of a task. Story points are meant to be a relatively objective measure of "complexity", and not necessarily indicate how long it would take any particular developer to do.

For each task, you should:

* Read through the description.
* Decide whether you can take it.
  * If you do want it, it's ok to spend a bit more time investigating.
* If you have questions, put them in comments on the ticket.
* Once you understand the task, assign points as described in the [process section](#sprints).
* Use the "estimate comments" box to qualify estimates.
* Choose `?` if you have no idea how many points.
* A little green `(/)` will appear next to your picture up top when you are done.


    * We use a fibonacci scale for story points (please see
      [task workflows](../task_workflows.md) for more detail on story points for different types
      of tasks).
    * If anything about the task or the acceptance criteria is unclear,
      post a question as a comment on the task and try to work with the epic owner
      (or the reporter if there is no epic owner) to get it answered before the start of the upcoming sprint.
      Note that during the asynchronous estimation sessions, any comments/questions
      posted in the estimation session will be lost once the estimation session ends,
      so it's preferable to post questions on the tickets themselves.


Complete the estimation session [before estimation session closes](#estimation-session-deadline). Stay conscious of time budgets and try not to log more than a few minutes per task to do these estimates.

### Task insertion

The following process applies when someone wants to add a ticket to the sprint after the [deadline for creating new tasks](#new-tasks-deadline):

* If the ticket replaces another ticket already in the sprint, or is split out from one: explicitly reference the ticket being replaced or split out from in the task description, and ping the sprint planning manager for confirmation.
* Otherwise, place the ticket at the top of the stretch goals, and ping the sprint planning manager to inform of the desire to include the ticket in the sprint if possible. There would not be any guarantee of such an inclusion, but once the sprint is fully ready (ie, all the tickets in the sprint are ready and all the other verifications before the start of the sprint have been completed, including ensuring nobody is overcommitted), then the ticket can be considered for a sprint insertion if someone still needs work. This is handled the same way we would treat a ticket that we want to add in the middle of a sprint.
* No other exception. A sprint insertion has to follow either of these paths, or it will be moved to the next sprint or the stretch goals.

### Dealing with sprints without enough tasks

To ensure that all sprints have enough tickets and work available to fill everyone's capacity, we use the [Work Finding Checklist](https://app.listaflow.com/templates) from the templates in Listaflow. This checklist is comprehensive, helping you check every angle for more work and making sure that any issues with finding it are not swept under the rug.

#### “Last resort” backlog

A special backlog, called “last resort” has a reserve of tasks with a special budget, for when all other attempts to find enough tasks for a sprint have failed. It is populated by tasks that we would normally have not considered for a sprint, but that could be taken, as its name indicates, as a last resort (and *only* as a last resort!). The idea is to ensure we don't let team members without enough tasks on their own in these cases, by providing both a last resort option, and a way to notice immediately when this happens.

Taking a task from the Last Resort backlog (created as a sprint named 'Last Resort - Accepted') requires completing the [Work Finding Checklist](https://app.listaflow.com/templates), which includes special instructions not only for selecting a last resort task, but what other actions to take when doing so.

##### Populating the "Last resort" backlog

Anyone can suggest tasks to include in that backlog, and they are then reviewed and approved by the CEO.

Tasks which go in it are mainly of the “important but not urgent” type, to keep the tasks useful without having to change them every sprint. They should also be tasks that wouldn’t be done otherwise - for example things that are a few sprints out, or don’t have a budget. Many are internal/non-billed tasks, but we should try to add billable tasks there whenever possible.

Do you know of tasks like this? Please [add them to the “Last resort - Proposed” backlog](https://tasks.opencraft.com/secure/RapidBoard.jspa?rapidView=77&view=planning.nodetail)[^1], and ping the CEO on them to let him know to review them. If accepted, they will be moved to “Last resort - Accepted”, and get a budget assigned if needed.

For work done ahead on existing clients, ping the billing specialist to organize how to log and later bill the client.

### Sprint updates

These are text and video updates posted in a dedicated "sprint updates" forum thread for the cell, which is reused between sprints.  There are two.

**Mid-sprint updates** are text-only posts on the first Monday. In this update the cell member will say how their sprint is going, and whether they anticipate not being able to finish some of their stories, or finishing early.  And, optionally, the cell member give kudos to others' work that deserve positive feedback.  They are a way for the cell to get mid-term feedback on the sprint progress and anticipate blockers.

**End-of-sprint updates** are video posts on the second Monday.  They are a way for us to keep a regular occasion for everyone in a cell to see & hear each other directly on video, rather than switch to 100% textual. They also include an optional text component, that complements the video format, if necessary.

The video recording should have 1 to 2 minutes maximum on [Loom](https://www.loom.com/) ([credentials](https://vault.opencraft.com/ui/vault/secrets/secret/show/shared/www.loom.com) for core members) or using the software of your choice such as [OBS Studio](https://drive.google.com/file/d/1NnAfy0_Iskzfs_1O_D6IsFL9laG9Ey7C/view):

* Share **both** your screen & camera, and name the video `Sprint X - <Cell> - <Your name>` with X being the number of the sprint finishing
* If you are a core member using a Loom video, copy the link from the "Send your video" box on the top right of the Loom video page, and [place it on a line by itself to ensure discourse shows the video inline](https://forum.opencraft.com/t/sprint-updates-serenity/634/9)[^1].
* If you create your own video, upload it to the [Sprint update videos folder](https://drive.google.com/drive/folders/19L9MDRnfUpUxpIzYbcowiqYg-0ahgJEU), and [use an iframe to display the video inline in your post](https://forum.opencraft.com/t/sprint-updates-serenity/634/11)[^1]:
    * `<iframe src="https://drive.google.com/file/d/[hash]/preview" width="640" height="360" frameborder="0" allowfullscreen></iframe>`
    * Note that it might be necessary to disable the enhanced tracking protection from Firefox on `forum.opencraft.com` to play videos embedded this way.
* [Optional] Include a text update below your video - for example to highlight points that shouldn't be missed, or provide links to elements you mention in your video.

Present the following points in your video update:

* If a newcomer is scheduled to join this week, ensure you've created a quick introductory video of yourself and stored it under your cell in [OpenCraft Shared Drive > Meeting videos > Team introductions](https://drive.google.com/drive/u/2/folders/1ryyhg5J51MesV1FpbYyxMZLk0njYIo-M). Provide your name, how long you have been with OpenCraft, your current location, and an interesting fact about you.
* Some of your work, in a mini-demo/presentation of what you did during the previous sprint, mentioning any major accomplishments, interesting tickets, lessons learned, or problems encountered that are worth sharing with the team. What has been accomplished, what went well, what went not so well, and the reason for any spillover. Don't just show your Jira board though -- people can already go see this by themselves.
* Any upcoming vacation if you have some in the upcoming sprint.
* [Optional] Giving kudos
* [Optional] Pointing to announcements and discussion started on the forum, introducing them orally when it’s useful.
* [Optional] If you would like to make an extended presentation or tech demo, which doesn't fit in the 2 minutes of your weekly update, record a separate second video, which you will post in the forum in a dedicated thread. Mention who the intended audience is (the rest of your cell, the whole company, etc.), ask for questions explicitly and come back to the thread regularly to answer them.


### All-hands meetings and tech talks

From time to time we get together and do tech talks, to share knowledge and as an opportunity to meet members of the other cells. Note that a tech talk can also be contributed asynchronously using the video recording at the end of any sprint.

An all-hands meeting will be scheduled when there's something we want to discuss at the team level, or when there's a tech talk. To schedule a tech talk, create a ticket in your own cell to prepare it, then discuss in [the tech talks thread](https://forum.opencraft.com/t/all-hands-meetings-and-tech-talks/189/10)[^1] a date that works for all cells (it must be on the same day as a mid-sprint update).

### Sprint Retrospectives

Each cell performs a retrospective every sprint. The purpose of the sprint retrospective is to allowed continued improvement on our processes and methods by creating dedicated time for reviewing how each sprint went, and how things might be improved.

At the beginning of the sprint, a synchronous retrospective meeting is automatically scheduled for the day after the sprint ends.


All team members for whom the meeting falls in their regular working hours are encouraged (but not required) to join the sprint retrospective meeting. One team member leads the meeting-- conventionally, this is Firefighter 1, or the Sprint Manager if the sprint firefighter isn't available. However, any team member can take the lead if they like, especially if neither of the people with those roles are in the meeting. During the meeting:

1. Each team member is asked how they felt about the sprint.
2. Team members discuss what went well.
3. Team members discuss what did not go well.
4. The team suggests ideas on how to address the things that didn't go well.
5. Any other items worth commenting on from the last sprint are brought up for discussion.

After the meeting concludes, the meeting lead writes a summary of the issues in the Sprint Retrospective thread on the forum. The meeting recording is posted automatically by Crafty. If necessary, the meeting lead (or the team members affected) schedule follow-up tasks to address issues, or ping the [Developer Advocate](../../roles/list/#developer-advocate) for consult.

### Social Chat

The sprint retrospective exists to help address problems discovered in the previous sprint. To make sure that members still have an opportunity to have fun face-to-face interactions with their team, we also have *social chats*. They are informal and attendance is optional.

The social chat is freeform. Talk about whatever you like! It doesn't need to be about work (shouldn't be about work?): games, teaching how to prepare a cocktail or recipe, participants showing an interesting place in their neighborhood, etc.

Social chats are available on the global team calendar. At the time of writing there are two, an 'AM' and 'PM' chat. Pick whichever chat is most convenient for you.

#### Duration, logging time, and attending multiple chats

* Calendar events for social chats are 30 minutes long, but participants can stay as long (or short) as they like. Anyone can leave at any time.
* Team members are welcome to attend more than one social chat per sprint, **but total time logged is limited to 30 minutes per sprint**.
* Participants log time on the [recurring *Social Chat* ticket](https://tasks.opencraft.com/issues/?filter=14421)[^1] for their cell.

## Support Cell Sprints

[Support cells](../organization.md#support-cells) have sprints on one-week intervals instead of two-week intervals, and meet synchronously for sprint planning. This may change to asynchronous in time.

### Preparing for a Support Cell Sprint

Ahead of the support cell sprint planning meeting, look through all of your current tasks and make sure any which have not been updated to their most current state are updated.

Then, look in the backlog. Consider any spillover you may have, and look at the upcoming sprint. If it does not exist, create it. Put any tickets you anticipate working on into the new sprint to reduce time spent planning during the meeting.

### Support Cell Sprint Planning Meeting Agenda

The sprint meetings have the following agenda:

1. Each member gives an overview of what they worked on this sprint, and any items that spilled over and will affect the next one, as well as reasons for the spillover.
2. Close the current sprint, setting all still open tickets to move to the next sprint.
3. With the team, look over the items in the upcoming sprint. Take a moment to verify:
    * Each team member acknowledges the workload they're committing to.
    * All tickets have story points, assignees, and reviewers.
    * Ask about vacation or other reasons for reduced capacity. Will this affect the work being taken on?
    * If there is spare capacity and no intentional reduction in hours, work with the team to pull items from the backlog until capacity is reached.
4. After confirmation on all items above, start the next sprint.
5. Any final notes/announcements/kudos.

### Support Cell Sprint Video Updates

Like the development cells, support cell members are expected to post video updates to keep the team at large abreast of new developments [in this thread](https://forum.opencraft.com/t/sprint-updates-non-cell-team-members/690)[^1]. However, since support cell members have one-week sprints instead of two-week sprints, they are only expected to post these every other sprint, at the same time as development cell members post theirs.

This is to make sure that time can be scheduled to run through all updates in one go to reduce context switching among team members.

### Support Cell Social Chats

Since support cells do not have firefighters, [social chats](#social-chat) are lead by the [Sprint Manager](../roles/list.md#sprint-manager) by default. Other team members are encouraged to volunteer to lead particular social chats, especially if they have an idea for an activity.

[SprintCraft]: https://sprintcraft.opencraft.com
[Listaflow]: https://app.listaflow.com

[^1]: Private for OpenCraft Employees
