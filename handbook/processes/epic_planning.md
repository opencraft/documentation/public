# Epic Planning

## The Lifecycle of an Epic

1. Most epics are created following a discovery based on a client requirement.
   (For internal projects, the client is OpenCraft).
1. If the client is not a client yet (a prospect), the epic is placed in the *Prospect* column. Once the discovery and corresponding estimates are shared with the client, the epic is moved to *Offer / Waiting on client*.
1. If the client accepts the work, the epic is moved to *Accepted*.
1. Once actual development starts, the epic is moved to *In Development*.
1. Once the client's requirements are met, and the client has access to the
   completed work, the epic can be moved to *Delivered to client / QA*.
1. The epic should be moved back from *Delivered to client / QA* to *In Development*
   if the client requests additional work or modifications that need development.
1. When all the work in the epic is complete (for instance if all upstream PRs
   have been reviewed and merged) the epic can be moved to *Done*.

*Recurring* epics are generally not based on a project or discovery, but are used to track work
for different cell roles (or long-running maintenance contracts for specific clients).

## Tracking Epic Status

* Each cell has its own **Epics** board and epics, and is responsible for making sure
  that the projects represented by the epics are being properly delivered based on
  the lifecycle described above.
* To find the epics board for your cell, go to **Boards** > **View all boards** > **Epics - &lt;your cell&gt;**
  in JIRA. (Note that if you've recently accessed the board, it may also be listed under **Boards** > **Recent boards**.)
* Every sprint, status changes of individual epics from the past sprint should be reviewed and evaluated
  to be able to keep both the Epics board and the [epic planning spreadsheet][^1] up-to-date.
  This involves making sure that:
    * New epics are moved through the set of initial states (*Prospect*, *Offer / Waiting on client*, *Accepted*)
      as appropriate until they reach *In Development*.
    * Each epic that is *In Development* has an epic update.
    * Delivery and bugfix deadlines for each epic that is *In Development* are on target.
      If that's not the case, ping epic owners on the epic(s) as necessary and remind them to:
        * Determine the impact of shifting deadlines and figure out how to minimize it.
        * Discuss the situation with the client (or the CEO in the case of internal epics),
          negotiate scope adjustments as needed, and make sure that the client (or CEO) is OK with them.
    * Epics that have become blocked on the client and/or other work are moved to *Offer / Waiting on client*.
    * Delivered epics are moved to *Delivered to client / QA*.
    * Completed epics are moved to *Done*.
* Every sprint, the [epic planning manager](../roles/list.md#epic-planning-and-sustainability-manager) should compare the Epics board for their cell
  with the corresponding sheet(s) of the [epic planning spreadsheet][^1] and update the spreadsheet
  as necessary. This involves:
    * Adding new clients and/or epics.
    * Updating info about client and/or epic ownership.
    * Removing entries for clients and/or epics from future months based on contract and/or completion status.

## Recording High-Level Information about Epics

We keep a record of all epics that we work on (except onboarding epics) in the [Time / Estimates](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1)[^1] sheet
of the [epic planning spreadsheet][^1] so that high-level info (such as who estimated and led an epic,
and how long it took to complete) can be [referenced as necessary](../how_to_do_estimates.md#how-to-do-a-ballpark-estimate)
in the context of future discoveries.

As part of the epic planning process, the epic planning manager is responsible for keeping the [Time / Estimates](https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit#gid=841714444&range=A1)[^1] sheet
up-to-date by:

* Adding entries for new epics.
* Updating entries for completed epics.

## Tracking Cell Member Availability

To be able to determine capacity allocations for individual projects, we need to track cell member availability.
As an epic planning manager you should:

* Keep a look out for newcomers and core members joining or leaving the team,
  and add their details to the [epic planning spreadsheet][^1].
    * For newcomers this includes adding onboarding time to the onboarding section
      and specifying their availability in the availability section
      of the [epic planning spreadsheet][^1].
      *Only make these updates when the corresponding information is fully public,
      i.e., the newcomer should know whether they have been accepted or not
      by the time these updates are made.*
    * Availability for each member is calculated in full-time weeks.
      For example, if someone's commitment is:
        * 40h/week, their availability will be `40 / 40 = 1`.
        * 30h/week, their availability will be `30 / 40 = 0.75`.
    * If you prefer, you can also specify availability of individual cell members in hours per week.
      Just note that if you do, you'll also need to modify the calculation of
      total availability accordingly. (I.e., instead of just summing up the availability
      of the cell members to get the number of person-months available for each month,
      you'll need to sum up the weekly hours *and* divide the sum by 40.)
    * For team members leaving the team, make sure to remove (or reset to zero)
      their availability for the remaining months of the year.
* Handle requests from cell members to change their weekly hour commitments,
  if the requested commitment is in the range of 30-40h per week:
    * Outside of that range, refer to the CEO: We generally avoid commitments that are
      lower or higher than 30-40h/week because they may
        * prevent cell members from taking some types of tasks,
        * increase overhead for commitments below 30h/week, and
        * can lead to burnout for commitments that exceed 40h/week.
    * However, temporary changes lasting 1-2 months that fall outside of the 30-40h/week range
      are generally acceptable.
    * If you need time to implement the change, for example to finish a round of recruitment,
      don't hesitate to discuss a specific date at which to make the change.
      Generally we agree to requests for changing weekly hour commitments,
      but not necessarily to implementing them immediately.
    * To implement the requested changes:
        * Update the cell member's availability in the [epic planning spreadsheet][^1]
          to take the impact on your cell's total availability into account for epic planning.
        * Update the cell member's hours by via **Tempo** > **Administration** > [**Workload schemes**](https://tasks.opencraft.com/secure/TempoSchemeWorkload!default.jspa)[^1]:
            * Locate the team member's current workload scheme (e.g. 35h), and click **Members**.
            * Click **Move** to move the team member to the new workload scheme (e.g. 30h).
* Check the calendar for vacations coming up in the next couple months:
    * If someone has scheduled time off for a total of 1 week or longer, [comment on the cell(s)](https://gitlab.com/opencraft/documentation/public/merge_requests/99#note_198626533)
      in the [epic planning spreadsheet][^1] that represent their availability for the month(s)
      during which they will be away:
        * Mention the number of weeks they will be away.
        * Mention the remaining availability for the affected month(s).
    * *Don't change their availability for the affected month(s) in the spreadsheet:*
      [Holidays are already taken into account in the **holidays buffer**](https://gitlab.com/opencraft/documentation/public/-/merge_requests/99#note_193174809), which reduces the cell's monthly availability according to the cap
      on the number of cell members that can be off at the same time.
      (That cap is usually [20%](vacation.md#requesting-time-off).)
* Keep [role statistics][^1] for your cell up-to-date.
* Keep the Discovery Duty rotations up-to-date in the [Weekly Rotation Schedule][^1], taking into account latest role statistics.
    * This can happen at any time during the sprint, as long as the spreadsheet is checked at least once per sprint, and updated as necessary.
    * Discovery budget: 2.5% of the cell's total work hours (10h/week for 10 full times). The discovery budget and the discovery duty allocation are two different things. The weekly discovery duty allocation (5h/cell/week) is to be used for new tasks that pop up over the course of a sprint - it is funded by the discovery budget. Any leftover discovery budget can be used for discovery tasks that are planned in advance.
    * Members with a medium to high role load should be removed from DD rotations and re-added when their role load drops to a level that lets them take on new epics.

## Capacity Requirements

* For each client and epic, maintain a count of the amount of time required to complete the accepted
  project scope over the next months in the [epic planning spreadsheet][^1]
  for your cell. This is used to [inform recruitment needs](recruitment.md#launch-of-a-recruitment-round)
  (cf. [below](#criteria-for-recruitment-decisions)).
  Note that the amount of time required to complete the accepted scope of a project
  isn't always equal to the estimated budget for the project:
    * Work may progress more quickly than expected in some cases,
      allowing us to reduce capacity allocations for coming months.
    * In other cases there might be a need for additional work that was not accounted for in the original estimates,
      requiring us to increase capacity allocations for coming months.
* Coordinate with the team's recruitment manager to slow down/speed up recruitment as necessary.

### Criteria for recruitment decisions

When making decisions about recruitment for a cell, we mainly focus on matching available capacity with upcoming billable work.

!!! info "Upcoming billable work"

    Upcoming billable work includes work that has already been confirmed (= the client signed a quote for it), **and** work that we *might* get if a prospect decides to hire us for it.

    When it comes to prospects, the business development specialist will usually have the best feel for the amount of work that we are likely to get from them in the near future.
    For additional work from existing clients, client owners should be able to provide information both on upcoming work that has already been confirmed and on potential projects that are still being discussed.

Specifically, if it looks like the cell

* might not have enough [capacity for upcoming billable work](#determining-capacity-for-upcoming-billable-work), **and**
* has [capacity to mentor](#determining-mentoring-capacity) additional newcomers.

... we should recruit, *even if the cell's sustainability ratio is currently off-target*.

The reasoning behind this is as follows:

* Once a project has been confirmed, it's usually pretty late to ramp up capacity for it through hiring, considering that it can take some time to find suitable candidates and get them through the full recruitment process.
* On the other hand, we never get projects from all prospects that knock on our door, so hiring for all leads would likely be too much.

The only scenario where we would make an exception and use a different approach is if a cell's sustainability is in the red while a large proportion of its capacity is filled with internal, non-billable work.
Here, we would try and get at least some of the missing capacity from de-prioritizing internal work.
If more capacity is needed beyond what we can reasonably free up this way, we'd still need to start a (smaller) round of recruitment.

#### Determining capacity for upcoming billable work

To determine a cell's capacity for upcoming billable work, we have to answer two questions:

1. *What's the total volume of hours that the cell is expecting to need for **new projects** in the coming weeks?* As explained in the info box above:
    * The business development specialist should be able to provide an estimate for the amount of capacity that will be needed for projects from new clients and promising leads.
      Epic planning managers and recruitment managers can also check bi-weekly updates that the business development specialist posts on the leads epic ([STAR-1634](https://tasks.opencraft.com/browse/STAR-1634)[^1])
      as well as the list of [ongoing discoveries](https://tasks.opencraft.com/issues/?filter=14301)[^1] to get a sense of capacity requirements for new projects.
    * Client owners should be able to provide information both on new projects that have already been confirmed and on potential projects that are still being discussed.
1. *What's the total volume of hours that the cell will need for **existing projects** (billable and non-billable) in the coming weeks?*
   Epic updates should provide information about capacity requirements for existing projects in the short and medium term.
   Epic planning managers use that information to [populate](#capacity-requirements) the [epic planning spreadsheet][^1] with monthly capacity allocations.
   For each month, the spreadsheet will sum up individual allocations to provide the total number of hours that the cell can expect to spend on existing projects that month.
   It will also calculate the amount of remaining capacity for each month, i.e., the difference between required and available capacity.

If the total volume of hours needed for upcoming billable work is significantly larger than the remaining capacity, that would be a strong indicator that a cell needs to recruit.

#### Determining mentoring capacity

To determine mentoring capacity, check your cell's [role statistics][^1].

Note that we want to remain flexible when it comes to recruitment decisions, so we don't impose strict limits on the number of newcomers that a cell might decide to take in at a given point in time.

However, there are some criteria to consider and use as a rule of thumb when determining recruitment pace for a cell:

* Properly mentoring a newcomer takes time and effort, so it makes sense to allow individual team members to focus on one mentee at a time.
* Experience has shown that cell size influences recruitment pace.
    * Smaller cells can get overwhelmed more easily when we try to grow them quickly; they usually do better with a smaller number of concurrent newcomers.
    * Larger cells may be able to support a faster recruitment pace with multiple concurrent newcomers.
* We accept newcomers into the core team upon successful completion of their trial project.
  And even though we try to make the right decision about whether someone is a good fit for the team early on in the recruitment process,
  there are cases where a newcomer ends up leaving at the end of their onboarding period.
  When this happens, the workload for the rest of the team increases (at least temporarily) because it has to compensate for the reduction in total capacity and take over any tickets that the newcomer doesn't complete by their last day.
  *This means that having a high ratio of newcomers to seasoned core team members carries some risk for a cell.*
  To keep that risk within reasonable bounds, it can be helpful to consider mentorship capacity capped at a certain ratio of newcomers, based on cell size, e.g.:
    * 50% for small cells (5-6 members).
    * 40% for medium-size cells (10-11 members).
    * 30% for large cells (15-16 members).

Lastly, it's worth noting that [mentoring starts](../roles/list.md#mentor) when a candidate starts their trial project.
So the number of *concurrent newcomers* includes both candidates doing their trial and newly-accepted core team members in their onboarding period.

#### Notes on sustainability

* The effect of recruitment on sustainability is much more complex than that of most other types of internal, non-billable work:
  It can have a large negative effect on sustainability in the short term, but lead to an even larger positive effect on sustainability in the medium/long term, by helping us grow capacity to match demand and new projects.
    * Therefore, decisions about recruitment should always be based on short and medium-term capacity needs.
      To make sure that sustainability doesn't "get in the way" of that, we exclude recruitment hours when calculating sustainability at cell and company level.
      We also don't set quarterly/yearly budgets for recruitment at the cell or company level ahead of time.
      Instead, we dynamically allocate recruitment budgets to match the number of trial projects and newcomers we take in.
    * That way, we avoid being too conservative with recruitment and getting stuck in a chicken-and-egg problem, i.e.,
      needing more billable projects to offset non-billable work and improving sustainability, but not being able to accept them due to lack of capacity.
* Sustainability managers should still keep an eye on recruitment when reviewing and reporting on sustainability.
  This allows us to remain conscious of its cost, while being able to ignore it when deciding whether to start recruiting for a cell or not.
* There are economies of scale that help with sustainability at bigger team sizes, but it will always take some investment and leap of faith to grow capacity to match demand.

[epic planning spreadsheet]: https://docs.google.com/spreadsheets/d/1j-fOflCXRyC8qL8yp7zPcbklQqdeMTQnvrVS-7E0aWE/edit
[role statistics]: https://docs.google.com/spreadsheets/d/1i1BOjiikB_zFvd8PgB4pteNVYI5tmE7DNbrE8JQI7UQ/edit
[Weekly Rotation Schedule]: https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit

[^1]: Private for OpenCraft Employees
