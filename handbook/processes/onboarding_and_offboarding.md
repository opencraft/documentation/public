# Onboarding & Offboarding

## Trial Project Onboarding

Trial project candidates are [onboarded to a limited set of tools](./recruitment.md#assigning-trial-project) using an internal checklist.

## Core Team Onboarding

Once a candidate successfully completes their trial project, the Recruitment Manager performs the tasks assigned to them in the onboarding checklist and notifies the other roles involved in the process.

Once the newcomer has access to the tools and joined the team, the onboarding period starts.

Since newcomers may start at any time during the sprint, this process overlays the [sprint process](sprints.md). Newcomers are expected to participate in sprint planning process, commit to tasks for the upcoming sprint, and practice time management using the sprint planning tools and by updating the Remaining Time estimate fields on their tasks.

As with all things at OpenCraft, this process is continually being reviewed and improved, so please provide any suggestions or feedback on your onboarding task.

| Newcomer Weeks | |
|----------------|----------------------------------------------------------|
| **Week -1** | Prior to your arrival, we will arrange for a core team member to be your mentor and to review your onboarding task.<br/>We'll also arrange your accounts and access to email, JIRA and the other communication tools.|
| **Week 1** | Work on your onboarding task, which involves reading documentation, completing the onboarding course, and setting up an Open edX devstack, if you didn't get to do it as a part of your trial project.<br/>You'll also have one or two tasks (max 5 story points) assigned to work on in the first sprint, after finishing your onboarding.<br/>Attend the 121 meeting scheduled by the reviewer of your onboarding task to say hello and discuss your progress.<br/>If your devstack gives you trouble, be sure to ask your reviewer or on the Mattermost #devstack channel for help, and/or arrange a synchronous meeting to work through any issues.|
| **Week 2** | You've likely finished the first week of your onboarding course, and are ready to work on the tasks you are assigned to.<br/>Reach out to your mentor or the sprint firefighter to help find tasks and a reviewer from the core team to help you.<br/>To avoid spillover, we recommend against pulling new tasks into the current sprint in the first instance -- the review cycles can often take more time than expected. So instead, especially if a new sprint is starting soon, commit to a task in the next sprint, and work ahead.|
| **Week 3** | You've likely finished your 1st sprint or will be finishing it this week. This is a good time to schedule a 121 with your mentor to ask any doubts or clarifications in the processes that you're unsure about and gather any feedback they might have. |
| **Week 4** | By the end of this week, you should have [completed some tasks](../task_workflows.md#done), with [story points](../task_workflows.md#general-tasks) totalling around 8-12 points. If you haven't, bring this up as soon as possible with your mentor.<br/>If you've had spillover, consider what went wrong during these tasks and talk about it with your mentor.<br/>Take care not to overcommit during the next sprints to get this under control. Time management is one of the hardest parts, so after each sprint ends, take care to ensure that the [Spillover Spreadsheet](https://docs.google.com/spreadsheets/d/1aJD-e2NkDsyBq_yBHykGiMYE29FvSOuYSCZjOJ5sjkM/edit#gid=0) is accurate, and your spillover is improving as you progress through the onboarding period.|
| **Week 5** | By this time, depending on when you started, you've completed 2-3 sprints, so it's time to ensure that you're completing a breadth of tasks to showcase your skills.<br/>Have you taken on increasingly difficult tasks?<br/>Have you submitted a PR to the Open edX platform?<br/>Have you launched appservers or contributed to Ocim?<br/>Have you completed any devops tasks?<br/>Have you been the primary reviewer on some tasks?<br/>If not, try to find tasks for the next sprints which would fill these gaps, and discuss any cell-specific expectations with your mentor.|
| **Week 7** | This week will be your [developer review](./developer_review.md).<br/>All the core team members in your cell (plus one developer from each other cell) will review your tasks, PRs, and communications, and check if there are any red flags.<br/>The key points of the review and feedback would be communicated to you by your mentor.|
| **Week 8** | This marks the end of your onboarding period, congratulations! You will be added to our `dev` mailing list. You can continue logging "onboarding" time to your onboarding ticket for a while.|

## End of onboarding and developer review schedules

When a newcomer first joins OpenCraft, we set a date for the end of the onboarding and a cutoff date for [developer
reviews](./developer_review.md). The end of a trial is calculated taking into consideration the date the newcomer started working at
OpenCraft and is based on the current practice of a four sprint onboarding period.

A newcomer should only join one day before a sprint starts (currently Monday). This means the end of the onboarding date
is exactly 57 days after the starting date.

```python
end_of_onboarding = start_date + 57
```

To make sure the developer reviews are completed in time for a fair discussion, these must be completed at least
seven days before the end of the onboarding or 7 weeks after the start:

```python
review_deadline = end_of_trial - 7 = start_date + 50
```

The developer reviews tasks must be completed in the first week of the last sprint of the onboarding period, with the second week for discussions.

## Offboarding

When a team member leaves the team, the offboarding is scheduled based on their last working day.
The recruitment manager notes the last day on the team member's onboarding/offboarding checklist
and completes the steps from the checklist.

If a new member doesn't meet the expectations during the developer review and it is their last 
sprint it is definitely better to assign them tasks that they might be able to complete without
wasting budget hours, even if it's from the onboarding budget. But, if this type of task isn't 
available, we can then focus on giving them tasks that aren't urgent, i.e. that can be rescheduled
for the following sprint when they leave, to limit the impact on the rest of the team.

## Other references

See also:

* [Roles: Newcomer](../roles/list.md#newcomer) for the specific expectations for you during this period.
* [Roles: Mentor](../roles/list.md#mentor) for details about your mentor's responsibilities.
* [What is expected from everyone](../roles/list.md#what-is-expected-from-everyone) on the team.
