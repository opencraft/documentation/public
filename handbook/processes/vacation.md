# Vacation

The following rules apply to all cells by default.  Project cells have the option of specifying [their own](#project-cells), but if they don't do so explicitly, these ones will apply.

To take vacation, you must complete the vacation checklist. The vacation checklist will assist you in coordinating your vacation with your team members and making sure that all of your responsibilities are handed off. You can find the vacation checklist on the [templates page of Listaflow](https://app.listaflow.com/templates).

## Requesting time off

* Make sure to book your vacation:
    * For 1 day off, 3 days in advance
    * For 2 days to 1 week off, by Thursday evening of the week preceding the sprint in which time off starts
    * For more than a week off, two weeks in advance.
* Check the [team calendar](https://www.google.com/calendar/embed?src=auscaqbrvc0uatk6e7126614kk%40group.calendar.google.com)
  first to see if anyone else has booked those dates off. To ensure maximal availability and
  responsiveness within the cell, if the number of persons corresponding to 20% of the cell size
  (rounded up) are already away on some days, you can't book the same days. There is however
  a tolerated 1 day/week per person that can overlap. If in doubt, ask Xavier.
* If you decide to travel and still work, it's useful to let everyone know, but it's not necessary
  to go through the vacations process. As long as you have a good internet connection and maintain
  work hours and communication / reactivity levels, you can work from anywhere.

### Special vacation

Besides the "normal" vacation, we also offer the following two variants:

* *Reduced time off*: A temporary reduction of the number of hours you work each week.
* *Scoped time off*: A temporary change of the scope of work you will work on, to reduce availability to/from
  specific projects, clients or roles. This is usually used to allow to focus on one or several specific
  projects for a time, or to take time off projects without being fully off.

The same process and limit as for normal vacations applies. Simply also mention the amount of time that
you will have available, or your exact scope of work, for that period of time while you announce your vacations.

### Sick days

* When you are sick and unable to work, let the rest of the team know asap using the
  [forum thread](https://forum.opencraft.com/t/unscheduled-uncontrolled-short-term-availability-updates-eg-sickness/630)[^1], and let people
  know where you would need help/cover as much as possible. In particular, mention which tasks
  are at risk of spillover or have a looming deadline.
* Then, rest! Don't half work while sick - it will take you longer to recover, and your work will
  likely not be very good.
* Note that it is also possible, and encouraged, to take
  [sick days for mental health](https://doist.com/blog/mental-health-and-remote-work/), when you need it.
  Just like other sick days, use them to rest and disconnect - don't check emails or tickets!

## Project cells

Because project cells are tailored to specific project needs, they are not required to subscribe to the default cell rules that govern member availability.  For instance, a four-person cell in an initial development phase may establish that short absences that would otherwise break the 20% rule are allowed.  (On the other end of the spectrum, a cell with a strict client SLA may want to expand on its [firefighting rules](./firefighting/fire_management.md) so that more timezones are adequately covered.)

If the rules are different from the default, they must be explicitly mentioned in the list of
[Cell-Specific Rules](../cells/cell_specific_rules.md). If no custom vacation rules are defined there,
the default cell rules apply.

There are constraints, though:

* The project cell's vacation rules, if different from the standard, must be reviewed and approved unanimously by all of its members and the CEO during the cell's inception, and at any time thereafter when a change is proposed, as a change request to this handbook.
* The rules must always provision for self-approval of vacation requests without ambiguity, like the default vacation rules, and not require to seek explicit approval from another cell member. The rules can still specify _dependencies_ on other cell members, like ensuring to find backups, but we don't want vacation rules to depend on the subjective approval of a member - the rules themselves should explicitly encapsulate all the constraints to be considered when taking vacation, and empower individual cell members to evaluate them by themselves.
* The default OpenCraft client-facing [SLA](../resources/sla.md) always applies.  In other words, the 24-hour reply rule continues to apply if not superceded by a stricter SLA, so a cell with few members needs to make sure that the client is being adequately supported when some of its members are on vacation.

## End of Year Holidays

Near the end of the year, especially around holidays like Christmas and New Years, most of the team takes at least some time off. During this time, some requirements for vacations are relaxed, but a special process is used to prevent a scenario where systems are entirely unmanned. The process begins in the first week of November, in which the [administrative specialist](../roles/list.md#administrative-specialist) creates a thread on the [forum](https://forum.opencraft.com/), in the "Announcements - Private" category to coordinate vacation bookings. [See an example thread here](https://forum.opencraft.com/t/end-of-year-holidays-vacation-2024-2025/1736)[^1].

Booking days off will use the vacation checklist, as always. However, there are two critical changes to the requirements:

* The [coverage rules](./firefighting/fire_management.md#firefighter-rotations) are changed to only require one firefighter to be active on any given day, including weekends.
* Posts and discussions for booking vacations are to be done in the dedicated thread, rather than the main vacation announcement thread.

To coordinate the days that team members will take, use a tool like [When is Good](https://whenisgood.net/) for each cell, and have them mark when they **WILL** be working so that it's clear how many people are available on which days. Create the polls for different cells using `contact@opencraft.com` as the organizer so that you can also mark your own days.

The deadline for submission should be made at least a week before the end of November, preferably a bit more (say, November 20th.) This gives time to resolve any conflicts.

### Team-wide autoresponder

While a firefighter is required to be available each day to ensure systems are operating in a safe and stable manner, our general mailing addresses are given more leeway. However, since addresses like `contact@opencraft.com` and `billing@opencraft.com` are addressed to no specific person, a specific autoresponder must be set up for the mailing list itself. The primary monitoring role of the email (such as the [business development specialist] for the contact email, or the [administrative specialist] for the billing email) is responsible for setting up an autoresponder for these cases.

This can be done one of two ways-- either by setting up special personal email filters to [auto-respond to the list in Gmail](https://knowledge.workspace.google.com/kb/how-to-create-an-auto-reply-for-emails-000007410), or by turning on the mailing list's [global autoresponder](https://lists.opencraft.com/mailman3/lists/contact.lists.opencraft.com/settings/automatic_responses)[^1][^2] right before the business development specialist goes on leave, and off once they return. This auto-response should inform contacts that their email's response may be delayed a few days during the holiday period.

[administrative specialist]: ../roles/list.md#administrative-specialist
[business development specialist]: ../roles/list.md#business-development-specialist
[^1]: Private for OpenCraft Employees
[^2]: This example is for the contact@ mailing list. The credentials for this can be found in the vault, accessible only to OpenCraft Employees.
