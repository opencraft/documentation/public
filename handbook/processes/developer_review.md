# Developer Review

Developer Reviews are done to assess the working form of a team member.
While the first one is scheduled automatically at the end of the onboarding
period of a new team member, it can be request anytime by any team member.

The developer reviews will be evaluated on the following criteria:

* Technical skills.<br/>
  Team members must demonstrate development and devops abilities on basic and complex tasks.
* Time management and spillovers.<br/>
  Newcomers must have at least half of their sprints clean during their onboarding
  period (2/4) and at least 75% of the sprints clean afterward.
* Communication.<br/>
  See [Roles: Communication](../roles/list.md#communication) for the expected
  response times, and the additional expectations for [Newcomers](../roles/list.md#newcomer).
* Adaptability.<br/>
  Team members should respond gracefully to changes in task requirements and scope,
  communicate concerns and issues, and allocate effort appropriately across the
  current or follow-up tasks.
* Potential for growth.<br/>
  Team members should demonstrate an enthusiasm for learning and improvement
  across all aspects of their work.

The developer review is not meant to produce a yes or no result like a [trial project review](./recruitment.md#trial-project-review),
but provide helpful feedback for the team member.

## Review Guidelines

The developer review should

* Follow the general structure as the one below, skipping sections only when there is nothing to note.
    ```markdown
    ## Positives
    
    Technical Skills
    * ...
    
    Time Management
    * ...
    
    Communication
    * ...
    
    Adaptability
    * ...
    
    ## Improvements
    
    Technical Skills
    * ...
    
    Time Management
    * ...
    
    Communication
    * ...
    
    Adaptability
    * ...
    
    **Rating:**
    ```
* Have one of the following as the *Rating*:
    - Outstanding
    - Good
    - Issues to solve
    - Below team standards
* Include links to relevant content like JIRA or GitHub comments. These are important to see what the reviewer is trying to communicate and also useful when sharing feedback with the developer.
* Adhere to the deadline mentioned in the ticket when submitting the review, as they are usually in the middle of the sprint, unlike other tickets which are usually worked on until the end of the sprint.

## Tips to ace your first developer review

### Delivering On-Time

Avoiding [spillover](../time_management.md#spillovers) and delivering on schedule is really
important in an environment where we make direct promises to clients about
deliverables. Our reputation as an organization is on the line when we cannot
deliver as we promised, so it matters tremendously to us to see a newcomer
making deadlines consistently. It's required that you communicate explicitly
when you feel there is going to be spillover, as soon as you can detect it, and
try to find someone else who can complete or help you complete them. It’s totally
ok to do this, and even welcomed by people who have time left in their sprint.
We are a team, and we work together to avoid spillover.

### Communication

As an international remote team, there is
little progress we can make if we don't constantly communicate (with respect
to not being interruptive if it isn't necessarily urgent). We promise you
that we didn't recruit any mind readers! We won't magically figure anything
out unless it's been talked about, through any of our multiple modes of communication.
You should be communicating with your reviewers daily or every 2 days minimum on what
your progress on their task is (by commenting on the JIRA tickets). Even if they have
no questions, just stating status is important and can give reviewers/mentors somewhere
to jump in and help. On the other hand, when blocked in a task, make sure to reach
the reviewer for help. If the reviewer isn't available, you can reach for the sprint
firefighters.

### Show your skills

It's important to take tasks of progressive difficulty, and make sure to take
reviews on too. It's much easier for the core team to review your onboarding
if you have picked varied tasks of different complexity and skillset. We’re looking for a
cross-section of tasks across all our required work areas: full stack dev, devops, and ops.

### "Nice"

This point is in quotes because everyone obviously likes being around
other nice people, so you'd assume this was obvious. But of course everyone believes,
"Yeah, I'm nice!", but it goes a long way to being deliberately nice with your
colleagues, and not just believing you are; they will simply enjoy working with you more.


