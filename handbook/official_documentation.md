# Official Documentation

* [edX documentation](http://docs.edx.org/):
  * [edX Insights](http://edx-insights.readthedocs.org/en/latest/)
  * [Open Learning XML specification (OLX)](http://edx-open-learning-xml.readthedocs.org)
  * [edX Developers Guide](http://edx.readthedocs.org/projects/edx-developer-guide/en/latest/index.html)
  * [Developing on the edX Developer Stack](https://github.com/edx/edx-platform/wiki/Developing-on-the-edX-Developer-Stack)
  * [Open edX Technical Architecture](https://edx.readthedocs.io/projects/edx-developer-guide/en/latest/architecture.html)
  * [XBlock API docs](http://edx.readthedocs.org/projects/xblock/en/latest/)
  * [Debugging EdX](https://openedx.atlassian.net/wiki/display/OpenOPS/Debugging+Edxapp)
* Standards, requirements, and expectations
    * [Code Review Guidelines & Gotchas](https://gitlab.com/opencraft/documentation/private/tree/master/pdfs/CodeReviewGuidelinesandGotchas-2017-25-07.pdf)[^1] Contains some outdated information, e.g. Teaching & Learning (TNL) was the edX team doing code reviews.
    * [edX Python Coding Standards](http://edx.readthedocs.io/projects/edx-developer-guide/en/latest/style_guides/python-guidelines.html) and [code quality standards](http://edx-developer-guide.readthedocs.org/en/latest/testing/code-quality.html)
    * [edX Browser Support](http://edx.readthedocs.org/projects/edx-developer-guide/en/latest/front_matter/browsers.html)
    * [edX Accessibility Guidelines](http://edx-developer-guide.readthedocs.io/en/latest/conventions/accessibility.html)
    * [Accessibility mandatory course](https://edge.edx.org/courses/course-v1:edX+AC101+2015/about)
    * [UX Pattern Library](https://github.com/edx/ux-pattern-library)
* Testing
    * [Testing documentation](https://docs.openedx.org/projects/edx-platform/en/latest/concepts/testing/testing.html)
    * [Test engineering FAQ](https://github.com/edx/edx-platform/wiki/Test-engineering-FAQ)
    * [Jenkins Guide](https://openedx.atlassian.net/wiki/display/TE/Jenkins+Guide) - Jenkins runs the automated tests as part of the pull request process
* How to:
    * [How to report bugs to edX](https://openedx.atlassian.net/wiki/spaces/SUST/pages/5144625/Archived+How+to+File+a+Quality+Bug+Report)
    * [Handling flaky tests](https://openedx.atlassian.net/wiki/display/TE/Flaky+Test+Process)
    * [Running performance tests](https://gitlab.com/opencraft/documentation/private/tree/master/pdfs/EdxOps-HowtoRunPerformanceTests-2017-25-07.pdf)[^1]
    * [Cross-Browser Testing](https://openedx.atlassian.net/wiki/display/TE/Cross+Browser+Testing)
    * [DevOps HOWTOs](https://openedx.atlassian.net/wiki/spaces/OpenOPS)
    * [SASS guide](https://edx.readthedocs.io/projects/edx-developer-guide/en/latest/style_guides/sass-guidelines.html)

[^1]: Private for OpenCraft Employees
