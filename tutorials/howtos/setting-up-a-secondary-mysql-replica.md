# Setting up a Secondary MySQL Replica

We use a MySQL setup with one master server and two replicas for Ocim production.
Having two replicas makes it possible to replace/rebuild one of them without
requiring any downtime of the master server.

## Bootstrapping MySQL Replication

Assume we already have an existing MySQL setup with the master (M) server and a MySQL replica (R1).
We want to add a second replica (R2) to the settup.

Here is a high-level overview of setting up the secondary (R2) MySQL replica:

1. Run the [`mysql.yml` playbook](https://github.com/open-craft/ansible-playbooks/blob/master/playbooks/mysql.yml) playbook  on the new R2 server for basic MySQL set up.
2. Stop `mysqld` on R2.
3. **Empty `/var/lib/mysql` folder on R2.** (this is important as you could end up with inconsistent tables if you `rsync` into prexisting data.)
4. Rsync the contents of `/var/lib/mysql` from R1 to R2.
    1. As the `mysql` user on R2, create an ssh key if there is not one already with `ssh-keygen`.
    2. Shell into R1 and add the contents of `~mysql/.ssh/id_rsa.pub` on R2 to `~mysql/.ssh/authorized_keys`. If `~mysql/.ssh/authorized_keys` does not exist, create it, `chown` it to `mysql:mysql`, set the `.ssh` directory's mode to `700` and the `authorized_keys` file to `600`.
    3. From R2, as the `mysql` user, run `rsync -a --info=progress2  mysql@hostname.of.replica:/var/lib/mysql/ /var/lib/mysql/`
5. Stop `mysqld` on R1 and run the rsync command again to make sure files are up to date and in consistent state.
6. Start `mysqld` on R1.
7. Remove `/var/lib/mysql/auto.cnf` from R2 (this file contains the GUID of the server and must be different than the one on R1; if the file is missing, MySQL will auto-generate it with a random GUID on startup, so simply deleting the file is enough to get a new random GUID).
8. Start `mysqld` on R2.
9. Run `top` and watch `mysqld`. It will take a few minutes to validate its configuration and state.
10. Once CPU is no longer thrashing, it will be possible to connect to the server. Become `root` and use `mysql` to run the MySQL shell.
11. Validate that replication is working with `SHOW SLAVE STATUS\G;`. The slave should most likely be waiting on master for new commands, or else processing them. It should not show an error.


MySQL installation adds a logrotate configuration as well. Because of the replication setup, the auto-generated `debian-sys-maint` password will not match with what we have set in the DB. You can fix this by updating the `debian-sys-maint`'s password on R2, to match what is in the `/etc/mysql/debian.cnf`.

To do this, connect again to the mysql shell as root, and run:

```sql
SET PASSWORD FOR 'debian-sys-maint'@'localhost' = PASSWORD('password_from_debian_cnf_goes_here');
FLUSH PRIVILEGES;
```

To ensure logrotate works fine, run the following:

```bash
MYADMIN="/usr/bin/mysqladmin --defaults-file=/etc/mysql/debian.cnf"
[ -z "`$MYADMIN ping 2>/dev/null`" ] || echo `$MYADMIN ping`
```

If the above prints "mysqld is alive", logrotation will work.
