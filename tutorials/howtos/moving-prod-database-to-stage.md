How to move the production database to the existing stage environment
======================================================================

# Preparations
Before doing the migrations use Django admin to check the following places that are going to have different values for the stage and prod environment. Save the values used on the stage - you will need them for the last step from this doc.

## Open Edx platform
1. Home › Django OAuth Toolkit › Applications
1. Home › Oauth2 › Clients
1. Home › Sites › Sites
1. Home › Site_Configuration › Site configurations
1. Home › Waffle › Flags

## Ecommerce
1. Home › Sites › Sites
1. Home › Core › Site configurations

## Course Discovery
1. Home › Sites › Sites

# Migration
## MySQL
1. Log into the MySQL console and check which databases need to be moved:
  ```bash
  mysql -h "${PROD_HOST}" -u edxapp -p"${PROD_PASSWORD}" edxapp
  mysql> SHOW DATABASES;
  +---------------------------------------+
  | Database                              |
  +---------------------------------------+
  | information_schema                    |
  | discovery                             |
  | ecommerce                             |
  | edxapp                                |
  | edxapp_csmh                           |
  | hidden_hosting_wordpress_stage        |
  | innodb                                |
  | mysql                                 |
  | notifier                              |
  | performance_schema                    |
  | programs                              |
  | sys                                   |
  | tmp                                   |
  | xqueue                                |
  +---------------------------------------+
  ```
In this case, we're going to move `discovery, ecommerce, edxapp, edxapp_csmh, notifier, programs, xqueue`.

1. Use the following snippet on the production to create the dump:
  ```bash
  mkdir backups
  for DB in discovery ecommerce edxapp edxapp_csmh notifier programs xqueue; do mysqldump -h "${PROD_HOST}" -u edxapp -p"${PROD_PASSWORD}" --events --single-transaction --set-gtid-purged=OFF "${DB}" > backups/"${DB}".sql; done
  ```
1. Use the following snippet on the stage to restore the dump.
  ```bash
  cd configuration-secure
  scp -rF ssh-config director-prod:~/backups/ ../
  cd ..
  for DB in discovery ecommerce edxapp edxapp_csmh notifier programs xqueue; do mysql -h "${STAGE_HOST}" -u edxapp -p"${STAGE_PASSWORD}" "${DB}" < backups/"${DB}".sql; done
  ```
  **Important: make sure that none of these commands return any errors. You can refer to the [Troubleshooting](#troubleshooting) section below to see an example error and how to debug it.**

### Troubleshooting
While writing this doc we had `ERROR 1215 (HY000) at line 13188: Cannot add foreign key constraint` while restoring the `edxapp.sql`. The dump file was too big to open with the server's editor, so we did this to create a debug file:
```bash
cd backups
sed '13188q;d' edxapp.sql  # check the actual line with the problem
sed -n '12188,14188p;14189q' edxapp.sql > tmp.sql  # create a smaller file (with 1000 extra lines before and after the line that caused an error) for debugging and fixing the problem
```
The investigation of the block of code containing line 13188 (we found its content above for convenience)  showed that there were two constraints. One of them was ```CONSTRAINT `subscription_ptr_id_refs_id_75c0b518` FOREIGN KEY (`subscription_ptr_id`) REFERENCES `notify_subscription` (`id`)```. To checked why it's failing we used:
```bash
mysql -h "${STAGE_HOST}" -u edxapp -p"${STAGE_PASSWORD}" edxapp
mysql > SHOW COLUMNS FROM notify_subscription;
```
and found that the primary key was `subscription_id` instead of `id`. To fix this we:
1. Moved the part related to `notify_subscription` table to the top of our `tmp.sql` file.
1. Checked `edxapp.sql` for `SET` instructions that would disable the checks and temporarily change other specific DB settings (like timezone or encoding) that could alter the data. We extracted them with `head -50 edxapp.sql` and `tail -50 edxapp.sql` (50 is just an example number that should be sufficient to view all necessary instructions).
1. Added the extracted `SET` instructions to our `tmp.sql`.
1. Applied the `tmp.sql` with `mysql -h "${STAGE_HOST}" -u edxapp -p"${STAGE_PASSWORD}" edxapp < tmp.sql`.
1. Applied the `edxapp.sql` dump again.


## MongoDB
1. Log into Atlas.
1. Go to `Database Access` of the production cluster and create a temporary `Atlas admin` user for the database (Altas has an option to delete the user automatically after 6 hours).
1. Wait for the change to be deployed.
1. Go to the stage cluster and use the `··· -> Migrate Data to this Cluster` button with the `Clear any existing data on your target cluster?` set to `True`.
1. Click "Cut over" after the migration completes.

# Cleanup
1. Restore the values from the [Preparations](#preparations) section.
1. Disable access for non-staff users. We can do this by [setting unusable passwords](https://docs.djangoproject.com/en/dev/ref/contrib/auth/#django.contrib.auth.models.User.set_unusable_password), which additionally [disables an option to reset password](https://docs.djangoproject.com/en/dev/topics/auth/default/#django.contrib.auth.views.PasswordResetView):
  ```python
  from django.contrib.auth import get_user_model
  User = get_user_model()
  excluded = []  # list of the excluded usernames (e.g. client's test accounts)
  users = User.objects.exclude(username__in=excluded).filter(is_staff=False)
  for u in users:
      u.set_unusable_password()
      u.save()
  ```
