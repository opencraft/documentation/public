# Serving static files from edx-platform via CloudFront

## CloudFront configuration

Go to CloudFront and create new distribution with the following parameters:

1. Origin domain name: `your_domain.net`. If you have LMS under `courses.your_domain.net`, and Studio under `studio.your_domain.net`, then you can set it to `your_domain.net`. 
2. Origin path: leave this empty.
3. Path pattern: `Default (*)` (this is the default value).
4. Compress objects automatically: `Yes`.
5. Viewer protocol policy: `Redirect HTTP to HTTPS`. 
6. Allowed HTTP methods: `GET, HEAD` (this is the default value).
7. Cache and origin request settings: `Cache policy and origin request policy`.
8. Cache policy -> `Create a new policy`:
	1. Name: `oc-edxapp-static-cache-policy`.
	2. Minimum TTL: `1`.
	3. Maximum TTL: `31536000`.
	4. Default TTL: `31536000`.
	5. Headers: `Include the following headers`, and choose `Origin`.
	6. Query strings: `All` (this allows us to update resources without invalidating cache).
	7. Check `Gzip` and `Brotli`.
9. Click on the "refresh" icon and choose the newly created policy (`oc-edxapp-static-cache-policy`).
10. Origin request policy and Response headers policy: leave empty.
11. (Optional) If you want to use a custom domain for the CDN, you should:
	1. Add it as the Alternate domain name (CNAME).
	2. Choose a proper Custom SSL certificate.
12. Description: `oc-edxapp-prod static` (this is useful, because at the moment AWS Console does not display the distribution's name on the list view).

## edx-platform configuration

Add the following variables to your `configuration-secure` repository:
```yaml
# CloudFront CDN
EDXAPP_STATIC_URL_BASE: "https://{{ CLOUDFRONT_DOMAIN_NAME }}/static/"
EDXAPP_CORS_ORIGIN_WHITELIST:
  - "{{ EDXAPP_LMS_SITE_NAME }}"
  - "{{ EDXAPP_CMS_SITE_NAME }}"
 ```
