"""
Type annotations used primarily for validating YAML.

NOTE: Any change to the structure (or the URL location) of the organization definition file
must be accompanied by relevant updates to SprintCraft, in its settings and dashboard utils module.

Otherwise, certain automations will break, like the sprint checklist.
"""

from typing import TypedDict, List, Union, Literal, Sequence

KNOWN_ROLES = Literal[   # pylint: disable=invalid-name
    'Firefighting Manager',
    'Recruitment Manager',
    'Sprint Manager',
    'Sprint Planning Manager',
    'Epic Planning and Sustainability Manager',
    'Business Development Specialist',
    'Marketing Specialist',
    'Administrative Specialist',
    'Community Liaison',
    'OSPR Liaison',
    'Developer Advocate',
    'Cell Supporter',
    'Sales Mentor',
    'UX Designer',
    'Product Manager',
    'Chief Executive Officer (CEO)',
    'Chief Technology Officer (CTO)',
    'Lawyer',
]

# pylint: disable=invalid-name
KNOWN_POSITIONS = Literal[None, 'delegated', 'primary', 'backup', 'backup2']
# pylint: enable=invalid-name

# Turns out that TypedDicts do not like it when you override on inheritance. So when inheriting here, only do so
# if you will not be overwriting existing fields.


class Role(TypedDict):
    """
    Role definition for specialist functions.
    """
    name: KNOWN_ROLES
    scope: Literal['cell', 'company']


class BackedUpRole(Role):
    """
    Role that has backup team members.
    """
    position: Literal['primary', 'backup', 'backup2']


class DelegatedRole(TypedDict):
    """
    Role that has been delegated by a cell.
    """
    name: KNOWN_ROLES
    scope: Literal['delegated']
    delegating_cell: str


class Member(TypedDict):
    """
    All cell members will be expected to have these fields.
    """
    name: str
    email: str
    membership: Literal['core', 'newcomer', 'contractor', 'junior']
    roles: Sequence[Union[Role, BackedUpRole, DelegatedRole]]


class ToolKit(TypedDict):
    """
    Links to standard tools each cell should have. These can be empty strings if the tools do not exist yet.
    """
    chat: str
    sprint_board: str
    backlog: str
    epics_board: str


class RawCell(TypedDict):
    """
    Defines information about a cell.
    """
    name: str
    notes: str
    type: Literal['Generalist', 'Project', 'Support']
    tags: List[str]
    members: List[Member]
    tools: ToolKit
    rules: List[str]


class MetaCell(TypedDict):
    """
    Structure for the 'Meta-cell', or where non-team members go.
    """
    members: List[Member]
    tags: List[str]


class RawOpenCraft(TypedDict):
    """
    Defines information about the entire organization.
    """
    cells: List[RawCell]
    meta_cell: MetaCell
