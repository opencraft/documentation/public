"""
Build hooks used by mkdocs for automating some portions of our handbook.
"""
from collections import defaultdict
from itertools import chain
from pathlib import Path
from typing import Callable, Mapping, List, Sequence

import os
import yaml

from jinja2 import Environment, FileSystemLoader
from typeguard import typechecked

from hooks.constants import (
    TEMPLATES_PATH, DATA_PATH, TEAM_FILE_PATH, CELL_TEMPLATE, CELL_OUTPUT_PATH,
)
from hooks.schema import RawOpenCraft, KNOWN_POSITIONS
from hooks.types import MembersRolePosition, RoleAssignment, TeamRoles, RefinedCell, CellsByType, CellRolesByName, \
    RefinedOpenCraft, CellsByName, TeamMappings


@typechecked
def load_raw_team(path: Path) -> RawOpenCraft:
    """
    Loads information about the team, validating the YAML schema as it returns. If the dictionary that results from
    loading the YAML does not match expectations, this will throw and give a hint as to where in the schema the error
    was.
    """
    with open(path, 'r', encoding='utf-8') as source_file:
        return yaml.load(source_file)


@typechecked
def reformat_roles(roles_dict: TeamRoles) -> List[RoleAssignment]:
    """
    Take a set of roles compiled by discovery and order it for easy templating.
    """
    roles: List[RoleAssignment] = []
    positions: Sequence[KNOWN_POSITIONS] = [None, 'delegated', 'primary', 'backup', 'backup2']
    for role_name, values_dict in roles_dict.items():
        revised_roles: List[MembersRolePosition] = []
        for position in positions:
            if position not in values_dict:
                continue
            revised_roles.append({'members': values_dict[position], 'position': position})
        roles.append({'name': role_name, 'positions': revised_roles})
    return roles


@typechecked
def consolidate_roles(team: RawOpenCraft) -> TeamMappings:
    """
    Run through all roles in the organization and rearrange them
    to make useful mappings.
    """
    cells_by_name: CellsByName = {}
    team_roles: TeamRoles = defaultdict(lambda: defaultdict(list))
    cells_by_type: CellsByType = {'Support': [], 'Project': [], 'Generalist': []}
    cell_roles_by_name: CellRolesByName = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    # Find all roles from all cells and recompile them to get canonical listings.
    for raw_cell in chain(team['cells']):
        cell = RefinedCell(
            # MyPy won't allow double-star construction here.
            name=raw_cell['name'],
            type=raw_cell['type'],
            tags=raw_cell['tags'],
            notes=raw_cell['notes'],
            members=raw_cell['members'],
            rules=raw_cell['rules'],
            tools=raw_cell['tools'],
            roles=[],
        )
        cells_by_name[cell['name']] = cell
        cells_by_type[cell['type']].append(cell)
        cell_roles = cell_roles_by_name[cell['name']]
        for member in cell['members']:
            revised_roles = []
            for role in member['roles']:
                position = role.get('position', None)  # type: ignore
                if role['scope'] == 'company':
                    # Company-wide roles in support cells are the norm. It would be odd
                    # to have them in the team roles listing when displaying the cell
                    # members.
                    if cell['type'] == 'Support':
                        revised_roles.append(role)
                        continue
                    position_dict = team_roles
                elif role['scope'] == 'cell':
                    position_dict = cell_roles
                elif role['scope'] == 'delegated':
                    # Need to find which cell the role was delegated from and add the
                    # listing there instead.
                    position_dict = cell_roles_by_name[role['delegating_cell']]
                    position = 'delegated'
                else:
                    raise RuntimeError(f'Unimplemented scope, {role["scope"]}')
                position_dict[role['name']][position].append(member['name'])
            member['roles'] = revised_roles
    # Make sure we haven't done something silly like having delegated a role for a cell which does not exist.
    assert sorted(cell_roles_by_name.keys()) == sorted(cells_by_name.keys())
    return {
        'cells_by_name': cells_by_name,
        'cell_roles_by_name': cell_roles_by_name,
        'team_roles': team_roles,
    }


def load_team(path: Path) -> RefinedOpenCraft:
    """
    Loads the team from a YAML file and then applies checks and transformations.
    """
    team = load_raw_team(path)
    # Now, take the resulting entries and order them for easy template use.
    mappings = consolidate_roles(team)
    reformatted_cell_roles = {}
    for name, roles in mappings['cell_roles_by_name'].items():
        reformatted_cell_roles[name] = reformat_roles(roles)
    for key, value in reformatted_cell_roles.items():
        mappings['cells_by_name'][key]['roles'] = value
    result = RefinedOpenCraft(
        # Doing this to make sure types carry over correctly.
        cells=list(mappings['cells_by_name'].values()),
        meta_cell=team['meta_cell'],
        roles=reformat_roles(mappings['team_roles']),
    )
    return result


def md_slug(input_string: str) -> str:
    """
    Take a string and return a 'slugified' version used by our Markdown processor.
    """
    string = input_string.lower().replace(' ', '-')
    return ''.join(char for char in string if char.isalnum() or char == '-')


def render_template(*, context: Mapping, template: str) -> str:
    """
    Given a dictionary, and a path to a template file, renders out a template to string.
    """
    env = Environment(
        trim_blocks=True,
        lstrip_blocks=True,
        keep_trailing_newline=True,
        loader=FileSystemLoader([str(TEMPLATES_PATH)]),
    )
    env.filters['md_slug'] = md_slug
    compiled_template = env.get_template(template)
    return compiled_template.render(**context)


def update_cells_page(team_file: Path, cell_template: str, output_path: Path) -> None:
    """
    Compiles team data from a YAML file and then renders it out to markdown template.
    """
    team = load_team(team_file)

    output = render_template(context=team, template=cell_template)
    try:
        with open(output_path, 'r', encoding='utf-8') as original_file:
            existing = original_file.read()
    except IOError:
        existing = ""
    if output == existing:
        # Exit early, or else we'll trigger a watch loop, since only write time is checked by the watcher.
        return
    with open(str(output_path), 'w', encoding='utf-8') as output_file:
        output_file.write(output)


def update_site_url_in_config(config: dict):
    """
    Updates the site_url in the config dict based of the environment variables.

    NOTE: Not adding a correct site_url will lead to invalid absolute path for
    the assets in 404 page.
    """
    if os.getenv('READTHEDOCS'):
        language_code = os.getenv('READTHEDOCS_LANGUAGE')
        version = os.getenv('READTHEDOCS_VERSION')
        domain = os.getenv('DOCUMENTATION_SITE_URL')

        if domain is None:
            raise Exception('Please set the DOCUMENTATION_SITE_URL variable in the readthedocs environment.')

        config['site_url'] = f'{domain}/{language_code}/{version}/'

    return config


def update_templated(*_args, **_kwargs):
    """
    Prebuild hook to be called before each build. Referenced in mkdocs.yml.
    """
    update_cells_page(TEAM_FILE_PATH, CELL_TEMPLATE, CELL_OUTPUT_PATH)


def include_hook_files(server, config: dict, builder: Callable):  # pylint: disable=unused-argument
    """
    Hook run when mkdocs serve initial starts up. Using this, we can add files/directories to the watcher.
    We need to do this in order to make sure that updating this module, or the data it uses, results in those changes
    being updated.

    Note: Watching python modules used in hooks doesn't work. Once imported, they are not reloaded. You'll have
    to restart the serve process for changes in this file to be recognized.
    """
    server.watch(str(TEMPLATES_PATH), builder)
    server.watch(str(DATA_PATH), builder)
